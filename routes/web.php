<?php

//Client
use App\Http\Controllers\Clients\ClientAuthController;
use App\Http\Controllers\Clients\HomeController;

//Admin
use App\Http\Controllers\Admins\AdminAuthController;
use App\Http\Controllers\Admins\AdProfileController;
use App\Http\Controllers\Admins\AmenitiesController;
use App\Http\Controllers\Admins\DashBoardController;
use App\Http\Controllers\Admins\FloorPlanController;
use App\Http\Controllers\Admins\HouseBusinessController;
use App\Http\Controllers\Admins\WebMenuController;

use App\Http\Controllers\Admins\ProjectCategoryController;
use App\Http\Controllers\Admins\ProjectController;

use App\Http\Controllers\Admins\HouseCategoryController;
use App\Http\Controllers\Admins\HouseController;
use App\Http\Controllers\Admins\HouseGalleryController;
use App\Http\Controllers\Admins\HouseReviewController;
use App\Http\Controllers\Admins\MediaSupportController;
use App\Http\Controllers\Admins\ProjectGalleryController;
use App\Http\Controllers\Admins\WebFeedbackController;
use App\Http\Controllers\Admins\WebHireController;
use App\Http\Controllers\Admins\WebInfoController;
use App\Http\Controllers\Admins\WebPolicyController;
use App\Http\Controllers\Admins\WebReviewController;
use App\Http\Controllers\Clients\ClientHouseController;
use App\Http\Controllers\Clients\ClientProjectController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//-------- Clear Cache in Laravel (Browser)
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

//--------Client

Route::get('/', [HomeController::class, 'index'])->name('clients.default');
Route::get('/trang-chu', [HomeController::class, 'index'])->name('clients.index');
Route::get('/gioi-thieu', [HomeController::class, 'about'])->name('clients.about');
Route::match(['get', 'post'],'/lien-he', [HomeController::class, 'contact'])->name('clients.contact');
Route::get('/chinh-sach/{policy?}', [HomeController::class, 'policy'])->name('clients.policy');
Route::get('/tu-van', [HomeController::class, 'advice'])->name('clients.advice');

Route::get('/dang-nhap', [ClientAuthController::class, 'login'])->name('clients.login');
Route::get('/loi', [ClientAuthController::class, 'error'])->name('clients.error');

// CLIENT PROJECT
Route::get('/tuyen-dung', [HomeController::class, 'hire'])->name('clients.hire');
Route::get('/tuyen-dung/{hire}', [HomeController::class, 'hireDetail'])->name('clients.hire.detail');
//END CLIENT PROJECT

// CLIENT PROJECT
Route::get('/du-an', [ClientProjectController::class, 'list'])->name('clients.project.default');
Route::get('/du-an/{category}', [ClientProjectController::class, 'list'])->name('clients.project.list');
Route::get('/du-an/{category}/{project}', [ClientProjectController::class, 'detail'])->name('clients.project.detail');
Route::get('/dia-chi/du-an/{provincial}', [ClientProjectController::class, 'changeProvincial'])->name('clients.project.changeProvincial');
//END CLIENT PROJECT

// CLIENT HOUSE
Route::get('/bat-dong-san', [ClientHouseController::class, 'list'])->name('clients.house.default');
Route::get('/bat-dong-san/{business}', [ClientHouseController::class, 'list'])->name('clients.house.business');
Route::get('/bat-dong-san/{business}/{category}', [ClientHouseController::class, 'list'])->name('clients.house.category');
Route::get('/bat-dong-san/{business}/{category}/{house}', [ClientHouseController::class, 'detail'])->name('clients.house.detail');
Route::get('/dia-chi/bat-dong-san/{provincial}', [ClientHouseController::class, 'changeProvincial'])->name('clients.house.changeProvincial');
Route::post('/danh-gia/bat-dong-san/', [ClientHouseController::class, 'review'])->name('clients.house.review');
//END CLIENT HOUSE

//-----------------------------------------------------------------------------------------------------
//Authentication-------------------------------
Route::match(['get', 'post'], '/admins-forget', [AdminAuthController::class, 'forget'])->name('admins.forget');
Route::match(['get', 'post'], '/admins', [AdminAuthController::class, 'login'])->name('admins.login');
Route::get('/admins/logout', [AdminAuthController::class, 'logout'])->name('admins.logout');
Route::get('/admins/error403', [AdminAuthController::class, 'error403'])->name('admins.403');
//END Authentication

//-------Admin

//THONG KE
Route::get('/admins/dashboard', [DashBoardController::class, 'index'])->name('admins.dashboard.index')->middleware('auth:admin');
//END THONG KE

//PROFILE
Route::get('/admins/profile', [AdProfileController::class, 'profile'])->name('admins.profile')->middleware('auth:admin');
//END PROFILE


//WEB MENU
Route::get('/admins/web-menu/index', [WebMenuController::class, 'index'])->name('admins.webmenu.index')->middleware('auth:admin');
Route::get('/admins/web-menu/create', [WebMenuController::class, 'create'])->name('admins.webmenu.create')->middleware('auth:admin');
Route::post('/admins/web-menu/store', [WebMenuController::class, 'store'])->name('admins.webmenu.store')->middleware('auth:admin');
Route::get('/admins/web-menu/edit', [WebMenuController::class, 'edit'])->name('admins.webmenu.edit')->middleware('auth:admin');
Route::post('/admins/web-menu/update', [WebMenuController::class, 'update'])->name('admins.webmenu.update')->middleware('auth:admin');
Route::get('/admins/web-menu/show', [WebMenuController::class, 'show'])->name('admins.webmenu.show')->middleware('auth:admin');
Route::post('/admins/web-menu/destroy', [WebMenuController::class, 'destroy'])->name('admins.webmenu.destroy')->middleware('auth:admin');
Route::post('/admins/web-menu/delete', [WebMenuController::class, 'delete'])->name('admins.webmenu.delete')->middleware('auth:admin');
Route::post('/admins/web-menu/recovered', [WebMenuController::class, 'recovered'])->name('admins.webmenu.recovered')->middleware('auth:admin');

Route::get('/admins/web-menu/change/{level}', [WebMenuController::class, 'changeCreate'])->middleware('auth:admin');
Route::get('/admins/web-menu/change/{id}/{level}', [WebMenuController::class, 'changeEdit'])->middleware('auth:admin');
//END WEB MENU


//WEB FEEDBACK
Route::get('/admins/web-feedback/new', [WebFeedbackController::class, 'getCount'])->name('admins.webfeedback.count')->middleware('auth:admin');
Route::get('/admins/web-feedback/index', [WebFeedbackController::class, 'index'])->name('admins.webfeedback.index')->middleware('auth:admin');
Route::post('/admins/web-feedback/update', [WebFeedbackController::class, 'update'])->name('admins.webfeedback.update')->middleware('auth:admin');
Route::post('/admins/web-feedback/delete', [WebFeedbackController::class, 'delete'])->name('admins.webfeedback.delete')->middleware('auth:admin');
//END WEB FEEDBACK


//PROJECT CATEGORY
Route::get('/admins/project-category/index', [ProjectCategoryController::class, 'index'])->name('admins.procategory.index')->middleware('auth:admin');
Route::get('/admins/project-category/create', [ProjectCategoryController::class, 'create'])->name('admins.procategory.create')->middleware('auth:admin');
Route::post('/admins/project-category/store', [ProjectCategoryController::class, 'store'])->name('admins.procategory.store')->middleware('auth:admin');
Route::get('/admins/project-category/edit', [ProjectCategoryController::class, 'edit'])->name('admins.procategory.edit')->middleware('auth:admin');
Route::post('/admins/project-category/update', [ProjectCategoryController::class, 'update'])->name('admins.procategory.update')->middleware('auth:admin');
Route::get('/admins/project-category/show', [ProjectCategoryController::class, 'show'])->name('admins.procategory.show')->middleware('auth:admin');
Route::post('/admins/project-category/destroy', [ProjectCategoryController::class, 'destroy'])->name('admins.procategory.destroy')->middleware('auth:admin');
Route::post('/admins/project-category/delete', [ProjectCategoryController::class, 'delete'])->name('admins.procategory.delete')->middleware('auth:admin');
Route::post('/admins/project-category/recovered', [ProjectCategoryController::class, 'recovered'])->name('admins.procategory.recovered')->middleware('auth:admin');
//END PROJECT CATEGORY

//PROJECT
Route::get('/admins/project/index', [ProjectController::class, 'index'])->name('admins.project.index')->middleware('auth:admin');
Route::get('/admins/project/create', [ProjectController::class, 'create'])->name('admins.project.create')->middleware('auth:admin');
Route::post('/admins/project/store', [ProjectController::class, 'store'])->name('admins.project.store')->middleware('auth:admin');
Route::get('/admins/project/edit', [ProjectController::class, 'edit'])->name('admins.project.edit')->middleware('auth:admin');
Route::post('/admins/project/update', [ProjectController::class, 'update'])->name('admins.project.update')->middleware('auth:admin');
Route::get('/admins/project/show', [ProjectController::class, 'show'])->name('admins.project.show')->middleware('auth:admin');
Route::post('/admins/project/destroy', [ProjectController::class, 'destroy'])->name('admins.project.destroy')->middleware('auth:admin');
Route::post('/admins/project/delete', [ProjectController::class, 'delete'])->name('admins.project.delete')->middleware('auth:admin');
Route::post('/admins/project/recovered', [ProjectController::class, 'recovered'])->name('admins.project.recovered')->middleware('auth:admin');
//END PROJECT

//PROJECT GALLERY
Route::get('/admins/{id}/project-gallery/index', [ProjectGalleryController::class, 'index'])->name('admins.projectgallery.index')->middleware('auth:admin');
Route::get('/admins/{id}/project-gallery/create', [ProjectGalleryController::class, 'create'])->name('admins.projectgallery.create')->middleware('auth:admin');
Route::post('/admins/{id}/project-gallery/store', [ProjectGalleryController::class, 'store'])->name('admins.projectgallery.store')->middleware('auth:admin');
Route::get('/admins/{id}/project-gallery/edit', [ProjectGalleryController::class, 'edit'])->name('admins.projectgallery.edit')->middleware('auth:admin');
Route::post('/admins/{id}/project-gallery/update', [ProjectGalleryController::class, 'update'])->name('admins.projectgallery.update')->middleware('auth:admin');
Route::get('/admins/{id}/project-gallery/show', [ProjectGalleryController::class, 'show'])->name('admins.projectgallery.show')->middleware('auth:admin');
Route::post('/admins/{id}/project-gallery/destroy', [ProjectGalleryController::class, 'destroy'])->name('admins.projectgallery.destroy')->middleware('auth:admin');
//END PROJECT GALLERY

//HOUSE BUSINESS
Route::get('/admins/house-business/index', [HouseBusinessController::class, 'index'])->name('admins.houbusiness.index')->middleware('auth:admin');
Route::get('/admins/house-business/create', [HouseBusinessController::class, 'create'])->name('admins.houbusiness.create')->middleware('auth:admin');
Route::post('/admins/house-business/store', [HouseBusinessController::class, 'store'])->name('admins.houbusiness.store')->middleware('auth:admin');
Route::get('/admins/house-business/edit', [HouseBusinessController::class, 'edit'])->name('admins.houbusiness.edit')->middleware('auth:admin');
Route::post('/admins/house-business/update', [HouseBusinessController::class, 'update'])->name('admins.houbusiness.update')->middleware('auth:admin');
Route::get('/admins/house-business/show', [HouseBusinessController::class, 'show'])->name('admins.houbusiness.show')->middleware('auth:admin');
Route::post('/admins/house-business/destroy', [HouseBusinessController::class, 'destroy'])->name('admins.houbusiness.destroy')->middleware('auth:admin');
Route::post('/admins/house-business/delete', [HouseBusinessController::class, 'delete'])->name('admins.houbusiness.delete')->middleware('auth:admin');
Route::post('/admins/house-business/recovered', [HouseBusinessController::class, 'recovered'])->name('admins.houbusiness.recovered')->middleware('auth:admin');
//END HOUSE BUSINESS


//HOUSE CATEGORY
Route::get('/admins/house-category/index', [HouseCategoryController::class, 'index'])->name('admins.houcategory.index')->middleware('auth:admin');
Route::get('/admins/house-category/create', [HouseCategoryController::class, 'create'])->name('admins.houcategory.create')->middleware('auth:admin');
Route::post('/admins/house-category/store', [HouseCategoryController::class, 'store'])->name('admins.houcategory.store')->middleware('auth:admin');
Route::get('/admins/house-category/edit', [HouseCategoryController::class, 'edit'])->name('admins.houcategory.edit')->middleware('auth:admin');
Route::post('/admins/house-category/update', [HouseCategoryController::class, 'update'])->name('admins.houcategory.update')->middleware('auth:admin');
Route::get('/admins/house-category/show', [HouseCategoryController::class, 'show'])->name('admins.houcategory.show')->middleware('auth:admin');
Route::post('/admins/house-category/destroy', [HouseCategoryController::class, 'destroy'])->name('admins.houcategory.destroy')->middleware('auth:admin');
Route::post('/admins/house-category/delete', [HouseCategoryController::class, 'delete'])->name('admins.houcategory.delete')->middleware('auth:admin');
Route::post('/admins/house-category/recovered', [HouseCategoryController::class, 'recovered'])->name('admins.houcategory.recovered')->middleware('auth:admin');
//END HOUSE CATEGORY

//HOUSE
Route::get('/admins/house/index', [HouseController::class, 'index'])->name('admins.house.index')->middleware('auth:admin');
Route::get('/admins/house/create', [HouseController::class, 'create'])->name('admins.house.create')->middleware('auth:admin');
Route::post('/admins/house/store', [HouseController::class, 'store'])->name('admins.house.store')->middleware('auth:admin');
Route::get('/admins/house/edit', [HouseController::class, 'edit'])->name('admins.house.edit')->middleware('auth:admin');
Route::post('/admins/house/update', [HouseController::class, 'update'])->name('admins.house.update')->middleware('auth:admin');
Route::get('/admins/house/show', [HouseController::class, 'show'])->name('admins.house.show')->middleware('auth:admin');
Route::post('/admins/house/destroy', [HouseController::class, 'destroy'])->name('admins.house.destroy')->middleware('auth:admin');
Route::post('/admins/house/delete', [HouseController::class, 'delete'])->name('admins.house.delete')->middleware('auth:admin');
Route::post('/admins/house/recovered', [HouseController::class, 'recovered'])->name('admins.house.recovered')->middleware('auth:admin');
//END HOUSE

//HOUSE GALLERY
Route::get('/admins/{id}/house-gallery/index', [HouseGalleryController::class, 'index'])->name('admins.housegallery.index')->middleware('auth:admin');
Route::get('/admins/{id}/house-gallery/create', [HouseGalleryController::class, 'create'])->name('admins.housegallery.create')->middleware('auth:admin');
Route::post('/admins/{id}/house-gallery/store', [HouseGalleryController::class, 'store'])->name('admins.housegallery.store')->middleware('auth:admin');
Route::get('/admins/{id}/house-gallery/edit', [HouseGalleryController::class, 'edit'])->name('admins.housegallery.edit')->middleware('auth:admin');
Route::post('/admins/{id}/house-gallery/update', [HouseGalleryController::class, 'update'])->name('admins.housegallery.update')->middleware('auth:admin');
Route::get('/admins/{id}/house-gallery/show', [HouseGalleryController::class, 'show'])->name('admins.housegallery.show')->middleware('auth:admin');
Route::post('/admins/{id}/house-gallery/destroy', [HouseGalleryController::class, 'destroy'])->name('admins.housegallery.destroy')->middleware('auth:admin');
//END HOUSE GALLERY

//HOUSE FLOOR PLAN
Route::get('/admins/{id}/house-floorplan/index', [FloorPlanController::class, 'index'])->name('admins.housefloorplan.index')->middleware('auth:admin');
Route::get('/admins/{id}/house-floorplan/create', [FloorPlanController::class, 'create'])->name('admins.housefloorplan.create')->middleware('auth:admin');
Route::post('/admins/{id}/house-floorplan/store', [FloorPlanController::class, 'store'])->name('admins.housefloorplan.store')->middleware('auth:admin');
Route::get('/admins/{id}/house-floorplan/edit', [FloorPlanController::class, 'edit'])->name('admins.housefloorplan.edit')->middleware('auth:admin');
Route::post('/admins/{id}/house-floorplan/update', [FloorPlanController::class, 'update'])->name('admins.housefloorplan.update')->middleware('auth:admin');
Route::get('/admins/{id}/house-floorplan/show', [FloorPlanController::class, 'show'])->name('admins.housefloorplan.show')->middleware('auth:admin');
Route::post('/admins/{id}/house-floorplan/destroy', [FloorPlanController::class, 'destroy'])->name('admins.housefloorplan.destroy')->middleware('auth:admin');
//END HOUSE FLOOR PLAN

//HOUSE REVIEW
Route::get('/admins/house-review/new', [HouseReviewController::class, 'getCount'])->name('admins.housereview.count')->middleware('auth:admin');
Route::get('/admins/house-review/index', [HouseReviewController::class, 'index'])->name('admins.housereview.index')->middleware('auth:admin');
Route::post('/admins/house-review/update', [HouseReviewController::class, 'update'])->name('admins.housereview.update')->middleware('auth:admin');
Route::get('/admins/house-review/show', [HouseReviewController::class, 'show'])->name('admins.housereview.show')->middleware('auth:admin');
Route::post('/admins/house-review/delete', [HouseReviewController::class, 'delete'])->name('admins.housereview.delete')->middleware('auth:admin');
//END HOUSE REVIEW



//WEBHIRE
Route::get('/admins/web-hire/index', [WebHireController::class, 'index'])->name('admins.webhire.index')->middleware('auth:admin');
Route::get('/admins/web-hire/create', [WebHireController::class, 'create'])->name('admins.webhire.create')->middleware('auth:admin');
Route::post('/admins/web-hire/store', [WebHireController::class, 'store'])->name('admins.webhire.store')->middleware('auth:admin');
Route::get('/admins/web-hire/edit', [WebHireController::class, 'edit'])->name('admins.webhire.edit')->middleware('auth:admin');
Route::post('/admins/web-hire/update', [WebHireController::class, 'update'])->name('admins.webhire.update')->middleware('auth:admin');
Route::get('/admins/web-hire/show', [WebHireController::class, 'show'])->name('admins.webhire.show')->middleware('auth:admin');
Route::post('/admins/web-hire/destroy', [WebHireController::class, 'destroy'])->name('admins.webhire.destroy')->middleware('auth:admin');
Route::post('/admins/web-hire/delete', [WebHireController::class, 'delete'])->name('admins.webhire.delete')->middleware('auth:admin');
Route::post('/admins/web-hire/recovered', [WebHireController::class, 'recovered'])->name('admins.webhire.recovered')->middleware('auth:admin');
//END WEBHIRE

//WEBREVIEW
Route::get('/admins/web-review/index', [WebReviewController::class, 'index'])->name('admins.webreview.index')->middleware('auth:admin');
Route::get('/admins/web-review/create', [WebReviewController::class, 'create'])->name('admins.webreview.create')->middleware('auth:admin');
Route::post('/admins/web-review/store', [WebReviewController::class, 'store'])->name('admins.webreview.store')->middleware('auth:admin');
Route::get('/admins/web-review/edit', [WebReviewController::class, 'edit'])->name('admins.webreview.edit')->middleware('auth:admin');
Route::post('/admins/web-review/update', [WebReviewController::class, 'update'])->name('admins.webreview.update')->middleware('auth:admin');
Route::get('/admins/web-review/show', [WebReviewController::class, 'show'])->name('admins.webreview.show')->middleware('auth:admin');
Route::post('/admins/web-review/destroy', [WebReviewController::class, 'destroy'])->name('admins.webreview.destroy')->middleware('auth:admin');
Route::post('/admins/web-review/delete', [WebReviewController::class, 'delete'])->name('admins.webreview.delete')->middleware('auth:admin');
Route::post('/admins/web-review/recovered', [WebReviewController::class, 'recovered'])->name('admins.webreview.recovered')->middleware('auth:admin');
//END WEBREVIEW

//WEBINFO
Route::get('/admins/web-info/index', [WebInfoController::class, 'index'])->name('admins.webinfo.index')->middleware('auth:admin');
Route::get('/admins/web-info/create', [WebInfoController::class, 'create'])->name('admins.webinfo.create')->middleware('auth:admin');
Route::post('/admins/web-info/store', [WebInfoController::class, 'store'])->name('admins.webinfo.store')->middleware('auth:admin');
Route::get('/admins/web-info/edit', [WebInfoController::class, 'edit'])->name('admins.webinfo.edit')->middleware('auth:admin');
Route::post('/admins/web-info/update', [WebInfoController::class, 'update'])->name('admins.webinfo.update')->middleware('auth:admin');
Route::get('/admins/web-info/show', [WebInfoController::class, 'show'])->name('admins.webinfo.show')->middleware('auth:admin');
Route::post('/admins/web-info/destroy', [WebInfoController::class, 'destroy'])->name('admins.webinfo.destroy')->middleware('auth:admin');
Route::post('/admins/web-info/delete', [WebInfoController::class, 'delete'])->name('admins.webinfo.delete')->middleware('auth:admin');
Route::post('/admins/web-info/recovered', [WebInfoController::class, 'recovered'])->name('admins.webinfo.recovered')->middleware('auth:admin');
//END WEBINFO

//WEBPOLICY
Route::get('/admins/web-policy/index', [WebPolicyController::class, 'index'])->name('admins.webpolicy.index')->middleware('auth:admin');
Route::get('/admins/web-policy/create', [WebPolicyController::class, 'create'])->name('admins.webpolicy.create')->middleware('auth:admin');
Route::post('/admins/web-policy/store', [WebPolicyController::class, 'store'])->name('admins.webpolicy.store')->middleware('auth:admin');
Route::get('/admins/web-policy/edit', [WebPolicyController::class, 'edit'])->name('admins.webpolicy.edit')->middleware('auth:admin');
Route::post('/admins/web-policy/update', [WebPolicyController::class, 'update'])->name('admins.webpolicy.update')->middleware('auth:admin');
Route::get('/admins/web-policy/show', [WebPolicyController::class, 'show'])->name('admins.webpolicy.show')->middleware('auth:admin');
Route::post('/admins/web-policy/destroy', [WebPolicyController::class, 'destroy'])->name('admins.webpolicy.destroy')->middleware('auth:admin');
Route::post('/admins/web-policy/delete', [WebPolicyController::class, 'delete'])->name('admins.webpolicy.delete')->middleware('auth:admin');
Route::post('/admins/web-policy/recovered', [WebPolicyController::class, 'recovered'])->name('admins.webpolicy.recovered')->middleware('auth:admin');
//END WEBPOLICY


//AMENITIES
Route::get('/admins/amenity/index', [AmenitiesController::class, 'index'])->name('admins.amenity.index')->middleware('auth:admin');
Route::get('/admins/amenity/create', [AmenitiesController::class, 'create'])->name('admins.amenity.create')->middleware('auth:admin');
Route::post('/admins/amenity/store', [AmenitiesController::class, 'store'])->name('admins.amenity.store')->middleware('auth:admin');
Route::get('/admins/amenity/edit', [AmenitiesController::class, 'edit'])->name('admins.amenity.edit')->middleware('auth:admin');
Route::post('/admins/amenity/update', [AmenitiesController::class, 'update'])->name('admins.amenity.update')->middleware('auth:admin');
Route::get('/admins/amenity/show', [AmenitiesController::class, 'show'])->name('admins.amenity.show')->middleware('auth:admin');
Route::post('/admins/amenity/destroy', [AmenitiesController::class, 'destroy'])->name('admins.amenity.destroy')->middleware('auth:admin');
Route::post('/admins/amenity/delete', [AmenitiesController::class, 'delete'])->name('admins.amenity.delete')->middleware('auth:admin');
Route::post('/admins/amenity/recovered', [AmenitiesController::class, 'recovered'])->name('admins.amenity.recovered')->middleware('auth:admin');
//END AMENITIES



//HOUSE GALLERY
Route::get('/admins/media-support/index', [MediaSupportController::class, 'index'])->name('admins.mediasupport.index')->middleware('auth:admin');
Route::get('/admins/media-support/ckfinder', [MediaSupportController::class, 'ckfinder'])->name('admins.mediasupport.ckfinder')->middleware('auth:admin');
Route::get('/admins/media-support/create', [MediaSupportController::class, 'create'])->name('admins.mediasupport.create')->middleware('auth:admin');
Route::post('/admins/media-support/store', [MediaSupportController::class, 'store'])->name('admins.mediasupport.store')->middleware('auth:admin');
Route::get('/admins/media-support/edit', [MediaSupportController::class, 'edit'])->name('admins.mediasupport.edit')->middleware('auth:admin');
Route::post('/admins/media-support/update', [MediaSupportController::class, 'update'])->name('admins.mediasupport.update')->middleware('auth:admin');
Route::get('/admins/media-support/show', [MediaSupportController::class, 'show'])->name('admins.mediasupport.show')->middleware('auth:admin');
Route::post('/admins/media-support/destroy', [MediaSupportController::class, 'destroy'])->name('admins.mediasupport.destroy')->middleware('auth:admin');
//END HOUSE GALLERY

//CKFinder
Route::any('/ckfinder/connector', '\CKSource\CKFinderBridge\Controller\CKFinderController@requestAction')
    ->name('ckfinder_connector');

Route::any('/ckfinder/browser', '\CKSource\CKFinderBridge\Controller\CKFinderController@browserAction')
    ->name('ckfinder_browser');
