<?php

namespace App\Models\Helpers;

use Illuminate\Support\Facades\DB;

class FunctionHelper
{
    public static function topMenu()
    {
        //Lay danh sach menu cap 1 sap xep theo thu tu
        $menu1 = DB::table('re_web_menus')->where('active_status', 1)->whereNull('menu_lv1')->whereNull('menu_lv2')
            ->orderBy('menu_order')->get();
        //Lay danh sach menu cap 2 sap xep theo thu tu nam trong menu cap 1
        $menu2 = DB::table('re_web_menus')->where('active_status', 1)->whereNotNull('menu_lv1')->whereNull('menu_lv2')
            ->orderBy('menu_lv1')->orderBy('menu_order')->get();
        //Lay danh sach menu cap 3
        $menu3 = DB::table('re_web_menus')->where('active_status', 1)->whereNotNull('menu_lv1')->whereNotNull('menu_lv2')
            ->orderBy('menu_lv1')->orderBy('menu_lv2')->orderBy('menu_order')->get();
        $menu = collect();
        foreach ($menu1 as $lv1) {
            $lsMenu2 = collect();
            //Kiem tra xem co danh sach lv 2 khong
            $hasLv2 = false;
            //Kiem tra xem co danh sach lv 3 khong
            $hasLv3 = false;
            foreach ($menu2 as $lv2) {
                if ($lv2->menu_lv1 == $lv1->menu_id) {
                    $hasLv2 = true;
                    // Lay danh sach menu lv3 add vao doi
                    $lsMenu3 = collect();
                    //Kiem tra xem co danh sach lv3 trong lv2 khong
                    $has = false;
                    foreach ($menu3 as $lv3) {
                        if ($lv3->menu_lv2 == $lv2->menu_id) {
                            $has = true;
                            $hasLv3 = true;
                            $lsMenu3->add(['menu_name' => $lv3->menu_name, 'menu_url' => $lv3->menu_url]);
                        }
                    }
                    $lsMenu2->add(['menu_name' => $lv2->menu_name, 'menu_url' => $lv2->menu_url, 'has_child' => $has, 'menu_list' => $lsMenu3]);
                }
            }

            $menu->add(['menu_name' => $lv1->menu_name, 'menu_url' => $lv1->menu_url, 'has_child1' => $hasLv2, 'has_child2' => $hasLv3, 'menu_list' => $lsMenu2]);
        }
        return $menu;
    }

    public static function botMenu()
    {
        $botMenu = DB::table('re_house_businesses')->where('active_status', 1)->select('house_business_name','web_canonical')->get();
        return $botMenu;
    }
    public static function webInfo()
    {
        $webInfo = DB::table('re_web_infos')->where('active_status', 1)->orderByDesc('updated_at')->first();
        return $webInfo;
    }
}
