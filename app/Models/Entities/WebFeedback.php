<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebFeedback extends Model
{
    use HasFactory;

    protected $table = 're_web_feedbacks';
    // protected $dateFormat = 'd-m-Y';
    // public $timestamps = true;
    protected $primaryKey  = 'web_feedback_id';

    const WEB_FEEDBACK_NAME = 'web_feedback_name';
    const WEB_FEEDBACK_EMAIL = 'web_feedback_email';
    const WEB_FEEDBACK_CONTENT = 'web_feedback_content';

    const ACTIVE_STATUS = 'active_status';
    const ADMIN_UPDATED = 'admin_updated';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';

    protected $fillable = [
        self::WEB_FEEDBACK_NAME,
        self::WEB_FEEDBACK_EMAIL,
        self::WEB_FEEDBACK_CONTENT,

        self::ACTIVE_STATUS,
    ];
}
