<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Administrator extends Model
{
    use HasFactory;

    protected $table = 're_administrators';
    // protected $dateFormat = 'd-m-Y';
    // public $timestamps = true;
    protected $primaryKey  = 'admin_id';

    const ADMIN_LOGIN_LOCK = 'admin_login_lock';
    const ADMIN_LOGIN_FAIL = 'admin_login_fail';
    const USER_NAME = 'user_name';
    const PASSWORD = 'password';
    const ADMIN_FORGET_CODE = 'admin_forget_code';
    const LAST_LOGIN_TIME = 'last_login_time';

    const ADMIN_AVATAR = 'admin_avatar';
    const ADMIN_FULL_NAME = 'admin_full_name';
    const ADMIN_BRITHDAY = 'admin_brithday';
    const ADMIN_GENDER = 'admin_gender';
    const ADMIN_PHONE = 'admin_phone';
    const ADMIN_FAX = 'admin_fax';
    const ADMIN_EMAIL = 'admin_email';
    const ADMIN_ZIP_CODE = 'admin_zip_code';
    const ADMIN_ADDRESS = 'admin_address';
    const SPECIAL_NOTES = 'special_notes';

    const ADMIN_UPDATED = 'admin_updated';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';
    const DELETED_AT  = 'deleted_at';
    const EMAIL_VERIFIED_AT = 'email_verified_at';
    const REMEMBER_TOKEN = 'remember_token';

    protected $fillable = [
        self::ADMIN_LOGIN_LOCK,
        self::ADMIN_LOGIN_FAIL,
        self::USER_NAME,
        self::PASSWORD,
        self::ADMIN_FORGET_CODE,
        self::LAST_LOGIN_TIME,

        self::ADMIN_AVATAR,
        self::ADMIN_BRITHDAY,
        self::ADMIN_GENDER,
        self::ADMIN_PHONE,
        self::ADMIN_FAX,
        self::ADMIN_EMAIL,
        self::ADMIN_ZIP_CODE,
        self::ADMIN_ADDRESS,
        self::SPECIAL_NOTES,
        self::ADMIN_UPDATED,
        self::ACTIVE_STATUS,
        self::EMAIL_VERIFIED_AT,
        self::REMEMBER_TOKEN,
    ];

}
