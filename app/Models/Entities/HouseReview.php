<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HouseReview extends Model
{
    use HasFactory;

    protected $table = 're_house_reviews';
    // protected $dateFormat = 'd-m-Y';
    // public $timestamps = true;
    protected $primaryKey  = 'house_review_id';

    const HOUSE_REVIEW_SUB = 'house_review_sub';
    const HOUSE_REVIEW_MESSAGE = 'house_review_message';
    const HOUSE_REVIEW_POINT = 'house_review_point';

    const HOUSE_REVIEW_NAME = 'house_review_name';
    const HOUSE_REVIEW_EMAIL = 'house_review_email';
    const HOUSE_REVIEW_IP = 'house_review_ip';
    const HOUSE_ID = 'house_id';
    const CUSTOMER_CODE = 'customer_code';

    const ADMIN_UPDATED = 'admin_updated';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';
    const DELETED_AT  = 'deleted_at';

    protected $fillable = [
        self::HOUSE_REVIEW_SUB,
        self::HOUSE_REVIEW_MESSAGE,
        self::HOUSE_REVIEW_POINT,

        self::HOUSE_REVIEW_NAME,
        self::HOUSE_REVIEW_EMAIL,
        self::HOUSE_REVIEW_IP,
        self::HOUSE_ID,
        self::CUSTOMER_CODE,

        self::ADMIN_UPDATED,
        self::ACTIVE_STATUS,
    ];
}
