<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HouseCategory extends Model
{
    use HasFactory;

    protected $table = 're_house_categories';
    // protected $dateFormat = 'd-m-Y';
    // public $timestamps = true;
    protected $primaryKey  = 'house_category_id';

    const HOUSE_CATEGORY_NAME = 'house_category_name';
    const HOUSE_CATEGORY_DESCRIPTION = 'house_category_description';
    const WEB_KEYWORDS = 'web_keywords';
    const WEB_DESCRIPTION = 'web_description';
    const WEB_TITLE = 'web_title';
    const WEB_CANONICAL = 'web_canonical';

    const ADMIN_UPDATED = 'admin_updated';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';
    const DELETED_AT  = 'deleted_at';

    protected $fillable = [
        self::HOUSE_CATEGORY_NAME,
        self::HOUSE_CATEGORY_DESCRIPTION,
        self::WEB_KEYWORDS,
        self::WEB_DESCRIPTION,
        self::WEB_TITLE,
        self::WEB_CANONICAL,

        self::ADMIN_UPDATED,
        self::ACTIVE_STATUS,
    ];
}
