<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebHire extends Model
{
    use HasFactory;

    protected $table = 're_web_hires';
    // protected $dateFormat = 'd-m-Y';
    // public $timestamps = true;
    protected $primaryKey  = 'web_hire_id';

    const WEB_HIRE_IMAGE = 'web_hire_image';
    const WEB_HIRE_NAME = 'web_hire_name';
    const WEB_HIRE_DESCRIPTION = 'web_hire_description';
    const WEB_HIRE_DETAIL = 'web_hire_detail';

    const WEB_KEYWORDS = 'web_keywords';
    const WEB_DESCRIPTION = 'web_description';
    const WEB_TITLE = 'web_title';
    const WEB_CANONICAL = 'web_canonical';

    const ADMIN_UPDATED = 'admin_updated';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';
    const DELETED_AT  = 'deleted_at';

    protected $fillable = [
        self::WEB_HIRE_IMAGE,
        self::WEB_HIRE_NAME,
        self::WEB_HIRE_DESCRIPTION,
        self::WEB_HIRE_DETAIL,

        self::WEB_KEYWORDS,
        self::WEB_DESCRIPTION,
        self::WEB_TITLE,
        self::WEB_CANONICAL,

        self::ADMIN_UPDATED,
        self::ACTIVE_STATUS,
    ];
}
