<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $table = 're_projects';
    // protected $dateFormat = 'd-m-Y';
    // public $timestamps = true;
    protected $primaryKey  = 'project_id';

    const PROJECT_NAME = 'project_name';
    const PROJECT_DESCRIPTION = 'project_description';
    const PROJECT_DETAIL = 'project_detail';
    const PROJECT_AMENITIES = 'project_amenities';

    const PROJECT_POSTAL_CODE = 'project_postal_code';
    const PROJECT_PROVINCIAL = 'project_provincial';
    const PROJECT_DISTRICT = 'project_district';
    const PROJECT_COMMUNE = 'project_commune';
    const PROJECT_ADDRESS = 'project_address';
    const PROJECT_MAP_LOCATION = 'project_map_location';

    const PROJECT_OWN = 'project_own';
    const PROJECT_BUILD_TIME = 'project_build_time';
    const PROJECT_BLOCK = 'project_block';
    const PROJECT_FLOOR = 'project_floor';
    const PROJECT_HOUSE = 'project_house';
    const PROJECT_ACREAGE = 'project_acreage';
    const PROJECT_HOUSE_ACREAGE = 'project_house_acreage';

    const PROJECT_CATEGORY_ID = 'project_category_id';

    const WEB_KEYWORDS = 'web_keywords';
    const WEB_DESCRIPTION = 'web_description';
    const WEB_TITLE = 'web_title';
    const WEB_CANONICAL = 'web_canonical';


    const ADMIN_UPDATED = 'admin_updated';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';
    const DELETED_AT  = 'deleted_at';

    protected $fillable = [

        self::PROJECT_NAME,
        self::PROJECT_DESCRIPTION,
        self::PROJECT_DETAIL,

        self::PROJECT_POSTAL_CODE,
        self::PROJECT_PROVINCIAL,
        self::PROJECT_DISTRICT,
        self::PROJECT_COMMUNE,
        self::PROJECT_ADDRESS,
        self::PROJECT_MAP_LOCATION,

        self::PROJECT_OWN,
        self::PROJECT_BUILD_TIME,
        self::PROJECT_BLOCK,
        self::PROJECT_FLOOR,
        self::PROJECT_HOUSE,
        self::PROJECT_ACREAGE,
        self::PROJECT_HOUSE_ACREAGE,
        self::PROJECT_CATEGORY_ID,

        self::WEB_KEYWORDS,
        self::WEB_DESCRIPTION,
        self::WEB_TITLE,
        self::WEB_CANONICAL,

        self::ADMIN_UPDATED,
        self::ACTIVE_STATUS,
    ];
}
