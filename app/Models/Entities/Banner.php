<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;

    protected $table = 're_banners';
    // protected $dateFormat = 'd-m-Y';
    // public $timestamps = true;
    protected $primaryKey  = 'banner_id';

    const BANNER_NAME = 'banner_name';
    const BANNER_URL = 'banner_url';
    const BANNER_ORDER = 'banner_order';
    const BANNER_DESCRIPTION = 'banner_description';
    const BANNER_DETAIL = 'banner_detail';
    const BANNER_IMAGE = 'banner_image';

    const ADMIN_UPDATED = 'admin_updated';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';
    const DELETED_AT  = 'deleted_at';

    protected $fillable = [
        self::BANNER_NAME,
        self::BANNER_URL,
        self::BANNER_ORDER,
        self::BANNER_DESCRIPTION,
        self::BANNER_DETAIL,
        self::BANNER_IMAGE,

        self::ADMIN_UPDATED,
        self::ACTIVE_STATUS,
    ];
}
