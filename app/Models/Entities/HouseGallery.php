<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HouseGallery extends Model
{
    use HasFactory;

    protected $table = 're_house_galleries';
    // protected $dateFormat = 'd-m-Y';
    // public $timestamps = true;
    protected $primaryKey  = 'house_gallery_id';

    const HOUSE_GALLERY_NAME = 'house_gallery_name';
    const HOUSE_GALLERY_BANNER = 'house_gallery_banner';
    const HOUSE_GALLERY_LINK = 'house_gallery_link';
    const HOUSE_GALLERY_ORDER = 'house_gallery_order';
    const HOUSE_ID = 'house_id';

    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';

    protected $fillable = [
        self::HOUSE_GALLERY_NAME,
        self::HOUSE_GALLERY_BANNER,
        self::HOUSE_GALLERY_LINK,
        self::HOUSE_GALLERY_ORDER,
        self::HOUSE_ID,

        self::ACTIVE_STATUS,
    ];
}
