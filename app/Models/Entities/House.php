<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    use HasFactory;

    protected $table = 're_houses';
    // protected $dateFormat = 'd-m-Y';
    // public $timestamps = true;
    protected $primaryKey  = 'house_id';

    const HOUSE_CODE = 'house_code';
    const HOUSE_NAME = 'house_name';
    const HOUSE_DESCRIPTION = 'house_description';
    const HOUSE_DETAIL = 'house_detail';
    const HOUSE_AMENITIES = 'house_amenities';

    const HOUSE_OWN = 'house_own';
    const HOUSE_BUILD_TIME = 'house_build_time';
    const HOUSE_ACREAGE = 'house_acreage';
    const HOUSE_MAP_LOCATION = 'house_map_location';
    const HOUSE_LENGTH = 'house_length';
    const HOUSE_WIDTH = 'house_width';
    const HOUSE_DIRECTION = 'house_direction';
    const HOUSE_ROOMS = 'house_rooms';
    const HOUSE_BATH_ROOMS = 'house_bath_rooms';
    const HOUSE_GARAGES = 'house_garages';
    const HOUSE_POSTAL_CODE = 'house_postal_code';
    const HOUSE_PROVINCIAL = 'house_provincial';
    const HOUSE_DISTRICT = 'house_district';
    const HOUSE_ADDRESS = 'house_address';
    const HOUSE_COMMUNE = 'house_commune';

    const HOUSE_PRICE = 'house_price';
    const HOUSE_PRICE_SUB = 'house_price_sub';


    const HOUSE_RATING = 'house_rating';
    const HOUSE_RATING_POINT = 'house_rating_point';
    const HOUSE_RATING_COUNT = 'house_rating_count';
    const HOUSE_STATUS = 'house_status';
    const HOUSE_STATUS_COLOR = 'house_status_color';

    const HOUSE_BUSINESS_ID = 'house_business_id';
    const HOUSE_CATEGORY_ID = 'house_category_id';
    const PROJECT_ID = 'project_id';

    const HOUSE_VIEWED = 'house_viewed';
    const WEB_KEYWORDS = 'web_keywords';
    const WEB_DESCRIPTION = 'web_description';
    const WEB_TITLE = 'web_title';
    const WEB_CANONICAL = 'web_canonical';



    const ADMIN_UPDATED = 'admin_updated';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';
    const DELETED_AT  = 'deleted_at';

    protected $fillable = [
        self::HOUSE_CODE,
        self::HOUSE_NAME,
        self::HOUSE_DESCRIPTION,
        self::HOUSE_DETAIL,

        self::HOUSE_OWN,
        self::HOUSE_BUILD_TIME,
        self::HOUSE_ACREAGE,
        self::HOUSE_LENGTH,
        self::HOUSE_WIDTH,
        self::HOUSE_DIRECTION,
        self::HOUSE_ROOMS,
        self::HOUSE_BATH_ROOMS,
        self::HOUSE_GARAGES,

        self::HOUSE_POSTAL_CODE,
        self::HOUSE_PROVINCIAL,
        self::HOUSE_DISTRICT,
        self::HOUSE_ADDRESS,
        self::HOUSE_COMMUNE,
        self::HOUSE_MAP_LOCATION,

        self::HOUSE_PRICE,
        self::HOUSE_PRICE_SUB,

        self::HOUSE_RATING,
        self::HOUSE_RATING_POINT,
        self::HOUSE_RATING_COUNT,
        self::HOUSE_STATUS,
        self::HOUSE_STATUS_COLOR,

        self::HOUSE_BUSINESS_ID,
        self::HOUSE_CATEGORY_ID,
        self::PROJECT_ID,

        self::HOUSE_VIEWED,
        self::WEB_KEYWORDS,
        self::WEB_DESCRIPTION,
        self::WEB_TITLE,
        self::WEB_CANONICAL,
        self::ADMIN_UPDATED,
        self::ACTIVE_STATUS,
    ];
}
