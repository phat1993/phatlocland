<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebReview extends Model
{
    use HasFactory;

    protected $table = 're_web_reviews';
    // protected $dateFormat = 'd-m-Y';
    // public $timestamps = true;
    protected $primaryKey  = 'web_review_id';


    const WEB_REVIEW_IMAGE = 'web_review_image';
    const WEB_REVIEW_NAME = 'web_review_name';
    const WEB_REVIEW_ADDRESS = 'web_review_address';
    const WEB_REVIEW_DETAIL = 'web_review_detail';

    const ADMIN_UPDATED = 'admin_updated';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';
    const DELETED_AT  = 'deleted_at';

    protected $fillable = [
        self::WEB_REVIEW_IMAGE,
        self::WEB_REVIEW_NAME,
        self::WEB_REVIEW_ADDRESS,
        self::WEB_REVIEW_DETAIL,

        self::ADMIN_UPDATED,
        self::ACTIVE_STATUS,
    ];
}
