<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebMenu extends Model
{
    use HasFactory;

    protected $table = 're_web_menus';
    // protected $dateFormat = 'd-m-Y';
    // public $timestamps = true;
    protected $primaryKey  = 'menu_id';

    const MENU_NAME = 'menu_name';
    const MENU_ORDER = 'menu_order';
    const MENU_URL = 'menu_url';
    const MENU_LV1 = 'menu_lv1';
    const MENU_LV2 = 'menu_lv2';

    const ADMIN_UPDATED = 'admin_updated';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';
    const DELETED_AT  = 'deleted_at';

    protected $fillable = [
        self::MENU_NAME,
        self::MENU_ORDER,
        self::MENU_URL,
        self::MENU_LV1,
        self::MENU_LV2,

        self::ADMIN_UPDATED,
        self::ACTIVE_STATUS,
    ];
}
