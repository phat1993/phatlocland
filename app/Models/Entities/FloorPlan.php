<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FloorPlan extends Model
{
    use HasFactory;

    protected $table = 're_floor_plans';
    // protected $dateFormat = 'd-m-Y';
    // public $timestamps = true;
    protected $primaryKey  = 'floor_plan_id';

    const FLOOR_PLAN_NAME = 'floor_plan_name';
    const FLOOR_PLAN_IMAGE = 'floor_plan_image';
    const FLOOR_PLAN_SIZE = 'floor_plan_size';
    const FLOOR_PLAN_ROOMS = 'floor_plan_rooms';
    const FLOOR_PLAN_BARTHS = 'floor_plan_barths';
    const FLOOR_PLAN_PRICE = 'floor_plan_price';
    const HOUSE_ID = 'house_id';

    const ADMIN_UPDATED = 'admin_updated';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';
    const DELETED_AT  = 'deleted_at';

    protected $fillable = [
        self::FLOOR_PLAN_NAME,
        self::FLOOR_PLAN_IMAGE,
        self::FLOOR_PLAN_SIZE,
        self::FLOOR_PLAN_ROOMS,
        self::FLOOR_PLAN_BARTHS,
        self::FLOOR_PLAN_PRICE,
        self::HOUSE_ID,

        self::ADMIN_UPDATED,
        self::ACTIVE_STATUS,
    ];
}
