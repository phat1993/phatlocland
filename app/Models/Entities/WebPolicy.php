<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebPolicy extends Model
{
    use HasFactory;

    protected $table = 're_web_policies';
    // protected $dateFormat = 'd-m-Y';
    // public $timestamps = true;
    protected $primaryKey  = 'web_policy_id';

    const WEB_POLICY_IMAGE = 'web_policy_image';
    const WEB_POLICY_NAME = 'web_policy_name';
    const WEB_POLICY_DESCRIPTION = 'web_policy_description';
    const WEB_POLICY_DETAIL = 'web_policy_detail';

    const WEB_KEYWORDS = 'web_keywords';
    const WEB_DESCRIPTION = 'web_description';
    const WEB_TITLE = 'web_title';
    const WEB_CANONICAL = 'web_canonical';

    const ADMIN_UPDATED = 'admin_updated';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';
    const DELETED_AT  = 'deleted_at';

    protected $fillable = [
        self::WEB_POLICY_IMAGE,
        self::WEB_POLICY_NAME,
        self::WEB_POLICY_DESCRIPTION,
        self::WEB_POLICY_DETAIL,

        self::WEB_KEYWORDS,
        self::WEB_DESCRIPTION,
        self::WEB_TITLE,
        self::WEB_CANONICAL,

        self::ADMIN_UPDATED,
        self::ACTIVE_STATUS,
    ];
}
