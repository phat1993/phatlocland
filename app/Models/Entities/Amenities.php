<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Amenities extends Model
{
    use HasFactory;

    protected $table = 're_amenities';
    // protected $dateFormat = 'd-m-Y';
    // public $timestamps = true;
    protected $primaryKey  = 'amenity_id';

    const AMENITY_NAME = 'amenity_name';
    const AMENITY_ICON = 'amenity_icon';
    const AMENITY_DETAIL = 'amenity_detail';

    const ADMIN_UPDATED = 'admin_updated';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';
    const DELETED_AT  = 'deleted_at';

    protected $fillable = [
        self::AMENITY_NAME,
        self::AMENITY_ICON,
        self::AMENITY_DETAIL,

        self::ADMIN_UPDATED,
        self::ACTIVE_STATUS,
    ];
}
