<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MediaSupport extends Model
{
    use HasFactory;

    protected $table = 're_media_supports';
    // protected $dateFormat = 'd-m-Y';
    // public $timestamps = true;
    protected $primaryKey  = 'media_support_id';

    const MEDIA_SUPPORT_NAME = 'media_support_name';
    const MEDIA_SUPPORT_URL = 'media_support_url';
    const MEDIA_SUPPORT_SIZE = 'media_support_size';
    const MEDIA_SUPPORT_EXTENSION = 'media_support_extension';
    const MEDIA_SUPPORT_DESCRIPTION = 'media_support_description';
    const ADMIN_UPDATED  = 'admin_updated';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';

    protected $fillable = [
        self::MEDIA_SUPPORT_NAME,
        self::MEDIA_SUPPORT_URL,
        self::MEDIA_SUPPORT_SIZE,
        self::MEDIA_SUPPORT_EXTENSION,
        self::MEDIA_SUPPORT_DESCRIPTION,
        self::ADMIN_UPDATED,
    ];
}
