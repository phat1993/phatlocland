<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebInfo extends Model
{
    use HasFactory;

    protected $table = 're_web_infos';
    // protected $dateFormat = 'd-m-Y';
    // public $timestamps = true;
    protected $primaryKey  = 'web_info_id';


    const WEB_INFO_TITLE = 'web_info_title';
    const WEB_INFO_PHONE = 'web_info_phone';
    const WEB_INFO_MAIL = 'web_info_mail';
    const WEB_INFO_ADDRESS = 'web_info_address';

    const WEB_INFO_ZALO = 'web_info_zalo';
    const WEB_INFO_FACEBOOK = 'web_info_facebook';
    const WEB_INFO_YOUTUBE = 'web_info_youtube';
    const WEB_INFO_INSTAGRAM = 'web_info_instagram';

    const WEB_INFO_MAP_LOCATION = 'web_info_map_location';
    const WEB_INFO_WORK_TIME = 'web_info_work_time';
    const WEB_INFO_SPECIAL_NOTE = 'web_info_special_note';

    const WEB_KEYWORDS = 'web_keywords';
    const WEB_DESCRIPTION = 'web_description';
    const WEB_TITLE = 'web_title';
    const WEB_CANONICAL = 'web_canonical';

    const ADMIN_UPDATED = 'admin_updated';
    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';
    const DELETED_AT  = 'deleted_at';

    protected $fillable = [
        self::WEB_INFO_TITLE,
        self::WEB_INFO_PHONE,
        self::WEB_INFO_MAIL,
        self::WEB_INFO_ADDRESS,

        self::WEB_INFO_MAP_LOCATION,
        self::WEB_INFO_WORK_TIME,

        self::WEB_INFO_ZALO,
        self::WEB_INFO_FACEBOOK,
        self::WEB_INFO_YOUTUBE,
        self::WEB_INFO_INSTAGRAM,

        self::WEB_INFO_MAP_LOCATION,
        self::WEB_INFO_WORK_TIME,
        self::WEB_INFO_SPECIAL_NOTE,

        self::ADMIN_UPDATED,
        self::ACTIVE_STATUS,
    ];
}
