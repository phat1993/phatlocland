<?php

namespace App\Models\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectGallery extends Model
{
    use HasFactory;

    protected $table = 're_project_galleries';
    // protected $dateFormat = 'd-m-Y';
    // public $timestamps = true;
    protected $primaryKey  = 'project_gallery_id';

    const PROJECT_GALLERY_NAME = 'project_gallery_name';
    const PROJECT_GALLERY_BANNER = 'project_gallery_banner';
    const PROJECT_GALLERY_LINK = 'project_gallery_link';
    const PROJECT_GALLERY_ORDER = 'project_gallery_order';
    const PROJECT_ID = 'project_id';

    const ACTIVE_STATUS = 'active_status';
    const CREATED_AT  = 'created_at';
    const UPDATED_AT  = 'updated_at';

    protected $fillable = [
        self::PROJECT_GALLERY_NAME,
        self::PROJECT_GALLERY_BANNER,
        self::PROJECT_GALLERY_LINK,
        self::PROJECT_GALLERY_ORDER,
        self::PROJECT_ID,

        self::ACTIVE_STATUS,
    ];
}
