<?php

namespace App\Models\ViewModels\Admins;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectAmenities extends Model
{
    use HasFactory;

    const AMENITY_ID = 'amenity_id';
    const AMENITY_NAME = 'amenity_name';
    const AMENITY_ICON = 'amenity_icon';

    protected $fillable = [
        self::AMENITY_ID,
        self::AMENITY_NAME,
        self::AMENITY_ICON,
    ];
}
