<?php

namespace App\Models\ViewModels\Clients;

use Illuminate\Database\Eloquent\Model;

class ClientBanner extends Model
{
    const ID = 'id';
    const IMAGE = 'image';
    const LINK = 'link';
    const TITLE = 'title';

    const ACREAGE = 'acreage';
    const LENGTH = 'length';
    const WIDTH = 'width';
    const DIRECTION = 'direction';
    const PRICE = 'price';


    const ADDRESS = 'address';
    const MAP = 'map';

    protected $fillable = [
        self::IMAGE,
        self::LINK,
        self::TITLE,

        self::ACREAGE,
        self::LENGTH,
        self::WIDTH,
        self::DIRECTION,
        self::PRICE,

        self::ADDRESS,
        self::MAP,
    ];
}
