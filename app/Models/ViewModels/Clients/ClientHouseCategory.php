<?php

namespace App\Models\ViewModels\Clients;

use Illuminate\Database\Eloquent\Model;

class ClientHouseCategory extends Model
{
    const ID = 'id';
    const IMAGE = 'image';
    const LINK = 'link';
    const FEATURES = 'features'; //array
    const GALLERIES = 'galleries'; //array
    const TITLE = 'title';
    const TIME = 'time';

    const RATING = 'rating';
    const RATING_COUNT = 'rating_count';
    const STATUS = 'status';
    const STATUS_COLOR = 'status_color';

    const ACREAGE = 'acreage';
    const LENGTH = 'length';
    const WIDTH = 'width';
    const DIRECTION = 'direction';
    const ROOMS = 'rooms';
    const PRICE = 'price';


    const ADDRESS = 'address';
    const BEFORE_DAY = 'before_day';
    const BEFORE_HOUR = 'before_hour';
    const MAP = 'map';

    protected $fillable = [
        self::IMAGE,
        self::LINK,
        self::FEATURES,
        self::GALLERIES,
        self::TITLE,
        self::TIME,

        self::RATING,
        self::RATING_COUNT,
        self::STATUS,
        self::STATUS_COLOR,

        self::ACREAGE,
        self::LENGTH,
        self::WIDTH,
        self::DIRECTION,
        self::ROOMS,
        self::PRICE,

        self::ADDRESS,
        self::BEFORE_DAY,
        self::BEFORE_HOUR,
        self::MAP,
    ];
}
