<?php

namespace App\Models\ViewModels\Clients;

use Illuminate\Database\Eloquent\Model;

class ClientProjectCategory extends Model
{
    const ID = 'id';
    const IMAGE = 'image';
    const LINK = 'link';
    const FEATURES = 'features'; //array
    const GALLERIES = 'galleries'; //array
    const TITLE = 'title';
    const BLOCK = 'block';
    const TIME = 'time';
    const FLOOR = 'floor';
    const ACREAGE = 'acreage';
    const HOUSE = 'house';
    const HOUSE_ACREAGE = 'house_acreage';
    const ADDRESS = 'address';
    const BEFORE_DAY = 'before_day';
    const BEFORE_HOUR = 'before_hour';
    const MAP = 'map';
    const DESCRIPTION = 'description';

    protected $fillable = [
        self::IMAGE,
        self::LINK,
        self::FEATURES,
        self::GALLERIES,
        self::TITLE,
        self::BLOCK,
        self::TIME,
        self::FLOOR,
        self::ACREAGE,
        self::HOUSE,
        self::HOUSE_ACREAGE,
        self::ADDRESS,
        self::BEFORE_DAY,
        self::BEFORE_HOUR,
        self::MAP,
        self::DESCRIPTION,
    ];
}
