<?php

namespace App\Http\Requests\Admins;


use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        $action = $this->route()->getActionMethod();
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($action == 'destroy'||$action == 'delete')
                    $rules = [];
                else
                    $rules = [
                        'project_name'  => 'required|max:500',
                        'project_description'  => 'required|max:1000',
                        'project_detail'  => 'required',

                        'project_own'  => 'max:100',
                        'project_block'  => 'max:50',
                        'project_floor'  => 'max:50',
                        'project_house'  => 'max:50',
                        'project_acreage'  => 'max:50',
                        'project_house_acreage'  => 'max:50',

                        'project_postal_code'  => 'required|max:100',
                        'project_provincial'  => 'required|max:100',
                        'project_district'  => 'required|max:100',
                        'project_commune'  => 'required|max:100',
                        'project_address'  => 'required|max:200',
                        'project_map_location'  => 'required|max:1000',

                        'web_title'  => 'max:20',
                        'web_keywords'  => 'max:200',
                        'web_description'  => 'max:145',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'project_name.required' => 'Tiêu Đề: Bạn không được để trống.',
            'project_name.max' => 'Tiêu Đề: Số kí tự cho phép nhập tối đa là 500.',
            'project_description.required' => 'Mô Tả: Bạn không được để trống.',
            'project_description.max' => 'Mô Tả: Số kí tự cho phép nhập tối đa là 1000.',
            'project_detail.required' => 'Chi Tiết: Bạn không được để trống.',

            'project_own.max' => 'Chủ Sở Hữu: Số kí tự cho phép nhập tối đa là 50.',
            'project_block.max' => 'Số Tòa Nhà: Số kí tự cho phép nhập tối đa là 50.',
            'project_floor.max' => 'Số Tầng: Số kí tự cho phép nhập tối đa là 50.',
            'project_house.max' => 'Số Bất Động Sản: Số kí tự cho phép nhập tối đa là 50.',
            'project_acreage.max' => 'Diện Tích: Số kí tự cho phép nhập tối đa là 50.',
            'project_house_acreage.max' => 'Diện Tích Bất Động Sản: Số kí tự cho phép nhập tối đa là 50.',

            'project_postal_code.required' => 'Mã Bưu Điện: Bạn không được để trống.',
            'project_postal_code.max' => 'Mã Bưu Điện: Số kí tự cho phép nhập tối đa là 100.',
            'project_provincial.required' => 'Tỉnh/Thành Phố: Bạn không được để trống.',
            'project_provincial.max' => 'Tỉnh/Thành Phố: Số kí tự cho phép nhập tối đa là 100.',
            'project_district.required' => 'Quận/Huyện: Bạn không được để trống.',
            'project_district.max' => 'Quận/Huyện: Số kí tự cho phép nhập tối đa là 100.',
            'project_commune.required' => 'Phường/Xã: Bạn không được để trống.',
            'project_commune.max' => 'Phường/Xã: Số kí tự cho phép nhập tối đa là 100.',
            'project_address.required' => 'Địa Chỉ: Bạn không được để trống.',
            'project_address.max' => 'Địa Chỉ: Số kí tự cho phép nhập tối đa là 200.',
            'project_map_location.required' => 'Định Vị Bản Đồ: Bạn không được để trống.',
            'project_map_location.max' => 'Định Vị Bản Đồ: Số kí tự cho phép nhập tối đa là 1000.',

            'web_title.max' => 'Tiêu Đề Website: Số kí tự cho phép nhập tối đa là 20.',
            'web_keywords.max' => 'Từ Khóa: Số kí tự cho phép nhập tối đa là 200.',
            'web_description.max' => 'Mô Tả Website: Số kí tự cho phép nhập tối đa là 145.',
        ];
    }
}
