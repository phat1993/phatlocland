<?php

namespace App\Http\Requests\Admins;

use Illuminate\Foundation\Http\FormRequest;

class WebHireRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        $action = $this->route()->getActionMethod();
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($action == 'destroy'||$action == 'delete')
                    $rules = [];
                else
                    $rules = [
                        'web_hire_name'  => 'required|max:200',
                        'web_hire_description'  => 'required|max:500',
                        'web_hire_detail'  => 'required',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'web_hire_name.required' => 'Tiêu Đề: Bạn không được để trống.',
            'web_hire_name.max' => 'Tiêu Đề: Số kí tự cho phép nhập tối đa là 200.',
            'web_hire_description.required' => 'Mô tả: Bạn không được để trống.',
            'web_hire_description.max' => 'Mô tả: Số kí tự cho phép nhập tối đa là 500.',
            'web_hire_detail.required' => 'Chi Tiết: Bạn không được để trống.',
        ];
    }
}
