<?php

namespace App\Http\Requests\Admins;

use Illuminate\Foundation\Http\FormRequest;

class WebMenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        $action = $this->route()->getActionMethod();
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($action == 'destroy'||$action == 'delete')
                    $rules = [];
                else
                    $rules = [
                        'menu_name'  => 'required|max:50',
                        'menu_order'  => 'required|numeric|max:50',
                        'menu_url'  => 'required|max:300',

                        // 'menu_lv1'  => 'max:5',
                        // 'menu_lv2'  => 'max:5',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'menu_name.required' => 'Tên Danh Mục: Bạn không được để trống.',
            'menu_name.max' => 'Tên Danh Mục: Số kí tự cho phép nhập tối đa là 50.',

            'menu_order.required' => 'Thứ Tự: Bạn không được để trống.',
            'menu_order.numeric' => 'Thứ Tự: Bạn phải nhập số.',
            'menu_order.max' => 'Thứ Tự: Số kí tự cho phép nhập tối đa là 2.',

            'menu_url.required' => 'Đường Dẫn: Bạn không được để trống.',
            'menu_url.max' => 'Đường Dẫn: Số kí tự cho phép nhập tối đa là 300.',

            // 'menu_lv1.max' => 'Danh Mục Cấp 1: Số kí tự cho phép nhập tối đa là 5.',
            // 'menu_lv2.max' => 'Danh Mục Cấp 2: Số kí tự cho phép nhập tối đa là 5.',
        ];
    }
}
