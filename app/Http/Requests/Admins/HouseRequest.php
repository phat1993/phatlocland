<?php

namespace App\Http\Requests\Admins;

use Illuminate\Foundation\Http\FormRequest;

class HouseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        $action = $this->route()->getActionMethod();
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($action == 'destroy'||$action == 'delete')
                    $rules = [];
                else
                    $rules = [
                        'house_name'  => 'required|max:500',
                        'house_description'  => 'required|max:1000',
                        'house_detail'  => 'required',
                        // 'house_price'  => 'required',

                        'house_own'  => 'max:100',
                        'house_acreage'  => 'max:50',
                        'house_length'  => 'max:10',
                        'house_width'  => 'max:10',
                        'house_direction'  => 'max:50',
                        'house_rooms'  => 'max:4',
                        'house_bath_rooms'  => 'max:2',
                        'house_garages'  => 'max:2',

                        'house_postal_code'  => 'required|max:100',
                        'house_provincial'  => 'required|max:100',
                        'house_district'  => 'required|max:100',
                        'house_commune'  => 'required|max:100',
                        'house_address'  => 'required|max:200',
                        'house_map_location'  => 'required|max:1000',


                        'web_title'  => 'max:20',
                        'web_keywords'  => 'max:200',
                        'web_description'  => 'max:145',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'house_name.required' => 'Tiêu Đề: Bạn không được để trống.',
            'house_name.max' => 'Tiêu Đề: Số kí tự cho phép nhập tối đa là 500.',
            'house_description.required' => 'Mô Tả: Bạn không được để trống.',
            'house_description.max' => 'Mô Tả: Số kí tự cho phép nhập tối đa là 1000.',
            'house_detail.required' => 'Chi Tiết: Bạn không được để trống.',

            // 'house_price.required' => 'Giá Tiền: Bạn không được để trống.',

            'house_own.max' => 'Chủ Sở Hữu: Số kí tự cho phép nhập tối đa là 50.',
            'house_acreage.max' => 'Diện tích: Số kí tự cho phép nhập tối đa là 50.',
            'house_length.max' => 'Chiều dài: Số kí tự cho phép nhập tối đa là 10.',
            'house_width.max' => 'Chiều Rộng: Số kí tự cho phép nhập tối đa là 10.',
            'house_direction.max' => 'Hướng Nhà: Số kí tự cho phép nhập tối đa là 50.',
            'house_rooms.max' => 'Số Phòng: Số kí tự cho phép nhập tối đa là 4.',
            'house_bath_rooms.max' => 'Số Phòng Tắm: Số kí tự cho phép nhập tối đa là 2.',
            'house_garages.max' => 'Số Nhà Để Xe: Số kí tự cho phép nhập tối đa là 2.',

            'house_postal_code.required' => 'Mã Bưu Điện: Bạn không được để trống.',
            'house_postal_code.max' => 'Mã Bưu Điện: Số kí tự cho phép nhập tối đa là 100.',
            'house_provincial.required' => 'Tỉnh/Thành Phố: Bạn không được để trống.',
            'house_provincial.max' => 'Tỉnh/Thành Phố: Số kí tự cho phép nhập tối đa là 100.',
            'house_district.required' => 'Quận/Huyện: Bạn không được để trống.',
            'house_district.max' => 'Quận/Huyện: Số kí tự cho phép nhập tối đa là 100.',
            'house_commune.required' => 'Phường/Xã: Bạn không được để trống.',
            'house_commune.max' => 'Phường/Xã: Số kí tự cho phép nhập tối đa là 100.',
            'house_address.required' => 'Địa Chỉ: Bạn không được để trống.',
            'house_address.max' => 'Địa Chỉ: Số kí tự cho phép nhập tối đa là 200.',
            'house_map_location.required' => 'Định Vị Bản Đồ: Bạn không được để trống.',
            'house_map_location.max' => 'Định Vị Bản Đồ: Số kí tự cho phép nhập tối đa là 1000.',

            'web_title.max' => 'Tiêu Đề Website: Số kí tự cho phép nhập tối đa là 20.',
            'web_keywords.max' => 'Từ Khóa: Số kí tự cho phép nhập tối đa là 200.',
            'web_description.max' => 'Mô Tả Website: Số kí tự cho phép nhập tối đa là 145.',
        ];
    }
}
