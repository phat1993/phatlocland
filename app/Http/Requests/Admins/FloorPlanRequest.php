<?php

namespace App\Http\Requests\Admins;


use Illuminate\Foundation\Http\FormRequest;

class FloorPlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        $action = $this->route()->getActionMethod();
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($action == 'destroy'||$action == 'delete')
                    $rules = [];
                else
                    $rules = [
                        'floor_plan_name'  => 'required|max:100',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'floor_plan_name.required' => 'Tên Hình Ảnh: Bạn không được để trống.',
            'floor_plan_name.max' => 'Tên Hình Ảnh: Số kí tự cho phép nhập tối đa là 100.',
        ];
    }
}
