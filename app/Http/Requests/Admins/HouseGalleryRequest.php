<?php

namespace App\Http\Requests\Admins;


use Illuminate\Foundation\Http\FormRequest;

class HouseGalleryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        $action = $this->route()->getActionMethod();
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($action == 'destroy'||$action == 'delete')
                    $rules = [];
                else
                    $rules = [
                        'house_gallery_name'  => 'required|max:100',
                        'house_gallery_order'  => 'required|numeric',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'house_gallery_name.required' => 'Tên Hình Ảnh: Bạn không được để trống.',
            'house_gallery_name.max' => 'Tên Hình Ảnh: Số kí tự cho phép nhập tối đa là 100.',
            'house_gallery_order.required' => 'Vị Trí: Bạn không được để trống.',
            'house_gallery_order.numeric' => 'Vị Trí: Bạn cần phải nhập số.',
        ];
    }
}
