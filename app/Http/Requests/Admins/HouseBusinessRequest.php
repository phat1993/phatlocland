<?php

namespace App\Http\Requests\Admins;

use Illuminate\Foundation\Http\FormRequest;

class HouseBusinessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        $action = $this->route()->getActionMethod();
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($action == 'destroy'||$action == 'delete')
                    $rules = [];
                else
                    $rules = [
                        'house_business_name'  => 'required|max:100',
                        'house_business_description'  => 'max:100',
                        'web_title'  => 'max:20',
                        'web_keywords'  => 'max:200',
                        'web_description'  => 'max:145',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'house_business_name.required' => 'Tiêu Đề: Bạn không được để trống.',
            'house_business_name.max' => 'Tiêu Đề: Số kí tự cho phép nhập tối đa là 100.',

            'house_business_description.max' => 'Mô Tả: Số kí tự cho phép nhập tối đa là 100.',

            'web_title.max' => 'Tiêu Đề Website: Số kí tự cho phép nhập tối đa là 20.',
            'web_keywords.max' => 'Từ Khóa: Số kí tự cho phép nhập tối đa là 200.',
            'web_description.max' => 'Mô Tả Website: Số kí tự cho phép nhập tối đa là 145.',
        ];
    }
}
