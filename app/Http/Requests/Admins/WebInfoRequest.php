<?php

namespace App\Http\Requests\Admins;

use Illuminate\Foundation\Http\FormRequest;

class WebInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        $action = $this->route()->getActionMethod();
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($action == 'destroy'||$action == 'delete')
                    $rules = [];
                else
                    $rules = [
                        'web_info_title'  => 'required|max:100',
                        'web_info_phone'  => 'required|max:50',
                        'web_info_mail'  => 'required|max:50',
                        'web_info_address'  => 'required|max:200',
                        'web_info_work_time'  => 'required|max:1000',
                        'web_info_map_location'  => 'required|max:5000',
                        'web_info_special_note'  => 'required|max:500',

                        'web_title'  => 'required|max:20',
                        'web_keywords'  => 'required|max:200',
                        'web_description'  => 'required|max:145',
                        'web_canonical'  => 'required|max:200',

                        'web_info_zalo'  =>  'max:500',
                        'web_info_facebook'  =>  'max:500',
                        'web_info_youtube'  =>  'max:500',
                        'web_info_instagram'  => 'max:500',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'web_info_title.required' => 'Tiêu Đề: Bạn không được để trống.',
            'web_info_title.max' => 'Tiêu Đề: Số kí tự cho phép nhập tối đa là 100.',
            'web_info_phone.required' => 'Điện thoại: Bạn không được để trống.',
            'web_info_phone.max' => 'Điện Thoại: Số kí tự cho phép nhập tối đa là 50.',
            'web_info_mail.required' => 'Email: Bạn không được để trống.',
            'web_info_mail.max' => 'Email: Số kí tự cho phép nhập tối đa là 50.',
            'web_info_address.required' => 'Địa chỉ: Bạn không được để trống.',
            'web_info_address.max' => 'Địa chỉ: Số kí tự cho phép nhập tối đa là 200.',
            'web_info_work_time.required' => 'Giờ Làm Việc: Bạn không được để trống.',
            'web_info_work_time.max' => 'Giờ Làm Việc: Số kí tự cho phép nhập tối đa là 1000.',
            'web_info_map_location.required' => 'Vị Trí Map: Bạn không được để trống.',
            'web_info_map_location.max' => 'Vị Trí Map: Số kí tự cho phép nhập tối đa là 5000.',

            'web_info_special_note.required' => 'Thông Điệp: Bạn không được để trống.',
            'web_info_special_note.max' => 'Thông Điệp: Số kí tự cho phép nhập tối đa là 500.',

            'web_title.required' => 'Tiêu đề Website: Bạn không được để trống.',
            'web_title.max' => 'Tiêu đề Website: Số kí tự cho phép nhập tối đa là 20.',
            'web_keywords.required' => 'Từ khóa tìm kiếm: Bạn không được để trống.',
            'web_keywords.max' => 'Từ khóa tìm kiếm: Số kí tự cho phép nhập tối đa là 200.',
            'web_description.required' => 'Mô Tả Website: Bạn không được để trống.',
            'web_description.max' => 'Mô Tả Website: Số kí tự cho phép nhập tối đa là 145.',
            'web_canonical.required' => 'Đường dẫn quảng cáo: Bạn không được để trống.',
            'web_canonical.max' => 'Đường dẫn quảng cáo: Số kí tự cho phép nhập tối đa là 200.',

            'web_info_zalo.max' => 'Đường Dẫn Zalo: Số kí tự cho phép nhập tối đa là 300.',
            'web_info_facebook.max' => 'Đường Dẫn Facebook: Số kí tự cho phép nhập tối đa là 300.',
            'web_info_youtube.max' => 'Đường Dẫn Youtube: Số kí tự cho phép nhập tối đa là 300.',
            'web_info_instagram.max' => 'Đường Dẫn Instagram: Số kí tự cho phép nhập tối đa là 300.',

        ];
    }
}
