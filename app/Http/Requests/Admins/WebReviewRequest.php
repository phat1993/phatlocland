<?php

namespace App\Http\Requests\Admins;

use Illuminate\Foundation\Http\FormRequest;

class WebReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        $action = $this->route()->getActionMethod();
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($action == 'destroy'||$action == 'delete')
                    $rules = [];
                else
                    $rules = [
                        'web_review_name'  => 'required|max:50',
                        'web_review_address'  => 'required|max:100',
                        'web_review_detail'  => 'required|max:500',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'web_review_name.required' => 'Tiêu Đề: Bạn không được để trống.',
            'web_review_name.max' => 'Tiêu Đề: Số kí tự cho phép nhập tối đa là 50.',

            'web_review_address.required' => 'Mô tả: Bạn không được để trống.',
            'web_review_address.max' => 'Mô tả: Số kí tự cho phép nhập tối đa là 100.',

            'web_review_detail.required' => 'Chi Tiết: Bạn không được để trống.',
            'web_review_detail.max' => 'Mô tả: Số kí tự cho phép nhập tối đa là 500.',
        ];
    }
}
