<?php

namespace App\Http\Requests\Admins;

use Illuminate\Foundation\Http\FormRequest;

class MediaSupportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        $action = $this->route()->getActionMethod();
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($action == 'destroy'||$action == 'delete')
                    $rules = [];
                else
                    $rules = [
                        'media_support_name'  => 'required|max:50',
                        'media_support_description'  => 'max:200',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'media_support_name.required' => 'Tiêu Đề: Bạn không được để trống.',
            'media_support_name.max' => 'Tiêu Đề: Số kí tự cho phép nhập tối đa là 50.',

            'media_support_description.max' => 'Mô tả: Số kí tự cho phép nhập tối đa là 1200.',
        ];
    }
}
