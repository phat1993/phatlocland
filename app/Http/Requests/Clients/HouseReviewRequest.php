<?php

namespace App\Http\Requests\Clients;

use Illuminate\Foundation\Http\FormRequest;

class HouseReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        $action = $this->route()->getActionMethod();
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($action == 'destroy'||$action == 'delete')
                    $rules = [];
                else
                    $rules = [
                        'rating_name'  => 'required|max:100',
                        'rating_mail'  => 'required|max:50',
                        'rating_comment'  => 'required|max:500',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'rating_name.required' => 'Họ Tên: Bạn không được để trống.',
            'rating_name.max' => 'Họ Tên: Số kí tự cho phép nhập tối đa là 50.',

            'rating_mail.required' => 'Email: Bạn không được để trống.',
            'rating_mail.max' => 'Email: Số kí tự cho phép nhập tối đa là 100.',

            'rating_comment.required' => 'Nội Dung: Bạn không được để trống.',
            'rating_comment.max' => 'Nội Dung: Số kí tự cho phép nhập tối đa là 500.',
        ];
    }
}
