<?php

namespace App\Http\Requests\Clients;

use Illuminate\Foundation\Http\FormRequest;

class FeedbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = '';
        $action = $this->route()->getActionMethod();
        switch ($this->method()) {
            case 'GET':
                $rules = [];
                break;
            case 'POST':
                if ($action == 'destroy'||$action == 'delete')
                    $rules = [];
                else
                    $rules = [
                        'fname'  => 'required|max:50',
                        'email'  => 'required|email|max:100',
                        'comment'  => 'required|max:200',
                    ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'fname.required' => 'Họ Tên: Bạn không được để trống.',
            'fname.max' => 'Họ Tên: Số kí tự cho phép nhập tối đa là 50.',

            'email.required' => 'Email: Bạn không được để trống.',
            'email.email' => 'Email: Bạn cần nhập chính xác địa chỉ email.',
            'email.max' => 'Email: Số kí tự cho phép nhập tối đa là 100.',

            'comment.required' => 'Nội Dung: Bạn không được để trống.',
            'comment.max' => 'Nội Dung: Số kí tự cho phép nhập tối đa là 200.',
        ];
    }
}
