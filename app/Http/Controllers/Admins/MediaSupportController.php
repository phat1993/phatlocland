<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\MediaSupportRequest;
use App\Models\Entities\MediaSupport;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use stdClass;

class MediaSupportController extends Controller
{
    const PER_PAGE = 10;

    public function ckfinder(MediaSupportRequest $request)
    {

        // dd(sys_get_temp_dir());
        return view('admins.pages.media.ckfinder');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(MediaSupportRequest $request)
    {
        try {
            $data = $request->all();
            $param = array();
            $perPage = self::PER_PAGE;
            $currentPage = LengthAwarePaginator::resolveCurrentPage();

            if (isset($data[MediaSupport::MEDIA_SUPPORT_NAME])) {
                $param[] =  [MediaSupport::MEDIA_SUPPORT_NAME, 'like', '%' . $data[MediaSupport::MEDIA_SUPPORT_NAME] . '%'];
            }
            if (isset($data[MediaSupport::MEDIA_SUPPORT_SIZE])) {
                $param[] =  [MediaSupport::MEDIA_SUPPORT_SIZE,  $data[MediaSupport::MEDIA_SUPPORT_SIZE]];
            }
            if (isset($data[MediaSupport::MEDIA_SUPPORT_EXTENSION])) {
                $param[] =  [MediaSupport::MEDIA_SUPPORT_EXTENSION,  $data[MediaSupport::MEDIA_SUPPORT_EXTENSION]];
            }
            if (isset($data[MediaSupport::MEDIA_SUPPORT_DESCRIPTION])) {
                $param[] =  [MediaSupport::MEDIA_SUPPORT_DESCRIPTION, 'like', '%' . $data[MediaSupport::MEDIA_SUPPORT_DESCRIPTION] . '%'];
            }

            $mediaSupports  = DB::table('re_media_supports')->where($param)
                ->select(
                    're_media_supports.media_support_id',
                    're_media_supports.media_support_name',
                    're_media_supports.media_support_url',
                    're_media_supports.media_support_size',
                    're_media_supports.media_support_extension',
                    're_media_supports.media_support_description',
                )
                ->orderByDesc('updated_at')->paginate($perPage);
            return view('admins.pages.media.index', compact('mediaSupports', 'currentPage', 'perPage'));
        } catch (\Throwable $th) {

            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(MediaSupportRequest $request)
    {
        try {
            $mediaSupport = new MediaSupport();
            return view('admins.pages.media.create', compact('mediaSupport'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MediaSupportRequest $request)
    {
        if ($request->isMethod('post')) {
            $validImg = [
                'file_input.*'  => 'required|max:5120', //5MB=5*1024
            ];
            $customMessages = [
                'file_input.*.required' => 'Bạn cần chọn file.',
                'file_input.*.max' => 'Kích thước giới hạn của file là 5MB',
            ];
            $request->validate($validImg, $customMessages);
            $paths = collect();
            try {
                //lay cac file truyen vao luu vao folder
                $files = $request->file_input;
                foreach ($files as $item) {
                    $file = new stdClass();
                    $file->path = $item->store(
                        'images/' .  Carbon::now()->format('Ymd'),
                        'public'
                    );
                    $file->size = $item->getSize();
                    $file->ext = $item->extension();
                    $paths->add($file);
                }

                $data = $request->all();
                if ($paths->count() > 0) {
                    DB::transaction(function () use ($data, $paths) {
                        $mediaSupports = collect();
                        foreach ($paths as $item) {
                            $mediaSupport = new MediaSupport();
                            $mediaSupport->{MediaSupport::MEDIA_SUPPORT_NAME} = $data[MediaSupport::MEDIA_SUPPORT_NAME];
                            $mediaSupport->{MediaSupport::MEDIA_SUPPORT_URL} = $item->path;
                            $mediaSupport->{MediaSupport::MEDIA_SUPPORT_SIZE} = $item->size;
                            $mediaSupport->{MediaSupport::MEDIA_SUPPORT_EXTENSION} = $item->ext;
                            $mediaSupport->{MediaSupport::MEDIA_SUPPORT_DESCRIPTION} = $data[MediaSupport::MEDIA_SUPPORT_DESCRIPTION];
                            $mediaSupport->{MediaSupport::ADMIN_UPDATED} = Auth::user()->admin_id;
                            $mediaSupports->add($mediaSupport);
                        }
                        MediaSupport::insert($mediaSupports->toArray());
                    });
                    DB::commit();
                    return redirect()->back()->with('success', 'Tạo mới đối tượng thành công !');
                }
                //Ko tao moi duoc thi xoa file da them
                if ($paths->count() > 0) {
                    foreach ($paths as $item) {
                        if ($item->path != '' && Storage::disk('public')->exists($item->path))
                            Storage::disk('public')->delete($item->path);
                    }
                }
                return redirect()->back()->withInput($request->all())->withErrors('Lỗi server! Tạo mới thất bại');
            } catch (\Throwable $th) {
                DB::rollBack();
                if ($paths->count() > 0) {
                    foreach ($paths as $item) {
                        if ($item->path != '' && Storage::disk('public')->exists($item->path))
                            Storage::disk('public')->delete($item->path);
                    }
                }
                $message = $th->getMessage();
                return view('admins.pages.auth.error_403', compact('message'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MediaSupport  $mediaSupport
     * @return \Illuminate\Http\Response
     */
    public function show(MediaSupportRequest $request)
    {
        try {
            $data = $request->all();
            //Lay doi tuong cua house business lien ket voi admins va updatemode de lay noi dung va hinh anh
            $mediaSupport = DB::table('re_media_supports')->select(
                're_media_supports.*',
                //Lay noi dung hinh anh cua admin
                're_administrators.admin_full_name'
            )
                ->leftJoin('re_administrators', 're_administrators.admin_id', '=', 're_media_supports.admin_updated')
                ->where('media_support_id', $data['media_support_id'])->first();

            if (isset($mediaSupport))
                return view('admins.pages.media.show', compact('mediaSupport'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MediaSupport  $mediaSupport
     * @return \Illuminate\Http\Response
     */
    public function edit(MediaSupportRequest $request)
    {
        try {
            $data = $request->all();
            $mediaSupport = DB::table('re_media_supports')->where('media_support_id', $data['media_support_id'])->first();
            if (isset($mediaSupport))
                return view('admins.pages.media.edit', compact('mediaSupport'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MediaSupport  $mediaSupport
     * @return \Illuminate\Http\Response
     */
    public function update(MediaSupportRequest $request)
    {
        $data = $request->all();
        $param = array();
        if ($request->hasFile('file_input')) {
            $validImg = [
                'file_input.*'  => 'required|max:5120', //5MB=5*1024
            ];
            $customMessages = [
                'file_input.*.required' => 'Bạn cần chọn file.',
                'file_input.*.max' => 'Kích thước giới hạn của file là 5MB',
            ];
            $request->validate($validImg, $customMessages);

            $param[MediaSupport::MEDIA_SUPPORT_URL] =  $request->file('file_input')->store(
                'images/' .  Carbon::now()->format('Ymd'),
                'public'
            );
            $param[MediaSupport::MEDIA_SUPPORT_SIZE] = $request->file('file_input')->getSize();
            $param[MediaSupport::MEDIA_SUPPORT_EXTENSION] = $request->file('file_input')->extension();
        }
        try {
            $param[MediaSupport::MEDIA_SUPPORT_NAME] = $data[MediaSupport::MEDIA_SUPPORT_NAME];
            $param[MediaSupport::MEDIA_SUPPORT_DESCRIPTION] = $data[MediaSupport::MEDIA_SUPPORT_DESCRIPTION];
            $param[MediaSupport::ADMIN_UPDATED] = Auth::user()->admin_id;
            $param[MediaSupport::UPDATED_AT] = Carbon::now()->format('Y-m-d H:i:s');

            $mediaSupport = DB::table('re_media_supports')->where('media_support_id', $data['media_support_id']);
            if ($mediaSupport->update($param)) {

                //Neu cap nhat thanh cong ma co hinh anh moi thi xoa hinh anh cu
                if (
                    isset($param[MediaSupport::MEDIA_SUPPORT_URL]) &&
                    $param[MediaSupport::MEDIA_SUPPORT_URL] != '' &&
                    Storage::disk('public')->exists($data[MediaSupport::MEDIA_SUPPORT_URL])
                )
                    Storage::disk('public')->delete($data[MediaSupport::MEDIA_SUPPORT_URL]);

                return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
            } else {
                //Neu luu anh ma ko luu dc doi tuong thi xoa
                if (
                    isset($param[MediaSupport::MEDIA_SUPPORT_URL]) &&
                    $param[MediaSupport::MEDIA_SUPPORT_URL] != '' &&
                    Storage::disk('public')->exists($param[MediaSupport::MEDIA_SUPPORT_URL])
                )
                    Storage::disk('public')->delete($param[MediaSupport::MEDIA_SUPPORT_URL]);

                return redirect()->back()->withErrors(['Cập Nhật Thất Bại!']);
            }
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $mediaSupportCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(MediaSupportRequest $request)
    {
        try {
            $data = $request->all();
            //Kiem tra doi tuong co trong database khong
            $img = DB::table('re_media_supports')->where('media_support_id', $data['media_support_id'])->first();
            if (!isset($img))
                return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);


            $deleted = DB::table('re_media_supports')->where('media_support_id', $data['media_support_id'])->delete();
            if ($deleted) {
                //Xoa anh
                if (Storage::disk('public')->exists($img->{MediaSupport::MEDIA_SUPPORT_URL}))
                    Storage::disk('public')->delete($img->{MediaSupport::MEDIA_SUPPORT_URL});

                return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
            } else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
}
