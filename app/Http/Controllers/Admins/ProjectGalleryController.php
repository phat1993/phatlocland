<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\ProjectGalleryRequest;
use App\Models\Entities\ProjectGallery;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProjectGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ProjectGalleryRequest $request)
    {
        try {
            $project = DB::table('re_projects')->where('project_id', $request->id)->select('project_id', 'project_name')->first();
            $proGalleries = DB::table('re_project_galleries')->where('project_id',  $request->id)->orderBy('updated_at', 'desc')->get();
            return view('admins.pages.projectgallery.index', compact('project', 'proGalleries'));
        } catch (\Throwable $th) {

            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ProjectGalleryRequest $request)
    {
        try {
            $project = DB::table('re_projects')->where('project_id', $request->id)->select('project_id', 'project_name')->first();
            $projectGallery = new ProjectGallery();
            $projectGallery->project_gallery_banner = 0;
            $projectGallery->project_gallery_order = 1;
            $projectGallery->active_status = 1;
            $projectGallery->project_id = $project->project_id;
            return view('admins.pages.projectgallery.create', compact('projectGallery', 'project'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectGalleryRequest $request)
    {

        if ($request->isMethod('post')) {
            $data = $request->all();
            $path = '';

            $validImg = ['file_input'  => 'image|required']; //|dimensions:width=800,height=500

            $customMessages = [
                'file_input.required' => 'Hình Ảnh: Bạn không được để trống.',
                'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
            ];

            //neu hinh anh la banner thi phai dat chuan 1400x700
            if ($data[ProjectGallery::PROJECT_GALLERY_BANNER] == 1) {
                $validImg = ['file_input'  => 'image|required|dimensions:width=1400,height=700'];
                $customMessages = [
                    'file_input.required' => 'Hình Ảnh: Bạn không được để trống.',
                    'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
                    'file_input.dimensions' => 'Hình Ảnh: Bạn cần chọn hình ảnh 1400x700.',
                ];
            }
            //neu hinh anh la anh dai dien thi phai dat chuan 700x500
            else if ($data[ProjectGallery::PROJECT_GALLERY_BANNER] == 2) {
                $validImg = ['file_input'  => 'image|required|dimensions:width=750,height=500'];
                $customMessages = [
                    'file_input.required' => 'Hình Ảnh: Bạn không được để trống.',
                    'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
                    'file_input.dimensions' => 'Hình Ảnh: Bạn cần chọn hình ảnh 750x500.',
                ];
            }
            $request->validate($validImg, $customMessages);

            try {
                $path = $request->file('file_input')->store(
                    'images/projects/' . $data[ProjectGallery::PROJECT_ID],
                    'public'
                );


                $data = $request->all();

                $projectGallery = new ProjectGallery();
                $projectGallery->{ProjectGallery::PROJECT_GALLERY_BANNER} = $data[ProjectGallery::PROJECT_GALLERY_BANNER];
                $projectGallery->{ProjectGallery::PROJECT_GALLERY_NAME} = $data[ProjectGallery::PROJECT_GALLERY_NAME];
                $projectGallery->{ProjectGallery::PROJECT_GALLERY_LINK} =  $path;
                $projectGallery->{ProjectGallery::PROJECT_GALLERY_ORDER} = $data[ProjectGallery::PROJECT_GALLERY_ORDER];
                $projectGallery->{ProjectGallery::PROJECT_ID} = $data[ProjectGallery::PROJECT_ID];
                $projectGallery->{ProjectGallery::ACTIVE_STATUS} = isset($data[ProjectGallery::ACTIVE_STATUS]) ? 1 : 0;

                if ($projectGallery->save())
                    return redirect()->back()->with('success', 'Tạo mới đối tượng thành công !');
                else {
                    //Neu them ko duoc doi tuong ma da luu hinh anh roi thi xoa hinh anh
                    if ($path != '' && Storage::disk('public')->exists($path))
                        Storage::disk('public')->delete($path);

                    return redirect()->back()->withInput($request->all())->withErrors('Lỗi server! Tạo mới thất bại');
                }
                return redirect()->back()->withInput($request->all())->withErrors('Lỗi server! Tạo mới thất bại');
            } catch (\Throwable $th) {
                $message = $th->getMessage();
                return view('admins.pages.auth.error_403', compact('message'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProjectGallery  $projectGallery
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectGalleryRequest $request)
    {
        try {
            $data = $request->all();
            //Lay doi tuong cua house business lien ket voi admins va updatemode de lay noi dung va hinh anh
            $projectGallery = DB::table('re_project_galleries')->select(
                're_project_galleries.*',
                //Lay noi dung hinh anh cua admin
                're_administrators.admin_full_name'
            )
                ->leftJoin('re_administrators', 're_administrators.admin_id', '=', 're_project_galleries.admin_updated')
                ->where('project_gallery_id', $data['project_gallery_id'])->first();

            if (isset($projectGallery))
                return view('admins.pages.projectgallery.show', compact('projectGallery'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProjectGallery  $projectGallery
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectGalleryRequest $request)
    {
        try {
            $data = $request->all();

            $project = DB::table('re_projects')->where('project_id', $request->id)->select('project_id', 'project_name')->first();
            $projectGallery = DB::table('re_project_galleries')->where('project_gallery_id', $data['project_gallery_id'])->first();

            if (isset($projectGallery))
                return view('admins.pages.projectgallery.edit', compact('projectGallery', 'project'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProjectGallery  $projectGallery
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectGalleryRequest $request)
    {

        $data = $request->all();

        $param = array();
        $path = '';
        if ($request->hasFile('file_input')) {

            $validImg = ['file_input'  => 'image'];

            $customMessages = [
                'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
            ];

            //neu hinh anh la banner thi phai dat chuan 1400x700
            if ($data[ProjectGallery::PROJECT_GALLERY_BANNER] == 1) {
                $validImg = ['file_input'  => 'image|dimensions:width=1400,height=700'];
                $customMessages = [
                    'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
                    'file_input.dimensions' => 'Hình Ảnh: Bạn cần chọn hình ảnh 1400x700.',
                ];
            }
            //neu hinh anh la anh dai dien thi phai dat chuan 700x500
            else if ($data[ProjectGallery::PROJECT_GALLERY_BANNER] == 2) {
                $validImg = ['file_input'  => 'image|dimensions:width=750,height=500'];
                $customMessages = [
                    'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
                    'file_input.dimensions' => 'Hình Ảnh: Bạn cần chọn hình ảnh 750x500.',
                ];
            }

            $request->validate($validImg, $customMessages);

            $path = $request->file('file_input')->store(
                'images/projects/' . $data[ProjectGallery::PROJECT_ID],
                'public'
            );

            //Luu Thanh cong hinh anh thi set cho bien
            if ($path != '')
                $param[ProjectGallery::PROJECT_GALLERY_LINK] = $path;
        }
        try {

            $param[ProjectGallery::PROJECT_GALLERY_NAME] = $data[ProjectGallery::PROJECT_GALLERY_NAME];
            $param[ProjectGallery::PROJECT_GALLERY_BANNER] =  $data[ProjectGallery::PROJECT_GALLERY_BANNER];
            $param[ProjectGallery::PROJECT_GALLERY_ORDER] =  $data[ProjectGallery::PROJECT_GALLERY_ORDER];

            $param[ProjectGallery::ACTIVE_STATUS] = isset($data[ProjectGallery::ACTIVE_STATUS]) ? 1 : 0;
            $param[ProjectGallery::UPDATED_AT] = Carbon::now()->format('Y-m-d H:i:s');

            $projectGallery = DB::table('re_project_galleries')->where('project_gallery_id', $data['project_gallery_id']);
            if ($projectGallery->update($param)) {

                //Neu cap nhat thanh cong ma co hinh anh moi thi xoa hinh anh cu
                if ($path != '' && Storage::disk('public')->exists($data[ProjectGallery::PROJECT_GALLERY_LINK]))
                    Storage::disk('public')->delete($data[ProjectGallery::PROJECT_GALLERY_LINK]);

                return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
            } else {
                //Neu luu anh ma ko luu dc doi tuong thi xoa
                if ($path != '' && Storage::disk('public')->exists($path))
                    Storage::disk('public')->delete($path);

                return redirect()->back()->withErrors(['Cập Nhật Thất Bại!']);
            }
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $projectGalleryCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectGalleryRequest $request)
    {
        try {
            $data = $request->all();
            //Kiem tra doi tuong co trong database khong
            $img = DB::table('re_project_galleries')->where('project_gallery_id', $data['project_gallery_id'])->first();
            if (!isset($img))
                return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);


            $deleted = DB::table('re_project_galleries')->where('project_gallery_id', $data['project_gallery_id'])->delete();
            if ($deleted) {
                //Xoa anh
                if (Storage::disk('public')->exists($img->{ProjectGallery::PROJECT_GALLERY_LINK}))
                    Storage::disk('public')->delete($img->{ProjectGallery::PROJECT_GALLERY_LINK});

                return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
            } else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
}
