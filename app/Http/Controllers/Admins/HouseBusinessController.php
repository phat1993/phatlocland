<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\HouseBusinessRequest;
use App\Models\Entities\HouseBusiness;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class HouseBusinessController extends Controller
{
    const PER_PAGE = 20;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HouseBusinessRequest $request)
    {
        try {
            $data = $request->all();
            $param = array();
            $perPage = self::PER_PAGE;
            $currentPage = LengthAwarePaginator::resolveCurrentPage();

            if (isset($data[HouseBusiness::HOUSE_BUSINESS_NAME])) {
                $param[] =  [HouseBusiness::HOUSE_BUSINESS_NAME, 'like', '%' . $data[HouseBusiness::HOUSE_BUSINESS_NAME] . '%'];
            }
            if (isset($data[HouseBusiness::HOUSE_BUSINESS_DESCRIPTION])) {
                $param[] =  [HouseBusiness::HOUSE_BUSINESS_DESCRIPTION, 'like', '%' . $data[HouseBusiness::HOUSE_BUSINESS_DESCRIPTION] . '%'];
            }
            if (isset($data[HouseBusiness::ACTIVE_STATUS])) {
                if ($data[HouseBusiness::ACTIVE_STATUS] == 1)
                    $param[] = [HouseBusiness::ACTIVE_STATUS, 1];
                elseif ($data[HouseBusiness::ACTIVE_STATUS] == 2)
                    $param[] = [HouseBusiness::ACTIVE_STATUS, 0];
            }

            $houseBusinesses = DB::table('re_house_businesses')
            ->select(
                're_house_businesses.house_business_id',
                're_house_businesses.house_business_name',
                're_house_businesses.house_business_description',
                're_house_businesses.active_status',
                're_house_businesses.web_title',
                're_house_businesses.web_keywords',
                're_house_businesses.web_description',
                're_house_businesses.web_canonical',
            )
            ->where($param)
            ->orderBy('updated_at', 'desc')->orderByDesc('updated_at')->paginate($perPage);
            return view('admins.pages.housebusiness.index', compact('houseBusinesses', 'currentPage', 'perPage'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(HouseBusinessRequest $request)
    {
        try {
            $houseBusiness = new HouseBusiness();
            $houseBusiness->active_status = 1;
            return view('admins.pages.housebusiness.create', compact('houseBusiness'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HouseBusinessRequest $request)
    {
        if ($request->isMethod('post')) {
            try {

                $data = $request->all();

                //Kiem tra duong dan moi co trung voi cac doi tuong da co
                $slug = Str::slug($data[HouseBusiness::HOUSE_BUSINESS_NAME]);
                if (DB::table('re_house_businesses')->where(HouseBusiness::WEB_CANONICAL, $slug)->count() > 0)
                    return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!', 'Tiêu đề danh mục đã có trong cơ sở dữ liệu!']);

                $houseBusiness = new HouseBusiness();
                $houseBusiness->{HouseBusiness::HOUSE_BUSINESS_NAME} = $data[HouseBusiness::HOUSE_BUSINESS_NAME];
                $houseBusiness->{HouseBusiness::HOUSE_BUSINESS_DESCRIPTION} = $data[HouseBusiness::HOUSE_BUSINESS_DESCRIPTION];

                $houseBusiness->{HouseBusiness::WEB_KEYWORDS} = $data[HouseBusiness::WEB_KEYWORDS];
                $houseBusiness->{HouseBusiness::WEB_DESCRIPTION} = $data[HouseBusiness::WEB_DESCRIPTION];
                $houseBusiness->{HouseBusiness::WEB_TITLE} = $data[HouseBusiness::WEB_TITLE];
                $houseBusiness->{HouseBusiness::WEB_CANONICAL} = $slug;
                $houseBusiness->{HouseBusiness::ACTIVE_STATUS} = isset($data[HouseBusiness::ACTIVE_STATUS]) ? 1 : 0;
                $houseBusiness->{HouseBusiness::ADMIN_UPDATED} = Auth::user()->admin_id;

                if ($houseBusiness->save()) {
                    return redirect()->route('admins.houbusiness.create')->with('success', 'Tạo mới đối tượng thành công !');
                } else
                    return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!']);
            } catch (\Throwable $th) {
                $message = $th->getMessage();
                return view('admins.pages.auth.error_403', compact('message'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HouseBusiness  $houseBusiness
     * @return \Illuminate\Http\Response
     */
    public function show(HouseBusinessRequest $request)
    {
        try {
            $data = $request->all();

            $houseBusiness = DB::table('re_house_businesses')->select(
                're_house_businesses.*',
                //Lay noi dung hinh anh cua admin
                're_administrators.admin_full_name'
            )
                ->leftJoin('re_administrators', 're_administrators.admin_id', '=', 're_house_businesses.admin_updated')
                ->where('house_business_id', $data['house_business_id'])->first();

            if (isset($houseBusiness))
                return view('admins.pages.housebusiness.show', compact('houseBusiness'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HouseBusiness  $houseBusiness
     * @return \Illuminate\Http\Response
     */
    public function edit(HouseBusinessRequest $request)
    {
        try {
            $data = $request->all();

            $houseBusiness = DB::table('re_house_businesses')->where('house_business_id', $data['house_business_id'])->first();

            if (isset($houseBusiness))
                return view('admins.pages.housebusiness.edit', compact('houseBusiness'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HouseBusiness  $houseBusiness
     * @return \Illuminate\Http\Response
     */
    public function update(HouseBusinessRequest $request)
    {
        try {

            $data = $request->all();

            //Kiem tra duong dan moi co trung voi cac doi tuong da co
            $slug = Str::slug($data[HouseBusiness::HOUSE_BUSINESS_NAME]);
            if (DB::table('re_house_businesses')->where([
                ['house_business_id', '<>', $data['house_business_id']],
                [HouseBusiness::WEB_CANONICAL, $slug]
            ])->count() > 0)
                return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!', 'Tiêu đề danh mục đã có trong cơ sở dữ liệu!']);

            $param = array();

            $param[HouseBusiness::HOUSE_BUSINESS_NAME] = $data[HouseBusiness::HOUSE_BUSINESS_NAME];
            $param[HouseBusiness::HOUSE_BUSINESS_DESCRIPTION] = $data[HouseBusiness::HOUSE_BUSINESS_DESCRIPTION];

            $param[HouseBusiness::WEB_KEYWORDS] = $data[HouseBusiness::WEB_KEYWORDS];
            $param[HouseBusiness::WEB_DESCRIPTION] = $data[HouseBusiness::WEB_DESCRIPTION];
            $param[HouseBusiness::WEB_TITLE] = $data[HouseBusiness::WEB_TITLE];
            $param[HouseBusiness::WEB_CANONICAL] =  $slug;

            $param[HouseBusiness::ACTIVE_STATUS] = isset($data[HouseBusiness::ACTIVE_STATUS]) ? 1 : 0;
            $param[HouseBusiness::ADMIN_UPDATED] = Auth::user()->admin_id;
            $param[HouseBusiness::UPDATED_AT] = Carbon::now()->format('Y-m-d H:i:s');


            $houseBusiness = DB::table('re_house_businesses')->where('house_business_id', $data['house_business_id']);
            if ($houseBusiness->update($param)) {
                return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
            } else
                return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $houseBusinessCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(HouseBusinessRequest $request)
    {
        try {

            $data = $request->all();

            //Kiem tra doi tuong co trong database khong
            $houseBusiness = HouseBusiness::withTrashed()->where('house_business_id', $data['house_business_id'])->firstOrFail();
            if (!isset($houseBusiness))
                return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);

            $deleted = HouseBusiness::where('house_business_id', $data['house_business_id'])->delete();
            if ($deleted)
                return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }


    /**
     * recovered the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $houseBusinessCategories
     * @return \Illuminate\Http\Response
     */
    public function recovered(HouseBusinessRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = HouseBusiness::withTrashed()->where('house_business_id', $data['house_business_id'])->restore();
            if ($deleted)
                return redirect()->back()->with('success', 'Phục hồi tượng thành công!');
            else
                return redirect()->back()->withErrors('Phục hồi tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $houseBusinessCategories
     * @return \Illuminate\Http\Response
     */
    public function delete(HouseBusinessRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = DB::table('re_house_businesses')->where('house_business_id', $data['house_business_id'])->delete();
            if ($deleted)
                return redirect()->route('admins.houbusiness.index')->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
}
