<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\WebHireRequest;
use App\Models\Entities\WebHire;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class WebHireController extends Controller
{
    const PER_PAGE = 10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(WebHireRequest $request)
    {
        try {
            $data = $request->all();
            $param = array();
            $perPage = self::PER_PAGE;
            $currentPage = LengthAwarePaginator::resolveCurrentPage();

            if (isset($data[WebHire::WEB_HIRE_NAME])) {
                $param[] =  [WebHire::WEB_HIRE_NAME, 'like', '%' . $data[WebHire::WEB_HIRE_NAME] . '%'];
            }
            if (isset($data[WebHire::WEB_HIRE_DESCRIPTION])) {
                $param[] =  [WebHire::WEB_HIRE_DESCRIPTION, 'like', '%' . $data[WebHire::WEB_HIRE_DESCRIPTION] . '%'];
            }
            if (isset($data[WebHire::ACTIVE_STATUS])) {
                if ($data[WebHire::ACTIVE_STATUS] == 1)
                    $param[] = [WebHire::ACTIVE_STATUS, 1];
                elseif ($data[WebHire::ACTIVE_STATUS] == 2)
                    $param[] = [WebHire::ACTIVE_STATUS, 0];
            }


            $webHires  = DB::table('re_web_hires')->where($param)->orderByDesc('updated_at')->paginate($perPage);
            return view('admins.pages.webhire.index', compact('webHires', 'currentPage', 'perPage'));
        } catch (\Throwable $th) {

            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(WebHireRequest $request)
    {
        try {
            $webHire = new WebHire();
            $webHire->active_status = 1;
            return view('admins.pages.webhire.create', compact('webHire'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WebHireRequest $request)
    {

        if ($request->isMethod('post')) {

            $data = $request->all();
            $path = '';
            //Kiem tra duong dan moi co trung voi cac doi tuong da co
            $slug = Str::slug($data[WebHire::WEB_HIRE_NAME]);
            if (DB::table('re_web_hires')->where(WebHire::WEB_CANONICAL, $slug)->count() > 0)
                return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!', 'Tiêu đề dự án đã có trong cơ sở dữ liệu!']);


            $validImg = ['file_input'  => 'image|required|dimensions:width=1024,height=450'];
            $customMessages = [
                'file_input.required' => 'Hình Ảnh: Bạn không được để trống.',
                'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
                'file_input.dimensions' => 'Hình Ảnh: Bạn cần chọn hình ảnh 1024x450.',
            ];
            $request->validate($validImg, $customMessages);

            $path = $request->file('file_input')->store(
                'images/webhires',
                'public'
            );
            try {

                $webHire = new WebHire();
                $webHire->{WebHire::WEB_HIRE_NAME} = $data[WebHire::WEB_HIRE_NAME];
                $webHire->{WebHire::WEB_HIRE_IMAGE} =  $path;

                $webHire->{WebHire::WEB_HIRE_DESCRIPTION} = $data[WebHire::WEB_HIRE_DESCRIPTION];
                $webHire->{WebHire::WEB_HIRE_DETAIL} = $data[WebHire::WEB_HIRE_DETAIL];
                //Web Info
                $webHire->{WebHire::WEB_KEYWORDS} = $data[WebHire::WEB_KEYWORDS];
                $webHire->{WebHire::WEB_DESCRIPTION} = $data[WebHire::WEB_DESCRIPTION];
                $webHire->{WebHire::WEB_TITLE} = $data[WebHire::WEB_TITLE];
                $webHire->{WebHire::WEB_CANONICAL} = $slug;

                $webHire->{WebHire::ACTIVE_STATUS} = isset($data[WebHire::ACTIVE_STATUS]) ? 1 : 0;
                $webHire->{WebHire::ADMIN_UPDATED} = Auth::user()->admin_id;

                if ($webHire->save())
                    return redirect()->back()->with('success', 'Tạo mới đối tượng thành công !');
                else {
                    //Neu them ko duoc doi tuong ma da luu hinh anh roi thi xoa hinh anh
                    if ($path != '' && Storage::disk('public')->exists($path))
                        Storage::disk('public')->delete($path);

                    redirect()->back()->withInput($request->all())->withErrors('Lỗi server! Tạo mới thất bại');
                }
            } catch (\Throwable $th) {
                $message = $th->getMessage();
                return view('admins.pages.auth.error_403', compact('message'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WebHire  $webHire
     * @return \Illuminate\Http\Response
     */
    public function show(WebHireRequest $request)
    {
        try {
            $data = $request->all();
            //Lay doi tuong cua webHire business lien ket voi admins va updatemode de lay noi dung va hinh anh
            $webHire = DB::table('re_web_hires')->where('web_hire_id', $request->web_hire_id)->select(
                're_web_hires.*',
                //Lay noi dung hinh anh cua admin
                're_administrators.admin_full_name'
            )
                ->leftJoin('re_administrators', 're_administrators.admin_id', '=', 're_web_hires.admin_updated')
                ->where('web_hire_id', $data['web_hire_id'])->first();

            if (isset($webHire))
                return view('admins.pages.webhire.show', compact('webHire'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WebHire  $webHire
     * @return \Illuminate\Http\Response
     */
    public function edit(WebHireRequest $request)
    {
        try {
            $data = $request->all();
            $webHire = DB::table('re_web_hires')->where('web_hire_id', $data['web_hire_id'])->first();
            if (isset($webHire))
                return view('admins.pages.webhire.edit', compact('webHire'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebHire  $webHire
     * @return \Illuminate\Http\Response
     */
    public function update(WebHireRequest $request)
    {

        $data = $request->all();

        $param = array();
        //Kiem tra duong dan moi co trung voi cac doi tuong da co
        $slug = Str::slug($data[WebHire::WEB_HIRE_NAME]);
        if (DB::table('re_web_hires')->where([
            ['web_hire_id', '<>', $data['web_hire_id']],
            [WebHire::WEB_CANONICAL, $slug]
        ])->count() > 0)
            return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!', 'Tiêu đề danh mục đã có trong cơ sở dữ liệu!']);

        $path = '';
        if ($request->hasFile('file_input')) {
            $validImg = ['file_input'  => 'image|dimensions:width=1024,height=450'];
            $customMessages = [
                'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
                'file_input.dimensions' => 'Hình Ảnh: Bạn cần chọn hình ảnh 1024x450.',
            ];
            $request->validate($validImg, $customMessages);

            $path = $request->file('file_input')->store(
                'images/webhires',
                'public'
            );
            //Luu Thanh cong hinh anh thi set cho bien
            if ($path != '')
                $param[WebHire::WEB_HIRE_IMAGE] = $path;
        }
        try {

            $param[WebHire::WEB_HIRE_NAME] = $data[WebHire::WEB_HIRE_NAME];
            $param[WebHire::WEB_HIRE_DESCRIPTION] = $data[WebHire::WEB_HIRE_DESCRIPTION];
            $param[WebHire::WEB_HIRE_DETAIL] = $data[WebHire::WEB_HIRE_DETAIL];

            //Web Info
            $param[WebHire::WEB_KEYWORDS] = $data[WebHire::WEB_KEYWORDS];
            $param[WebHire::WEB_DESCRIPTION] = $data[WebHire::WEB_DESCRIPTION];
            $param[WebHire::WEB_TITLE] = $data[WebHire::WEB_TITLE];
            $param[WebHire::WEB_CANONICAL] =  $slug;

            $param[WebHire::ACTIVE_STATUS] = isset($data[WebHire::ACTIVE_STATUS]) ? 1 : 0;
            $param[WebHire::ADMIN_UPDATED] = Auth::user()->admin_id;
            $param[WebHire::UPDATED_AT] = Carbon::now()->format('Y-m-d H:i:s');

            $webHire = DB::table('re_web_hires')->where('web_hire_id', $data['web_hire_id']);
            if ($webHire->update($param)) {

                //Neu cap nhat thanh cong ma co hinh anh moi thi xoa hinh anh cu
                if ($path != '' && Storage::disk('public')->exists($data[WebHire::WEB_HIRE_IMAGE]))
                    Storage::disk('public')->delete($data[WebHire::WEB_HIRE_IMAGE]);

                return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
            } else {
                //Neu luu anh ma ko luu dc doi tuong thi xoa
                if ($path != '' && Storage::disk('public')->exists($path))
                    Storage::disk('public')->delete($path);

                return redirect()->back()->withErrors(['Cập Nhật Thất Bại!']);
            }
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Productjects  $housejects
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebHireRequest $request)
    {
        try {

            $data = $request->all();

            //Kiem tra doi tuong co trong database khong
            $house = WebHire::withTrashed()->where('web_hire_id', $data['web_hire_id'])->firstOrFail();
            if (!isset($house))
                return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);

            $deleted = WebHire::where('web_hire_id', $data['web_hire_id'])->delete();
            if ($deleted)
                return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }


    /**
     * recovered the specified resource from storage.
     *
     * @param  \App\Models\Productjects  $housejects
     * @return \Illuminate\Http\Response
     */
    public function recovered(WebHireRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = WebHire::withTrashed()->where('web_hire_id', $data['web_hire_id'])->restore();
            if ($deleted)
                return redirect()->back()->with('success', 'Phục hồi tượng thành công!');
            else
                return redirect()->back()->withErrors('Phục hồi tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\Productjects  $housejects
     * @return \Illuminate\Http\Response
     */
    public function delete(WebHireRequest $request)
    {
        try {
            $data = $request->all();
            //Kiem tra doi tuong co trong database khong
            $img = DB::table('re_web_hires')->where('web_hire_id', $data['web_hire_id'])->first();
            if (!isset($img))
                return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);


            $deleted = DB::table('re_web_hires')->where('web_hire_id', $data['web_hire_id'])->delete();
            if ($deleted) {
                //Xoa anh
                if (Storage::disk('public')->exists($img->{WebHire::WEB_HIRE_IMAGE}))
                    Storage::disk('public')->delete($img->{WebHire::WEB_HIRE_IMAGE});

                return redirect()->route('admins.webhire.index')->with('success', 'Xóa đối tượng thành công!');
            } else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
}
