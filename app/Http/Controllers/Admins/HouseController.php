<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\HouseRequest;
use App\Models\Entities\House;
use App\Models\Entities\HouseGallery;
use App\Models\ViewModels\Admins\HouseAmenities;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class HouseController extends Controller
{
    const PER_PAGE = 20;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HouseRequest $request)
    {
        try {
            $data = $request->all();
            $param = array();
            $perPage = self::PER_PAGE;
            $currentPage = LengthAwarePaginator::resolveCurrentPage();


            if (isset($data[House::HOUSE_CATEGORY_ID])) {
                $param[] =  ['re_houses.house_category_id', $data[House::HOUSE_CATEGORY_ID]];
            }
            if (isset($data[House::HOUSE_BUSINESS_ID])) {
                $param[] =  ['re_houses.house_business_id', $data[House::HOUSE_BUSINESS_ID]];
            }
            if (isset($data[House::PROJECT_ID])) {
                $param[] =  ['re_houses.project_id', $data[House::PROJECT_ID]];
            }

            if (isset($data[House::HOUSE_PRICE])) {
                $price = str_replace(',', '', $data[House::HOUSE_PRICE]);
                $param[] = [House::HOUSE_PRICE, '=', $price];
            }
            if (isset($data[House::HOUSE_PRICE_SUB])) {
                $discount = str_replace(',', '', $data[House::HOUSE_PRICE_SUB]);
                $param[] = [House::HOUSE_PRICE_SUB, '=', $discount];
            }
            if (isset($data[House::HOUSE_OWN])) {
                $param[] =  [House::HOUSE_OWN, 'like', '%' . $data[House::HOUSE_OWN] . '%'];
            }
            if (isset($data[House::HOUSE_ACREAGE])) {
                $param[] =  [House::HOUSE_ACREAGE, 'like', '%' . $data[House::HOUSE_ACREAGE] . '%'];
            }
            if (isset($data[House::HOUSE_LENGTH])) {
                $param[] =  [House::HOUSE_LENGTH, 'like', '%' . $data[House::HOUSE_LENGTH] . '%'];
            }
            if (isset($data[House::HOUSE_WIDTH])) {
                $param[] =  [House::HOUSE_WIDTH, 'like', '%' . $data[House::HOUSE_WIDTH] . '%'];
            }
            if (isset($data[House::HOUSE_WIDTH])) {
                $param[] =  [House::HOUSE_WIDTH, 'like', '%' . $data[House::HOUSE_WIDTH] . '%'];
            }
            if (isset($data[House::HOUSE_DIRECTION])) {
                $param[] =  [House::HOUSE_DIRECTION, 'like', '%' . $data[House::HOUSE_DIRECTION] . '%'];
            }
            if (isset($data[House::HOUSE_ROOMS])) {
                $param[] =  [House::HOUSE_ROOMS, 'like', '%' . $data[House::HOUSE_ROOMS] . '%'];
            }
            if (isset($data[House::HOUSE_BATH_ROOMS])) {
                $param[] =  [House::HOUSE_BATH_ROOMS, 'like', '%' . $data[House::HOUSE_BATH_ROOMS] . '%'];
            }
            if (isset($data[House::HOUSE_GARAGES])) {
                $param[] =  [House::HOUSE_GARAGES, 'like', '%' . $data[House::HOUSE_GARAGES] . '%'];
            }
            if (isset($data[House::HOUSE_POSTAL_CODE])) {
                $param[] =  [House::HOUSE_POSTAL_CODE, 'like', '%' . $data[House::HOUSE_POSTAL_CODE] . '%'];
            }
            if (isset($data[House::HOUSE_PROVINCIAL])) {
                $param[] =  [House::HOUSE_PROVINCIAL, 'like', '%' . $data[House::HOUSE_PROVINCIAL] . '%'];
            }
            if (isset($data[House::HOUSE_DISTRICT])) {
                $param[] =  [House::HOUSE_DISTRICT, 'like', '%' . $data[House::HOUSE_DISTRICT] . '%'];
            }
            if (isset($data[House::HOUSE_COMMUNE])) {
                $param[] =  [House::HOUSE_COMMUNE, 'like', '%' . $data[House::HOUSE_COMMUNE] . '%'];
            }
            if (isset($data[House::HOUSE_ADDRESS])) {
                $param[] =  [House::HOUSE_ADDRESS, 'like', '%' . $data[House::HOUSE_ADDRESS] . '%'];
            }

            if (isset($data[House::ACTIVE_STATUS])) {
                if ($data[House::ACTIVE_STATUS] == 1)
                    $param[] = ['re_houses.active_status', 1];
                elseif ($data[House::ACTIVE_STATUS] == 2)
                    $param[] = ['re_houses.active_status', 0];
            }


            $houses = DB::table('re_houses')->select(
                're_houses.house_id',
                're_houses.house_name',
                're_houses.house_price',
                're_houses.house_price_sub',
                're_houses.house_own',
                're_houses.house_build_time',
                're_houses.house_acreage',
                're_houses.house_length',
                're_houses.house_width',
                're_houses.house_direction',
                're_houses.house_rooms',
                're_houses.house_bath_rooms',
                're_houses.house_garages',
                're_houses.house_postal_code',
                're_houses.house_provincial',
                're_houses.house_district',
                're_houses.house_commune',
                're_houses.house_address',
                're_houses.active_status',

                //Lay noi dung cua danh muc
                're_house_categories.house_category_name',
                're_house_categories.web_canonical AS category_web_canonical',
                //Lay noi dung cua kinh doanh
                're_house_businesses.house_business_name',
                're_house_businesses.web_canonical AS business_web_canonical',
                //Lay noi dung cua du an
                're_projects.project_name',
            )
                ->where($param)
                ->leftJoin('re_house_categories', 're_house_categories.house_category_id', '=', 're_houses.house_category_id')
                ->leftJoin('re_house_businesses', 're_house_businesses.house_business_id', '=', 're_houses.house_business_id')
                ->leftJoin('re_projects', 're_projects.project_id', '=', 're_houses.project_id')
                ->orderByDesc('re_houses.updated_at')->paginate($perPage);


            $businesses = DB::table('re_house_businesses')->where('active_status', 1)->select('house_business_id', 'house_business_name')->get();
            $projects = DB::table('re_projects')->where('active_status', 1)->select('project_id', 'project_name')->get();
            $categories = DB::table('re_house_categories')->where('active_status', 1)->select('house_category_id', 'house_category_name')->get();


            return view('admins.pages.house.index', compact('houses', 'businesses', 'projects', 'categories', 'currentPage', 'perPage'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(HouseRequest $request)
    {
        try {
            $house = new House();

            $house->{House::HOUSE_RATING} = '5';
            $house->{House::HOUSE_ACREAGE} = '25m²';
            // $house->{House::HOUSE_LENGTH} = '5';
            // $house->{House::HOUSE_WIDTH} = '5';
            // $house->{House::HOUSE_DIRECTION} = 'Tây';
            // $house->{House::HOUSE_ROOMS} = '3';
            // $house->{House::HOUSE_BATH_ROOMS} = '1';
            // $house->{House::HOUSE_GARAGES} = '1';
            $house->{House::ACTIVE_STATUS} =  1;

            $businesses = DB::table('re_house_businesses')->where('active_status', 1)->select('house_business_id', 'house_business_name')->get();
            $projects = DB::table('re_projects')->where('active_status', 1)->select('project_id', 'project_name')->get();
            $categories = DB::table('re_house_categories')->where('active_status', 1)->select('house_category_id', 'house_category_name')->get();
            $amenities = DB::table('re_amenities')->where('active_status', 1)->select('amenity_id', 'amenity_name')->get();

            return view('admins.pages.house.create', compact('house', 'businesses', 'projects', 'categories', 'amenities'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HouseRequest $request)
    {
        if ($request->isMethod('post')) {
            try {

                $data = $request->all();

                //Kiem tra duong dan moi co trung voi cac doi tuong da co
                $slug = Str::slug($data[House::HOUSE_NAME]);
                if (DB::table('re_houses')->where(House::WEB_CANONICAL, $slug)->count() > 0)
                    return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!', 'Tiêu đề bất động sản đã có trong cơ sở dữ liệu!']);

                //Xu ly tien nghi
                $selected = $request->house_amenities;
                $amenities = collect();
                if (isset($selected)) {

                    foreach ($selected as $value) {
                        $aty = DB::table('re_amenities')->where('amenity_id', $value)->first();
                        $amenity = new HouseAmenities();
                        $amenity->{HouseAmenities::AMENITY_ID} = $aty->amenity_id;
                        $amenity->{HouseAmenities::AMENITY_NAME} = $aty->amenity_name;
                        $amenity->{HouseAmenities::AMENITY_ICON} = $aty->amenity_icon;

                        $amenities->push($amenity);
                    }
                }


                $house = new House();
                $house->{House::HOUSE_CODE} = Str::upper(Str::random(15));
                //Reference Info
                $house->{House::HOUSE_BUSINESS_ID} = $data[House::HOUSE_BUSINESS_ID];
                $house->{House::HOUSE_CATEGORY_ID} = $data[House::HOUSE_CATEGORY_ID];
                if (isset($data[House::PROJECT_ID]))
                    $house->{House::PROJECT_ID} = $data[House::PROJECT_ID];
                //Object Info
                $house->{House::HOUSE_NAME} = $data[House::HOUSE_NAME];
                $house->{House::HOUSE_DESCRIPTION} = $data[House::HOUSE_DESCRIPTION];
                $house->{House::HOUSE_DETAIL} = $data[House::HOUSE_DETAIL];
                $house->{House::HOUSE_AMENITIES} = '' . $amenities->toJson() . '';

                if (isset($data[House::HOUSE_PRICE]))
                    $house->{House::HOUSE_PRICE} = str_replace(',', '', $data[House::HOUSE_PRICE]);

                if ($data[House::HOUSE_PRICE_SUB] != '')
                    $house->{House::HOUSE_PRICE_SUB} = str_replace(',', '', $data[House::HOUSE_PRICE_SUB]);


                //Trang thai sale-status
                $house->{House::HOUSE_RATING} = $data[House::HOUSE_RATING];
                if (isset($data[House::HOUSE_STATUS]))
                    $house->{House::HOUSE_STATUS} = $data[House::HOUSE_STATUS];
                if (isset($data[House::HOUSE_STATUS_COLOR]))
                    $house->{House::HOUSE_STATUS_COLOR} = $data[House::HOUSE_STATUS_COLOR];

                //Address
                $house->{House::HOUSE_POSTAL_CODE} = $data[House::HOUSE_POSTAL_CODE];
                $house->{House::HOUSE_PROVINCIAL} = $data[House::HOUSE_PROVINCIAL];
                $house->{House::HOUSE_DISTRICT} = $data[House::HOUSE_DISTRICT];
                $house->{House::HOUSE_COMMUNE} = $data[House::HOUSE_COMMUNE];
                $house->{House::HOUSE_ADDRESS} = $data[House::HOUSE_ADDRESS];
                $house->{House::HOUSE_MAP_LOCATION} = $data[House::HOUSE_MAP_LOCATION];
                //Info
                $house->{House::HOUSE_OWN} = $data[House::HOUSE_OWN];
                if (isset($data[House::HOUSE_BUILD_TIME]))
                    $house->{House::HOUSE_BUILD_TIME} = Carbon::createFromFormat('m/d/Y', $data[House::HOUSE_BUILD_TIME])->format('Y-m-d');

                $house->{House::HOUSE_ACREAGE} = $data[House::HOUSE_ACREAGE];
                $house->{House::HOUSE_LENGTH} = $data[House::HOUSE_LENGTH];
                $house->{House::HOUSE_WIDTH} = $data[House::HOUSE_WIDTH];
                $house->{House::HOUSE_DIRECTION} = $data[House::HOUSE_DIRECTION];
                $house->{House::HOUSE_ROOMS} = $data[House::HOUSE_ROOMS];
                $house->{House::HOUSE_BATH_ROOMS} = $data[House::HOUSE_BATH_ROOMS];
                $house->{House::HOUSE_GARAGES} = $data[House::HOUSE_GARAGES];

                //Web Info
                $house->{House::WEB_KEYWORDS} = $data[House::WEB_KEYWORDS];
                $house->{House::WEB_DESCRIPTION} = $data[House::WEB_DESCRIPTION];
                $house->{House::WEB_TITLE} = $data[House::WEB_TITLE];
                $house->{House::WEB_CANONICAL} = $slug;
                //General Info
                $house->{House::ACTIVE_STATUS} = isset($data[House::ACTIVE_STATUS]) ? 1 : 0;
                $house->{House::ADMIN_UPDATED} = Auth::user()->admin_id;

                if ($house->save()) {
                    //Luu ma code generate length 15char
                    $code = Str::upper(substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 5)) . Str::padLeft($house->house_id, 10, '0');
                    $updated = DB::table('re_houses')->where('house_id', $house->house_id);
                    $param = array();
                    $param['house_code'] = $code;
                    $updated->update($param);

                    return redirect()->route('admins.house.create')->with('success', 'Tạo mới đối tượng thành công !');
                } else
                    return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!']);
            } catch (\Throwable $th) {
                $message = $th->getMessage();
                return view('admins.pages.auth.error_403', compact('message'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\House  $house
     * @return \Illuminate\Http\Response
     */
    public function show(HouseRequest $request)
    {
        try {
            $data = $request->all();
            //Lay doi tuong cua house business lien ket voi admins va updatemode de lay noi dung va hinh anh
            $house = DB::table('re_houses')->select(
                're_houses.*',
                //Lay noi dung hinh anh cua admin
                're_administrators.admin_full_name',

                //Lay noi dung cua danh muc
                're_house_categories.house_category_name',
                're_house_categories.web_canonical AS category_web_canonical',
                //Lay noi dung cua kinh doanh
                're_house_businesses.house_business_name',
                're_house_businesses.web_canonical AS business_web_canonical',
                //Lay noi dung cua du an
                're_projects.project_name',
            )
                ->leftJoin('re_house_categories', 're_house_categories.house_category_id', '=', 're_houses.house_category_id')
                ->leftJoin('re_house_businesses', 're_house_businesses.house_business_id', '=', 're_houses.house_business_id')
                ->leftJoin('re_projects', 're_projects.project_id', '=', 're_houses.project_id')
                ->leftJoin('re_administrators', 're_administrators.admin_id', '=', 're_houses.admin_updated')
                ->where('house_id', $data['house_id'])->first();

            if (isset($house)) {
                $amenities = json_decode($house->house_amenities);
                $galleries = DB::table('re_house_galleries')->where([['house_id', $data['house_id']], ['active_status', 1]])->orderBy('house_gallery_order')->get();
                $floorplans = DB::table('re_floor_plans')->where([['house_id', $data['house_id']], ['active_status', 1]])->orderByDesc('updated_at')->get();
                return view('admins.pages.house.show', compact('house', 'amenities', 'galleries', 'floorplans'));
            } else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\House  $house
     * @return \Illuminate\Http\Response
     */
    public function edit(HouseRequest $request)
    {
        try {
            $data = $request->all();
            //Lay doi tuong cua house business lien ket voi admins va updatemode de lay noi dung va hinh anh
            $house = DB::table('re_houses')->where('house_id', $data['house_id'])->first();

            if (isset($house)) {
                $djson = json_decode($house->house_amenities);
                $businesses = DB::table('re_house_businesses')->where('active_status', 1)->select('house_business_id', 'house_business_name')->get();
                $projects = DB::table('re_projects')->where('active_status', 1)->select('project_id', 'project_name')->get();
                $categories = DB::table('re_house_categories')->where('active_status', 1)->select('house_category_id', 'house_category_name')->get();
                $amenities = DB::table('re_amenities')->where('active_status', 1)->select('amenity_id', 'amenity_name', 'amenity_detail AS checked')->get();
                //Kiem tra du lieu tien nghi duoc check
                foreach ($amenities as $item) {
                    $selected = '';
                    foreach ($djson as $ay) {
                        if ($item->amenity_id == $ay->amenity_id) {
                            $selected = 'selected';
                            break;
                        }
                    }
                    $item->checked = $selected;
                }
                if (isset($house->house_build_time)) {
                    $house->house_build_time = date('d/m/Y', strtotime($house->house_build_time));
                }
                return view('admins.pages.house.edit', compact('house', 'businesses', 'projects', 'categories', 'amenities'));
            } else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\House  $house
     * @return \Illuminate\Http\Response
     */
    public function update(HouseRequest $request)
    {
        try {

            $data = $request->all();

            //Kiem tra duong dan moi co trung voi cac doi tuong da co
            $slug = Str::slug($data[House::HOUSE_NAME]);
            if (DB::table('re_houses')->where([
                ['house_id', '<>', $data['house_id']],
                [House::WEB_CANONICAL, $slug]
            ])->count() > 0)
                return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!', 'Tiêu đề danh mục đã có trong cơ sở dữ liệu!']);

            //Xu ly tien nghi
            $selected = $request->house_amenities;
            $amenities = collect();
            if (isset($selected)) {

                foreach ($selected as $value) {
                    $aty = DB::table('re_amenities')->where('amenity_id', $value)->first();
                    $amenity = new HouseAmenities();
                    $amenity->{HouseAmenities::AMENITY_ID} = $aty->amenity_id;
                    $amenity->{HouseAmenities::AMENITY_NAME} = $aty->amenity_name;
                    $amenity->{HouseAmenities::AMENITY_ICON} = $aty->amenity_icon;

                    $amenities->push($amenity);
                }
            }
            $param = array();
            //Reference Info
            $param[House::HOUSE_BUSINESS_ID] = $data[House::HOUSE_BUSINESS_ID];
            $param[House::HOUSE_CATEGORY_ID] = $data[House::HOUSE_CATEGORY_ID];
            $param[House::PROJECT_ID] = isset($data[House::PROJECT_ID])?$data[House::PROJECT_ID]:NULL;

            //Object Info
            $param[House::HOUSE_NAME] = $data[House::HOUSE_NAME];
            $param[House::HOUSE_DESCRIPTION] = $data[House::HOUSE_DESCRIPTION];
            $param[House::HOUSE_DETAIL] = $data[House::HOUSE_DETAIL];
            $param[House::HOUSE_AMENITIES] = '' . $amenities->toJson() . '';


            $param[House::HOUSE_PRICE] = isset($data[House::HOUSE_PRICE])?str_replace(',', '', $data[House::HOUSE_PRICE]):NULL;
            $param[House::HOUSE_PRICE_SUB] = isset($data[House::HOUSE_PRICE_SUB])?str_replace(',', '', $data[House::HOUSE_PRICE_SUB]):NULL;

            //Trang thai sale-status
            $param[House::HOUSE_RATING] = $data[House::HOUSE_RATING];
            $param[House::HOUSE_STATUS] = isset($data[House::HOUSE_STATUS])?$data[House::HOUSE_STATUS]:NULL;
            $param[House::HOUSE_STATUS_COLOR] = isset($data[House::HOUSE_STATUS_COLOR])?$data[House::HOUSE_STATUS_COLOR]:NULL;

            //Address
            $param[House::HOUSE_POSTAL_CODE] = $data[House::HOUSE_POSTAL_CODE];
            $param[House::HOUSE_PROVINCIAL] = $data[House::HOUSE_PROVINCIAL];
            $param[House::HOUSE_DISTRICT] = $data[House::HOUSE_DISTRICT];
            $param[House::HOUSE_COMMUNE] = $data[House::HOUSE_COMMUNE];
            $param[House::HOUSE_ADDRESS] = $data[House::HOUSE_ADDRESS];
            $param[House::HOUSE_MAP_LOCATION] = $data[House::HOUSE_MAP_LOCATION];
            //Info
            $param[House::HOUSE_OWN] = $data[House::HOUSE_OWN];
            $param[House::HOUSE_BUILD_TIME] = isset($data[House::HOUSE_BUILD_TIME])?Carbon::createFromFormat('m/d/Y', $data[House::HOUSE_BUILD_TIME])->format('Y-m-d'):NULL;

            $param[House::HOUSE_ACREAGE] = $data[House::HOUSE_ACREAGE];
            $param[House::HOUSE_LENGTH] = $data[House::HOUSE_LENGTH];
            $param[House::HOUSE_WIDTH] = $data[House::HOUSE_WIDTH];
            $param[House::HOUSE_DIRECTION] = $data[House::HOUSE_DIRECTION];
            $param[House::HOUSE_ROOMS] = $data[House::HOUSE_ROOMS];
            $param[House::HOUSE_BATH_ROOMS] = $data[House::HOUSE_BATH_ROOMS];
            $param[House::HOUSE_GARAGES] = $data[House::HOUSE_GARAGES];
            //Web Info
            $param[House::WEB_KEYWORDS] = $data[House::WEB_KEYWORDS];
            $param[House::WEB_DESCRIPTION] = $data[House::WEB_DESCRIPTION];
            $param[House::WEB_TITLE] = $data[House::WEB_TITLE];
            $param[House::WEB_CANONICAL] =  $slug;

            //General Info
            $param[House::ACTIVE_STATUS] = isset($data[House::ACTIVE_STATUS]) ? 1 : 0;
            $param[House::ADMIN_UPDATED] = Auth::user()->admin_id;
            $param[House::UPDATED_AT] = Carbon::now()->format('Y-m-d H:i:s');

            $house = DB::table('re_houses')->where('house_id', $data['house_id']);
            if ($house->update($param)) {
                return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
            } else
                return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Housejects  $housejects
     * @return \Illuminate\Http\Response
     */
    public function destroy(HouseRequest $request)
    {
        try {

            $data = $request->all();

            //Kiem tra doi tuong co trong database khong
            $house = House::withTrashed()->where('house_id', $data['house_id'])->firstOrFail();
            if (!isset($house))
                return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);

            $deleted = House::where('house_id', $data['house_id'])->delete();
            if ($deleted)
                return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }


    /**
     * recovered the specified resource from storage.
     *
     * @param  \App\Models\Housejects  $housejects
     * @return \Illuminate\Http\Response
     */
    public function recovered(HouseRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = House::withTrashed()->where('house_id', $data['house_id'])->restore();
            if ($deleted)
                return redirect()->back()->with('success', 'Phục hồi tượng thành công!');
            else
                return redirect()->back()->withErrors('Phục hồi tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\Housejects  $housejects
     * @return \Illuminate\Http\Response
     */
    public function delete(HouseRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = DB::table('re_houses')->where('house_id', $data['house_id'])->delete();
            if ($deleted) {
                //xoa hinh anh
                $path = 'images/houses/' . $data[HouseGallery::HOUSE_ID];
                if (Storage::disk('public')->exists($path))
                    Storage::disk('public')->deleteDirectory($path);

                DB::table('re_house_galleries')->where('house_id', $data['house_id'])->delete();
                // Xoa ban ve

                $pathP = 'images/floorplans/' . $data[HouseGallery::HOUSE_ID];
                if (Storage::disk('public')->exists($pathP))
                    Storage::disk('public')->deleteDirectory($pathP);


                $deleted = DB::table('re_floor_plans')->where('house_id', $data['house_id'])->delete();

                return redirect()->route('admins.house.index')->with('success', 'Xóa đối tượng thành công!');
            } else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
}
