<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\HouseCategoryRequest;
use App\Models\Entities\HouseCategory;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class HouseCategoryController extends Controller
{
    const PER_PAGE = 20;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HouseCategoryRequest $request)
    {
        try {
            $data = $request->all();
            $param = array();
            $perPage = self::PER_PAGE;
            $currentPage = LengthAwarePaginator::resolveCurrentPage();

            if (isset($data[HouseCategory::HOUSE_CATEGORY_NAME])) {
                $param[] =  [HouseCategory::HOUSE_CATEGORY_NAME, 'like', '%' . $data[HouseCategory::HOUSE_CATEGORY_NAME] . '%'];
            }
            if (isset($data[HouseCategory::HOUSE_CATEGORY_DESCRIPTION])) {
                $param[] =  [HouseCategory::HOUSE_CATEGORY_DESCRIPTION, 'like', '%' . $data[HouseCategory::HOUSE_CATEGORY_DESCRIPTION] . '%'];
            }
            if (isset($data[HouseCategory::ACTIVE_STATUS])) {
                if ($data[HouseCategory::ACTIVE_STATUS] == 1)
                    $param[] = [HouseCategory::ACTIVE_STATUS, 1];
                elseif ($data[HouseCategory::ACTIVE_STATUS] == 2)
                    $param[] = [HouseCategory::ACTIVE_STATUS, 0];
            }

            $houseCategories = DB::table('re_house_categories')
            ->select(
                're_house_categories.house_category_id',
                're_house_categories.house_category_name',
                're_house_categories.house_category_description',
                're_house_categories.active_status',
                're_house_categories.web_title',
                're_house_categories.web_keywords',
                're_house_categories.web_description',
                're_house_categories.web_canonical',
            )
            ->where($param)
            ->orderBy('updated_at', 'desc')->orderByDesc('updated_at')->paginate($perPage);
            return view('admins.pages.housecategory.index', compact('houseCategories', 'currentPage', 'perPage'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(HouseCategoryRequest $request)
    {
        try {
            $houseCategory = new HouseCategory();
            $houseCategory->active_status = 1;
            return view('admins.pages.housecategory.create', compact('houseCategory'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HouseCategoryRequest $request)
    {
        if ($request->isMethod('post')) {
            try {

                $data = $request->all();

                //Kiem tra duong dan moi co trung voi cac doi tuong da co
                $slug = Str::slug($data[HouseCategory::HOUSE_CATEGORY_NAME]);
                if (DB::table('re_house_categories')->where(HouseCategory::WEB_CANONICAL, $slug)->count() > 0)
                    return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!', 'Tiêu đề danh mục đã có trong cơ sở dữ liệu!']);

                $houseCategory = new HouseCategory();
                $houseCategory->{HouseCategory::HOUSE_CATEGORY_NAME} = $data[HouseCategory::HOUSE_CATEGORY_NAME];
                $houseCategory->{HouseCategory::HOUSE_CATEGORY_DESCRIPTION} = $data[HouseCategory::HOUSE_CATEGORY_DESCRIPTION];

                $houseCategory->{HouseCategory::WEB_KEYWORDS} = $data[HouseCategory::WEB_KEYWORDS];
                $houseCategory->{HouseCategory::WEB_DESCRIPTION} = $data[HouseCategory::WEB_DESCRIPTION];
                $houseCategory->{HouseCategory::WEB_TITLE} = $data[HouseCategory::WEB_TITLE];
                $houseCategory->{HouseCategory::WEB_CANONICAL} = $slug;
                $houseCategory->{HouseCategory::ACTIVE_STATUS} = isset($data[HouseCategory::ACTIVE_STATUS]) ? 1 : 0;
                $houseCategory->{HouseCategory::ADMIN_UPDATED} = Auth::user()->admin_id;

                if ($houseCategory->save()) {
                    return redirect()->route('admins.houcategory.create')->with('success', 'Tạo mới đối tượng thành công !');
                } else
                    return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!']);
            } catch (\Throwable $th) {
                $message = $th->getMessage();
                return view('admins.pages.auth.error_403', compact('message'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HouseCategory  $houseCategory
     * @return \Illuminate\Http\Response
     */
    public function show(HouseCategoryRequest $request)
    {
        try {
            $data = $request->all();

            $houseCategory = DB::table('re_house_categories')->select(
                're_house_categories.*',
                //Lay noi dung hinh anh cua admin
                're_administrators.admin_full_name'
            )
                ->leftJoin('re_administrators', 're_administrators.admin_id', '=', 're_house_categories.admin_updated')
                ->where('house_category_id', $data['house_category_id'])->first();

            if (isset($houseCategory))
                return view('admins.pages.housecategory.show', compact('houseCategory'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HouseCategory  $houseCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(HouseCategoryRequest $request)
    {
        try {
            $data = $request->all();

            $houseCategory = DB::table('re_house_categories')->where('house_category_id', $data['house_category_id'])->first();

            if (isset($houseCategory))
                return view('admins.pages.housecategory.edit', compact('houseCategory'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HouseCategory  $houseCategory
     * @return \Illuminate\Http\Response
     */
    public function update(HouseCategoryRequest $request)
    {
        try {

            $data = $request->all();

            //Kiem tra duong dan moi co trung voi cac doi tuong da co
            $slug = Str::slug($data[HouseCategory::HOUSE_CATEGORY_NAME]);
            if (DB::table('re_house_categories')->where([
                ['house_category_id', '<>', $data['house_category_id']],
                [HouseCategory::WEB_CANONICAL, $slug]
            ])->count() > 0)
                return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!', 'Tiêu đề danh mục đã có trong cơ sở dữ liệu!']);

            $param = array();

            $param[HouseCategory::HOUSE_CATEGORY_NAME] = $data[HouseCategory::HOUSE_CATEGORY_NAME];
            $param[HouseCategory::HOUSE_CATEGORY_DESCRIPTION] = $data[HouseCategory::HOUSE_CATEGORY_DESCRIPTION];

            $param[HouseCategory::WEB_KEYWORDS] = $data[HouseCategory::WEB_KEYWORDS];
            $param[HouseCategory::WEB_DESCRIPTION] = $data[HouseCategory::WEB_DESCRIPTION];
            $param[HouseCategory::WEB_TITLE] = $data[HouseCategory::WEB_TITLE];
            $param[HouseCategory::WEB_CANONICAL] =  $slug;

            $param[HouseCategory::ACTIVE_STATUS] = isset($data[HouseCategory::ACTIVE_STATUS]) ? 1 : 0;
            $param[HouseCategory::ADMIN_UPDATED] = Auth::user()->admin_id;
            $param[HouseCategory::UPDATED_AT] = Carbon::now()->format('Y-m-d H:i:s');


            $houseCategory = DB::table('re_house_categories')->where('house_category_id', $data['house_category_id']);
            if ($houseCategory->update($param)) {
                return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
            } else
                return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $houseCategoryCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(HouseCategoryRequest $request)
    {
        try {

            $data = $request->all();

            //Kiem tra doi tuong co trong database khong
            $houseCategory = HouseCategory::withTrashed()->where('house_category_id', $data['house_category_id'])->firstOrFail();
            if (!isset($houseCategory))
                return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);

            $deleted = HouseCategory::where('house_category_id', $data['house_category_id'])->delete();
            if ($deleted)
                return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }


    /**
     * recovered the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $houseCategoryCategories
     * @return \Illuminate\Http\Response
     */
    public function recovered(HouseCategoryRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = HouseCategory::withTrashed()->where('house_category_id', $data['house_category_id'])->restore();
            if ($deleted)
                return redirect()->back()->with('success', 'Phục hồi tượng thành công!');
            else
                return redirect()->back()->withErrors('Phục hồi tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $houseCategoryCategories
     * @return \Illuminate\Http\Response
     */
    public function delete(HouseCategoryRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = DB::table('re_house_categories')->where('house_category_id', $data['house_category_id'])->delete();
            if ($deleted)
                return redirect()->route('admins.houcategory.index')->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
}
