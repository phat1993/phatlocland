<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\ProjectCategoryRequest;
use App\Models\Entities\ProjectCategory;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProjectCategoryController extends Controller
{
    const PER_PAGE = 20;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ProjectCategoryRequest $request)
    {
        try {
            $data = $request->all();
            $param = array();
            $perPage = self::PER_PAGE;
            $currentPage = LengthAwarePaginator::resolveCurrentPage();

            if (isset($data[ProjectCategory::PROJECT_CATEGORY_NAME])) {
                $param[] =  [ProjectCategory::PROJECT_CATEGORY_NAME, 'like', '%' . $data[ProjectCategory::PROJECT_CATEGORY_NAME] . '%'];
            }
            if (isset($data[ProjectCategory::PROJECT_CATEGORY_DESCRIPTION])) {
                $param[] =  [ProjectCategory::PROJECT_CATEGORY_DESCRIPTION, 'like', '%' . $data[ProjectCategory::PROJECT_CATEGORY_DESCRIPTION] . '%'];
            }
            if (isset($data[ProjectCategory::ACTIVE_STATUS])) {
                if ($data[ProjectCategory::ACTIVE_STATUS] == 1)
                    $param[] = [ProjectCategory::ACTIVE_STATUS, 1];
                elseif ($data[ProjectCategory::ACTIVE_STATUS] == 2)
                    $param[] = [ProjectCategory::ACTIVE_STATUS, 0];
            }
            $proCategories = DB::table('re_project_categories')
                ->select(
                    're_project_categories.project_category_id',
                    're_project_categories.project_category_name',
                    're_project_categories.project_category_description',
                    're_project_categories.active_status',
                    're_project_categories.web_title',
                    're_project_categories.web_keywords',
                    're_project_categories.web_description',
                    're_project_categories.web_canonical',
                )
                ->where($param)->orderByDesc('updated_at')->paginate($perPage);
            return view('admins.pages.projectcategory.index', compact('proCategories', 'currentPage', 'perPage'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ProjectCategoryRequest $request)
    {
        try {
            $projectCategory = new ProjectCategory();
            $projectCategory->active_status = 1;
            return view('admins.pages.projectcategory.create', compact('projectCategory'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectCategoryRequest $request)
    {
        if ($request->isMethod('post')) {
            try {

                $data = $request->all();

                //Kiem tra duong dan moi co trung voi cac doi tuong da co
                $slug = Str::slug($data[ProjectCategory::PROJECT_CATEGORY_NAME]);
                if (DB::table('re_project_categories')->where(ProjectCategory::WEB_CANONICAL, $slug)->count() > 0)
                    return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!', 'Tiêu đề danh mục đã có trong cơ sở dữ liệu!']);

                $projectCategory = new ProjectCategory();
                $projectCategory->{ProjectCategory::PROJECT_CATEGORY_NAME} = $data[ProjectCategory::PROJECT_CATEGORY_NAME];
                $projectCategory->{ProjectCategory::PROJECT_CATEGORY_DESCRIPTION} = $data[ProjectCategory::PROJECT_CATEGORY_DESCRIPTION];

                $projectCategory->{ProjectCategory::WEB_KEYWORDS} = $data[ProjectCategory::WEB_KEYWORDS];
                $projectCategory->{ProjectCategory::WEB_DESCRIPTION} = $data[ProjectCategory::WEB_DESCRIPTION];
                $projectCategory->{ProjectCategory::WEB_TITLE} = $data[ProjectCategory::WEB_TITLE];
                $projectCategory->{ProjectCategory::WEB_CANONICAL} = $slug;
                $projectCategory->{ProjectCategory::ACTIVE_STATUS} = isset($data[ProjectCategory::ACTIVE_STATUS]) ? 1 : 0;
                $projectCategory->{ProjectCategory::ADMIN_UPDATED} = Auth::user()->admin_id;

                if ($projectCategory->save()) {
                    return redirect()->route('admins.procategory.create')->with('success', 'Tạo mới đối tượng thành công !');
                } else
                    return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!']);
            } catch (\Throwable $th) {
                $message = $th->getMessage();
                return view('admins.pages.auth.error_403', compact('message'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProjectCategory  $projectCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectCategoryRequest $request)
    {
        try {
            $data = $request->all();

            $projectCategory = DB::table('re_project_categories')->select(
                're_project_categories.*',
                //Lay noi dung hinh anh cua admin
                're_administrators.admin_full_name'
            )
                ->leftJoin('re_administrators', 're_administrators.admin_id', '=', 're_project_categories.admin_updated')
                ->where('project_category_id', $data['project_category_id'])->first();

            if (isset($projectCategory))
                return view('admins.pages.projectcategory.show', compact('projectCategory'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProjectCategory  $projectCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectCategoryRequest $request)
    {
        try {
            $data = $request->all();

            $projectCategory = DB::table('re_project_categories')->where('project_category_id', $data['project_category_id'])->first();

            if (isset($projectCategory))
                return view('admins.pages.projectcategory.edit', compact('projectCategory'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProjectCategory  $projectCategory
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectCategoryRequest $request)
    {
        try {

            $data = $request->all();

            //Kiem tra duong dan moi co trung voi cac doi tuong da co
            $slug = Str::slug($data[ProjectCategory::PROJECT_CATEGORY_NAME]);
            if (DB::table('re_project_categories')->where([
                ['project_category_id', '<>', $data['project_category_id']],
                [ProjectCategory::WEB_CANONICAL, $slug]
            ])->count() > 0)
                return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!', 'Tiêu đề danh mục đã có trong cơ sở dữ liệu!']);

            $param = array();

            $param[ProjectCategory::PROJECT_CATEGORY_NAME] = $data[ProjectCategory::PROJECT_CATEGORY_NAME];
            $param[ProjectCategory::PROJECT_CATEGORY_DESCRIPTION] = $data[ProjectCategory::PROJECT_CATEGORY_DESCRIPTION];

            $param[ProjectCategory::WEB_KEYWORDS] = $data[ProjectCategory::WEB_KEYWORDS];
            $param[ProjectCategory::WEB_DESCRIPTION] = $data[ProjectCategory::WEB_DESCRIPTION];
            $param[ProjectCategory::WEB_TITLE] = $data[ProjectCategory::WEB_TITLE];
            $param[ProjectCategory::WEB_CANONICAL] =  $slug;

            $param[ProjectCategory::ACTIVE_STATUS] = isset($data[ProjectCategory::ACTIVE_STATUS]) ? 1 : 0;
            $param[ProjectCategory::ADMIN_UPDATED] = Auth::user()->admin_id;
            $param[ProjectCategory::UPDATED_AT] = Carbon::now()->format('Y-m-d H:i:s');


            $projectCategory = DB::table('re_project_categories')->where('project_category_id', $data['project_category_id']);
            if ($projectCategory->update($param)) {
                return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
            } else
                return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $projectCategoryCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectCategoryRequest $request)
    {
        try {

            $data = $request->all();

            //Kiem tra doi tuong co trong database khong
            $projectCategory = ProjectCategory::withTrashed()->where('project_category_id', $data['project_category_id'])->firstOrFail();
            if (!isset($projectCategory))
                return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);

            $deleted = ProjectCategory::where('project_category_id', $data['project_category_id'])->delete();
            if ($deleted)
                return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }


    /**
     * recovered the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $projectCategoryCategories
     * @return \Illuminate\Http\Response
     */
    public function recovered(ProjectCategoryRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = ProjectCategory::withTrashed()->where('project_category_id', $data['project_category_id'])->restore();
            if ($deleted)
                return redirect()->back()->with('success', 'Phục hồi tượng thành công!');
            else
                return redirect()->back()->withErrors('Phục hồi tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $projectCategoryCategories
     * @return \Illuminate\Http\Response
     */
    public function delete(ProjectCategoryRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = DB::table('re_project_categories')->where('project_category_id', $data['project_category_id'])->delete();
            if ($deleted)
                return redirect()->route('admins.procategory.index')->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
}
