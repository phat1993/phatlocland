<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminAuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function login(Request $request)
    {
        if ($request->isMethod('POST')) {
            $validate = ['user_name'  => 'required|max:30', 'password'  => 'required|max:16'];
            $customMessages = [
                'user_name.required' => 'Tài Khoản: Bạn không được để trống.',
                'user_name.max' => 'Tài Khoản: Số kí tự cho phép nhập tối đa là 30.',
                'password.required' => 'Mật Khẩu: Bạn không được để trống.',
                'password.max' => 'Mật Khẩu: Số kí tự cho phép nhập tối đa là 16.',
            ];
            $request->validate($validate, $customMessages);
            try {
                $data = $request->all();
                $user_name = trim($data['user_name']);
                $user = array(
                    'user_name' => $user_name,
                    'password' => $data['password'],
                    'active_status' => 1
                );

                if (Auth::guard('admin')->attempt($user)) {
                    DB::table('re_administrators')->where('admin_id', Auth::guard('admin')->user()->admin_id)->update(['last_login_time' => Carbon::now()]);
                    return redirect()->route('admins.profile');
                }
                return redirect()->back()->withInput()->withErrors('Tài khoản hoặc mật khẩu không chính xác!');
            } catch (\Throwable $th) {
                return redirect()->back()->withErrors('Đăng Nhập Thất Bại!');
            }
        }

        return view('admins.pages.auth.login');
    }

    public function logout(Request $request)
    {
        if (Auth::guard('admin')->user() != null) {
            DB::table('re_administrators')->where('admin_id', Auth::guard('admin')->user()->admin_id)->update(['last_login_time' => Carbon::now()]);
        }

        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->route('clients.index');
    }
    public function error403(Request $request)
    {
        return view('admins.pages.auth.error_403');
    }


    public function verify(Request $request)
    {
    }
    public function verifyAgain(Request $request)
    {
    }
    public function forget(Request $request)
    {
        return view('admins.pages.auth.forget');
    }
    public function formPassword(Request $request)
    {
    }
    public function changePassword(Request $request)
    {
    }
}
