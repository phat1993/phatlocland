<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\HouseGalleryRequest;
use App\Models\Entities\HouseGallery;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class HouseGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HouseGalleryRequest $request)
    {
        try {
            $house = DB::table('re_houses')->where('house_id', $request->id)->select('house_id', 'house_name')->first();
            $proGalleries = DB::table('re_house_galleries')->where('house_id',  $request->id)->orderBy('updated_at', 'desc')->get();
            return view('admins.pages.housegallery.index', compact('house', 'proGalleries'));
        } catch (\Throwable $th) {

            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(HouseGalleryRequest $request)
    {
        try {
            $house = DB::table('re_houses')->where('house_id', $request->id)->select('house_id', 'house_name')->first();
            $houseGallery = new HouseGallery();
            $houseGallery->house_gallery_order = 1;
            $houseGallery->active_status = 1;
            $houseGallery->house_id = $house->house_id;
            return view('admins.pages.housegallery.create', compact('houseGallery', 'house'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HouseGalleryRequest $request)
    {

        if ($request->isMethod('post')) {
            $data = $request->all();
            $path = '';

            $validImg = ['file_input'  => 'image|required']; //|dimensions:width=800,height=500

            $customMessages = [
                'file_input.required' => 'Hình Ảnh: Bạn không được để trống.',
                'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
            ];

            //neu hinh anh la banner thi phai dat chuan 1400x700
            if ($data[HouseGallery::HOUSE_GALLERY_BANNER] == 1) {
                $validImg = ['file_input'  => 'image|required|dimensions:width=1400,height=700'];
                $customMessages = [
                    'file_input.required' => 'Hình Ảnh: Bạn không được để trống.',
                    'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
                    'file_input.dimensions' => 'Hình Ảnh: Bạn cần chọn hình ảnh 1400x700.',
                ];
            }
            //neu hinh anh la anh dai dien thi phai dat chuan 700x500
            else if ($data[HouseGallery::HOUSE_GALLERY_BANNER] == 2) {
                $validImg = ['file_input'  => 'image|required|dimensions:width=750,height=500'];
                $customMessages = [
                    'file_input.required' => 'Hình Ảnh: Bạn không được để trống.',
                    'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
                    'file_input.dimensions' => 'Hình Ảnh: Bạn cần chọn hình ảnh 750x500.',
                ];
            }
            $request->validate($validImg, $customMessages);
            //validate bo trong try-catch thi khong chay
            try {

                $path = $request->file('file_input')->store(
                    'images/houses/' . $data[HouseGallery::HOUSE_ID],
                    'public'
                );


                $data = $request->all();

                $houseGallery = new HouseGallery();
                $houseGallery->{HouseGallery::HOUSE_GALLERY_NAME} = $data[HouseGallery::HOUSE_GALLERY_NAME];
                $houseGallery->{HouseGallery::HOUSE_GALLERY_BANNER} = $data[HouseGallery::HOUSE_GALLERY_BANNER];
                $houseGallery->{HouseGallery::HOUSE_GALLERY_LINK} =  $path;
                $houseGallery->{HouseGallery::HOUSE_GALLERY_ORDER} = $data[HouseGallery::HOUSE_GALLERY_ORDER];
                $houseGallery->{HouseGallery::HOUSE_ID} = $data[HouseGallery::HOUSE_ID];
                $houseGallery->{HouseGallery::ACTIVE_STATUS} = isset($data[HouseGallery::ACTIVE_STATUS]) ? 1 : 0;

                if ($houseGallery->save())
                    return redirect()->back()->with('success', 'Tạo mới đối tượng thành công !');
                else {
                    //Neu them ko duoc doi tuong ma da luu hinh anh roi thi xoa hinh anh
                    if ($path != '' && Storage::disk('public')->exists($path))
                        Storage::disk('public')->delete($path);

                    redirect()->back()->withInput($request->all())->withErrors('Lỗi server! Tạo mới thất bại');
                }
            } catch (\Throwable $th) {
                $message = $th->getMessage();
                return view('admins.pages.auth.error_403', compact('message'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HouseGallery  $houseGallery
     * @return \Illuminate\Http\Response
     */
    public function show(HouseGalleryRequest $request)
    {
        try {
            $data = $request->all();
            //Lay doi tuong cua house business lien ket voi admins va updatemode de lay noi dung va hinh anh
            $houseGallery = DB::table('re_house_galleries')->select(
                're_house_galleries.*',
                //Lay noi dung hinh anh cua admin
                're_administrators.admin_full_name'
            )
                ->leftJoin('re_administrators', 're_administrators.admin_id', '=', 're_house_galleries.admin_updated')
                ->where('house_gallery_id', $data['house_gallery_id'])->first();

            if (isset($houseGallery))
                return view('admins.pages.housegallery.show', compact('houseGallery'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HouseGallery  $houseGallery
     * @return \Illuminate\Http\Response
     */
    public function edit(HouseGalleryRequest $request)
    {
        try {
            $data = $request->all();

            $house = DB::table('re_houses')->where('house_id', $request->id)->select('house_id', 'house_name')->first();
            $houseGallery = DB::table('re_house_galleries')->where('house_gallery_id', $data['house_gallery_id'])->first();

            if (isset($houseGallery))
                return view('admins.pages.housegallery.edit', compact('houseGallery', 'house'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HouseGallery  $houseGallery
     * @return \Illuminate\Http\Response
     */
    public function update(HouseGalleryRequest $request)
    {
        $data = $request->all();
        $banner =  isset($data[HouseGallery::HOUSE_GALLERY_BANNER]) ? 1 : 0;

        $param = array();
        $path = '';
        if ($request->hasFile('file_input')) {

            $validImg = ['file_input'  => 'image'];

            $customMessages = [
                'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
            ];

            //neu hinh anh la banner thi phai dat chuan 1400x700
            if ($data[HouseGallery::HOUSE_GALLERY_BANNER] == 1) {
                $validImg = ['file_input'  => 'image|dimensions:width=1400,height=700'];
                $customMessages = [
                    'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
                    'file_input.dimensions' => 'Hình Ảnh: Bạn cần chọn hình ảnh 1400x700.',
                ];
            }
            //neu hinh anh la anh dai dien thi phai dat chuan 700x500
            else if ($data[HouseGallery::HOUSE_GALLERY_BANNER] == 2) {
                $validImg = ['file_input'  => 'image|dimensions:width=750,height=500'];
                $customMessages = [
                    'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
                    'file_input.dimensions' => 'Hình Ảnh: Bạn cần chọn hình ảnh 750x500.',
                ];
            }

            $request->validate($validImg, $customMessages);

            $path = $request->file('file_input')->store(
                'images/houses/' . $data[HouseGallery::HOUSE_ID],
                'public'
            );
            //Luu Thanh cong hinh anh thi set cho bien
            if ($path != '')
                $param[HouseGallery::HOUSE_GALLERY_LINK] = $path;
        }
        try {

            $param[HouseGallery::HOUSE_GALLERY_NAME] = $data[HouseGallery::HOUSE_GALLERY_NAME];
            $param[HouseGallery::HOUSE_GALLERY_BANNER] = $data[HouseGallery::HOUSE_GALLERY_BANNER];
            $param[HouseGallery::HOUSE_GALLERY_ORDER] =  $data[HouseGallery::HOUSE_GALLERY_ORDER];

            $param[HouseGallery::ACTIVE_STATUS] = isset($data[HouseGallery::ACTIVE_STATUS]) ? 1 : 0;
            $param[HouseGallery::UPDATED_AT] = Carbon::now()->format('Y-m-d H:i:s');

            $houseGallery = DB::table('re_house_galleries')->where('house_gallery_id', $data['house_gallery_id']);
            if ($houseGallery->update($param)) {

                //Neu cap nhat thanh cong ma co hinh anh moi thi xoa hinh anh cu
                if ($path != '' && Storage::disk('public')->exists($data[HouseGallery::HOUSE_GALLERY_LINK]))
                    Storage::disk('public')->delete($data[HouseGallery::HOUSE_GALLERY_LINK]);

                return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
            } else {
                //Neu luu anh ma ko luu dc doi tuong thi xoa
                if ($path != '' && Storage::disk('public')->exists($path))
                    Storage::disk('public')->delete($path);

                return redirect()->back()->withErrors(['Cập Nhật Thất Bại!']);
            }
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $houseGalleryCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(HouseGalleryRequest $request)
    {
        try {
            $data = $request->all();
            //Kiem tra doi tuong co trong database khong
            $img = DB::table('re_house_galleries')->where('house_gallery_id', $data['house_gallery_id'])->first();
            if (!isset($img))
                return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);


            $deleted = DB::table('re_house_galleries')->where('house_gallery_id', $data['house_gallery_id'])->delete();
            if ($deleted) {
                //Xoa anh
                if (Storage::disk('public')->exists($img->{HouseGallery::HOUSE_GALLERY_LINK}))
                    Storage::disk('public')->delete($img->{HouseGallery::HOUSE_GALLERY_LINK});

                return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
            } else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
}
