<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\WebFeedbackRequest;
use App\Models\Entities\WebFeedback;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class WebFeedbackController extends Controller
{

    const PER_PAGE = 20;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(WebFeedbackRequest $request)
    {
        try {
            $data = $request->all();
            $param = array();
            $perPage = self::PER_PAGE;
            $currentPage = LengthAwarePaginator::resolveCurrentPage();

            if (isset($data[WebFeedback::WEB_FEEDBACK_NAME])) {
                $param[] =  [WebFeedback::WEB_FEEDBACK_NAME, 'like', '%' . $data[WebFeedback::WEB_FEEDBACK_NAME] . '%'];
            }
            if (isset($data[WebFeedback::WEB_FEEDBACK_EMAIL])) {
                $param[] =  [WebFeedback::WEB_FEEDBACK_EMAIL, 'like', '%' . $data[WebFeedback::WEB_FEEDBACK_EMAIL] . '%'];
            }
            if (isset($data[WebFeedback::WEB_FEEDBACK_CONTENT])) {
                $param[] =  [WebFeedback::WEB_FEEDBACK_CONTENT, 'like', '%' . $data[WebFeedback::WEB_FEEDBACK_CONTENT] . '%'];
            }
            if (isset($data[WebFeedback::ACTIVE_STATUS])) {
                if ($data[WebFeedback::ACTIVE_STATUS] == 1)
                    $param[] = [WebFeedback::ACTIVE_STATUS, 1];
                elseif ($data[WebFeedback::ACTIVE_STATUS] == 2)
                    $param[] = [WebFeedback::ACTIVE_STATUS, 0];
            }

            $webFeedbacks = DB::table('re_web_feedbacks')->where($param)->orderBy('active_status')->orderByDesc('updated_at')->paginate($perPage);
            return view('admins.pages.webfeedback.index', compact('webFeedbacks'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebFeedback  $houseReview
     * @return \Illuminate\Http\Response
     */
    public function update(WebFeedbackRequest $request)
    {
        try {

            $data = $request->all();
            $param = array();
            $param[WebFeedback::ACTIVE_STATUS] = 1;
            $param[WebFeedback::ADMIN_UPDATED] = Auth::user()->admin_id;
            $param[WebFeedback::UPDATED_AT] = Carbon::now()->format('Y-m-d H:i:s');


            $houseReview = DB::table('re_web_feedbacks')->where('web_feedback_id', $data['web_feedback_id']);
            if ($houseReview->update($param)) {
                return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
            } else
                return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $houseReviewCategories
     * @return \Illuminate\Http\Response
     */
    public function delete(WebFeedbackRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = DB::table('re_web_feedbacks')->where('web_feedback_id', $data['web_feedback_id'])->delete();
            if ($deleted)
                return redirect()->route('admins.webfeedback.index')->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
    public function getCount()
    {
        $data = DB::table('re_web_feedbacks')->where('active_status', 0)->count();
        echo json_encode($data);
        exit;
    }
}
