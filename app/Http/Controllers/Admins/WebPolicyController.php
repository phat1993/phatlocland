<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\WebPolicyRequest;
use App\Models\Entities\WebPolicy;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class WebPolicyController extends Controller
{
    const PER_PAGE = 20;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(WebPolicyRequest $request)
    {
        try {
            $data = $request->all();
            $param = array();
            $perPage = self::PER_PAGE;
            $currentPage = LengthAwarePaginator::resolveCurrentPage();

            if (isset($data[WebPolicy::WEB_POLICY_NAME])) {
                $param[] =  [WebPolicy::WEB_POLICY_NAME, 'like', '%' . $data[WebPolicy::WEB_POLICY_NAME] . '%'];
            }
            if (isset($data[WebPolicy::WEB_POLICY_DESCRIPTION])) {
                $param[] =  [WebPolicy::WEB_POLICY_DESCRIPTION, 'like', '%' . $data[WebPolicy::WEB_POLICY_DESCRIPTION] . '%'];
            }
            if (isset($data[WebPolicy::ACTIVE_STATUS])) {
                if ($data[WebPolicy::ACTIVE_STATUS] == 1)
                    $param[] = [WebPolicy::ACTIVE_STATUS, 1];
                elseif ($data[WebPolicy::ACTIVE_STATUS] == 2)
                    $param[] = [WebPolicy::ACTIVE_STATUS, 0];
            }
            $webPolicys  = DB::table('re_web_policies')
                ->select(
                    're_web_policies.web_policy_id',
                    're_web_policies.web_policy_image',
                    're_web_policies.web_policy_name',
                    're_web_policies.web_policy_description',
                    're_web_policies.active_status',
                )
                ->where($param)->orderByDesc('updated_at')->paginate($perPage);
            return view('admins.pages.webpolicy.index', compact('webPolicys', 'currentPage', 'perPage'));
        } catch (\Throwable $th) {

            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(WebPolicyRequest $request)
    {
        try {
            $webPolicy = new WebPolicy();
            $webPolicy->active_status = 1;
            return view('admins.pages.webpolicy.create', compact('webPolicy'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WebPolicyRequest $request)
    {

        if ($request->isMethod('post')) {

            $data = $request->all();
            //Kiem tra duong dan moi co trung voi cac doi tuong da co
            $slug = Str::slug($data[WebPolicy::WEB_POLICY_NAME]);
            if (DB::table('re_web_policies')->where(WebPolicy::WEB_CANONICAL, $slug)->count() > 0)
                return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!', 'Tiêu đề dự án đã có trong cơ sở dữ liệu!']);


            $path = '';
            //Xu ly validator khi co hinh anh
            if ($request->hasFile('file_input')) {
                $validImg = ['file_input'  => 'image|dimensions:width=750,height=500'];
                $customMessages = [
                    'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
                    'file_input.dimensions' => 'Hình Ảnh: Bạn cần chọn hình ảnh 750x500.',
                ];
                $request->validate($validImg, $customMessages);

                $path = $request->file('file_input')->store(
                    'images/webpolicies',
                    'public'
                );
            }

            try {

                $webPolicy = new WebPolicy();
                $webPolicy->{WebPolicy::WEB_POLICY_NAME} = $data[WebPolicy::WEB_POLICY_NAME];
                $webPolicy->{WebPolicy::WEB_POLICY_IMAGE} = $path;

                $webPolicy->{WebPolicy::WEB_POLICY_DESCRIPTION} = $data[WebPolicy::WEB_POLICY_DESCRIPTION];
                $webPolicy->{WebPolicy::WEB_POLICY_DETAIL} = $data[WebPolicy::WEB_POLICY_DETAIL];
                //Web Info
                $webPolicy->{WebPolicy::WEB_KEYWORDS} = $data[WebPolicy::WEB_KEYWORDS];
                $webPolicy->{WebPolicy::WEB_DESCRIPTION} = $data[WebPolicy::WEB_DESCRIPTION];
                $webPolicy->{WebPolicy::WEB_TITLE} = $data[WebPolicy::WEB_TITLE];
                $webPolicy->{WebPolicy::WEB_CANONICAL} = $slug;

                $webPolicy->{WebPolicy::ACTIVE_STATUS} = isset($data[WebPolicy::ACTIVE_STATUS]) ? 1 : 0;
                $webPolicy->{WebPolicy::ADMIN_UPDATED} = Auth::user()->admin_id;

                if ($webPolicy->save())
                    return redirect()->back()->with('success', 'Tạo mới đối tượng thành công !');
                else {
                    //Neu them ko duoc doi tuong ma da luu hinh anh roi thi xoa hinh anh
                    if ($path != '' && Storage::disk('public')->exists($path))
                        Storage::disk('public')->delete($path);

                    redirect()->back()->withInput($request->all())->withErrors('Lỗi server! Tạo mới thất bại');
                }
            } catch (\Throwable $th) {
                $message = $th->getMessage();
                return view('admins.pages.auth.error_403', compact('message'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WebPolicy  $webPolicy
     * @return \Illuminate\Http\Response
     */
    public function show(WebPolicyRequest $request)
    {
        try {
            $data = $request->all();
            //Lay doi tuong cua webPolicy business lien ket voi admins va updatemode de lay noi dung va hinh anh
            $webPolicy = DB::table('re_web_policies')->where('web_policy_id', $request->web_policy_id)->select(
                're_web_policies.*',
                //Lay noi dung hinh anh cua admin
                're_administrators.admin_full_name'
            )
                ->leftJoin('re_administrators', 're_administrators.admin_id', '=', 're_web_policies.admin_updated')
                ->where('web_policy_id', $data['web_policy_id'])->first();

            if (isset($webPolicy))
                return view('admins.pages.webpolicy.show', compact('webPolicy'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WebPolicy  $webPolicy
     * @return \Illuminate\Http\Response
     */
    public function edit(WebPolicyRequest $request)
    {
        try {
            $data = $request->all();
            $webPolicy = DB::table('re_web_policies')->where('web_policy_id', $data['web_policy_id'])->first();
            if (isset($webPolicy))
                return view('admins.pages.webpolicy.edit', compact('webPolicy'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebPolicy  $webPolicy
     * @return \Illuminate\Http\Response
     */
    public function update(WebPolicyRequest $request)
    {

        $data = $request->all();

        $param = array();
        //Kiem tra duong dan moi co trung voi cac doi tuong da co
        $slug = Str::slug($data[WebPolicy::WEB_POLICY_NAME]);
        if (DB::table('re_web_policies')->where([
            ['web_policy_id', '<>', $data['web_policy_id']],
            [WebPolicy::WEB_CANONICAL, $slug]
        ])->count() > 0)
            return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!', 'Tiêu đề danh mục đã có trong cơ sở dữ liệu!']);

        $path = '';
        if ($request->hasFile('file_input')) {
            $validImg = ['file_input'  => 'image|dimensions:width=750,height=500'];
            $customMessages = [
                'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
                'file_input.dimensions' => 'Hình Ảnh: Bạn cần chọn hình ảnh 750x500.',
            ];
            $request->validate($validImg, $customMessages);

            $path = $request->file('file_input')->store(
                'images/webpolicies',
                'public'
            );
            //Luu Thanh cong hinh anh thi set cho bien
            if ($path != '')
                $param[WebPolicy::WEB_POLICY_IMAGE] = $path;
        }
        try {

            $param[WebPolicy::WEB_POLICY_NAME] = $data[WebPolicy::WEB_POLICY_NAME];
            $param[WebPolicy::WEB_POLICY_DESCRIPTION] = $data[WebPolicy::WEB_POLICY_DESCRIPTION];
            $param[WebPolicy::WEB_POLICY_DETAIL] = $data[WebPolicy::WEB_POLICY_DETAIL];

            //Web Info
            $param[WebPolicy::WEB_KEYWORDS] = $data[WebPolicy::WEB_KEYWORDS];
            $param[WebPolicy::WEB_DESCRIPTION] = $data[WebPolicy::WEB_DESCRIPTION];
            $param[WebPolicy::WEB_TITLE] = $data[WebPolicy::WEB_TITLE];
            $param[WebPolicy::WEB_CANONICAL] =  $slug;

            $param[WebPolicy::ACTIVE_STATUS] = isset($data[WebPolicy::ACTIVE_STATUS]) ? 1 : 0;
            $param[WebPolicy::ADMIN_UPDATED] = Auth::user()->admin_id;
            $param[WebPolicy::UPDATED_AT] = Carbon::now()->format('Y-m-d H:i:s');

            $webPolicy = DB::table('re_web_policies')->where('web_policy_id', $data['web_policy_id']);
            if ($webPolicy->update($param)) {

                //Neu cap nhat thanh cong ma co hinh anh moi thi xoa hinh anh cu
                if ($path != '' && Storage::disk('public')->exists($data[WebPolicy::WEB_POLICY_IMAGE]))
                    Storage::disk('public')->delete($data[WebPolicy::WEB_POLICY_IMAGE]);

                return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
            } else {
                //Neu luu anh ma ko luu dc doi tuong thi xoa
                if ($path != '' && Storage::disk('public')->exists($path))
                    Storage::disk('public')->delete($path);

                return redirect()->back()->withErrors(['Cập Nhật Thất Bại!']);
            }
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Productjects  $housejects
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebPolicyRequest $request)
    {
        try {

            $data = $request->all();

            //Kiem tra doi tuong co trong database khong
            $house = WebPolicy::withTrashed()->where('web_policy_id', $data['web_policy_id'])->firstOrFail();
            if (!isset($house))
                return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);

            $deleted = WebPolicy::where('web_policy_id', $data['web_policy_id'])->delete();
            if ($deleted)
                return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }


    /**
     * recovered the specified resource from storage.
     *
     * @param  \App\Models\Productjects  $housejects
     * @return \Illuminate\Http\Response
     */
    public function recovered(WebPolicyRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = WebPolicy::withTrashed()->where('web_policy_id', $data['web_policy_id'])->restore();
            if ($deleted)
                return redirect()->back()->with('success', 'Phục hồi tượng thành công!');
            else
                return redirect()->back()->withErrors('Phục hồi tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\Productjects  $housejects
     * @return \Illuminate\Http\Response
     */
    public function delete(WebPolicyRequest $request)
    {
        try {
            $data = $request->all();
            //Kiem tra doi tuong co trong database khong
            $img = DB::table('re_web_policies')->where('web_policy_id', $data['web_policy_id'])->first();
            if (!isset($img))
                return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);


            $deleted = DB::table('re_web_policies')->where('web_policy_id', $data['web_policy_id'])->delete();
            if ($deleted) {
                //Xoa anh
                if (Storage::disk('public')->exists($img->{WebPolicy::WEB_POLICY_IMAGE}))
                    Storage::disk('public')->delete($img->{WebPolicy::WEB_POLICY_IMAGE});

                return redirect()->route('admins.webpolicy.index')->with('success', 'Xóa đối tượng thành công!');
            } else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
}
