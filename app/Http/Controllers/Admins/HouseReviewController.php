<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\HouseReviewRequest;
use App\Models\Entities\HouseReview;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class HouseReviewController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(HouseReviewRequest $request)
    {
        try {
            $houseReviews = DB::table('re_house_reviews')->where('re_house_reviews.active_status', 0)
                ->select(
                    're_house_reviews.*',
                    'hou.house_name',
                    're_house_businesses.house_business_name',
                    're_house_categories.house_category_name',
                    're_projects.project_name'
                )
                ->leftJoin('re_houses AS hou', 'hou.house_id', '=', 're_house_reviews.house_id')
                ->leftJoin('re_house_businesses', 're_house_businesses.house_business_id', '=', 'hou.house_business_id')
                ->leftJoin('re_house_categories', 're_house_categories.house_category_id', '=', 'hou.house_category_id')
                ->leftJoin('re_projects', 're_projects.project_id', '=', 'hou.project_id')
                ->orderBy('updated_at', 'desc')->get();
            return view('admins.pages.housereview.index', compact('houseReviews'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HouseReview  $houseReview
     * @return \Illuminate\Http\Response
     */
    public function update(HouseReviewRequest $request)
    {
        try {

            $data = $request->all();
            $param = array();
            $param[HouseReview::ACTIVE_STATUS] = 1;
            $param[HouseReview::ADMIN_UPDATED] = Auth::user()->admin_id;
            $param[HouseReview::UPDATED_AT] = Carbon::now()->format('Y-m-d H:i:s');


            $houseReview = DB::table('re_house_reviews')->where('house_review_id', $data['house_review_id']);
            if ($houseReview->update($param)) {
                //Update diem rating va so suong
                $updated = DB::table('re_houses')->where('house_id', $houseReview->house_id);
                $param = array();
                $param['house_rating_count'] = $updated->house_rating_count + 1;
                $param['house_rating_point'] = $updated->house_rating_point + $houseReview->house_review_point;
                $updated->update($param);

                return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
            } else
                return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $houseReviewCategories
     * @return \Illuminate\Http\Response
     */
    public function delete(HouseReviewRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = DB::table('re_house_reviews')->where('house_review_id', $data['house_review_id'])->delete();
            if ($deleted)
                return redirect()->route('admins.housereview.index')->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
    public function getCount()
    {
        $data = DB::table('re_house_reviews')->where('active_status', 0)->count();
        echo json_encode($data);
        exit;
    }
}
