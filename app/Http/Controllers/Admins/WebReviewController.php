<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\WebReviewRequest;
use App\Models\Entities\WebReview;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class WebReviewController extends Controller
{
    const PER_PAGE = 20;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(WebReviewRequest $request)
    {
        try {
            $data = $request->all();
            $param = array();
            $perPage = self::PER_PAGE;
            $currentPage = LengthAwarePaginator::resolveCurrentPage();

            if (isset($data[WebReview::WEB_REVIEW_NAME])) {
                $param[] =  [WebReview::WEB_REVIEW_NAME, 'like', '%' . $data[WebReview::WEB_REVIEW_NAME] . '%'];
            }
            if (isset($data[WebReview::WEB_REVIEW_ADDRESS])) {
                $param[] =  [WebReview::WEB_REVIEW_ADDRESS, 'like', '%' . $data[WebReview::WEB_REVIEW_ADDRESS] . '%'];
            }
            if (isset($data[WebReview::WEB_REVIEW_DETAIL])) {
                $param[] =  [WebReview::WEB_REVIEW_DETAIL, 'like', '%' . $data[WebReview::WEB_REVIEW_DETAIL] . '%'];
            }
            if (isset($data[WebReview::ACTIVE_STATUS])) {
                if ($data[WebReview::ACTIVE_STATUS] == 1)
                    $param[] = [WebReview::ACTIVE_STATUS, 1];
                elseif ($data[WebReview::ACTIVE_STATUS] == 2)
                    $param[] = [WebReview::ACTIVE_STATUS, 0];
            }

            $webReviews  = DB::table('re_web_reviews')->where($param)
                ->select(
                    're_web_reviews.web_review_id',
                    're_web_reviews.web_review_image',
                    're_web_reviews.web_review_name',
                    're_web_reviews.web_review_address',
                    're_web_reviews.web_review_detail',
                    're_web_reviews.active_status',
                )
                ->orderByDesc('updated_at')->paginate($perPage);
            return view('admins.pages.webreview.index', compact('webReviews', 'currentPage', 'perPage'));
        } catch (\Throwable $th) {

            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(WebReviewRequest $request)
    {
        try {
            $webReview = new WebReview();
            $webReview->active_status = 1;
            return view('admins.pages.webreview.create', compact('webReview'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WebReviewRequest $request)
    {

        if ($request->isMethod('post')) {

            $data = $request->all();
            $path = '';

            $validImg = ['file_input'  => 'image|required|dimensions:width=400,height=400'];
            $customMessages = [
                'file_input.required' => 'Hình Ảnh: Bạn không được để trống.',
                'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
                'file_input.dimensions' => 'Hình Ảnh: Bạn cần chọn hình ảnh 400x400.',
            ];
            $request->validate($validImg, $customMessages);

            $path = $request->file('file_input')->store(
                'images/webreviews',
                'public'
            );
            try {

                $webReview = new WebReview();
                $webReview->{WebReview::WEB_REVIEW_NAME} = $data[WebReview::WEB_REVIEW_NAME];
                $webReview->{WebReview::WEB_REVIEW_IMAGE} =  $path;
                $webReview->{WebReview::WEB_REVIEW_ADDRESS} = $data[WebReview::WEB_REVIEW_ADDRESS];
                $webReview->{WebReview::WEB_REVIEW_DETAIL} = $data[WebReview::WEB_REVIEW_DETAIL];

                $webReview->{WebReview::ACTIVE_STATUS} = isset($data[WebReview::ACTIVE_STATUS]) ? 1 : 0;
                $webReview->{WebReview::ADMIN_UPDATED} = Auth::user()->admin_id;

                if ($webReview->save())
                    return redirect()->back()->with('success', 'Tạo mới đối tượng thành công !');
                else {
                    //Neu them ko duoc doi tuong ma da luu hinh anh roi thi xoa hinh anh
                    if ($path != '' && Storage::disk('public')->exists($path))
                        Storage::disk('public')->delete($path);

                    redirect()->back()->withInput($request->all())->withErrors('Lỗi server! Tạo mới thất bại');
                }
            } catch (\Throwable $th) {
                $message = $th->getMessage();
                return view('admins.pages.auth.error_403', compact('message'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WebReview  $webReview
     * @return \Illuminate\Http\Response
     */
    public function show(WebReviewRequest $request)
    {
        try {
            $data = $request->all();
            //Lay doi tuong cua webHire business lien ket voi admins va updatemode de lay noi dung va hinh anh
            $webReview = DB::table('re_web_reviews')->where('web_review_id', $request->web_review_id)->select(
                're_web_reviews.*',
                //Lay noi dung hinh anh cua admin
                're_administrators.admin_full_name'
            )
                ->leftJoin('re_administrators', 're_administrators.admin_id', '=', 're_web_reviews.admin_updated')
                ->where('web_review_id', $data['web_review_id'])->first();

            if (isset($webReview))
                return view('admins.pages.webreview.show', compact('webReview'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WebReview  $webReview
     * @return \Illuminate\Http\Response
     */
    public function edit(WebReviewRequest $request)
    {
        try {
            $data = $request->all();
            $webReview = DB::table('re_web_reviews')->where('web_review_id', $data['web_review_id'])->first();
            if (isset($webReview))
                return view('admins.pages.webreview.edit', compact('webReview'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebReview  $webReview
     * @return \Illuminate\Http\Response
     */
    public function update(WebReviewRequest $request)
    {

        $data = $request->all();

        $param = array();
        $path = '';
        if ($request->hasFile('file_input')) {
            $validImg = ['file_input'  => 'image|dimensions:width=400,height=400'];
            $customMessages = [
                'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
                'file_input.dimensions' => 'Hình Ảnh: Bạn cần chọn hình ảnh 400x400.',
            ];
            $request->validate($validImg, $customMessages);

            $path = $request->file('file_input')->store(
                'images/webreviews',
                'public'
            );
            //Luu Thanh cong hinh anh thi set cho bien
            if ($path != '')
                $param[WebReview::WEB_REVIEW_IMAGE] = $path;
        }
        try {

            $param[WebReview::WEB_REVIEW_NAME] = $data[WebReview::WEB_REVIEW_NAME];
            $param[WebReview::WEB_REVIEW_ADDRESS] = $data[WebReview::WEB_REVIEW_ADDRESS];
            $param[WebReview::WEB_REVIEW_DETAIL] = $data[WebReview::WEB_REVIEW_DETAIL];


            $param[WebReview::ACTIVE_STATUS] = isset($data[WebReview::ACTIVE_STATUS]) ? 1 : 0;
            $param[WebReview::ADMIN_UPDATED] = Auth::user()->admin_id;
            $param[WebReview::UPDATED_AT] = Carbon::now()->format('Y-m-d H:i:s');

            $webReview = DB::table('re_web_reviews')->where('web_review_id', $data['web_review_id']);
            if ($webReview->update($param)) {

                //Neu cap nhat thanh cong ma co hinh anh moi thi xoa hinh anh cu
                if ($path != '' && Storage::disk('public')->exists($data[WebReview::WEB_REVIEW_IMAGE]))
                    Storage::disk('public')->delete($data[WebReview::WEB_REVIEW_IMAGE]);

                return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
            } else {
                //Neu luu anh ma ko luu dc doi tuong thi xoa
                if ($path != '' && Storage::disk('public')->exists($path))
                    Storage::disk('public')->delete($path);

                return redirect()->back()->withErrors(['Cập Nhật Thất Bại!']);
            }
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Productjects  $housejects
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebReviewRequest $request)
    {
        try {

            $data = $request->all();

            //Kiem tra doi tuong co trong database khong
            $house = WebReview::withTrashed()->where('web_review_id', $data['web_review_id'])->firstOrFail();
            if (!isset($house))
                return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);

            $deleted = WebReview::where('web_review_id', $data['web_review_id'])->delete();
            if ($deleted)
                return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }


    /**
     * recovered the specified resource from storage.
     *
     * @param  \App\Models\Productjects  $housejects
     * @return \Illuminate\Http\Response
     */
    public function recovered(WebReviewRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = WebReview::withTrashed()->where('web_review_id', $data['web_review_id'])->restore();
            if ($deleted)
                return redirect()->back()->with('success', 'Phục hồi tượng thành công!');
            else
                return redirect()->back()->withErrors('Phục hồi tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\Productjects  $housejects
     * @return \Illuminate\Http\Response
     */
    public function delete(WebReviewRequest $request)
    {
        try {
            $data = $request->all();
            //Kiem tra doi tuong co trong database khong
            $img = DB::table('re_web_reviews')->where('web_review_id', $data['web_review_id'])->first();
            if (!isset($img))
                return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);


            $deleted = DB::table('re_web_reviews')->where('web_review_id', $data['web_review_id'])->delete();
            if ($deleted) {
                //Xoa anh
                if (Storage::disk('public')->exists($img->{WebReview::WEB_REVIEW_IMAGE}))
                    Storage::disk('public')->delete($img->{WebReview::WEB_REVIEW_IMAGE});

                return redirect()->route('admins.webreview.index')->with('success', 'Xóa đối tượng thành công!');
            } else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
}
