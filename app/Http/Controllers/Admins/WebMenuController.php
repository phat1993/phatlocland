<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\WebMenuRequest;
use App\Models\Entities\WebMenu;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class WebMenuController extends Controller
{
    const PER_PAGE = 20;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(WebMenuRequest $request)
    {
        try {
            $data = $request->all();
            $param = array();
            $paramOr = array();
            $perPage = self::PER_PAGE;
            $currentPage = LengthAwarePaginator::resolveCurrentPage();

            if (isset($data[WebMenu::MENU_LV1])) {
                $param[] =  ['menuA.menu_lv1', $data[WebMenu::MENU_LV1]];
                $paramOr[] =  ['menuA.menu_id', $data[WebMenu::MENU_LV1]];
                if (isset($data[WebMenu::MENU_LV2])) {
                    $param[] =  ['menuA.menu_lv2', $data[WebMenu::MENU_LV2]];
                    $paramOr[] =  ['menuA.menu_lv2', $data[WebMenu::MENU_LV2]];
                }
                if (isset($data[WebMenu::MENU_NAME])) {
                    $param[] =  ['menuA.menu_name', 'like', '%' . $data[WebMenu::MENU_NAME] . '%'];
                    $paramOr[] =  ['menuA.menu_name', 'like', '%' . $data[WebMenu::MENU_NAME] . '%'];
                }
                if (isset($data[WebMenu::MENU_ORDER])) {
                    $param[] =  ['menuA.menu_order', $data[WebMenu::MENU_ORDER]];
                    $paramOr[] =  ['menuA.menu_order', $data[WebMenu::MENU_ORDER]];
                }
                if (isset($data[WebMenu::MENU_URL])) {
                    $param[] =  ['menuA.menu_url', 'like', '%' . $data[WebMenu::MENU_URL] . '%'];
                    $paramOr[] =  ['menuA.menu_url', 'like', '%' . $data[WebMenu::MENU_URL] . '%'];
                }
                if (isset($data[WebMenu::ACTIVE_STATUS])) {
                    if ($data[WebMenu::ACTIVE_STATUS] == 1) {
                        $param[] = ['menuA.active_status', 1];
                        $paramOr[] = ['menuA.active_status', 1];
                    } elseif ($data[WebMenu::ACTIVE_STATUS] == 2) {
                        $param[] = ['menuA.active_status', 0];
                        $paramOr[] = ['menuA.active_status', 0];
                    }
                }
            } else {
                if (isset($data[WebMenu::MENU_LV2])) {
                    $param[] =  ['menuA.menu_lv2', $data[WebMenu::MENU_LV2]];
                }
                if (isset($data[WebMenu::MENU_NAME])) {
                    $param[] =  ['menuA.menu_name', 'like', '%' . $data[WebMenu::MENU_NAME] . '%'];
                }
                if (isset($data[WebMenu::MENU_ORDER])) {
                    $param[] =  ['menuA.menu_order', $data[WebMenu::MENU_ORDER]];
                }
                if (isset($data[WebMenu::MENU_URL])) {
                    $param[] =  ['menuA.menu_url', 'like', '%' . $data[WebMenu::MENU_URL] . '%'];
                }
                if (isset($data[WebMenu::ACTIVE_STATUS])) {
                    if ($data[WebMenu::ACTIVE_STATUS] == 1)
                        $param[] = ['menuA.active_status', 1];
                    elseif ($data[WebMenu::ACTIVE_STATUS] == 2)
                        $param[] = ['menuA.active_status', 0];
                }
            }
            $webMenus  = DB::table('re_web_menus AS menuA')
                ->select(
                    'menuA.menu_id',
                    'menuA.menu_name',
                    'menuA.menu_order',
                    'menuA.menu_url',
                    'menuA.active_status',
                    //lay ten doi tuong lien ket
                    'menuB.menu_name AS menu_name_lv1',
                    'menuC.menu_name AS menu_name_lv2'
                )
                ->where($param)->orWhere($paramOr)
                ->leftJoin('re_web_menus AS menuB', 'menuB.menu_id', '=', 'menuA.menu_lv1')
                ->leftJoin('re_web_menus AS menuC', 'menuC.menu_id', '=', 'menuA.menu_lv2')
                ->orderByDesc('menuA.updated_at')->paginate($perPage);

            $menuA = DB::table('re_web_menus')->select('re_web_menus.menu_id', 're_web_menus.menu_name')->where('active_status', 1)->whereNull('menu_lv1')->whereNull('menu_lv2')
                ->orderBy('menu_order')->get();

            return view('admins.pages.webmenu.index', compact('webMenus', 'currentPage', 'perPage', 'menuA'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(WebMenuRequest $request)
    {
        try {
            $webMenu = new WebMenu();
            $webMenu->menu_order = 1;
            $webMenu->active_status = 1;
            $menuA = DB::table('re_web_menus')->select('re_web_menus.menu_id', 're_web_menus.menu_name')->where('active_status', 1)->whereNull('menu_lv1')->whereNull('menu_lv2')
                ->orderBy('menu_order')->get();

            return view('admins.pages.webmenu.create', compact('webMenu', 'menuA'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WebMenuRequest $request)
    {
        if ($request->isMethod('post')) {
            try {

                $data = $request->all();

                $webMenu = new WebMenu();
                $webMenu->{WebMenu::MENU_NAME} = $data[WebMenu::MENU_NAME];
                $webMenu->{WebMenu::MENU_ORDER} = $data[WebMenu::MENU_ORDER];
                $webMenu->{WebMenu::MENU_URL} = $data[WebMenu::MENU_URL];

                if (isset($data[WebMenu::MENU_LV1]))
                    $webMenu->{WebMenu::MENU_LV1} = $data[WebMenu::MENU_LV1];
                else
                    $webMenu->{WebMenu::MENU_LV1} = null;

                if (isset($data[WebMenu::MENU_LV2]))
                    $webMenu->{WebMenu::MENU_LV2} = $data[WebMenu::MENU_LV2];
                else
                    $webMenu->{WebMenu::MENU_LV2} = null;

                $webMenu->{WebMenu::ACTIVE_STATUS} = isset($data[WebMenu::ACTIVE_STATUS]) ? 1 : 0;
                $webMenu->{WebMenu::ADMIN_UPDATED} = Auth::user()->admin_id;

                if ($webMenu->save()) {
                    return redirect()->route('admins.webmenu.create')->with('success', 'Tạo mới đối tượng thành công !');
                } else
                    return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!']);
            } catch (\Throwable $th) {
                $message = $th->getMessage();
                return view('admins.pages.auth.error_403', compact('message'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WebMenu  $webMenu
     * @return \Illuminate\Http\Response
     */
    public function show(WebMenuRequest $request)
    {
        try {
            $data = $request->all();

            $webMenu = DB::table('re_web_menus AS menuA')->select(
                'menuA.*',
                'menuB.menu_name AS menu_name_lv1',
                'menuC.menu_name AS menu_name_lv2',
                //Lay noi dung hinh anh cua admin
                're_administrators.admin_full_name'
            )
                ->leftJoin('re_web_menus AS menuB', 'menuB.menu_id', '=', 'menuA.menu_lv1')
                ->leftJoin('re_web_menus AS menuC', 'menuC.menu_id', '=', 'menuA.menu_lv2')
                ->leftJoin('re_administrators', 're_administrators.admin_id', '=', 'menuA.admin_updated')
                ->where('menuA.menu_id', $data['menu_id'])->first();

            if (isset($webMenu))
                return view('admins.pages.webmenu.show', compact('webMenu'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WebMenu  $webMenu
     * @return \Illuminate\Http\Response
     */
    public function edit(WebMenuRequest $request)
    {
        try {
            $data = $request->all();

            $webMenu = DB::table('re_web_menus')->where('menu_id', $data['menu_id'])->first();

            $menuA = DB::table('re_web_menus')->select('re_web_menus.menu_id', 're_web_menus.menu_name')
                ->whereNull('menu_lv1')->whereNull('menu_lv2')
                ->where([['menu_id', '<>', $data['menu_id']], ['active_status', 1]])
                ->orderBy('menu_order')->get();

            if (isset($webMenu))
                return view('admins.pages.webmenu.edit', compact('webMenu', 'menuA'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebMenu  $webMenu
     * @return \Illuminate\Http\Response
     */
    public function update(WebMenuRequest $request)
    {
        try {

            $data = $request->all();
            $param = array();

            $param[WebMenu::MENU_NAME] = $data[WebMenu::MENU_NAME];
            $param[WebMenu::MENU_ORDER] = $data[WebMenu::MENU_ORDER];
            $param[WebMenu::MENU_URL] = $data[WebMenu::MENU_URL];

            $param[WebMenu::MENU_LV1] = isset($data[WebMenu::MENU_LV1])?$data[WebMenu::MENU_LV1]:NULL;
            $param[WebMenu::MENU_LV2] = isset($data[WebMenu::MENU_LV2])?$data[WebMenu::MENU_LV2]:NULL;

            $param[WebMenu::ACTIVE_STATUS] = isset($data[WebMenu::ACTIVE_STATUS]) ? 1 : 0;
            $param[WebMenu::ADMIN_UPDATED] = Auth::user()->admin_id;
            $param[WebMenu::UPDATED_AT] = Carbon::now()->format('Y-m-d H:i:s');


            $webMenu = DB::table('re_web_menus')->where('menu_id', $data['menu_id']);
            if ($webMenu->update($param)) {
                return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
            } else
                return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $webMenuCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebMenuRequest $request)
    {
        try {

            $data = $request->all();

            //Kiem tra doi tuong co trong database khong
            $webMenu = WebMenu::withTrashed()->where('menu_id', $data['menu_id'])->firstOrFail();
            if (!isset($webMenu))
                return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);

            $deleted = WebMenu::where('menu_id', $data['menu_id'])->delete();
            if ($deleted)
                return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }


    /**
     * recovered the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $webMenuCategories
     * @return \Illuminate\Http\Response
     */
    public function recovered(WebMenuRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = WebMenu::withTrashed()->where('menu_id', $data['menu_id'])->restore();
            if ($deleted)
                return redirect()->back()->with('success', 'Phục hồi tượng thành công!');
            else
                return redirect()->back()->withErrors('Phục hồi tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $webMenuCategories
     * @return \Illuminate\Http\Response
     */
    public function delete(WebMenuRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = DB::table('re_web_menus')->where('menu_id', $data['menu_id'])->delete();
            if ($deleted)
                return redirect()->route('admins.webmenu.index')->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
    /**
     * Ho tro lay doi tuong khi change product type
     */
    public function changeCreate($level)
    {
        $data = DB::table('re_web_menus')->select('re_web_menus.menu_id', 're_web_menus.menu_name')
            ->whereNotNull('menu_lv1')->whereNull('menu_lv2')
            ->where([['menu_lv1', $level], ['active_status', 1]])->orderBy('menu_order')->get();
        echo json_encode($data);
        exit;
    }
    public function changeEdit($id, $level)
    {
        $data = DB::table('re_web_menus')->select('re_web_menus.menu_id', 're_web_menus.menu_name')
            ->whereNotNull('menu_lv1')->whereNull('menu_lv2')
            ->where([['menu_lv1', $level], ['menu_id', '<>', $id], ['active_status', 1]])
            ->orderBy('menu_order')->get();
        echo json_encode($data);
        exit;
    }
}
