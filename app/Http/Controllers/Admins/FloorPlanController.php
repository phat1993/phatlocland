<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\FloorPlanRequest;
use App\Models\Entities\FloorPlan;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FloorPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(FloorPlanRequest $request)
    {
        try {
            $house = DB::table('re_houses')->where('house_id', $request->id)->select('house_id', 'house_name')->first();
            $floorPlans  = DB::table('re_floor_plans')->where('house_id',  $request->id)->orderBy('updated_at', 'desc')->get();
            return view('admins.pages.housefloorplan.index', compact('house', 'floorPlans'));
        } catch (\Throwable $th) {

            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(FloorPlanRequest $request)
    {
        try {
            $house = DB::table('re_houses')->where('house_id', $request->id)->select('house_id', 'house_name')->first();
            $floorPlan = new FloorPlan();
            $floorPlan->house_gallery_order = 1;
            $floorPlan->active_status = 1;
            $floorPlan->house_id = $house->house_id;
            return view('admins.pages.housefloorplan.create', compact('floorPlan', 'house'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FloorPlanRequest $request)
    {

        if ($request->isMethod('post')) {

            $data = $request->all();
            $path = '';

            $validImg = ['file_input'  => 'image|required']; //|dimensions:width=800,height=500
            $customMessages = [
                'file_input.required' => 'Hình Ảnh: Bạn không được để trống.',
                'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
                // 'image.dimensions' => 'Hình Ảnh: Bạn cần chọn hình ảnh 800x500.',
            ];
            $request->validate($validImg, $customMessages);

            $path = $request->file('file_input')->store(
                'images/floorplans/' . $data[FloorPlan::HOUSE_ID],
                'public'
            );
            try {

                $floorPlan = new FloorPlan();
                $floorPlan->{FloorPlan::FLOOR_PLAN_NAME} = $data[FloorPlan::FLOOR_PLAN_NAME];
                $floorPlan->{FloorPlan::FLOOR_PLAN_IMAGE} =  $path;

                if (isset($data[FloorPlan::FLOOR_PLAN_SIZE]))
                    $floorPlan->{FloorPlan::FLOOR_PLAN_SIZE} = $data[FloorPlan::FLOOR_PLAN_SIZE];
                if (isset($data[FloorPlan::FLOOR_PLAN_ROOMS]))
                    $floorPlan->{FloorPlan::FLOOR_PLAN_ROOMS} = $data[FloorPlan::FLOOR_PLAN_ROOMS];
                if (isset($data[FloorPlan::FLOOR_PLAN_BARTHS]))
                    $floorPlan->{FloorPlan::FLOOR_PLAN_BARTHS} = $data[FloorPlan::FLOOR_PLAN_BARTHS];
                if (isset($data[FloorPlan::FLOOR_PLAN_PRICE]))
                    $floorPlan->{FloorPlan::FLOOR_PLAN_PRICE} = str_replace(',', '', $data[FloorPlan::FLOOR_PLAN_PRICE]);

                $floorPlan->{FloorPlan::HOUSE_ID} = $data[FloorPlan::HOUSE_ID];
                $floorPlan->{FloorPlan::ACTIVE_STATUS} = isset($data[FloorPlan::ACTIVE_STATUS]) ? 1 : 0;
                $floorPlan->{FloorPlan::ADMIN_UPDATED} = Auth::user()->admin_id;

                if ($floorPlan->save())
                    return redirect()->back()->with('success', 'Tạo mới đối tượng thành công !');
                else {
                    //Neu them ko duoc doi tuong ma da luu hinh anh roi thi xoa hinh anh
                    if ($path != '' && Storage::disk('public')->exists($path))
                        Storage::disk('public')->delete($path);

                    redirect()->back()->withInput($request->all())->withErrors('Lỗi server! Tạo mới thất bại');
                }
            } catch (\Throwable $th) {
                $message = $th->getMessage();
                return view('admins.pages.auth.error_403', compact('message'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FloorPlan  $floorPlan
     * @return \Illuminate\Http\Response
     */
    public function show(FloorPlanRequest $request)
    {
        try {
            $data = $request->all();
            //Lay doi tuong cua house business lien ket voi admins va updatemode de lay noi dung va hinh anh
            $floorPlans = DB::table('re_floor_plans')->select(
                're_floor_plans.*',
                //Lay noi dung hinh anh cua admin
                're_administrators.admin_full_name'
            )
                ->leftJoin('re_administrators', 're_administrators.admin_id', '=', 're_floor_plans.admin_updated')
                ->where('floor_plan_id', $data['floor_plan_id'])->first();

            if (isset($floorPlans))
                return view('admins.pages.housefloorplan.show', compact('floorPlans'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FloorPlan  $floorPlan
     * @return \Illuminate\Http\Response
     */
    public function edit(FloorPlanRequest $request)
    {
        try {
            $data = $request->all();

            $house = DB::table('re_houses')->where('house_id', $request->id)->select('house_id', 'house_name')->first();
            $floorPlan = DB::table('re_floor_plans')->where('floor_plan_id', $data['floor_plan_id'])->first();

            if (isset($floorPlan))
                return view('admins.pages.housefloorplan.edit', compact('floorPlan', 'house'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FloorPlan  $floorPlan
     * @return \Illuminate\Http\Response
     */
    public function update(FloorPlanRequest $request)
    {

        $data = $request->all();

        $param = array();
        $path = '';
        if ($request->hasFile('file_input')) {
            $validImg = ['file_input'  => 'image'];
            $customMessages = [
                'file_input.image' => 'Hình Ảnh: Bạn cần chọn file là hình ảnh.',
            ];
            $request->validate($validImg, $customMessages);

            $path = $request->file('file_input')->store(
                'images/floorplans/' . $data[FloorPlan::HOUSE_ID],
                'public'
            );
            //Luu Thanh cong hinh anh thi set cho bien
            if ($path != '')
                $param[FloorPlan::FLOOR_PLAN_IMAGE] = $path;
        }
        try {

            $param[FloorPlan::FLOOR_PLAN_NAME] = $data[FloorPlan::FLOOR_PLAN_NAME];
            $param[FloorPlan::FLOOR_PLAN_SIZE] = isset($data[FloorPlan::FLOOR_PLAN_SIZE]) ? $data[FloorPlan::FLOOR_PLAN_SIZE] : NULL;
            $param[FloorPlan::FLOOR_PLAN_ROOMS] = isset($data[FloorPlan::FLOOR_PLAN_ROOMS]) ? $data[FloorPlan::FLOOR_PLAN_ROOMS] : NULL;
            $param[FloorPlan::FLOOR_PLAN_BARTHS] = isset($data[FloorPlan::FLOOR_PLAN_BARTHS]) ? $data[FloorPlan::FLOOR_PLAN_BARTHS] : NULL;
            $param[FloorPlan::FLOOR_PLAN_PRICE] = isset($data[FloorPlan::FLOOR_PLAN_PRICE]) ? str_replace(',', '', $data[FloorPlan::FLOOR_PLAN_PRICE]) : NULL;
            $param[FloorPlan::ACTIVE_STATUS] = isset($data[FloorPlan::ACTIVE_STATUS]) ? 1 : 0;
            $param[FloorPlan::ADMIN_UPDATED] = Auth::user()->admin_id;
            $param[FloorPlan::UPDATED_AT] = Carbon::now()->format('Y-m-d H:i:s');

            $floorPlan = DB::table('re_floor_plans')->where('floor_plan_id', $data['floor_plan_id']);
            if ($floorPlan->update($param)) {

                //Neu cap nhat thanh cong ma co hinh anh moi thi xoa hinh anh cu
                if ($path != '' && Storage::disk('public')->exists($data[FloorPlan::FLOOR_PLAN_IMAGE]))
                    Storage::disk('public')->delete($data[FloorPlan::FLOOR_PLAN_IMAGE]);

                return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
            } else {
                //Neu luu anh ma ko luu dc doi tuong thi xoa
                if ($path != '' && Storage::disk('public')->exists($path))
                    Storage::disk('public')->delete($path);

                return redirect()->back()->withErrors(['Cập Nhật Thất Bại!']);
            }
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $floorPlanCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(FloorPlanRequest $request)
    {
        try {
            $data = $request->all();
            //Kiem tra doi tuong co trong database khong
            $img = DB::table('re_floor_plans')->where('floor_plan_id', $data['floor_plan_id'])->first();
            if (!isset($img))
                return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);


            $deleted = DB::table('re_floor_plans')->where('floor_plan_id', $data['floor_plan_id'])->delete();
            if ($deleted) {
                //Xoa anh
                if (Storage::disk('public')->exists($img->{FloorPlan::FLOOR_PLAN_IMAGE}))
                    Storage::disk('public')->delete($img->{FloorPlan::FLOOR_PLAN_IMAGE});

                return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
            } else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
}
