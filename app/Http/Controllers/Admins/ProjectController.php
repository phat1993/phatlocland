<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\ProjectRequest;
use App\Models\Entities\Project;
use App\Models\Entities\ProjectGallery;
use App\Models\ViewModels\Admins\ProjectAmenities;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


class ProjectController extends Controller
{
    const PER_PAGE = 20;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ProjectRequest $request)
    {
        try {
            $data = $request->all();
            $param = array();
            $perPage = self::PER_PAGE;
            $currentPage = LengthAwarePaginator::resolveCurrentPage();

            if (isset($data[Project::PROJECT_CATEGORY_ID])) {
                $param[] =  ['re_projects.project_category_id', $data[Project::PROJECT_CATEGORY_ID]];
            }
            if (isset($data[Project::PROJECT_NAME])) {
                $param[] =  [Project::PROJECT_NAME, 'like', '%' . $data[Project::PROJECT_NAME] . '%'];
            }

            if (isset($data[Project::PROJECT_OWN])) {
                $param[] =  [Project::PROJECT_OWN, 'like', '%' . $data[Project::PROJECT_OWN] . '%'];
            }

            if (isset($data[Project::PROJECT_ACREAGE])) {
                $param[] =  [Project::PROJECT_ACREAGE, 'like', '%' . $data[Project::PROJECT_ACREAGE] . '%'];
            }
            if (isset($data[Project::PROJECT_BLOCK])) {
                $param[] =  [Project::PROJECT_BLOCK, 'like', '%' . $data[Project::PROJECT_BLOCK] . '%'];
            }
            if (isset($data[Project::PROJECT_FLOOR])) {
                $param[] =  [Project::PROJECT_FLOOR, 'like', '%' . $data[Project::PROJECT_FLOOR] . '%'];
            }
            if (isset($data[Project::PROJECT_HOUSE])) {
                $param[] =  [Project::PROJECT_HOUSE, 'like', '%' . $data[Project::PROJECT_HOUSE] . '%'];
            }
            if (isset($data[Project::PROJECT_HOUSE_ACREAGE])) {
                $param[] =  [Project::PROJECT_HOUSE_ACREAGE, 'like', '%' . $data[Project::PROJECT_HOUSE_ACREAGE] . '%'];
            }

            if (isset($data[Project::ACTIVE_STATUS])) {
                if ($data[Project::ACTIVE_STATUS] == 1)
                    $param[] = ['re_projects.active_status', 1];
                elseif ($data[Project::ACTIVE_STATUS] == 2)
                    $param[] = ['re_projects.active_status', 0];
            }
            $projects = DB::table('re_projects')->where($param)->select(
                're_projects.project_id',
                're_projects.project_name',
                're_projects.project_own',
                're_projects.project_build_time',
                're_projects.project_acreage',
                're_projects.project_block',
                're_projects.project_floor',
                're_projects.project_house',
                're_projects.project_house_acreage',
                're_projects.web_title',
                're_projects.web_keywords',
                're_projects.web_description',
                're_projects.web_canonical',
                're_projects.active_status',
                //Lay noi dung cua danh muc
                're_project_categories.project_category_name',
                're_project_categories.web_canonical AS category_web_canonical',
            )
                ->leftJoin('re_project_categories', 're_project_categories.project_category_id', '=', 're_projects.project_category_id')
                ->orderByDesc('re_projects.updated_at')->paginate($perPage);
            $categories = DB::table('re_project_categories')->where('active_status', 1)->select('project_category_id', 'project_category_name')->get();

            return view('admins.pages.project.index', compact('projects', 'currentPage', 'perPage', 'categories'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ProjectRequest $request)
    {
        try {
            $project = new Project();
            $project->active_status = 1;
            $project->project_block = '30 Tòa';
            $project->project_floor = '20-30 Tầng';
            $project->project_house = '50-55 căn';
            $project->project_acreage = '50000m²';
            $project->project_house_acreage = '24-55m²';

            $categories = DB::table('re_project_categories')->where('active_status', 1)->select('project_category_id', 'project_category_name')->get();
            $amenities = DB::table('re_amenities')->where('active_status', 1)->select('amenity_id', 'amenity_name')->get();

            return view('admins.pages.project.create', compact('project', 'categories', 'amenities'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectRequest $request)
    {
        if ($request->isMethod('post')) {
            try {

                $data = $request->all();

                //Kiem tra duong dan moi co trung voi cac doi tuong da co
                $slug = Str::slug($data[Project::PROJECT_NAME]);
                if (DB::table('re_projects')->where(Project::WEB_CANONICAL, $slug)->count() > 0)
                    return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!', 'Tiêu đề dự án đã có trong cơ sở dữ liệu!']);

                //Xu ly tien nghi
                $selected = $request->project_amenities;
                $amenities = collect();
                if (isset($selected)) {

                    foreach ($selected as $value) {
                        $aty = DB::table('re_amenities')->where('amenity_id', $value)->first();
                        $amenity = new ProjectAmenities();
                        $amenity->{ProjectAmenities::AMENITY_ID} = $aty->amenity_id;
                        $amenity->{ProjectAmenities::AMENITY_NAME} = $aty->amenity_name;
                        $amenity->{ProjectAmenities::AMENITY_ICON} = $aty->amenity_icon;

                        $amenities->push($amenity);
                    }
                }
                // $json = ''.$amenities->toJson().'';
                // dump($json);
                // dd(json_decode($json));


                $project = new Project();
                //Reference Info
                $project->{Project::PROJECT_CATEGORY_ID} = $data[Project::PROJECT_CATEGORY_ID];
                //Object Info
                $project->{Project::PROJECT_NAME} = $data[Project::PROJECT_NAME];
                $project->{Project::PROJECT_DESCRIPTION} = $data[Project::PROJECT_DESCRIPTION];
                $project->{Project::PROJECT_DETAIL} = $data[Project::PROJECT_DETAIL];
                $project->{Project::PROJECT_AMENITIES} = '' . $amenities->toJson() . '';
                //Address
                $project->{Project::PROJECT_POSTAL_CODE} = $data[Project::PROJECT_POSTAL_CODE];
                $project->{Project::PROJECT_PROVINCIAL} = $data[Project::PROJECT_PROVINCIAL];
                $project->{Project::PROJECT_DISTRICT} = $data[Project::PROJECT_DISTRICT];
                $project->{Project::PROJECT_COMMUNE} = $data[Project::PROJECT_COMMUNE];
                $project->{Project::PROJECT_ADDRESS} = $data[Project::PROJECT_ADDRESS];
                $project->{Project::PROJECT_MAP_LOCATION} = $data[Project::PROJECT_MAP_LOCATION];
                //Info
                $project->{Project::PROJECT_OWN} = $data[Project::PROJECT_OWN];
                if (isset($data[Project::PROJECT_BUILD_TIME]))
                    $project->{Project::PROJECT_BUILD_TIME} = Carbon::createFromFormat('m/d/Y', $data[Project::PROJECT_BUILD_TIME])->format('Y-m-d');
                $project->{Project::PROJECT_BLOCK} = $data[Project::PROJECT_BLOCK];
                $project->{Project::PROJECT_FLOOR} = $data[Project::PROJECT_FLOOR];
                $project->{Project::PROJECT_HOUSE} = $data[Project::PROJECT_HOUSE];
                $project->{Project::PROJECT_ACREAGE} = $data[Project::PROJECT_ACREAGE];
                $project->{Project::PROJECT_HOUSE_ACREAGE} = $data[Project::PROJECT_HOUSE_ACREAGE];
                //Web Info
                $project->{Project::WEB_KEYWORDS} = $data[Project::WEB_KEYWORDS];
                $project->{Project::WEB_DESCRIPTION} = $data[Project::WEB_DESCRIPTION];
                $project->{Project::WEB_TITLE} = $data[Project::WEB_TITLE];
                $project->{Project::WEB_CANONICAL} = $slug;
                //General Info
                $project->{Project::ACTIVE_STATUS} = isset($data[Project::ACTIVE_STATUS]) ? 1 : 0;
                $project->{Project::ADMIN_UPDATED} = Auth::user()->admin_id;

                if ($project->save()) {
                    return redirect()->route('admins.project.create')->with('success', 'Tạo mới đối tượng thành công !');
                } else
                    return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!']);
            } catch (\Throwable $th) {
                $message = $th->getMessage();
                return view('admins.pages.auth.error_403', compact('message'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectRequest $request)
    {
        try {
            $data = $request->all();
            //Lay doi tuong cua house business lien ket voi admins va updatemode de lay noi dung va hinh anh
            $project = DB::table('re_projects')->select(
                're_projects.*',
                //Lay noi dung hinh anh cua admin
                're_administrators.admin_full_name',

                //Lay noi dung cua danh muc
                're_project_categories.project_category_name',
                're_project_categories.web_canonical AS category_web_canonical',
            )
                ->leftJoin('re_project_categories', 're_project_categories.project_category_id', '=', 're_projects.project_category_id')
                ->leftJoin('re_administrators', 're_administrators.admin_id', '=', 're_projects.admin_updated')
                ->where('project_id', $data['project_id'])->first();

            if (isset($project)) {
                $amenities = json_decode($project->project_amenities);
                $galleries = DB::table('re_project_galleries')->where([['project_id', $data['project_id']], ['active_status', 1]])->orderBy('project_gallery_order')->get();
                return view('admins.pages.project.show', compact('project', 'amenities', 'galleries'));
            } else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectRequest $request)
    {
        try {
            $data = $request->all();
            //Lay doi tuong cua house business lien ket voi admins va updatemode de lay noi dung va hinh anh
            $project = DB::table('re_projects')->where('project_id', $data['project_id'])->first();

            if (isset($project)) {
                $djson = json_decode($project->project_amenities);
                $categories = DB::table('re_project_categories')->where('active_status', 1)->select('project_category_id', 'project_category_name')->get();
                $amenities = DB::table('re_amenities')->where('active_status', 1)->select('amenity_id', 'amenity_name', 'amenity_detail AS checked')->get();
                //Kiem tra du lieu tien nghi duoc check
                foreach ($amenities as $item) {
                    $selected = '';
                    foreach ($djson as $ay) {
                        if ($item->amenity_id == $ay->amenity_id) {
                            $selected = 'selected';
                            break;
                        }
                    }
                    $item->checked = $selected;
                }
                if (isset($project->project_build_time)) {
                    $project->project_build_time = date('d/m/Y', strtotime($project->project_build_time));
                }
                return view('admins.pages.project.edit', compact('project', 'categories', 'amenities'));
            } else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectRequest $request)
    {
        try {

            $data = $request->all();

            //Kiem tra duong dan moi co trung voi cac doi tuong da co
            $slug = Str::slug($data[Project::PROJECT_NAME]);
            if (DB::table('re_projects')->where([
                ['project_id', '<>', $data['project_id']],
                [Project::WEB_CANONICAL, $slug]
            ])->count() > 0)
                return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!', 'Tiêu đề danh mục đã có trong cơ sở dữ liệu!']);

            //Xu ly tien nghi
            $selected = $request->project_amenities;
            $amenities = collect();
            if (isset($selected)) {

                foreach ($selected as $value) {
                    $aty = DB::table('re_amenities')->where('amenity_id', $value)->first();
                    $amenity = new ProjectAmenities();
                    $amenity->{ProjectAmenities::AMENITY_ID} = $aty->amenity_id;
                    $amenity->{ProjectAmenities::AMENITY_NAME} = $aty->amenity_name;
                    $amenity->{ProjectAmenities::AMENITY_ICON} = $aty->amenity_icon;

                    $amenities->push($amenity);
                }
            }
            $param = array();
            //Reference Info
            $param[Project::PROJECT_CATEGORY_ID] = $data[Project::PROJECT_CATEGORY_ID];
            //Object Info
            $param[Project::PROJECT_NAME] = $data[Project::PROJECT_NAME];
            $param[Project::PROJECT_DESCRIPTION] = $data[Project::PROJECT_DESCRIPTION];
            $param[Project::PROJECT_DETAIL] = $data[Project::PROJECT_DETAIL];
            $param[Project::PROJECT_AMENITIES] = '' . $amenities->toJson() . '';
            //Address
            $param[Project::PROJECT_POSTAL_CODE] = $data[Project::PROJECT_POSTAL_CODE];
            $param[Project::PROJECT_PROVINCIAL] = $data[Project::PROJECT_PROVINCIAL];
            $param[Project::PROJECT_DISTRICT] = $data[Project::PROJECT_DISTRICT];
            $param[Project::PROJECT_COMMUNE] = $data[Project::PROJECT_COMMUNE];
            $param[Project::PROJECT_ADDRESS] = $data[Project::PROJECT_ADDRESS];
            $param[Project::PROJECT_MAP_LOCATION] = $data[Project::PROJECT_MAP_LOCATION];
            //Info
            $param[Project::PROJECT_OWN] = $data[Project::PROJECT_OWN];
            $param[Project::PROJECT_BUILD_TIME] = isset($data[Project::PROJECT_BUILD_TIME]) ? Carbon::createFromFormat('m/d/Y', $data[Project::PROJECT_BUILD_TIME])->format('Y-m-d') : NULL;
            $param[Project::PROJECT_BLOCK] = $data[Project::PROJECT_BLOCK];
            $param[Project::PROJECT_FLOOR] = $data[Project::PROJECT_FLOOR];
            $param[Project::PROJECT_HOUSE] = $data[Project::PROJECT_HOUSE];
            $param[Project::PROJECT_ACREAGE] = $data[Project::PROJECT_ACREAGE];
            $param[Project::PROJECT_HOUSE_ACREAGE] = $data[Project::PROJECT_HOUSE_ACREAGE];
            //Web Info
            $param[Project::WEB_KEYWORDS] = $data[Project::WEB_KEYWORDS];
            $param[Project::WEB_DESCRIPTION] = $data[Project::WEB_DESCRIPTION];
            $param[Project::WEB_TITLE] = $data[Project::WEB_TITLE];
            $param[Project::WEB_CANONICAL] =  $slug;

            //General Info
            $param[Project::ACTIVE_STATUS] = isset($data[Project::ACTIVE_STATUS]) ? 1 : 0;
            $param[Project::ADMIN_UPDATED] = Auth::user()->admin_id;
            $param[Project::UPDATED_AT] = Carbon::now()->format('Y-m-d H:i:s');

            $project = DB::table('re_projects')->where('project_id', $data['project_id']);
            if ($project->update($param)) {
                return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
            } else
                return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Productjects  $projectjects
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectRequest $request)
    {
        try {

            $data = $request->all();

            //Kiem tra doi tuong co trong database khong
            $project = Project::withTrashed()->where('project_id', $data['project_id'])->firstOrFail();
            if (!isset($project))
                return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);

            $deleted = Project::where('project_id', $data['project_id'])->delete();
            if ($deleted)
                return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }


    /**
     * recovered the specified resource from storage.
     *
     * @param  \App\Models\Productjects  $projectjects
     * @return \Illuminate\Http\Response
     */
    public function recovered(ProjectRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = Project::withTrashed()->where('project_id', $data['project_id'])->restore();
            if ($deleted)
                return redirect()->back()->with('success', 'Phục hồi tượng thành công!');
            else
                return redirect()->back()->withErrors('Phục hồi tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\Productjects  $projectjects
     * @return \Illuminate\Http\Response
     */
    public function delete(ProjectRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = DB::table('re_projects')->where('project_id', $data['project_id'])->delete();
            if ($deleted) {
                //xoa hinh anh

                $path = 'images/projects/' . $data[ProjectGallery::PROJECT_ID];
                if (Storage::disk('public')->exists($path))
                    Storage::disk('public')->deleteDirectory($path);

                DB::table('re_project_galleries')->where('project_id', $data['project_id'])->delete();
                // $imgs

                return redirect()->route('admins.project.index')->with('success', 'Xóa đối tượng thành công!');
            } else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
}
