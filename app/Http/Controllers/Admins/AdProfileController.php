<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdProfileController extends Controller
{
    public function profile(Request $request)
    {
        return view('admins.pages.profile.profile');
    }
}
