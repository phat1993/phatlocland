<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\AmenityRequest;
use App\Models\Entities\Amenities;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AmenitiesController extends Controller
{
    const PER_PAGE = 20;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AmenityRequest $request)
    {
        try {
            $data = $request->all();
            $param = array();
            $perPage = self::PER_PAGE;
            $currentPage = LengthAwarePaginator::resolveCurrentPage();

            if (isset($data[Amenities::AMENITY_NAME])) {
                $param[] =  [Amenities::AMENITY_NAME, 'like', '%' . $data[Amenities::AMENITY_NAME] . '%'];
            }
            if (isset($data[Amenities::AMENITY_DETAIL])) {
                $param[] =  [Amenities::AMENITY_DETAIL, 'like', '%' . $data[Amenities::AMENITY_DETAIL] . '%'];
            }
            if (isset($data[Amenities::ACTIVE_STATUS])) {
                if ($data[Amenities::ACTIVE_STATUS] == 1)
                    $param[] = [Amenities::ACTIVE_STATUS, 1];
                elseif ($data[Amenities::ACTIVE_STATUS] == 2)
                    $param[] = [Amenities::ACTIVE_STATUS, 0];
            }

            $amenities = DB::table('re_amenities')
            ->select(
                're_amenities.amenity_id',
                're_amenities.amenity_icon',
                're_amenities.amenity_name',
                're_amenities.amenity_detail',
                're_amenities.active_status',
            )
            ->where($param)->orderByDesc('updated_at')->paginate($perPage);
            return view('admins.pages.amenity.index', compact('amenities', 'currentPage', 'perPage'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(AmenityRequest $request)
    {
        try {
            $amenity = new Amenities();
            $amenity->active_status = 1;
            return view('admins.pages.amenity.create', compact('amenity'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AmenityRequest $request)
    {
        if ($request->isMethod('post')) {
            try {

                $data = $request->all();

                //Kiem tra xem ten tien nghi moi co trong database chua
                if (DB::table('re_amenities')->where(Amenities::AMENITY_NAME, $data[Amenities::AMENITY_NAME])->count() > 0)
                    return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!', 'Tiêu đề tiện nghi đã có trong cơ sở dữ liệu!']);

                $amenity = new Amenities();
                $amenity->{Amenities::AMENITY_NAME} = $data[Amenities::AMENITY_NAME];
                $amenity->{Amenities::AMENITY_ICON} = $data[Amenities::AMENITY_ICON];
                $amenity->{Amenities::AMENITY_DETAIL} = $data[Amenities::AMENITY_DETAIL];
                $amenity->{Amenities::ACTIVE_STATUS} = isset($data[Amenities::ACTIVE_STATUS]) ? 1 : 0;
                $amenity->{Amenities::ADMIN_UPDATED} = Auth::user()->admin_id;

                if ($amenity->save()) {
                    return redirect()->route('admins.amenity.create')->with('success', 'Tạo mới đối tượng thành công !');
                } else
                    return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!']);
            } catch (\Throwable $th) {
                $message = $th->getMessage();
                return view('admins.pages.auth.error_403', compact('message'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Amenity  $amenity
     * @return \Illuminate\Http\Response
     */
    public function show(AmenityRequest $request)
    {
        try {
            $data = $request->all();

            $amenity = DB::table('re_amenities')->select(
                're_amenities.*',
                //Lay noi dung hinh anh cua admin
                're_administrators.admin_full_name'
            )
                ->leftJoin('re_administrators', 're_administrators.admin_id', '=', 're_amenities.admin_updated')
                ->where('amenity_id', $data['amenity_id'])->first();

            if (isset($amenity))
                return view('admins.pages.amenity.show', compact('amenity'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Amenity  $amenity
     * @return \Illuminate\Http\Response
     */
    public function edit(AmenityRequest $request)
    {
        try {
            $data = $request->all();

            $amenity = DB::table('re_amenities')->where('amenity_id', $data['amenity_id'])->first();

            if (isset($amenity))
                return view('admins.pages.amenity.edit', compact('amenity'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Amenity  $amenity
     * @return \Illuminate\Http\Response
     */
    public function update(AmenityRequest $request)
    {
        try {

            $data = $request->all();
            //Kiem tra xem ten tien nghi moi co trong database chua
            if (DB::table('re_amenities')->where([
                ['amenity_id', '<>', $data['amenity_id']],
                [Amenities::AMENITY_NAME, $data[Amenities::AMENITY_NAME]]
            ])->count() > 0)
                return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!', 'Tiêu đề danh mục đã có trong cơ sở dữ liệu!']);

            $param = array();

            $param[Amenities::AMENITY_NAME] = $data[Amenities::AMENITY_NAME];
            $param[Amenities::AMENITY_ICON] = $data[Amenities::AMENITY_ICON];
            $param[Amenities::AMENITY_DETAIL] = $data[Amenities::AMENITY_DETAIL];
            $param[Amenities::ACTIVE_STATUS] = isset($data[Amenities::ACTIVE_STATUS]) ? 1 : 0;
            $param[Amenities::ADMIN_UPDATED] = Auth::user()->admin_id;
            $param[Amenities::UPDATED_AT] = Carbon::now()->format('Y-m-d H:i:s');


            $amenity = DB::table('re_amenities')->where('amenity_id', $data['amenity_id']);
            if ($amenity->update($param)) {
                return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
            } else
                return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $amenityCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(AmenityRequest $request)
    {
        try {

            $data = $request->all();

            //Kiem tra doi tuong co trong database khong
            $amenity = Amenities::withTrashed()->where('amenity_id', $data['amenity_id'])->firstOrFail();
            if (!isset($amenity))
                return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);

            $deleted = Amenities::where('amenity_id', $data['amenity_id'])->delete();
            if ($deleted)
                return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }


    /**
     * recovered the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $amenityCategories
     * @return \Illuminate\Http\Response
     */
    public function recovered(AmenityRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = Amenities::withTrashed()->where('amenity_id', $data['amenity_id'])->restore();
            if ($deleted)
                return redirect()->back()->with('success', 'Phục hồi tượng thành công!');
            else
                return redirect()->back()->withErrors('Phục hồi tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $amenityCategories
     * @return \Illuminate\Http\Response
     */
    public function delete(AmenityRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = DB::table('re_amenities')->where('amenity_id', $data['amenity_id'])->delete();
            if ($deleted)
                return redirect()->route('admins.amenity.index')->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
}
