<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admins\WebInfoRequest;
use App\Models\Entities\WebInfo;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class WebInfoController extends Controller
{
    const PER_PAGE = 20;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(WebInfoRequest $request)
    {
        try {
            $data = $request->all();
            $param = array();
            $perPage = self::PER_PAGE;
            $currentPage = LengthAwarePaginator::resolveCurrentPage();

            if (isset($data[WebInfo::WEB_INFO_TITLE])) {
                $param[] =  [WebInfo::WEB_INFO_TITLE, 'like', '%' . $data[WebInfo::WEB_INFO_TITLE] . '%'];
            }
            if (isset($data[WebInfo::WEB_INFO_PHONE])) {
                $param[] =  [WebInfo::WEB_INFO_PHONE, 'like', '%' . $data[WebInfo::WEB_INFO_PHONE] . '%'];
            }
            if (isset($data[WebInfo::WEB_INFO_MAIL])) {
                $param[] =  [WebInfo::WEB_INFO_MAIL, 'like', '%' . $data[WebInfo::WEB_INFO_MAIL] . '%'];
            }
            if (isset($data[WebInfo::WEB_INFO_ADDRESS])) {
                $param[] =  [WebInfo::WEB_INFO_ADDRESS, 'like', '%' . $data[WebInfo::WEB_INFO_ADDRESS] . '%'];
            }
            if (isset($data[WebInfo::ACTIVE_STATUS])) {
                if ($data[WebInfo::ACTIVE_STATUS] == 1)
                    $param[] = [WebInfo::ACTIVE_STATUS, 1];
                elseif ($data[WebInfo::ACTIVE_STATUS] == 2)
                    $param[] = [WebInfo::ACTIVE_STATUS, 0];
            }
            $webInfos  = DB::table('re_web_infos')->where($param)
                ->select(
                    're_web_infos.web_info_id',
                    're_web_infos.web_info_title',
                    're_web_infos.web_info_phone',
                    're_web_infos.web_info_mail',
                    're_web_infos.web_info_address',
                    're_web_infos.active_status',
                )
                ->orderByDesc('updated_at')->paginate($perPage);
            return view('admins.pages.webinfo.index', compact('webInfos', 'currentPage', 'perPage'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(WebInfoRequest $request)
    {
        try {
            $webInfo = new WebInfo();
            $webInfo->active_status = 1;
            $webInfo->web_canonical = route('clients.default');

            return view('admins.pages.webinfo.create', compact('webInfo'));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WebInfoRequest $request)
    {
        if ($request->isMethod('post')) {
            try {

                $data = $request->all();

                $webInfo = new WebInfo();
                $webInfo->{WebInfo::WEB_INFO_TITLE} = $data[WebInfo::WEB_INFO_TITLE];
                $webInfo->{WebInfo::WEB_INFO_PHONE} = $data[WebInfo::WEB_INFO_PHONE];
                $webInfo->{WebInfo::WEB_INFO_MAIL} = $data[WebInfo::WEB_INFO_MAIL];
                $webInfo->{WebInfo::WEB_INFO_ADDRESS} = $data[WebInfo::WEB_INFO_ADDRESS];
                $webInfo->{WebInfo::WEB_INFO_WORK_TIME} = $data[WebInfo::WEB_INFO_WORK_TIME];
                $webInfo->{WebInfo::WEB_INFO_MAP_LOCATION} = $data[WebInfo::WEB_INFO_MAP_LOCATION];
                $webInfo->{WebInfo::WEB_INFO_SPECIAL_NOTE} = $data[WebInfo::WEB_INFO_SPECIAL_NOTE];

                $webInfo->{WebInfo::WEB_KEYWORDS} = $data[WebInfo::WEB_KEYWORDS];
                $webInfo->{WebInfo::WEB_TITLE} = $data[WebInfo::WEB_TITLE];
                $webInfo->{WebInfo::WEB_DESCRIPTION} = $data[WebInfo::WEB_DESCRIPTION];
                $webInfo->{WebInfo::WEB_CANONICAL} = $data[WebInfo::WEB_CANONICAL];

                if (isset($data[WebInfo::WEB_INFO_ZALO]))
                    $webInfo->{WebInfo::WEB_INFO_ZALO} = $data[WebInfo::WEB_INFO_ZALO];
                if (isset($data[WebInfo::WEB_INFO_FACEBOOK]))
                    $webInfo->{WebInfo::WEB_INFO_FACEBOOK} = $data[WebInfo::WEB_INFO_FACEBOOK];
                if (isset($data[WebInfo::WEB_INFO_YOUTUBE]))
                    $webInfo->{WebInfo::WEB_INFO_YOUTUBE} = $data[WebInfo::WEB_INFO_YOUTUBE];
                if (isset($data[WebInfo::WEB_INFO_INSTAGRAM]))
                    $webInfo->{WebInfo::WEB_INFO_INSTAGRAM} = $data[WebInfo::WEB_INFO_INSTAGRAM];

                $webInfo->{WebInfo::ACTIVE_STATUS} = isset($data[WebInfo::ACTIVE_STATUS]) ? 1 : 0;
                $webInfo->{WebInfo::ADMIN_UPDATED} = Auth::user()->admin_id;

                if ($webInfo->save()) {
                    return redirect()->route('admins.webinfo.create')->with('success', 'Tạo mới đối tượng thành công !');
                } else
                    return redirect()->back()->withInput()->withErrors(['Tạo mới đối tượng Thất Bại!']);
            } catch (\Throwable $th) {
                $message = $th->getMessage();
                return view('admins.pages.auth.error_403', compact('message'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WebInfo  $webInfo
     * @return \Illuminate\Http\Response
     */
    public function show(WebInfoRequest $request)
    {
        try {
            $data = $request->all();

            $webInfo = DB::table('re_web_infos')->select(
                're_web_infos.*',
                //Lay noi dung hinh anh cua admin
                're_administrators.admin_full_name'
            )
                ->leftJoin('re_administrators', 're_administrators.admin_id', '=', 're_web_infos.admin_updated')
                ->where('re_web_infos.web_info_id', $data['web_info_id'])->first();

            if (isset($webInfo))
                return view('admins.pages.webinfo.show', compact('webInfo'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WebInfo  $webInfo
     * @return \Illuminate\Http\Response
     */
    public function edit(WebInfoRequest $request)
    {
        try {
            $data = $request->all();

            $webInfo = DB::table('re_web_infos')->where('web_info_id', $data['web_info_id'])->first();
            if (isset($webInfo))
                return view('admins.pages.webinfo.edit', compact('webInfo'));
            else
                return redirect()->back()->withErrors(['Lỗi Không Tìm Thấy Đối Tượng!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebInfo  $webInfo
     * @return \Illuminate\Http\Response
     */
    public function update(WebInfoRequest $request)
    {
        try {

            $data = $request->all();
            $param = array();

            $param[WebInfo::WEB_INFO_TITLE] = $data[WebInfo::WEB_INFO_TITLE];
            $param[WebInfo::WEB_INFO_PHONE] = $data[WebInfo::WEB_INFO_PHONE];
            $param[WebInfo::WEB_INFO_MAIL] = $data[WebInfo::WEB_INFO_MAIL];
            $param[WebInfo::WEB_INFO_ADDRESS] = $data[WebInfo::WEB_INFO_ADDRESS];
            $param[WebInfo::WEB_INFO_WORK_TIME] = $data[WebInfo::WEB_INFO_WORK_TIME];
            $param[WebInfo::WEB_INFO_MAP_LOCATION] = $data[WebInfo::WEB_INFO_MAP_LOCATION];
            $param[WebInfo::WEB_INFO_SPECIAL_NOTE] = $data[WebInfo::WEB_INFO_SPECIAL_NOTE];

            $param[WebInfo::WEB_KEYWORDS] = $data[WebInfo::WEB_KEYWORDS];
            $param[WebInfo::WEB_TITLE] = $data[WebInfo::WEB_TITLE];
            $param[WebInfo::WEB_DESCRIPTION] = $data[WebInfo::WEB_DESCRIPTION];
            $param[WebInfo::WEB_CANONICAL] = $data[WebInfo::WEB_CANONICAL];

            $param[WebInfo::WEB_INFO_ZALO] = isset($data[WebInfo::WEB_INFO_ZALO])?$data[WebInfo::WEB_INFO_ZALO]:NULL;
            $param[WebInfo::WEB_INFO_FACEBOOK] = isset($data[WebInfo::WEB_INFO_FACEBOOK])?$data[WebInfo::WEB_INFO_FACEBOOK]:NULL;
            $param[WebInfo::WEB_INFO_YOUTUBE] = isset($data[WebInfo::WEB_INFO_YOUTUBE])?$data[WebInfo::WEB_INFO_YOUTUBE]:NULL;
            $param[WebInfo::WEB_INFO_INSTAGRAM] = isset($data[WebInfo::WEB_INFO_INSTAGRAM])?$data[WebInfo::WEB_INFO_INSTAGRAM]:NULL;

            $param[WebInfo::ACTIVE_STATUS] = isset($data[WebInfo::ACTIVE_STATUS]) ? 1 : 0;
            $param[WebInfo::ADMIN_UPDATED] = Auth::user()->admin_id;
            $param[WebInfo::UPDATED_AT] = Carbon::now()->format('Y-m-d H:i:s');


            $webInfo = DB::table('re_web_infos')->where('web_info_id', $data['web_info_id']);
            if ($webInfo->update($param)) {
                return redirect()->back()->with('success', 'Cập nhật đối tượng thành công !');
            } else
                return redirect()->back()->withInput()->withErrors(['Cập Nhật Thất Bại!']);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $webInfoCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebInfoRequest $request)
    {
        try {

            $data = $request->all();

            //Kiem tra doi tuong co trong database khong
            $webInfo = WebInfo::withTrashed()->where('web_info_id', $data['web_info_id'])->firstOrFail();
            if (!isset($webInfo))
                return redirect()->back()->withErrors(['Không tìm thấy đối tượng muốn xóa!']);

            $deleted = WebInfo::where('web_info_id', $data['web_info_id'])->delete();
            if ($deleted)
                return redirect()->back()->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }


    /**
     * recovered the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $webInfoCategories
     * @return \Illuminate\Http\Response
     */
    public function recovered(WebInfoRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = WebInfo::withTrashed()->where('web_info_id', $data['web_info_id'])->restore();
            if ($deleted)
                return redirect()->back()->with('success', 'Phục hồi tượng thành công!');
            else
                return redirect()->back()->withErrors('Phục hồi tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }

    /**
     * delete the specified resource from storage.
     *
     * @param  \App\Models\ProductCategories  $webInfoCategories
     * @return \Illuminate\Http\Response
     */
    public function delete(WebInfoRequest $request)
    {
        try {
            $data = $request->all();

            $deleted = DB::table('re_web_infos')->where('web_info_id', $data['web_info_id'])->delete();
            if ($deleted)
                return redirect()->route('admins.webinfo.index')->with('success', 'Xóa đối tượng thành công!');
            else
                return redirect()->back()->withErrors('Xóa đối tượng thất bại!');
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return view('admins.pages.auth.error_403', compact('message'));
        }
    }
    /**
     * Ho tro lay doi tuong khi change product type
     */
    public function changeCreate($level)
    {
        $data = DB::table('re_web_infos')->select('re_web_infos.web_info_id', 're_web_infos.menu_name')
            ->whereNotNull('menu_lv1')->whereNull('menu_lv2')
            ->where([['menu_lv1', $level], ['active_status', 1]])->orderBy('menu_order')->get();
        echo json_encode($data);
        exit;
    }
    public function changeEdit($id, $level)
    {
        $data = DB::table('re_web_infos')->select('re_web_infos.web_info_id', 're_web_infos.menu_name')
            ->whereNotNull('menu_lv1')->whereNull('menu_lv2')
            ->where([['menu_lv1', $level], ['web_info_id', '<>', $id], ['active_status', 1]])
            ->orderBy('menu_order')->get();
        echo json_encode($data);
        exit;
    }
}
