<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use App\Models\Entities\Project;
use App\Models\Helpers\FunctionHelper;
use App\Models\ViewModels\Clients\ClientProjectCategory;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use stdClass;

class ClientProjectController extends Controller
{
    //So luong doi tuong hien thi trong 1 page
    const PER_PAGE = 6;

    //////////////////////////
    /*Main method*/

    // hien thi danh sach cac du an
    public function list(Request $request)
    {
        try {
            //Menu
            $menuTops = FunctionHelper::topMenu();
            $menuBots = FunctionHelper::botMenu();
            $webInfo = FunctionHelper::webInfo();
            //doi tuong danh muc
            $category = null;
            $perPage = self::PER_PAGE;
            $sortObj = 're_projects.updated_at';
            $sort = 'asc';
            // $sort = 'desc';

            //filter
            $param = array();
            $paramOr = array();
            $param[] = ['re_projects.active_status', 1];
            $provincials = $this->getProvincials();
            if (isset($request->sort)) {
                if ($request->sort == 'new') {
                    $sortObj = 're_projects.updated_at';
                    $sort = 'asc';
                } else if ($request->sort == 'old') {
                    $sortObj = 're_projects.updated_at';
                    $sort = 'desc';
                } else if ($request->sort == 'trend') {
                    $sortObj = 're_projects.project_viewed';
                    $sort = 'asc';
                }
            } else
                $request->sort = 'new';
            if (isset($request->provincial)) {
                $param[] = ['re_projects.project_provincial', $request->provincial];
            }
            if (isset($request->district)) {
                $param[] = ['re_projects.project_district', $request->district];
            }
            if (isset($request->address)) {
                $param[] = ['re_projects.project_name', 'like', '%' . $request->address . '%'];
                $paramOr[] = ['re_projects.project_commune', 'like', '%' . $request->address . '%'];
                $paramOr[] = ['re_projects.project_address', 'like', '%' . $request->address . '%'];
            }

            //Lay du lieu tu databae
            if (isset($request->category)) {
                $category = DB::table('re_project_categories')->where([['active_status', 1], ['web_canonical', $request->category]])->first();
                if ($category != null)
                    $param[] = ['re_projects.project_category_id', $category->project_category_id];
            }

            //doi tuong danh sach du an
            $dbProjects = $this->paginateProject($param, $paramOr, $perPage, $sortObj, $sort);

            $projects = collect();
            //Xu ly du lieu lay duoc
            if ($dbProjects->total() > 0) {
                foreach ($dbProjects as $item) {
                    $pro = $this->setProject($item);
                    $projects->add($pro);
                }
            }


            return view('clients.pages.project.list', compact(
                'menuTops',
                'menuBots',
                'webInfo',
                'provincials',

                'projects',
                'dbProjects',
                'category'
            ));
        } catch (\Throwable $th) {
            //develop
            // dd($th->getMessage());
        }
        abort(404);
    }

    //hien thi chi tiet cua du an
    public function detail(Request $request)
    {
        try {
            $menuTops = FunctionHelper::topMenu();
            $menuBots = FunctionHelper::botMenu();
            $webInfo = FunctionHelper::webInfo();

            //param: category, project
            if (isset($request->project) && isset($request->category)) {
                $category = DB::table('re_project_categories')->where([['active_status', 1], ['web_canonical', $request->category]])->first();

                $project = DB::table('re_projects')->where([
                    ['re_projects.active_status', 1],
                    ['web_canonical', $request->project],
                    ['re_projects.project_category_id', $category->project_category_id]
                ])->first();
                if (isset($project)) {
                    //Update luot view du an
                    $updated = DB::table('re_projects')->where('project_id', $project->project_id);
                    $param = array();
                    $param['project_viewed'] = $project->project_viewed + 1;
                    $updated->update($param);
                    //Lay danh sach hinh anh
                    $galleries = DB::table('re_project_galleries')->where(
                        [
                            ['active_status', 1],
                            ['project_id', $project->project_id]
                        ]
                    )->orderBy('project_gallery_order')->get();

                    $banner = $galleries->where('project_gallery_banner', 1)->first();
                    $amenities = json_decode($project->project_amenities);
                    //cho phat trien
                    // $floorPlans = null;
                    // $locationNearbies = null;
                    // $comments = null;

                    // cac danh sach ho tro
                    //da xem
                    $vieweds = $this->projectViewed($category, $project);
                    //noi bat
                    $features = $this->listProjectFeature();
                    //moi them
                    $addeds = $this->listProjectRecentlyAdded();

                    return view(
                        'clients.pages.project.detail',
                        compact(
                            'menuTops',
                            'menuBots',
                            'webInfo',
                            'project',
                            'category',
                            'galleries',
                            'banner',
                            'amenities',
                            // 'floorPlan',
                            // 'locationNearbies',
                            // 'comments',
                            'vieweds',
                            'features',
                            'addeds'
                        )
                    );
                }
            }
        } catch (\Throwable $th) {
            //     //develop
                // dd($th->getMessage());
        }

        abort(404);
    }


    //////////////////////////
    /*Helper filter*/

    //Lay danh sach cac tinh thanh co trong du an
    protected function getProvincials()
    {
        ////Lay danh sach tinh them cho filter
        // $provincials = collect();
        // if (!session()->has('filterProvincials')) {
        $provincials = DB::table('re_projects')->where('active_status', 1)
            ->select('project_provincial')->distinct()->get();
        // session()->put('filterProvincials', $provincials);
        // } else
        //     $provincials = session('filterProvincials');
        return $provincials;
    }
    //Lay danh sach cac quan huyen nam trong du an thuoc ve tinh thanh
    //tra ve json
    public function changeProvincial($provincial)
    {
        $districts = DB::table('re_projects')->where([
            ['active_status', 1],
            ['project_provincial', $provincial]
        ])->select('project_district')->distinct()->get();
        $data = collect();
        foreach ($districts as $item) {
            $data->add($item->project_district);
        }
        echo json_encode($data);
        exit;
    }


    //////////////////////////
    /*ham ho tro cho function list*/

    //Tao doi tuong view cho danh sach
    //tra ve doi tuong view
    protected function setProject($item)
    {
        $pro = new ClientProjectCategory();
        $pro->{ClientProjectCategory::LINK} = 'du-an/' . $item->category_web_canonical . '/' . $item->web_canonical;
        $pro->{ClientProjectCategory::FEATURES} = array();
        //Lay danh sach hinh anh
        $pro->{ClientProjectCategory::GALLERIES} = DB::table('re_project_galleries')->where(
            [
                ['active_status', 1],
                ['project_id', $item->project_id]
            ]
        )->orderBy('project_gallery_order')->get();

        //Anh dai dien
        $represent = $pro->{ClientProjectCategory::GALLERIES}->where('project_gallery_banner', 2)->first();
        $pro->{ClientProjectCategory::IMAGE} =   $represent != null ? $represent : null;

        $pro->{ClientProjectCategory::TITLE} = $item->project_name;
        $pro->{ClientProjectCategory::TIME} = $item->project_build_time;
        $pro->{ClientProjectCategory::BLOCK} = $item->project_block;
        $pro->{ClientProjectCategory::FLOOR} = $item->project_floor;
        $pro->{ClientProjectCategory::ACREAGE} = $item->project_acreage;
        $pro->{ClientProjectCategory::HOUSE} = $item->project_house;
        $pro->{ClientProjectCategory::MAP} = $item->project_map_location;

        $area = strlen($item->project_address) > 1 ? $item->project_address . ', ' : '';
        $pro->{ClientProjectCategory::ADDRESS} = $area . $item->project_commune . ', ' . $item->project_district . ', ' . $item->project_provincial;
        $pro->{ClientProjectCategory::BEFORE_HOUR} = now()->diffInHours($item->updated_at);
        $pro->{ClientProjectCategory::BEFORE_DAY} = now()->diffInDays($item->updated_at);
        return $pro;
    }

    //Phan trang cung voi filter
    protected function paginateProject($param, $paramOr, $perPage, $sortObj, $sort)
    {
        $dbProjects = DB::table('re_projects')->where($param)->orWhere($paramOr)
            ->select(
                're_projects.project_id',
                're_projects.project_name',
                're_projects.project_build_time',
                're_projects.project_block',
                're_projects.project_floor',
                're_projects.project_acreage',
                're_projects.project_house',
                // DB::Raw("CONCAT(re_projects.project_address,re_projects.project_commune,
                // re_projects.project_district,re_projects.project_provincial AS address"),
                're_projects.project_provincial',
                're_projects.project_district',
                're_projects.project_commune',
                're_projects.project_address',
                're_projects.project_map_location',

                're_projects.web_canonical',
                're_project_categories.web_canonical AS category_web_canonical',
                're_projects.updated_at',

            )
            ->leftJoin('re_project_categories', 're_project_categories.project_category_id', '=', 're_projects.project_category_id')
            ->orderByDesc($sortObj, $sort)->paginate($perPage);
        return $dbProjects;
    }

    //////////////////////////
    /*ham ho tro cho function detail*/

    //luu lai cac doi tuong da xem
    protected function projectViewed($category, $project)
    {
        //Tao session luu project viewed
        if (!session()->has('projectViewed'))
            session()->put('projectViewed', collect());

        $viewed = session('projectViewed');
        if ($viewed != null && $viewed->where('id', $project->project_id)->count() <= 0) {

            $pro = $this->setViewProject($category, $project);
            $viewed->add($pro);
        }
        session()->put('projectViewed', $viewed);

        return $viewed;
    }


    //Tao doi tuong view cho cac danh sach feature, viewed, added
    protected function setViewProject($category, $project)
    {
        $pro = new ClientProjectCategory();
        $pro->{ClientProjectCategory::LINK} = 'du-an/' . $category->web_canonical . '/' . $project->web_canonical;
        $pro->{ClientProjectCategory::FEATURES} = array();
        //Lay danh sach hinh anh
        $pro->{ClientProjectCategory::GALLERIES} = DB::table('re_project_galleries')->where(
            [
                ['active_status', 1],
                ['project_id', $project->project_id]
            ]
        )->orderBy('project_gallery_order')->get();
        //Anh dai dien
        $represent = $pro->{ClientProjectCategory::GALLERIES}->where('project_gallery_banner', 2)->first();
        $pro->{ClientProjectCategory::IMAGE} =   $represent != null ? $represent : null;

        $pro->{ClientProjectCategory::ID} = $project->project_id;
        $pro->{ClientProjectCategory::TITLE} = $project->project_name;
        $pro->{ClientProjectCategory::TIME} = $project->project_build_time;
        $pro->{ClientProjectCategory::BLOCK} = $project->project_block;
        $pro->{ClientProjectCategory::FLOOR} = $project->project_floor;
        $pro->{ClientProjectCategory::ACREAGE} = $project->project_acreage;
        $pro->{ClientProjectCategory::HOUSE} = $project->project_house;
        $pro->{ClientProjectCategory::MAP} = $project->project_map_location;

        $area = strlen($project->project_address) > 1 ? $project->project_address . ', ' : '';
        $pro->{ClientProjectCategory::ADDRESS} = $area . $project->project_commune . ', ' . $project->project_district . ', ' . $project->project_provincial;
        $pro->{ClientProjectCategory::BEFORE_HOUR} = now()->diffInHours($project->updated_at);
        $pro->{ClientProjectCategory::BEFORE_DAY} = now()->diffInDays($project->updated_at);
        return $pro;
    }

    //tao view danh sach 3 doi tuong noi bat(nhieu nguoi xem)
    protected function listProjectFeature()
    {
        $trends = collect();
        if (!session()->has('listProjectFeature')) {
            $projects = DB::table('re_projects')->where('re_projects.active_status', 1)
                ->select(
                    'project_id',
                    'project_name',
                    'project_acreage',

                    're_projects.web_canonical',
                    're_project_categories.web_canonical AS category_web_canonical',

                )
                ->leftJoin('re_project_categories', 're_project_categories.project_category_id', '=', 're_projects.project_category_id')
                ->orderByDesc('project_viewed')->orderByDesc('re_projects.updated_at')->take(3)->get();
            foreach ($projects as $item) {
                $trend = new \stdClass();
                $trend->name = $item->project_name;
                $trend->link = 'du-an/' . $item->category_web_canonical . '/' . $item->web_canonical;
                $trend->acreage = $item->project_acreage;
                //Anh dai dien
                $trend->image = DB::table('re_project_galleries')->where([
                    ['active_status', 1],
                    ['project_gallery_banner', 2],
                    ['project_id', $item->project_id]
                ])->select('project_gallery_link', 'project_gallery_name')->first();
                $trends->add($trend);
            }
            session()->put('listProjectFeature', $trends);
        } else
            $trends = session('listProjectFeature');
        return $trends;
    }

    //tao view danh sach 3 doi tuong moi them gan nhat
    protected function listProjectRecentlyAdded()
    {
        $addeds = collect();
        if (!session()->has('listProjectRecentlyAdded')) {
            $projects = DB::table('re_projects')->where('re_projects.active_status', 1)
                ->select(
                    'project_id',
                    'project_name',
                    'project_provincial',
                    're_projects.updated_at',

                    're_projects.web_canonical',
                    're_project_categories.web_canonical AS category_web_canonical',
                )
                ->leftJoin('re_project_categories', 're_project_categories.project_category_id', '=', 're_projects.project_category_id')
                ->orderByDesc('re_projects.updated_at')->take(3)->get();
            foreach ($projects as $item) {
                $added = new \stdClass();
                $added->name = $item->project_name;
                $added->link = 'du-an/' . $item->category_web_canonical . '/' . $item->web_canonical;
                //Lay anh dai dien
                $added->image = DB::table('re_project_galleries')->where([
                    ['active_status', 1],
                    ['project_gallery_banner', 2],
                    ['project_id', $item->project_id]
                ])->select('project_gallery_link', 'project_gallery_name')->first();
                $added->address = $item->project_provincial;
                $added->before_hour = now()->diffInHours($item->updated_at);
                $added->before_day = now()->diffInDays($item->updated_at);
                $addeds->add($added);
            }
            session()->put('listProjectRecentlyAdded', $addeds);
        } else
            $addeds = session('listProjectRecentlyAdded');
        return $addeds;
    }
}
