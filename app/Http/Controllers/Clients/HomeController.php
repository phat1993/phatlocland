<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use App\Http\Requests\Clients\FeedbackRequest;
use App\Models\Entities\WebFeedback;
use App\Models\Entities\WebPolicy;
use App\Models\Helpers\FunctionHelper;
use App\Models\ViewModels\Clients\ClientBanner;
use App\Models\ViewModels\Clients\ClientHouseCategory;
use App\Models\ViewModels\Clients\ClientProjectCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    const HIRE_PER_PAGE = 5;


    public function index(Request $request)
    {
        $menuTops = FunctionHelper::topMenu();
        $menuBots = FunctionHelper::botMenu();
        $webInfo = FunctionHelper::webInfo();

        //banner
        $dbBanners = $this->getBanners();
        $banners = collect();
        //Xu ly du lieu lay duoc
        if ($dbBanners->count() > 0) {
            foreach ($dbBanners as $item) {
                $hou = $this->setBanner($item);
                $banners->add($hou);
            }
        }

        //filter
        $filterBusinesses = $this->filterBusinesses();
        $filterCategories = $this->filterCategories();
        $filterProjects = $this->filterProjects();
        $provincials = $this->getProvincials();


        //du an noi ban trong danh muc
        $dbTrendProjects = DB::table('re_projects')->where('re_projects.active_status',1)
        ->select(
            're_projects.project_id',
            're_projects.project_name',
            're_projects.project_build_time',
            're_projects.project_block',
            're_projects.project_floor',
            're_projects.project_acreage',
            're_projects.project_house',
            // DB::Raw("CONCAT(re_projects.project_address,re_projects.project_commune,
            // re_projects.project_district,re_projects.project_provincial AS address"),
            're_projects.project_provincial',
            're_projects.project_district',
            're_projects.project_commune',
            're_projects.project_address',
            're_projects.project_map_location',

            're_projects.project_description',

            're_projects.web_canonical',
            're_project_categories.web_canonical AS category_web_canonical',
            're_projects.updated_at',
        )
        ->leftJoin('re_project_categories', 're_project_categories.project_category_id', '=','re_projects.project_category_id')
        ->orderByDesc('project_viewed')->orderByDesc('re_projects.updated_at')->take(4)->get();

        $trendProjects = collect();
        //Xu ly du lieu lay duoc
        if ($dbTrendProjects->count() > 0) {
            foreach ($dbTrendProjects as $item) {
                $pro = $this->setProject($item);
                $trendProjects->add($pro);
            }
        }

        //bat dong san noi bat trong danh muc
        $dbTrendHouses = DB::table('re_houses')->where('re_houses.active_status', 1)
            ->select(
                're_houses.house_id',
                're_houses.house_name',
                're_houses.house_build_time',

                're_houses.house_acreage',
                're_houses.house_length',
                're_houses.house_width',
                're_houses.house_direction',
                're_houses.house_rooms',

                're_houses.house_rating',
                're_houses.house_rating_count',
                're_houses.house_rating_point',
                're_houses.house_status',
                're_houses.house_status_color',

                're_houses.house_price',
                're_houses.house_provincial',
                're_houses.house_district',
                're_houses.house_commune',
                're_houses.house_address',
                're_houses.house_map_location',

                're_houses.web_canonical',
                're_house_businesses.web_canonical AS business_web_canonical',
                're_house_categories.web_canonical AS category_web_canonical',
                're_houses.updated_at',

            )
            ->leftJoin('re_house_businesses', 're_house_businesses.house_business_id', '=', 're_houses.house_business_id')
            ->leftJoin('re_house_categories', 're_house_categories.house_category_id', '=', 're_houses.house_category_id')
            ->orderByDesc('house_viewed')->orderByDesc('updated_at')->take(4)->get();


        $trendHouses = collect();
        //Xu ly du lieu lay duoc
        if ($dbTrendHouses->count() > 0) {
            foreach ($dbTrendHouses as $item) {
                $hou = $this->setHouse($item);
                $trendHouses->add($hou);
            }
        }

        //WebFeedback
        $webReviews = DB::table('re_web_reviews')->where('active_status', 1)->orderByDesc('updated_at')->get();

        return view('clients.pages.main.index', compact(
            'menuTops',
            'menuBots',
            'webInfo',
            'provincials',
            'filterBusinesses',
            'filterCategories',
            'filterProjects',
            'banners',
            'trendProjects',
            'trendHouses',
            'webReviews',
        ));
    }
    public function contact(FeedbackRequest $request)
    {
        $menuTops = FunctionHelper::topMenu();
        $menuBots = FunctionHelper::botMenu();
        $webInfo = FunctionHelper::webInfo();

        if ($request->isMethod('post')) {
            $data = $request->all();
            $webFeedback = new WebFeedback();
            $webFeedback->{WebFeedback::WEB_FEEDBACK_NAME} = $data['fname'];
            $webFeedback->{WebFeedback::WEB_FEEDBACK_EMAIL} = $data['email'];
            $webFeedback->{WebFeedback::WEB_FEEDBACK_CONTENT} = $data['comment'];

            if ($webFeedback->save())
                return redirect()->back()->with('success', 'Cảm ơn bạn đã gửi lời nhắn đến chúng tôi!');
            else
                redirect()->back()->withInput()->withErrors('Lỗi Server!');
        }

        return view('clients.pages.main.contact', compact(
            'menuTops',
            'menuBots',
            'webInfo',
        ));
    }
    public function about(Request $request)
    {
        $menuTops = FunctionHelper::topMenu();
        $menuBots = FunctionHelper::botMenu();
        $webInfo = FunctionHelper::webInfo();
        return view('clients.pages.main.about', compact(
            'menuTops',
            'menuBots',
            'webInfo',
        ));
    }
    public function hire(Request $request)
    {
        try {
            //Menu
            $menuTops = FunctionHelper::topMenu();
            $menuBots = FunctionHelper::botMenu();
            $webInfo = FunctionHelper::webInfo();
            //doi tuong danh muc
            $category = null;
            $perPage = self::HIRE_PER_PAGE;

            //filter
            $param = array();
            $param[] = ['active_status', 1];

            $webHires = DB::table('re_web_hires')->where($param)
                ->select(
                    'web_hire_image',
                    'web_hire_name',
                    'web_hire_description',

                    'web_canonical',
                    'updated_at',

                )
                ->orderByDesc('updated_at')->paginate($perPage);


            return view('clients.pages.hire.list', compact(
                'menuTops',
                'menuBots',
                'webInfo',
                'webHires'
            ));
        } catch (\Throwable $th) {
            //develop
            // dd($th->getMessage());
        }
        abort(404);
    }
    public function hireDetail(Request $request)
    {
        $menuTops = FunctionHelper::topMenu();
        $menuBots = FunctionHelper::botMenu();
        $webInfo = FunctionHelper::webInfo();

        if (isset($request->hire)) {
            $webHire = DB::table('re_web_hires')->where([
                ['active_status', 1],
                ['web_canonical', $request->hire]
            ])->first();
            if (isset($webHire))
                return view('clients.pages.hire.detail', compact(
                    'menuTops',
                    'menuBots',
                    'webInfo',
                    'webHire'
                ));
        }

        abort(404);
    }

    public function policy(Request $request)
    {
        $menuTops = FunctionHelper::topMenu();
        $menuBots = FunctionHelper::botMenu();
        $webInfo = FunctionHelper::webInfo();

        $webPolicy = null;
        $webPolicies = null;
        if (isset($request->policy)) {
            $webPolicy = DB::table('re_web_policies')->where([
                ['active_status', 1],
                ['web_canonical', $request->policy]
            ])->first();
        } else
            $webPolicy = DB::table('re_web_policies')->where('active_status', 1)->orderByDesc('updated_at')->first();

        if (isset($webPolicy))
            $webPolicies = DB::table('re_web_policies')->where([
                ['active_status', 1],
                ['web_policy_id', '<>', $webPolicy->web_policy_id]
            ])->orderByDesc('updated_at')->get();
        else
            abort(404);

        return view('clients.pages.main.policy', compact(
            'menuTops',
            'menuBots',
            'webInfo',
            'webPolicy',
            'webPolicies'
        ));
    }
    public function advice(Request $request)
    {
        $menuTops = FunctionHelper::topMenu();
        $menuBots = FunctionHelper::botMenu();
        $webInfo = FunctionHelper::webInfo();
        return view('clients.pages.main.advice', compact(
            'menuTops',
            'menuBots',
            'webInfo',
        ));
    }


    /*Helper index*/
    protected function getBanners()
    {
        $dbBanners = DB::table('re_houses')
            ->select(
                're_houses.house_id',
                're_houses.house_name',
                're_houses.house_build_time',

                're_houses.house_acreage',
                're_houses.house_length',
                're_houses.house_width',
                're_houses.house_direction',

                're_houses.house_price',
                're_houses.house_provincial',
                're_houses.house_district',
                're_houses.house_commune',
                're_houses.house_address',
                're_houses.house_map_location',

                're_houses.web_canonical',
                're_house_businesses.web_canonical AS business_web_canonical',
                're_house_categories.web_canonical AS category_web_canonical',

            )
            ->leftJoin('re_house_businesses', 're_house_businesses.house_business_id', '=', 're_houses.house_business_id')
            ->leftJoin('re_house_categories', 're_house_categories.house_category_id', '=', 're_houses.house_category_id')
            ->orderByDesc('re_houses.updated_at')->take(3)->get();
        return $dbBanners;
    }

    /*Helper filter*/

    //lay danh sach cac du an co trong bat dong san de filter
    protected function filterProjects()
    {
        $filterProjects = DB::table('re_houses')
            ->select('re_houses.project_id', 're_projects.project_name')
            ->join('re_projects', 're_projects.project_id', '=', 're_houses.project_id')->get();
        return $filterProjects;
    }

    //lay danh sach business de filter
    protected function filterBusinesses()
    {
        $filterBusinesses = DB::table('re_house_businesses')
            ->select('house_business_id', 'house_business_name')->get();
        return $filterBusinesses;
    }

    //lay danh sach category de filter
    protected function filterCategories()
    {
        $filterCategories = DB::table('re_house_categories')
            ->select('house_category_id', 'house_category_name')->get();
        return $filterCategories;
    }


    //Lay danh sach cac tinh thanh co trong bat dong san
    protected function getProvincials()
    {
        ////Lay danh sach tinh them cho filter
        // $houvincials = collect();
        // if (!session()->has('filterProvincials')) {
        $houvincials = DB::table('re_houses')->where('active_status', 1)
            ->select('house_provincial')->distinct()->get();
        // session()->put('filterProvincials', $houvincials);
        // } else
        //     $houvincials = session('filterProvincials');
        return $houvincials;
    }

    //Lay danh sach cac quan huyen nam trong bat dong san thuoc ve tinh thanh
    //tra ve json
    public function changeProvincial($houvincial)
    {
        $districts = DB::table('re_houses')->where([
            ['active_status', 1],
            ['house_provincial', $houvincial]
        ])->select('house_district')->distinct()->get();
        $data = collect();
        foreach ($districts as $item) {
            $data->add($item->house_district);
        }
        echo json_encode($data);
        exit;
    }

    //Tao doi tuong view cho danh sach banner
    //tra ve doi tuong view
    protected function setBanner($item)
    {
        $hou = new ClientBanner();
        $hou->{ClientBanner::LINK} = 'bat-dong-san/' . $item->business_web_canonical . '/' . $item->category_web_canonical . '/' . $item->web_canonical;
        //Lay danh sach hinh anh
        $banner = DB::table('re_house_galleries')->where(
            [
                ['active_status', 1],
                ['house_id', $item->house_id],
                ['house_gallery_banner', 1]
            ]
        )->orderBy('updated_at')->first();

        $hou->{ClientBanner::IMAGE} =   $banner != null ? $banner->house_gallery_link : null;

        $hou->{ClientBanner::TITLE} = $item->house_name;

        $hou->{ClientBanner::ACREAGE} = $item->house_acreage;
        $hou->{ClientBanner::LENGTH} = $item->house_length;
        $hou->{ClientBanner::WIDTH} = $item->house_width;
        $hou->{ClientBanner::DIRECTION} = $item->house_direction;
        $hou->{ClientBanner::PRICE} = $item->house_price;
        $hou->{ClientBanner::MAP} = $item->house_map_location;

        $area = strlen($item->house_address) > 1 ? $item->house_address . ', ' : '';
        $commune = strlen($item->house_commune) > 1 ? $item->house_commune . ', ' : '';
        $hou->{ClientBanner::ADDRESS} = $area . $commune . $item->house_district . ', ' . $item->house_provincial;
        return $hou;
    }

    //Tao doi tuong view cho danh sach House
    //tra ve doi tuong view
    protected function setHouse($item)
    {
        $hou = new ClientHouseCategory();
        $hou->{ClientHouseCategory::LINK} = 'bat-dong-san/' . $item->business_web_canonical . '/' . $item->category_web_canonical . '/' . $item->web_canonical;
        $hou->{ClientHouseCategory::FEATURES} = array();
        //Lay danh sach hinh anh
        $hou->{ClientHouseCategory::GALLERIES} = DB::table('re_house_galleries')->where(
            [
                ['active_status', 1],
                ['house_id', $item->house_id]
            ]
        )->orderBy('house_gallery_order')->get();

        $banner = $hou->{ClientHouseCategory::GALLERIES}->where('house_gallery_banner', 1)->first();
        $hou->{ClientHouseCategory::IMAGE} =   $banner != null ? $banner : null;

        $hou->{ClientHouseCategory::TITLE} = $item->house_name;
        $hou->{ClientHouseCategory::TIME} = $item->house_build_time;
        $rating = $item->house_rating_point > 0 ? $item->house_rating_point / $item->house_rating_count : $item->house_rating;
        $hou->{ClientHouseCategory::RATING} = $rating;
        $hou->{ClientHouseCategory::RATING_COUNT} = $item->house_rating_count;
        $hou->{ClientHouseCategory::STATUS} = $item->house_status;
        $hou->{ClientHouseCategory::STATUS_COLOR} = $item->house_status_color;

        $hou->{ClientHouseCategory::ACREAGE} = $item->house_acreage;
        $hou->{ClientHouseCategory::LENGTH} = $item->house_length;
        $hou->{ClientHouseCategory::WIDTH} = $item->house_width;
        $hou->{ClientHouseCategory::DIRECTION} = $item->house_direction;
        $hou->{ClientHouseCategory::ROOMS} = $item->house_rooms;
        $hou->{ClientHouseCategory::PRICE} = $item->house_price;
        $hou->{ClientHouseCategory::MAP} = $item->house_map_location;

        $area = strlen($item->house_address) > 1 ? $item->house_address . ', ' : '';
        $commune = strlen($item->house_commune) > 1 ? $item->house_commune . ', ' : '';
        $hou->{ClientHouseCategory::ADDRESS} = $area . $commune . $item->house_district . ', ' . $item->house_provincial;
        $hou->{ClientHouseCategory::BEFORE_HOUR} = now()->diffInHours($item->updated_at);
        $hou->{ClientHouseCategory::BEFORE_DAY} = now()->diffInDays($item->updated_at);
        return $hou;
    }
    //Tao doi tuong view cho danh sach Project
    //tra ve doi tuong view
    protected function setProject($item)
    {
        $pro = new ClientProjectCategory();
        $pro->{ClientProjectCategory::LINK} = 'du-an/' . $item->category_web_canonical . '/' . $item->web_canonical;
        $pro->{ClientProjectCategory::FEATURES} = array();
        //Lay danh sach hinh anh
        $pro->{ClientProjectCategory::GALLERIES} = DB::table('re_project_galleries')->where(
            [
                ['active_status', 1],
                ['project_id', $item->project_id]
            ]
        )->orderBy('project_gallery_order')->get();

        //Anh dai dien
        $represent = $pro->{ClientProjectCategory::GALLERIES}->where('project_gallery_banner', 2)->first();
        $pro->{ClientProjectCategory::IMAGE} =   $represent != null ? $represent : null;

        $pro->{ClientProjectCategory::TITLE} = $item->project_name;
        $pro->{ClientProjectCategory::TIME} = $item->project_build_time;
        $pro->{ClientProjectCategory::BLOCK} = $item->project_block;
        $pro->{ClientProjectCategory::FLOOR} = $item->project_floor;
        $pro->{ClientProjectCategory::ACREAGE} = $item->project_acreage;
        $pro->{ClientProjectCategory::HOUSE} = $item->project_house;
        $pro->{ClientProjectCategory::MAP} = $item->project_map_location;
        $pro->{ClientProjectCategory::DESCRIPTION} = $item->project_description;

        $area = strlen($item->project_address) > 1 ? $item->project_address . ', ' : '';
        $pro->{ClientProjectCategory::ADDRESS} = $area . $item->project_commune . ', ' . $item->project_district . ', ' . $item->project_provincial;
        $pro->{ClientProjectCategory::BEFORE_HOUR} = now()->diffInHours($item->updated_at);
        $pro->{ClientProjectCategory::BEFORE_DAY} = now()->diffInDays($item->updated_at);
        return $pro;
    }
}
