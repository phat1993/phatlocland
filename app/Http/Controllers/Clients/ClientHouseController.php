<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use App\Http\Requests\Clients\HouseReviewRequest;
use App\Models\Entities\House;
use App\Models\Entities\HouseReview;
use App\Models\Helpers\FunctionHelper;
use App\Models\ViewModels\Clients\ClientHouseCategory;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class ClientHouseController extends Controller
{
    //So luong doi tuong hien thi trong 1 page
    const PER_PAGE = 6;
    const PER_COMMENT = 3;

    //////////////////////////
    /*Main method*/

    // hien thi danh sach cac bat dong san
    public function list(Request $request)
    {
        try {
            //Menu
            $menuTops = FunctionHelper::topMenu();
            $menuBots = FunctionHelper::botMenu();
            $webInfo = FunctionHelper::webInfo();

            $perPage = self::PER_PAGE;
            $sortObj = 're_houses.updated_at'; //Gia tri sort
            $sort = 'asc'; //Thu tu sort -asc|desc

            //filter
            $filterBusinesses = $this->filterBusinesses();
            $filterCategories = $this->filterCategories();
            $filterProjects = $this->filterProjects();
            $provincials = $this->getProvincials();

            //filterParam
            $param = array(); //su dung cho where
            $paramOr = array(); //su dung cho Or Where
            //lay cac parameter xu ly filter
            $this->getFilterParamter($request, $param, $sortObj, $sort);

            //business
            $business = null;
            if (isset($request->business)) {
                $business = DB::table('re_house_businesses')->where('web_canonical', $request->business)->first();
                if (isset($business)) {
                    $param[] = ['re_houses.house_business_id', $business->house_business_id];
                    //set lai du lieu cho request
                    $request->merge([
                        'filterBusiness' => $business->house_business_id
                    ]);
                }
            }
            //category
            $category = null;
            if (isset($request->category)) {
                $category = DB::table('re_house_categories')->where('web_canonical', $request->category)->first();
                if (isset($category)) {
                    $param[] = ['re_houses.house_category_id', $category->house_category_id];
                    //set lai du lieu cho request
                    $request->merge([
                        'filterCategory' => $category->house_category_id
                    ]);
                }
            }

            //doi tuong danh sach du an
            $dbHouses = $this->paginateHouse($param, $paramOr, $perPage, $sortObj, $sort);

            $houses = collect();
            //Xu ly du lieu lay duoc
            if ($dbHouses->total() > 0) {
                foreach ($dbHouses as $item) {
                    $hou = $this->setHouse($item);
                    $houses->add($hou);
                }
            }


            return view('clients.pages.house.list', compact(
                'menuTops',
                'menuBots',
                'webInfo',
                'provincials',
                'filterBusinesses',
                'filterCategories',
                'filterProjects',
                'business',
                'category',
                'houses',
                'dbHouses'
            ));
        } catch (\Throwable $th) {
            //develop
            dd($th->getMessage());
        }
        // abort(404);
    }

    //hien thi chi tiet cua bat dong san
    public function detail(Request $request)
    {
        try {
            $menuTops = FunctionHelper::topMenu();
            $menuBots = FunctionHelper::botMenu();
            $webInfo = FunctionHelper::webInfo();

            //param: category, house
            if (isset($request->house) && isset($request->business) && isset($request->category)) {
                $business = DB::table('re_house_businesses')->where([['active_status', 1], ['web_canonical', $request->business]])->first();
                if ($business == null) abort(404);
                $category = DB::table('re_house_categories')->where([['active_status', 1], ['web_canonical', $request->category]])->first();
                if ($category == null) abort(404);

                $house = DB::table('re_houses')->where([
                    ['re_houses.active_status', 1],
                    ['web_canonical', $request->house],
                    ['re_houses.house_category_id', $category->house_category_id],
                    ['re_houses.house_business_id', $business->house_business_id]
                ])->first();
                if (isset($house)) {
                    //Update luot view bat dong san
                    $updated = DB::table('re_houses')->where('house_id', $house->house_id);
                    $param = array();
                    $param['house_viewed'] = $house->house_viewed + 1;
                    $updated->update($param);

                    //Lay danh sach hinh anh
                    $galleries = DB::table('re_house_galleries')->where(
                        [
                            ['active_status', 1],
                            ['house_id', $house->house_id]
                        ]
                    )->orderBy('house_gallery_order')->get();

                    $banner = $galleries->where('house_gallery_banner', 1)->first();
                    $amenities = json_decode($house->house_amenities);

                    //cho phat trien
                    $floorPlans = $this->getFloorPlan($house);
                    $locationNearbies = null;

                    $perComment = self::PER_COMMENT;
                    $comments = DB::table('re_house_reviews')->where('active_status', 1)->orderByDesc('updated_at')->paginate($perComment);

                    // cac danh sach ho tro
                    //da xem
                    $vieweds = $this->houseViewed($business, $category, $house);
                    //noi bat
                    $features = $this->listHouseFeature();
                    //moi them
                    $addeds = $this->listHouseRecentlyAdded();

                    return view(
                        'clients.pages.house.detail',
                        compact(
                            'menuTops',
                            'menuBots',
                            'webInfo',
                            'house',
                            'category',
                            'galleries',
                            'banner',
                            'amenities',
                            'floorPlans',
                            'locationNearbies',
                            'comments',
                            'vieweds',
                            'features',
                            'addeds'
                        )
                    );
                }
            }
        } catch (\Throwable $th) {
            //develop
            // dd($th->getMessage());
        }

        abort(404);
    }

    //Gui cac danh gia
    public function review(HouseReviewRequest $request)
    {
        try {
            //Kiem tra xem doi tuong da danh gia chua:
            $house = DB::table('re_houses')->where([
                ['active_status', 1],
                ['house_code', $request->rating_code]
            ])->select('house_id')->first();

            if (isset($house)) {
                $hasReview = DB::table('re_house_reviews')->where([
                    ['active_status', 1],
                    ['house_id', $house->house_id],
                    ['house_review_email', $request->rating_mail]
                ])->first();
                if (isset($hasReview))
                    return redirect()->back()->withErrors(["Bạn đã đánh giá bất động sản này rồi! Xin cảm ơn bạn."]);
                else {
                    $review = new HouseReview();
                    if (isset($request->rating_star)) {
                        switch ($request->rating_star) {
                            case '1':
                                $review->{HouseReview::HOUSE_REVIEW_POINT} = 1;
                                break;
                            case '2':
                                $review->{HouseReview::HOUSE_REVIEW_POINT} = 2;
                                break;
                            case '3':
                                $review->{HouseReview::HOUSE_REVIEW_POINT} = 3;
                                break;
                            case '4':
                                $review->{HouseReview::HOUSE_REVIEW_POINT} = 4;
                                break;
                        }
                    }
                    $review->{HouseReview::HOUSE_ID} = $house->house_id;
                    $review->{HouseReview::HOUSE_REVIEW_NAME} = $request->rating_name;
                    $review->{HouseReview::HOUSE_REVIEW_EMAIL} = $request->rating_mail;
                    $review->{HouseReview::HOUSE_REVIEW_MESSAGE} = $request->rating_comment;
                    $review->{HouseReview::HOUSE_REVIEW_SUB} = 1;
                    $review->{HouseReview::HOUSE_REVIEW_IP} = $request->ip();
                    $review->{HouseReview::ACTIVE_STATUS} = 0;
                    if ($review->save())
                        return redirect()->back()->with('success', 'Cảm ơn bạn đã gửi đánh giá đến chúng tôi!');
                }
            }

            return redirect()->back()->withInput()->withErrors(["Lỗi server! Quý khách vui lòng thực hiện lại một lần nữa."]);
        } catch (\Throwable $th) {
            //develop
            // dd($th->getMessage());
        }

        abort(404);
    }


    //////////////////////////
    /*Helper filter*/

    //lay danh sach cac du an co trong bat dong san de filter
    protected function filterProjects()
    {
        $filterProjects = DB::table('re_houses')
            ->select('re_houses.project_id', 're_projects.project_name')
            ->join('re_projects', 're_projects.project_id', '=', 're_houses.project_id')->get();
        return $filterProjects;
    }

    //lay danh sach business de filter
    protected function filterBusinesses()
    {
        $filterBusinesses = DB::table('re_house_businesses')
            ->select('house_business_id', 'house_business_name')->get();
        return $filterBusinesses;
    }

    //lay danh sach category de filter
    protected function filterCategories()
    {
        $filterCategories = DB::table('re_house_categories')
            ->select('house_category_id', 'house_category_name')->get();
        return $filterCategories;
    }

    //truyen vao request, mang filter, doi tuong sort, kieu sort
    //Xu ly request tao thanh cac dieu kien filter
    protected function getFilterParamter($request, &$param, &$sortObj, &$sort)
    {
        $param[] = ['re_houses.active_status', 1];
        //Sort
        if (isset($request->sort)) {
            if ($request->sort == 'new') {
                $sortObj = 're_houses.updated_at';
                $sort = 'asc';
            } else if ($request->sort == 'old') {
                $sortObj = 're_houses.updated_at';
                $sort = 'desc';
            } else if ($request->sort == 'trend') {
                $sortObj = 're_houses.house_viewed';
                $sort = 'asc';
            }
        } else
            $request->sort = 'new';
        //Address
        if (isset($request->provincial)) {
            $param[] = ['re_houses.house_provincial', $request->provincial];
        }
        if (isset($request->district)) {
            $param[] = ['re_houses.house_district', $request->district];
        }
        if (isset($request->address)) {
            $param[] = ['re_houses.house_name', 'like', '%' . $request->address . '%'];
            $paramOr[] = ['re_houses.house_commune', 'like', '%' . $request->address . '%'];
            $paramOr[] = ['re_houses.house_address', 'like', '%' . $request->address . '%'];
        }
        //project
        if (isset($request->filterProject)) {
            $param[] = ['re_houses.project_id', $request->filterProject];
        }
        //detail price
        if (isset($request->filterPrice)) {
            $price = $request->filterPrice;
            // 'below','10','20','50','100','more'
            switch ($price) {
                case 'below':
                    $param[] = ['re_houses.house_price', '<', 1000000];
                    break;
                case '10':
                    $param[] = ['re_houses.house_price', '>=', 1000000];
                    $param[] = ['re_houses.house_price', '<', 10000000];
                    break;
                case '20':
                    $param[] = ['re_houses.house_price', '>=', 10000000];
                    $param[] = ['re_houses.house_price', '<', 20000000];
                    break;
                case '50':
                    $param[] = ['re_houses.house_price', '>=', 20000000];
                    $param[] = ['re_houses.house_price', '<', 50000000];
                    break;
                case '100':
                    $param[] = ['re_houses.house_price', '>=', 50000000];
                    $param[] = ['re_houses.house_price', '<', 100000000];
                    break;
                case 'more':
                    $param[] = ['re_houses.house_price', '>=', 100000000];
                    break;
            };
        }
        //project
        if (isset($request->filterDirection)) {
            $param[] = ['re_houses.house_direction', $request->filterDirection];
        }
        //room
        if (isset($request->filterRooms)) {
            if (is_int($request->filterRooms))
                $param[] = ['re_houses.house_rooms', $request->filterRooms];
            elseif ($request->filterRooms == 'more')
                $param[] = ['re_houses.house_rooms', $request->filterRooms];
        }
        //acreage
        if (isset($request->filterAcreage)) {
            // 'below','10','20','50','100','more'
            switch ($request->filterAcreage) {
                case 'below':
                    $param[] = ['re_houses.house_acreage', '<', 25];
                    break;
                case '25':
                    $param[] = ['re_houses.house_acreage', '>=', 25];
                    $param[] = ['re_houses.house_acreage', '<', 50];
                    break;
                case '50':
                    $param[] = ['re_houses.house_acreage', '>=', 50];
                    $param[] = ['re_houses.house_acreage', '<', 100];
                    break;
                case 'more':
                    $param[] = ['re_houses.house_acreage', '>=', 100];
                    break;
            };
        }
        //length
        if (isset($request->filterLength)) {
            // 'below','25','50','more'
            switch ($request->filterLength) {
                case 'below':
                    $param[] = ['re_houses.house_length', '<', 25];
                    break;
                case '25':
                    $param[] = ['re_houses.house_length', '>=', 25];
                    $param[] = ['re_houses.house_length', '<', 50];
                    break;
                case '50':
                    $param[] = ['re_houses.house_length', '>=', 50];
                    $param[] = ['re_houses.house_length', '<', 100];
                    break;
                case 'more':
                    $param[] = ['re_houses.house_length', '>=', 100];
                    break;
            };
        }
        //width
        if (isset($request->filterWidth)) {
            // 'below','25','50','more'
            switch ($request->filterWidth) {
                case 'below':
                    $param[] = ['re_houses.house_width', '<', 25];
                    break;
                case '25':
                    $param[] = ['re_houses.house_width', '>=', 25];
                    $param[] = ['re_houses.house_width', '<', 50];
                    break;
                case '50':
                    $param[] = ['re_houses.house_width', '>=', 50];
                    $param[] = ['re_houses.house_width', '<', 100];
                    break;
                case 'more':
                    $param[] = ['re_houses.house_width', '>=', 100];
                    break;
            };
        }
    }

    //Lay danh sach cac tinh thanh co trong bat dong san
    protected function getProvincials()
    {
        ////Lay danh sach tinh them cho filter
        // $houvincials = collect();
        // if (!session()->has('filterProvincials')) {
        $houvincials = DB::table('re_houses')->where('active_status', 1)
            ->select('house_provincial')->distinct()->get();
        // session()->put('filterProvincials', $houvincials);
        // } else
        //     $houvincials = session('filterProvincials');
        return $houvincials;
    }

    //Lay danh sach cac quan huyen nam trong bat dong san thuoc ve tinh thanh
    //tra ve json
    public function changeProvincial($houvincial)
    {
        $districts = DB::table('re_houses')->where([
            ['active_status', 1],
            ['house_provincial', $houvincial]
        ])->select('house_district')->distinct()->get();
        $data = collect();
        foreach ($districts as $item) {
            $data->add($item->house_district);
        }
        echo json_encode($data);
        exit;
    }

    //////////////////////////
    /*ham ho tro cho function list*/


    //Tao doi tuong view cho danh sach
    //tra ve doi tuong view
    protected function setHouse($item)
    {
        $hou = new ClientHouseCategory();
        $hou->{ClientHouseCategory::LINK} = 'bat-dong-san/' . $item->business_web_canonical . '/' . $item->category_web_canonical . '/' . $item->web_canonical;
        $hou->{ClientHouseCategory::FEATURES} = array();
        //Lay danh sach hinh anh
        $hou->{ClientHouseCategory::GALLERIES} = DB::table('re_house_galleries')->where(
            [
                ['active_status', 1],
                ['house_id', $item->house_id]
            ]
        )->orderBy('house_gallery_order')->get();

        $banner = $hou->{ClientHouseCategory::GALLERIES}->where('house_gallery_banner', 1)->first();
        $hou->{ClientHouseCategory::IMAGE} =   $banner != null ? $banner : null;

        $hou->{ClientHouseCategory::TITLE} = $item->house_name;
        $hou->{ClientHouseCategory::TIME} = $item->house_build_time;

        $hou->{ClientHouseCategory::RATING} = $item->house_rating;
        $hou->{ClientHouseCategory::STATUS} = $item->house_status;
        $hou->{ClientHouseCategory::STATUS_COLOR} = $item->house_status_color;

        $hou->{ClientHouseCategory::ACREAGE} = $item->house_acreage;
        $hou->{ClientHouseCategory::LENGTH} = $item->house_length;
        $hou->{ClientHouseCategory::WIDTH} = $item->house_width;
        $hou->{ClientHouseCategory::DIRECTION} = $item->house_direction;
        $hou->{ClientHouseCategory::ROOMS} = $item->house_rooms;
        $hou->{ClientHouseCategory::PRICE} = $item->house_price;
        $hou->{ClientHouseCategory::MAP} = $item->house_map_location;

        $area = strlen($item->house_address) > 1 ? $item->house_address . ', ' : '';
        $commune = strlen($item->house_commune) > 1 ? $item->house_commune . ', ' : '';
        $hou->{ClientHouseCategory::ADDRESS} = $area . $commune . $item->house_district . ', ' . $item->house_provincial;
        $hou->{ClientHouseCategory::BEFORE_HOUR} = now()->diffInHours($item->updated_at);
        $hou->{ClientHouseCategory::BEFORE_DAY} = now()->diffInDays($item->updated_at);
        return $hou;
    }

    //Phan trang cung voi filter
    protected function paginateHouse($param, $paramOr, $perPage, $sortObj, $sort)
    {
        $dbHouses = DB::table('re_houses')->where($param)->orWhere($paramOr)
            ->select(
                're_houses.house_id',
                're_houses.house_name',
                're_houses.house_build_time',

                're_houses.house_rating',
                're_houses.house_rating_count',
                're_houses.house_rating_point',
                're_houses.house_status',
                're_houses.house_status_color',

                're_houses.house_acreage',
                're_houses.house_length',
                're_houses.house_width',
                're_houses.house_direction',
                're_houses.house_rooms',

                're_houses.house_price',

                // DB::Raw("CONCAT(re_houses.house_address,re_houses.house_commune,
                // re_houses.house_district,re_houses.house_provincial AS address"),
                're_houses.house_provincial',
                're_houses.house_district',
                're_houses.house_commune',
                're_houses.house_address',
                're_houses.house_map_location',

                're_houses.web_canonical',
                're_house_businesses.web_canonical AS business_web_canonical',
                're_house_categories.web_canonical AS category_web_canonical',
                're_houses.updated_at',

            )
            ->leftJoin('re_house_businesses', 're_house_businesses.house_business_id', '=', 're_houses.house_business_id')
            ->leftJoin('re_house_categories', 're_house_categories.house_category_id', '=', 're_houses.house_category_id')
            ->orderByDesc($sortObj, $sort)->paginate($perPage);
        return $dbHouses;
    }


    //////////////////////////
    /*ham ho tro cho function detail*/

    protected function getFloorPlan($house)
    {
        return DB::table('re_floor_plans')->where([['active_status', 1], ['house_id', $house->house_id]])->get();
    }

    //luu lai cac doi tuong da xem
    protected function houseViewed($business, $category, $house)
    {
        //Tao session luu house viewed
        if (!session()->has('houseViewed'))
            session()->put('houseViewed', collect());

        $viewed = session('houseViewed');
        if ($viewed != null && $viewed->where('id', $house->house_id)->count() <= 0) {

            $hou = $this->setViewHouse($business, $category, $house);
            $viewed->add($hou);
        }
        session()->put('houseViewed', $viewed);

        return $viewed;
    }

    //Tao doi tuong view cho cac danh sach feature, viewed, added
    protected function setViewHouse($business, $category, $house)
    {
        $hou = new ClientHouseCategory();
        $hou->{ClientHouseCategory::LINK} = 'bat-dong-san/' . $business->web_canonical . '/' . $category->web_canonical . '/' . $house->web_canonical;
        $hou->{ClientHouseCategory::FEATURES} = array();
        //Lay danh sach hinh anh
        $hou->{ClientHouseCategory::GALLERIES} = DB::table('re_house_galleries')->where(
            [
                ['active_status', 1],
                ['house_id', $house->house_id]
            ]
        )->orderBy('house_gallery_order')->get();

        $banner = $hou->{ClientHouseCategory::GALLERIES}->where('house_gallery_banner', 1)->first();
        $hou->{ClientHouseCategory::IMAGE} =   $banner != null ? $banner : null;

        $hou->{ClientHouseCategory::ID} = $house->house_id;
        $hou->{ClientHouseCategory::TITLE} = $house->house_name;
        $hou->{ClientHouseCategory::TIME} = $house->house_build_time;

        $hou->{ClientHouseCategory::RATING} = $house->house_rating;
        $hou->{ClientHouseCategory::STATUS} = $house->house_status;
        $hou->{ClientHouseCategory::STATUS_COLOR} = $house->house_status_color;

        $hou->{ClientHouseCategory::ACREAGE} = $house->house_acreage;
        $hou->{ClientHouseCategory::LENGTH} = $house->house_length;
        $hou->{ClientHouseCategory::WIDTH} = $house->house_width;
        $hou->{ClientHouseCategory::DIRECTION} = $house->house_direction;
        $hou->{ClientHouseCategory::ROOMS} = $house->house_rooms;
        $hou->{ClientHouseCategory::PRICE} = $house->house_price;

        $hou->{ClientHouseCategory::MAP} = $house->house_map_location;

        $area = strlen($house->house_address) > 1 ? $house->house_address . ', ' : '';
        $commune = strlen($house->house_commune) > 1 ? $house->house_commune . ', ' : '';
        $hou->{ClientHouseCategory::ADDRESS} = $area . $commune . $house->house_district . ', ' . $house->house_provincial;
        $hou->{ClientHouseCategory::BEFORE_HOUR} = now()->diffInHours($house->updated_at);
        $hou->{ClientHouseCategory::BEFORE_DAY} = now()->diffInDays($house->updated_at);
        return $hou;
    }

    //tao view danh sach 3 doi tuong noi bat(nhieu nguoi xem)
    protected function listHouseFeature()
    {
        $trends = collect();
        if (!session()->has('listHouseFeature')) {
            $houses = DB::table('re_houses')->where('re_houses.active_status', 1)
                ->select(
                    're_houses.house_id',
                    're_houses.house_name',
                    're_houses.house_acreage',
                    're_houses.house_rating',
                    're_houses.house_rating_count',
                    're_houses.house_rating_point',
                    're_houses.house_status',
                    're_houses.house_status_color',
                    // 'house_provincial',
                    // 'house_district',
                    // 'house_commune',
                    // 'house_address',
                    // 're_houses.house_map_location',

                    're_houses.web_canonical',
                    're_house_businesses.web_canonical AS business_web_canonical',
                    're_house_categories.web_canonical AS category_web_canonical',
                    're_houses.updated_at',

                )
                ->leftJoin('re_house_businesses', 're_house_businesses.house_business_id', '=', 're_houses.house_business_id')
                ->leftJoin('re_house_categories', 're_house_categories.house_category_id', '=', 're_houses.house_category_id')
                ->orderByDesc('house_viewed')->orderByDesc('re_houses.updated_at')->take(3)->get();
            foreach ($houses as $item) {
                $trend = new \stdClass();
                $trend->name = $item->house_name;
                $trend->link = 'bat-dong-san/' . $item->business_web_canonical . '/' . $item->category_web_canonical . '/' . $item->web_canonical;
                $trend->acreage = $item->house_acreage;

                $rating = $item->house_rating_point > 0 ? $item->house_rating_point / $item->house_rating_count : $item->house_rating;
                $trend->rating = $rating;
                $trend->rating_count = $item->house_rating_count;
                $trend->status = $item->house_status;
                $trend->status_color = $item->house_status_color;

                $trend->image = DB::table('re_house_galleries')->where([
                    ['active_status', 1],
                    ['house_gallery_banner', 1],
                    ['house_id', $item->house_id]
                ])->select('house_gallery_link', 'house_gallery_name')->first();
                // $area = strlen($item->house_address) > 1 ? $item->house_address . ', ' : '';
                // $trend->address = $area . $item->house_commune . ', ' . $item->house_district . ', ' . $item->house_provincial;
                $trends->add($trend);
            }
            session()->put('listHouseFeature', $trends);
        } else
            $trends = session('listHouseFeature');
        return $trends;
    }

    //tao view danh sach 3 doi tuong moi them gan nhat
    protected function listHouseRecentlyAdded()
    {
        $addeds = collect();
        if (!session()->has('listHouseRecentlyAdded')) {
            $houses = DB::table('re_houses')->where('re_houses.active_status', 1)
                ->select(
                    'house_id',
                    'house_name',
                    'house_provincial',
                    // 'house_district',
                    // 'house_commune',
                    // 'house_address',
                    're_houses.updated_at',

                    're_houses.web_canonical',
                    're_house_businesses.web_canonical AS business_web_canonical',
                    're_house_categories.web_canonical AS category_web_canonical',
                    're_houses.updated_at',

                )
                ->leftJoin('re_house_businesses', 're_house_businesses.house_business_id', '=', 're_houses.house_business_id')
                ->leftJoin('re_house_categories', 're_house_categories.house_category_id', '=', 're_houses.house_category_id')
                ->orderByDesc('re_houses.updated_at')->take(3)->get();
            foreach ($houses as $item) {
                $added = new \stdClass();
                $added->name = $item->house_name;
                $added->link = 'bat-dong-san/' . $item->business_web_canonical . '/' . $item->category_web_canonical . '/' . $item->web_canonical;
                $added->image = DB::table('re_house_galleries')->where([
                    ['active_status', 1],
                    ['house_gallery_banner', 1],
                    ['house_id', $item->house_id]
                ])->select('house_gallery_link', 'house_gallery_name')->first();
                $added->address = $item->house_provincial;
                // $area = strlen($item->house_address) > 1 ? $item->house_address . ', ' : '';
                // $added->address = $area . $item->house_commune . ', ' . $item->house_district . ', ' . $item->house_provincial;
                $added->before_hour = now()->diffInHours($item->updated_at);
                $added->before_day = now()->diffInDays($item->updated_at);
                $addeds->add($added);
            }
            session()->put('listHouseRecentlyAdded', $addeds);
        } else
            $addeds = session('listHouseRecentlyAdded');
        return $addeds;
    }
}
