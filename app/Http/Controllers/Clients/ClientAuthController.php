<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ClientAuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:customer')->except('logout');
    }

    public function login(Request $request)
    {
        if ($request->isMethod('POST')) {
            $data = $request->all();
            $user_name = trim($data['user_name']);
            $user = array(
                'user_name' => $user_name,
                'password' => $data['password'],
                'active_status' => 1
            );

            if (Auth::guard('customer')->attempt($user)) {
                DB::table('re_administrators')->where('admin_id', Auth::guard('customer')->user()->admin_id)->update(['last_login_time' => Carbon::now()]);
                return redirect()->route('client.profile');
            }
            return redirect()->back()->withErrors('Tài khoản hoặc mật khẩu bạn nhập vào không chính xác! Hãy liên hệ với quản trị viên !');
        }


        return view('clients.pages.auth.login');
    }

    public function logout(Request $request)
    {
        if (Auth::guard('customer')->user() != null) {
            DB::table('re_administrators')->where('admin_id', Auth::guard('customer')->user()->admin_id)->update(['last_login_time' => Carbon::now()]);
        }

        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->route('index');
    }


    public function verify(Request $request)
    {
    }
    public function verifyAgain(Request $request)
    {
    }
    public function forget(Request $request)
    {
    }
    public function formPassword(Request $request)
    {
    }
    public function changePassword(Request $request)
    {
    }

    public function error(Request $request)
    {
        return view('clients.pages.auth.error404');
    }
}
