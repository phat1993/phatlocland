<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomCKFinderAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        config(['ckfinder.authentication' => function () {
            return true;
        }]);
        // if (Auth::guard('admin')->check())
        //     config(['ckfinder.authentication' => function () {
        //         return true;
        //     }]);
        // else
        //     config(['ckfinder.authentication' => function () {
        //         return false;
        //     }]);
        return $next($request);
    }
}
