<?php

namespace Database\Seeders;

use App\Models\Entities\Amenities;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AmenitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('re_amenities')->delete();

        $types = [
            [
                'amenity_id' => '1',
                'amenity_name' => 'Sân bóng rổ',
                'amenity_icon'     => 'fas fa-basketball-ball',
                'amenity_detail'     => 'sân bóng rổ',
                'admin_updated'     => 1,
                'active_status'     => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'amenity_id' => '2',
                'amenity_name' => 'Khu vực cấm hút thuốc',
                'amenity_icon'     => 'fas fa-smoking-ban',
                'amenity_detail'     => 'khu vực cấm hút thuốc',
                'admin_updated'     => 1,
                'active_status'     => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'amenity_id' => '3',
                'amenity_name' => 'Bãi đậu xe miễn phí trong khuôn viên',
                'amenity_icon'     => 'fas fa-car-side',
                'amenity_detail'     => 'Bãi đậu xe miễn phí trong khuôn viên',
                'admin_updated'     => 1,
                'active_status'     => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'amenity_id' => '4',
                'amenity_name' => 'Điều hòa nhiệt độ ',
                'amenity_icon'     => 'fas fa-fan',
                'amenity_detail'     => 'Điều hòa nhiệt độ ',
                'admin_updated'     => 1,
                'active_status'     => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'amenity_id' => '5',
                'amenity_name' => 'Gym',
                'amenity_icon'     => 'fas fa-dumbbell',
                'amenity_detail'     => 'Gym',
                'admin_updated'     => 1,
                'active_status'     => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'amenity_id' => '6',
                'amenity_name' => 'Hồ bơi',
                'amenity_icon'     => 'fas fa-swimmer',
                'amenity_detail'     => 'Hồ bơi',
                'admin_updated'     => 1,
                'active_status'     => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'amenity_id' => '7',
                'amenity_name' => 'Thú cưng',
                'amenity_icon'     => 'fas fa-paw',
                'amenity_detail'     => 'Thú cưng',
                'admin_updated'     => 1,
                'active_status'     => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'amenity_id' => '8',
                'amenity_name' => 'Máy giặt và máy sấy',
                'amenity_icon'     => 'fas fa-dumpster',
                'amenity_detail'     => 'Máy giặt và máy sấy',
                'admin_updated'     => 1,
                'active_status'     => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'amenity_id' => '9',
                'amenity_name' => 'Rạp hát tại nhà',
                'amenity_icon'     => 'fas fa-tv',
                'amenity_detail'     => 'Rạp hát tại nhà',
                'admin_updated'     => 1,
                'active_status'     => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'amenity_id' => '10',
                'amenity_name' => 'Khu hút thuốc',
                'amenity_icon'     => 'fas fa-smoking',
                'amenity_detail'     => 'Khu hút thuốc',
                'admin_updated'     => 1,
                'active_status'     => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'amenity_id' => '11',
                'amenity_name' => 'Wifi miễn phí',
                'amenity_icon'     => 'fas fa-wifi',
                'amenity_detail'     => 'Wifi miễn phí',
                'admin_updated'     => 1,
                'active_status'     => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ];

        Amenities::insert($types);
    }
}
