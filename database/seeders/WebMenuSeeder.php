<?php

namespace Database\Seeders;

use App\Models\Entities\WebMenu;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WebMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('re_web_menus')->delete();

        $menus = [
            [
                'menu_id' => '1',
                'menu_name' => 'Trang Chủ',
                'menu_order'     => '1',
                'menu_url'     => '/trang-chu',
                'menu_lv1'     => null,
                'menu_lv2'     => null,
                'admin_updated'     => 1,
                'active_status'     => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'menu_id' => '2',
                'menu_name' => 'Giới Thiệu',
                'menu_order'     => '2',
                'menu_url'     => '/gioi-thieu',
                'menu_lv1'     => null,
                'menu_lv2'     => null,
                'admin_updated'     => 1,
                'active_status'     => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'menu_id' => '3',
                'menu_name' => 'Dự Án',
                'menu_order'     => '3',
                'menu_url'     => '/du-an',
                'menu_lv1'     => null,
                'menu_lv2'     => null,
                'admin_updated'     => 1,
                'active_status'     => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'menu_id' => '4',
                'menu_name' => 'Bất Động Sản',
                'menu_order'     => '4',
                'menu_url'     => '/bat-dong-san',
                'menu_lv1'     => null,
                'menu_lv2'     => null,
                'admin_updated'     => 1,
                'active_status'     => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'menu_id' => '5',
                'menu_name' => 'Tuyển Dụng',
                'menu_order'     => '5',
                'menu_url'     => '/tuyen-dung',
                'menu_lv1'     => null,
                'menu_lv2'     => null,
                'admin_updated'     => 1,
                'active_status'     => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ];

        WebMenu::insert($menus);
    }
}
