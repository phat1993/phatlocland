<?php

namespace Database\Seeders;

use App\Models\Entities\WebInfo;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WebInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('re_web_infos')->delete();

        WebInfo::create(
            [
                'web_info_id'     => 1,
                'web_info_title'     => 'Giới Thiệu',
                'web_info_phone'     => '0123 456 789',
                'web_info_mail'   => 'mail@support.com',
                'web_info_address'   => '75 Đường số 7, KDC CITYLAND, P7, Q .Gò Vấp, TP .HCM',

                'web_info_zalo'   => '',
                'web_info_facebook'   => 'https://vi-vn.facebook.com/',
                'web_info_youtube'   => 'https://www.youtube.com/',
                'web_info_instagram'   => 'https://www.instagram.com/',
                'web_info_work_time'   => '<p><strong>Chủ Nhật : </strong>Đ&oacute;ng cửa</p> <p><strong>Thứ 2 - Thứ 6 : </strong>09:00 - 18:00</p> <p><strong>Thứ 7 : </strong>09:00 - 14:00</p>',
                'web_info_map_location'   => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d244.92295401332478!2d106.68051961899081!3d10.829114607536257!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317528fe97051645%3A0x734e3f1e079cc9bb!2zUk9NQUQgU1RPUkUgR8OyIFbhuqVw!5e0!3m2!1svi!2s!4v1618284229384!5m2!1svi!2s',
                'web_info_special_note'   => 'Phát Lộc Real Land - Tin tức mới nóng về giá nhà đất, các dự án bất động sản. Tư vấn mua bán nhà đất, mẫu thiết kế nhà đẹp, thiết kế nội thất hợp phong thủy.',

                'web_keywords'   => 'phatlocland',
                'web_title'   => 'Thay đồi, khác biệt, nâng tầm',
                'web_description'   => 'Thay đồi, khác biệt, nâng tầm',
                'web_canonical'   => 'http://phatlocland.vn',

                'admin_updated'     => 1,
                'active_status'     => 1,
            ]
        );
    }
}
