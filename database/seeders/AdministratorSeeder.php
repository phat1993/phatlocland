<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Entities\Administrator;
use Illuminate\Support\Facades\DB;

class AdministratorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('re_administrators')->delete();
        Administrator::create(
            [
                'admin_id'     => '1',
                'user_name'     => 'trung001',
                'password'     => Hash::make('123'),
                'admin_forget_code'   => random_int(100000000, 999999999999999999),

                'admin_avatar'     => 'asset/admins/images/demo/users/face11.jpg',
                'admin_full_name'     => 'Phạm Văn Trung',
                'admin_brithday'     => '1995/01/01',
                'admin_gender'     => 1,
                'admin_phone'     => '0909066066',
                'admin_fax'     => '012301230123',
                'admin_email'     => 'mail@gmail.com',
                'admin_zip_code'     => '10000010',//
                'admin_address'     => 'Số 22/2/24 Đường Cao Tốc P.10 Quận Tân Bình',
                'special_notes'     => 'Never give up',
                'admin_updated'     => 1,
                'active_status'     => 1,
            ]
        );
    }
}
