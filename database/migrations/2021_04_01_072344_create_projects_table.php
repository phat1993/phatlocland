<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     * Du an
     *
     * @return void
     */
    public function up()
    {
        Schema::create('re_projects', function (Blueprint $table) {
            $table->increments('project_id');
            //Common Info
                //Object Info
            $table->string('project_name', 500);
            $table->string('project_description', 1000)->nullable();
            $table->text('project_detail')->nullable();
            $table->string('project_video', 500)->nullable();
            $table->text('project_amenities')->nullable();

                //Address
            $table->string('project_postal_code', 100)->nullable();//ma buu dien
            $table->string('project_provincial', 100)->nullable();//tinh, thanh pho
            $table->string('project_district', 100)->nullable();//huyen, tp truc thuoc
            $table->string('project_commune', 100)->nullable();//phuong, xa
            $table->string('project_address', 200)->nullable();//dia chi
            $table->string('project_map_location', 1000)->nullable();

            $table->string('project_own', 100)->nullable();//Chu thau
            $table->date('project_build_time')->nullable();//thoi gian xay dung
            $table->string('project_block', 50)->nullable();//so toa nha
            $table->string('project_floor', 50)->nullable();//so tang trung binh
            $table->string('project_house', 50)->nullable();//so bat dong san trung binh
            $table->string('project_acreage', 50)->nullable();//tong dien tich
            $table->string('project_house_acreage', 50)->nullable();//dien tich can nha trung binh
                //Reference Info
            $table->Integer('project_category_id');

                //Web Info
            $table->Integer('project_viewed')->default(0);//luot view
            $table->string('web_keywords', 200)->nullable();
            $table->string('web_description', 145)->nullable();
            $table->string('web_title', 50)->nullable();
            $table->string('web_canonical', 200)->nullable();
            //General Info
            $table->Integer('admin_updated')->default(1);
            $table->tinyInteger('active_status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('re_projects');
    }
}
