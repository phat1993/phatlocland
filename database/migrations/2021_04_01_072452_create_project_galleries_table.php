<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     * Hinh anh cua du an
     *
     * @return void
     */
    public function up()
    {
        Schema::create('re_project_galleries', function (Blueprint $table) {
            $table->increments('project_gallery_id');
            //Object Info
            $table->string('project_gallery_name', 100)->nullable();//ten
            $table->tinyInteger('project_gallery_banner')->default(0);//Hinh anh bang ron 1: is banner, 0: isn't banner
            $table->string('project_gallery_link', 200);//hinh anh
            $table->smallInteger('project_gallery_order')->default(1);//sap xep
            //Reference Info
            $table->Integer('project_id');
            //General Info
            $table->tinyInteger('active_status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('re_project_galleries');
    }
}
