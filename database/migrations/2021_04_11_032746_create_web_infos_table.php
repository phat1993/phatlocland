<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('re_web_infos', function (Blueprint $table) {
            $table->increments('web_info_id');
            $table->string('web_info_title', 100);
            $table->string('web_info_phone', 50);
            $table->string('web_info_mail', 50);
            $table->string('web_info_address', 200);

            $table->string('web_info_zalo', 500)->nullable();
            $table->string('web_info_facebook', 500)->nullable();
            $table->string('web_info_youtube', 500)->nullable();
            $table->string('web_info_instagram', 500)->nullable();
            //General Info
            $table->Integer('admin_updated')->default(1);
            $table->tinyInteger('active_status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('re_web_infos');
    }
}
