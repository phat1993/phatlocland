<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaSupportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('re_media_supports', function (Blueprint $table) {
            $table->increments('media_support_id');
            //Object Info
            $table->string('media_support_name', 50);
            $table->string('media_support_url', 500);
            $table->Integer('media_support_size');
            $table->string('media_support_extension', 50);
            $table->string('media_support_description',200)->nullable();
            //General Info
            $table->Integer('admin_updated')->default(1);
            $table->dateTime('created_at')->default(now());
            $table->dateTime('updated_at')->default(now());
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('re_media_supports');
    }
}
