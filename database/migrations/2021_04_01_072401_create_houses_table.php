<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     * bat dong san
     *
     * @return void
     */
    public function up()
    {
        Schema::create('re_houses', function (Blueprint $table) {
            $table->increments('house_id');
            $table->string('house_code', 15)->unique();
            //Object Info
                //Common
            $table->string('house_name', 500);
            $table->string('house_description', 1000)->nullable();
            $table->string('house_video', 500)->nullable();
            $table->text('house_detail')->nullable();
            $table->text('house_amenities')->nullable();

            $table->string('house_own', 100)->nullable();//Chu thau
            $table->date('house_build_time')->nullable();//thoi gian xay dung
            $table->string('house_acreage', 50)->nullable();//tong dien tich
            $table->string('house_length', 10)->nullable();//Chieu dai
            $table->string('house_width', 10)->nullable();//Chieu rong
            $table->string('house_direction', 50)->nullable();//huong
            $table->smallInteger('house_rooms')->nullable();//So phong max:100
            $table->tinyInteger('house_bath_rooms')->nullable();//So phong tam
            $table->tinyInteger('house_garages')->nullable();//nha de xe

                //Address
            $table->string('house_postal_code', 100)->nullable();//ma buu dien
            $table->string('house_provincial', 100)->nullable();//tinh, thanh pho
            $table->string('house_district', 100)->nullable();//huyen, tp truc thuoc
            $table->string('house_commune', 100)->nullable();//phuong, xa
            $table->string('house_address', 200)->nullable();//dia chi
            $table->string('house_map_location', 1000)->nullable();//dinh vi tren map

                //Price
            $table->decimal('house_price', 18, 0)->nullable();;
            $table->decimal('house_price_sub', 18, 0)->nullable();


            //Trang thai sale-status
            $table->tinyInteger('house_rating')->default(5);
            $table->Integer('house_rating_count')->nullable()->default(0);//Chi so rating se tu cap nhat khi review cua customer duoc active
            $table->Integer('house_rating_point')->nullable()->default(0);//Chi so rating se tu cap nhat khi review cua customer duoc active
            $table->string('house_status', 20)->nullable();
            $table->string('house_status_color', 20)->nullable();


            //Reference Info
            $table->Integer('house_business_id');
            $table->Integer('house_category_id');
            $table->Integer('project_id')->nullable();

            //Web Info
            $table->Integer('house_viewed')->default(0);//luot view
            $table->string('web_keywords', 200)->nullable();
            $table->string('web_description', 145)->nullable();
            $table->string('web_title', 50)->nullable();
            $table->string('web_canonical', 200)->nullable();
            //General Info
            $table->Integer('admin_updated')->default(1);
            $table->tinyInteger('active_status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('re_houses');
    }
}
