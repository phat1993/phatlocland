<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('re_web_reviews', function (Blueprint $table) {
            $table->increments('web_review_id');
            //Object Info
            $table->string('web_review_image', 100);
            $table->string('web_review_name', 50);
            $table->string('web_review_address', 100)->nullable();
            $table->string('web_review_detail',500)->nullable();
            //General Info
            $table->Integer('admin_updated')->default(1);
            $table->tinyInteger('active_status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('re_web_reviews');
    }
}
