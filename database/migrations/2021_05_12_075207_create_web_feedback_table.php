<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('re_web_feedbacks', function (Blueprint $table) {
            $table->increments('web_feedback_id');
            //Object Info
            $table->string('web_feedback_name', 50);
            $table->string('web_feedback_email', 100)->nullable();
            $table->string('web_feedback_content',200)->nullable();
            //General Info
            $table->Integer('admin_updated')->default(1);
            $table->tinyInteger('active_status')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('re_web_feedbacks');
    }
}
