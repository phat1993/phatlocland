<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmenitiesTable extends Migration
{
    /**
     * Run the migrations.
     * Tien nghi cua bat dong san
     *
     * @return void
     */
    public function up()
    {
        Schema::create('re_amenities', function (Blueprint $table) {
            $table->increments('amenity_id');
            //Object Info
            $table->string('amenity_name', 50);//ten tien nghi
            $table->string('amenity_icon', 100)->nullable();// ki hieu trong fontawsome
            $table->string('amenity_detail', 200)->nullable();//mo ta
            //General Info
            $table->Integer('admin_updated')->default(1);
            $table->tinyInteger('active_status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('re_amenities');
    }
}
