<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHouseGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     * Hinh anh cua bat dong san
     *
     * @return void
     */
    public function up()
    {
        Schema::create('re_house_galleries', function (Blueprint $table) {
            $table->increments('house_gallery_id');
            //Object Info
            $table->string('house_gallery_name', 100)->nullable();//ten
            $table->tinyInteger('house_gallery_banner')->default(1);//Banner
            $table->string('house_gallery_link', 200);//hinh anh
            $table->smallInteger('house_gallery_order')->default(1);
            //Reference Info
            $table->Integer('house_id');
            //General Info
            $table->tinyInteger('active_status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('re_house_galleries');
    }
}
