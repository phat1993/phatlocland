<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebMenusTable extends Migration
{
    /**
     * Run the migrations.
     * Danh sach menu
     * Neu 2 lv de null thi la 'cap lon nhat'
     * Neu lv_1 co du lieu thi nam trong 'cap lon nhat'
     * Neu 2 lv co du lieu thi 'cap nho nhat'
     *
     * @return void
     */
    public function up()
    {
        Schema::create('re_web_menus', function (Blueprint $table) {
            $table->increments('menu_id');
            //Object info
            $table->string('menu_name', 50);
            $table->smallInteger('menu_order')->default(1);
            $table->string('menu_url', 300);
            $table->string('menu_lv1', 5)->nullable();//Su dung id menu
            $table->string('menu_lv2', 5)->nullable();//Su dung id menu
            //General Info
            $table->Integer('admin_updated')->default(1);
            $table->tinyInteger('active_status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('re_web_menus');
    }
}
