<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdministratorsTable extends Migration
{
    /**
     * Run the migrations.
     * Quan Tri Vien
     *
     * @return void
     */
    public function up()
    {
        Schema::create('re_administrators', function (Blueprint $table) {
            $table->increments('admin_id');
            //Account info
            $table->string('user_name', 30);
            $table->string('password', 100);
            $table->string('admin_forget_code', 20);//Dung de tao link lay lai mat khau
            $table->timestamp('last_login_time')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('admin_login_lock')->nullable();//thoi gian khoa, sau 1 ngay thi mo khoa
            $table->tinyInteger('admin_login_fail')->default(0);//max: 3 khoa
            //Person Info
            $table->string('admin_avatar', 100);
            $table->string('admin_full_name', 100)->nullable();
            $table->date('admin_brithday', 50)->nullable();
            $table->tinyInteger('admin_gender')->default(1);//1:name,0:nu
            $table->string('admin_phone', 20)->nullable();
            $table->string('admin_fax', 20)->nullable();
            $table->string('admin_email', 50)->nullable();
            $table->string('admin_zip_code', 10)->nullable();
            $table->string('admin_address', 200)->nullable();
            $table->string('special_notes', 200)->nullable();
            //General Info
            $table->Integer('admin_updated')->default(1);
            $table->tinyInteger('active_status')->default(1);
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('re_administrators');
    }
}
