<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFloorPlansTable extends Migration
{
    /**
     * Run the migrations.
     * Ban ve ky thuat
     *
     * @return void
     */
    public function up()
    {
        Schema::create('re_floor_plans', function (Blueprint $table) {
            $table->increments('floor_plan_id');
            //Object Info
            $table->string('floor_plan_name', 100);
            $table->string('floor_plan_image', 100);
            $table->string('floor_plan_size', 50)->nullable();
            $table->string('floor_plan_rooms', 50)->nullable();
            $table->string('floor_plan_barths', 50)->nullable();
            $table->decimal('floor_plan_price', 18, 0)->nullable();
            //Reference Info
            $table->Integer('house_id');
            //General Info
            $table->Integer('admin_updated')->default(1);
            $table->tinyInteger('active_status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('re_floor_plans');
    }
}
