<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     * danh gia ve bat dong san
     *
     * @return void
     */
    public function up()
    {
        Schema::create('re_house_reviews', function (Blueprint $table) {
            $table->increments('house_review_id');
            //Object Info
            $table->string('house_review_avatar', 100)->nullable();
            $table->tinyInteger('house_review_sub')->default(1);//Chi muc do cac review, lv1=1, lv2=2
            $table->string('house_review_message', 500);
            $table->tinyInteger('house_review_point')->default(5);
            //Customer Info
            $table->string('house_review_name', 100)->nullable();
            $table->string('house_review_email', 50);
            $table->string('house_review_phone', 20)->nullable();
            //Reference Info
            $table->Integer('house_id');
            $table->string('customer_code')->nullable();//lien ket voi khach hang
            //General Info
            $table->Integer('admin_updated')->default(1);
            $table->tinyInteger('active_status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('re_house_reviews');
    }
}
