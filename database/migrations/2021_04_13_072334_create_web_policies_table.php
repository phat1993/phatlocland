<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebPoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('re_web_policies', function (Blueprint $table) {
            $table->increments('web_policy_id');
            //Object Info
            $table->string('web_policy_name', 200);
            $table->string('web_policy_image', 100)->nullable();
            $table->string('web_policy_description', 500)->nullable();
            $table->text('web_policy_detail')->nullable();
            //Web Info
            $table->string('web_keywords', 200)->nullable();
            $table->string('web_description', 145)->nullable();
            $table->string('web_title', 50)->nullable();
            $table->string('web_canonical', 200)->nullable();
            //General Info
            $table->Integer('admin_updated')->default(1);
            $table->tinyInteger('active_status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('re_web_policies');
    }
}
