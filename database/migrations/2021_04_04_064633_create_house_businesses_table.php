<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHouseBusinessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('re_house_businesses', function (Blueprint $table) {
            $table->increments('house_business_id');
            //Object Info
            $table->string('house_business_name', 100);
            $table->string('house_business_description', 200)->nullable();
            //Web Info
            $table->string('web_keywords', 200)->nullable();
            $table->string('web_description', 145)->nullable();
            $table->string('web_title', 50)->nullable();
            $table->string('web_canonical', 200)->nullable();
            //General Info
            $table->Integer('admin_updated')->default(1);
            $table->tinyInteger('active_status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('re_house_businesses');
    }
}
