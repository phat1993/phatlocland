<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     * Bang ron trang chu
     *
     * @return void
     */
    public function up()
    {
        Schema::create('re_banners', function (Blueprint $table) {
            $table->increments('banner_id');
            //Object info
            $table->string('banner_name', 50);
            $table->string('banner_url', 300);
            $table->smallInteger('banner_order');
            $table->string('banner_description', 200)->nullable();
            $table->string('banner_detail', 500)->nullable();
            $table->string('banner_image', 100);
            //General Info
            $table->Integer('admin_updated')->default(1);
            $table->tinyInteger('active_status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('re_banners');
    }
}
