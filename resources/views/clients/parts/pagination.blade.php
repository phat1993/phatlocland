@if ($paginator->hasPages())
    <div class="row">
        <div class="col-md-8 offset-md-2  col-xs-12 ">
            <div class="page-num text-center">
                <ul>
                    @if ($paginator->onFirstPage()==false)
                        <li><a href="{{ $paginator->previousPageUrl() }}"><i class="lnr lnr-chevron-left"></i></a></li>
                    @endif
                    @foreach ($elements as $element)
                        @if (is_string($element))
                            <li class="disabled">
                                <a href="#">{{ $element }}
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>
                        @endif

                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <li class="active"><a href="#">{{ $page }}</a></li>
                                @else
                                    <li><a href="{{ $url }}">{{ $page }}</a></li>
                                @endif
                            @endforeach
                        @endif
                    @endforeach


                    @if ($paginator->hasMorePages())
                        <li><a href="{{ $paginator->nextPageUrl() }}"><i class="lnr lnr-chevron-right"></i></a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endif

