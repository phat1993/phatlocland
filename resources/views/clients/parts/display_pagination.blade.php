@if ($paginator->hasPages())
    <div class="col-lg-5 col-sm-12">
        <div class="item-element res-box  text-right sm-left">
            <p>Hiển thị <span>{{ $paginator->currentPage() }}-{{ $paginator->currentPage()*$paginator->perPage() }} trong số {{ $paginator->total() }}</span> đối tượng</p>
        </div>
    </div>
@endif

