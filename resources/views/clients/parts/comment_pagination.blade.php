@if ($paginator->hasPages())
<ul class="pagination pagination-sm justify-content-center">
    @if ($paginator->onFirstPage()==false)
        <li class="page-item"><a class="page-link" href="{{ $paginator->previousPageUrl() }}"><i class="lnr lnr-chevron-left"></i></a></li>
    @endif
    @foreach ($elements as $element)
        @if (is_string($element))
            <li class="page-item disabled">
                <a class="page-link" href="#">{{ $element }}
                    <span class="sr-only">(current)</span>
                </a>
            </li>
        @endif

        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <li class="page-item active"><a class="page-link" href="#">{{ $page }}</a></li>
                @else
                    <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                @endif
            @endforeach
        @endif
    @endforeach


    @if ($paginator->hasMorePages())
        <li class="page-item"><a class="page-link" href="{{ $paginator->nextPageUrl() }}"><i class="lnr lnr-chevron-right"></i></a></li>
    @endif
</ul>
@endif

