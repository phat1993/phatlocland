@extends('layouts.client_layout')
@section('title', 'Tuyển dụng')
@section('content')

        <!--Breadcrumb section starts-->
        <div class="breadcrumb-section bg-h" style="background-image:url({{ asset('asset/clients/images/breadcrumb/breadcrumb_2.jpg') }})">
            <div class="overlay op-5"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        <div class="breadcrumb-menu">
                            <h2>Tuyển dụng</h2>
                            <span><a href="{{ route('clients.index') }}">Trang chủ</a></span>
                            <span>Tuyển dụng</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Breadcrumb section ends-->

        <!--Blog post starts-->
        <div class="blog-area section-padding pb-40">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @forelse ($webHires as $item)
                            <div class="single-blog-item v3">
                                <div class="row">
                                    <div class="col-md-10 offset-md-1">
                                        <img src="{{ asset('storage/'.$item->web_hire_image) }}" alt="{{ $item->web_hire_name }}">
                                    </div>
                                    <div class="col-md-8 offset-md-2">
                                        <div class="blog-content text-center pt-30    ">
                                            <a href="#" class="blog-cat">{{ $item->web_hire_description }}</a>
                                            <p class="date">{{ date('d/m/Y',strtotime($item->updated_at)) }} {{-- <a href="#" class="text-dark">Louis Fonsi</a> --}}</p>

                                            <h4 class="card-title"><a href="{{ url('tuyen-dung/'.$item->web_canonical) }}">{{ $item->web_hire_name }}</a></h4>
                                            <a href="{{ url('tuyen-dung/'.$item->web_canonical) }}" class="btn v7">Chi Tiết</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @empty

                        @endforelse
                    </div>
                </div>
                <!--pagination starts-->
                <div class="post-nav nav-res pt-20 pb-60">
                    {{ $webHires->links('clients.parts.pagination') }}
                </div>
                <!--pagination ends-->
            </div>
        </div>
        <!--Blog post ends-->
@stop

@section('javascript')

@stop
