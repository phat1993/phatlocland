@extends('layouts.client_layout')
@section('title')
    {{ $webHire->web_title }}
@endsection
@section('styles')
    <meta name="keywords" content="{{ $webHire->web_keywords }}">
    <meta name="description" content="{{ $webHire->web_description }}">
    @isset($category)
        <link rel="canonical" href="{{ url('tuyen-dung/'.$category->web_canonical.'/'.$webHire->web_canonical) }}">
    @else
        <link rel="canonical" href="{{ url('tuyen-dung/'.$webHire->web_canonical) }}">
    @endisset
@endsection
@section('content')
        <!--Breadcrumb section starts-->
        <div class="breadcrumb-section bg-h" style="background-image:url({{ asset('asset/clients/images/breadcrumb/breadcrumb_2.jpg') }})">
            <div class="overlay op-5"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        <div class="breadcrumb-menu">
                            <h2>Tuyển dụng</h2>
                            <span><a href="{{ route('clients.index') }}">Trang chủ</a></span>
                            <span>{{ $webHire->web_hire_name }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Breadcrumb section ends-->
 <!--Blog Details section starts-->
 <div class="blog-area">
    <div class="container">
        <div class="row">
            <!--Blog post starts-->
            <div class="col-md-10 offset-md-1 py-100">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <article class="post-single">
                                <div class="post-content-wrap">
                                    {!! $webHire->web_hire_detail !!}
                                </div>
                            </article>
                            {{-- <div class="comments">
                                <h3 class="comments-title">Comments (4)</h3>
                                <ul class="comment-list">
                                    <div class="review-box comment-box">
                                        <ul class="review_wrap">
                                            <li>
                                                <div class="customer-review_wrap">
                                                    <div class="reviewer-img">
                                                        <img src="images/others/client_4.jpg" class="img-fluid" alt="#">
                                                    </div>
                                                    <div class="customer-content-wrap">
                                                        <div class="customer-content">
                                                            <div class="customer-review">
                                                                <h6>Bob Haris</h6>
                                                                <p class="comment-date">
                                                                    October 02, 2017
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <p class="customer-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium voluptates eum, velit recusandae, ducimus earum aperiam commodi error officia optio aut et quae nam nostrum!
                                                        </p>
                                                        <div class="like-btn">
                                                            <a href="#" class="rate-review float-right"><i class="fas fa-reply"></i>Reply</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <ul class="review_wrap has-child">
                                                    <li>
                                                        <div class="customer-review_wrap">
                                                            <div class="reviewer-img">
                                                                <img src="images/others/client_3.jpg" class="img-fluid" alt="#">
                                                            </div>
                                                            <div class="customer-content-wrap">
                                                                <div class="customer-content">
                                                                    <div class="customer-review">
                                                                        <h6>Amanda</h6>
                                                                        <p class="comment-date">
                                                                            October 02, 2017
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <p class="customer-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore quod, nihil consectetur sit natus eum similique quae omnis doloribus culpa!
                                                                </p>
                                                                <div class="like-btn">
                                                                    <a href="#" class="rate-review float-right"><i class="fas fa-reply"></i>Reply</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="customer-review_wrap">
                                                            <div class="reviewer-img">
                                                                <img src="images/others/client_2.jpg" class="img-fluid" alt="#">
                                                            </div>
                                                            <div class="customer-content-wrap">
                                                                <div class="customer-content">
                                                                    <div class="customer-review">
                                                                        <h6>Louis Fonsi</h6>
                                                                        <p class="comment-date">
                                                                            October 02, 2017
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <p class="customer-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore quod, nihil consectetur sit natus eum similique quae omnis doloribus culpa!
                                                                </p>
                                                                <div class="like-btn">
                                                                    <a href="#" class="rate-review float-right"><i class="fas fa-reply"></i>Reply</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <div class="customer-review_wrap">
                                                    <div class="reviewer-img">
                                                        <img src="images/others/client_1.jpg" class="img-fluid" alt="#">
                                                    </div>
                                                    <div class="customer-content-wrap">
                                                        <div class="customer-content">
                                                            <div class="customer-review">
                                                                <h6>Stacy Grace</h6>
                                                                <p class="comment-date">
                                                                    October 02, 2017
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <p class="customer-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium voluptates eum, velit recusandae, ducimus earum aperiam commodi error officia optio aut et quae nam nostrum!
                                                        </p>
                                                        <div class="like-btn">
                                                            <a href="#" class="rate-review float-right"><i class="fas fa-reply"></i>Reply</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </ul>
                            </div>
                            <div class="comment-respond mt-60">
                                <h3>Write a comment</h3>
                                <p>Your email address will not be published. Required fields are marked with *</p>
                                <form class="contact-form pt-30    " id="leave-review">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input class="contact-form__input-text" type="text" name="name" id="name" placeholder="Name*">
                                        </div>
                                        <div class="col-md-6">
                                            <input class="contact-form__input-text" type="text" name="mail" id="mail" placeholder="Email*">
                                        </div>
                                    </div>
                                    <textarea class="contact-form__textarea" name="comment" id="comment" placeholder="Comment"></textarea>
                                    <input class="btn v3" type="submit" name="submit-contact" id="submit_contact" value="Submit">
                                </form>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
            <!--Blog post ends-->
        </div>
    </div>
</div>
<!--Blog Details section  ends-->

@stop

@section('javascript')


@stop
