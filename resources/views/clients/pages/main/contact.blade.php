@extends('layouts.client_layout')
@section('title', 'Liên hệ')

@section('content')

        <!--Breadcrumb section starts-->
        <div class="breadcrumb-section bg-h" style="background-image:url({{ asset('asset/clients/images/breadcrumb/breadcrumb_2.jpg') }})">
            <div class="overlay op-5"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        <div class="breadcrumb-menu">
                            <h2>Liên hệ</h2>
                            <span><a href="{{ route('clients.index') }}">Trang chủ</a></span>
                            <span>Liên hệ</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Breadcrumb section ends-->
        <!--Contact info starts -->
        <div class="contact-info section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="contact-info-item">
                            <i class="fas fa-map-marker-alt"></i>
                            <h4>Văn phòng công ty</h4>
                            <p>	{{ $webInfo->web_info_address }}</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="contact-info-item">
                            <i class="fas fa-phone-alt"></i>
                            <h4>Liên hệ trực tiếp</h4>
                            <p><strong>Điện Thoại : </strong> {{ $webInfo->web_info_phone }}</p>
                            <p><strong>Email : </strong> {{ $webInfo->web_info_mail }}</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="contact-info-item">
                            <i class="fas fa-business-time"></i>
                            <h4>Giờ mở cửa</h4>
                            {!! $webInfo->web_info_work_time !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Contact info ends -->
        <!--Contact section starts-->
        <div class="contact-section v1 mt-10 mb-120">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <iframe src="{{ $webInfo->web_info_map_location }}" class="w-100" height="600" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="section-title v2">
                            <h2>Lời nhắn cho chúng tôi</h2>
                        </div>
                        <form action="{{ route('clients.contact') }}" method="post" name="contact-form">
                            {{csrf_field()}}
                            <div class="form-control-wrap">
                                <div id="message" class="alert alert-danger alert-dismissible fade"></div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="fname" placeholder="Họ tên*" name="fname" value="{{ old('fname') }}">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="email_address" placeholder="email*" name="email" value="{{ old('email') }}">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="8" name="comment" id="comment" placeholder="Lời nhắn">{{ old('comment') }}</textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn v7">Gửi cho chúng tôi</button>
                                </div>
                            </div>
                        </form>
                        @if(session('success'))
                        <div class="alert alert-success notifyHidden">
                            <span>{{session('success')}}</span>
                        </div>
                        @endif

                        @if(count($errors) > 0)
                            <div class="alert alert-danger notifyHidden">
                                @foreach($errors->all() as $error)
                                <p>{{$error}}</p>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!--Contact section ends-->
@stop

@section('javascript')

<script src="{{ asset('asset/clients/js/validate.min.js')}}"></script>
<script>
    $( document ).ready( function () {
        $( "form[name=contact-form]" ).validate({
            rules: {
                fname: {
                    required: true,
                    maxlength: 50
                },
                email: {
                    required: true,
                    email: true,
                    maxlength: 50
                },
                comment: {
                    required: true,
                    minlength: 10,
                    maxlength: 500
                }
            },
            messages: {
                fname:  {
                    required: "Hãy nhập vào họ tên.",
                    maxlength: "Bạn chỉ được nhập tối đa 30 ký tự."
                },
                email:  {
                    required: "Hãy nhập vào email.",
                    email: "Hãy nhập vào chính xác địa chỉ email.",
                    maxlength: "Bạn chỉ được nhập tối đa 50 ký tự."
                },
                comment:  {
                    required: "Hãy nhập vào Nội Dung.",
                    minlength: "Bạn phải nhập tối thiểu 10 ký tự.",
                    maxlength: "Bạn chỉ được nhập tối đa 500 ký tự."
                },
            },
            errorElement: "span",
            errorPlacement: function ( error, element ) {
                // Add the `invalid-feedback` class to the error element
                error.addClass("form-text").addClass( "text-danger" );
                error.insertAfter( element );
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
            }
        });
    });

</script>
@stop
