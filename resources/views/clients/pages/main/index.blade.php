@extends('layouts.client_layout')
@section('title', 'Bất Động Sản')

@section('content')

<!--Hero section starts-->
<div class="hero-slider v2">
    <div class="single-property-header v1">
        <div class="hero-slider-wrap swiper-container">
            <div class="swiper-wrapper">
                @forelse ($banners as $item)
                <div class="swiper-slide">
                    <div class="property-slider-img bg-f" style="background-image: url({{ $item->image!=''?asset('storage/'.$item->image): asset('asset/clients/images/default/default_house_banner.jpg')}});">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 col-md-7 col-sm-12">
                                    <div class="list-details-title v3">
                                        <div class="single-listing-title float-left">
                                            <h5>{{ $item->title }}</h5>
                                            <p><i class="fas fa-map-marker-alt"></i>{{ $item->address }}</p>

                                            <ul class="property-feature">
                                                @isset($item->direction)
                                                    <li> <i class="fas fa-compass"></i>
                                                        <span>Hướng {{ $item->direction }}</span>
                                                    </li>
                                                @endisset
                                                @isset($item->acreage)
                                                <li> <i class="fas fa-arrows-alt"></i>
                                                    <span>{{ $item->acreage }}</span>
                                                </li>
                                                @endisset
                                                @isset($item->length)
                                                    <li> <i class="fas fa-ruler-vertical"></i>
                                                        <span>{{ $item->length }} m</span>
                                                    </li>
                                                @endisset
                                                @isset($item->width)
                                                    <li> <i class="fas fa-ruler-horizontal"></i>
                                                        <span>{{ $item->width }} m</span>
                                                    </li>
                                                @endisset
                                            </ul>
                                            <div class="trend-open">
                                                <p>
                                                    @if ($item->price<=0)
                                                        <strong class="text-danger">Thương lượng</strong>
                                                    @else
                                                        @if (strlen($item->price)>9)
                                                            {{ round($item->price/1000000000, 2).' Tỷ' }}
                                                        @elseif(strlen($item->price)>6)
                                                            {{ is_int(($item->price/1000000))?($item->price/1000000).' Triệu': number_format($item->price,0,'',',') }}
                                                        @else
                                                            {{ number_format($item->price,0,'',',') }}
                                                        @endif
                                                        <small class="text-danger">vnđ</small>
                                                    @endif
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @empty
                @endforelse
            </div>
            <!--Slider Arrows-->
            <div class="single-property_btn">
                <div class="single-property_prev"><i class="lnr lnr-chevron-left"></i></div>
                <div class="single-property_next"><i class="lnr lnr-chevron-right"></i></div>
            </div>
        </div>
    </div>
</div>
<!--Hero section ends-->
<!--Form starts-->
<div class="container slider-form v2 mt-130">
    <div class="row">
        <div class="col-md-12">
            <div class="section-title v1">
                <p>Bạn muốn tìm kiếm bất động sản phù hợp ?</p>
                <h2>Tìm kiếm ngay</h2>
            </div>
        </div>
        <div class="col-md-12">
            <form class="hero__form v1 filter listing-filter property-filter" action="{{ route('clients.house.default') }}" method="get" name="filter-form">
                {{csrf_field()}}

                <input type="hidden" name="sort" value="new">
                <div class="row">
                    <div class="col-xl-5 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                        <div class="input-search">
                            <input type="text" placeholder="Nhập vào tên, địa chỉ ..." id="address" name="address" value="">
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                        <select class="hero__form-input  form-control custom-select"name="provincial" onchange="changeType()">
                            <option value="">Chọn Tỉnh/ Thành Phố</option>
                            @forelse ($provincials as $item)
                                <option value="{{ $item->house_provincial }}">{{ $item->house_provincial }}</option>
                            @empty

                            @endforelse
                        </select>
                    </div>
                    <div class="col-xl-2 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                        <select class="hero__form-input  form-control custom-select" id="filterDistrict" name="district">
                            <option value="">Chọn Quận/ Huyện</option>
                        </select>
                    </div>
                    {{-- <div class="col-xl-2 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                        <select class="hero__form-input  form-control custom-select" id="filterCommenu" name="district">
                            <option value="">Chọn Phường/ Xã</option>
                        </select>
                    </div> --}}
                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                        <div class="submit_btn">
                            <button class="btn v3" type="submit">Tìm kiếm</button>
                        </div>
                        <div class="dropdown-filter"><span>Thêm</span></div>
                    </div>
                    <div class="explore__form-checkbox-list full-filter">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 py-1 pr-30">
                                <select class="hero__form-input  form-control custom-select mb-20" name="filterBusiness">
                                    <option value="">Kinh Doanh</option>
                                    @forelse ($filterBusinesses as $item)
                                        <option value="{{ $item->house_business_id }}">{{ $item->house_business_name }}</option>
                                    @empty

                                    @endforelse
                                </select>
                            </div>
                            <div class="col-lg-4 col-md-6 py-1 pr-30">
                                <select class="hero__form-input  form-control custom-select mb-20" name="filterCategory">
                                    <option value="">Danh Mục</option>
                                    @forelse ($filterCategories as $item)
                                        <option value="{{ $item->house_category_id }}">{{ $item->house_category_name }}</option>
                                    @empty

                                    @endforelse
                                </select>
                            </div>
                            @if($filterProjects!=null && $filterProjects->count()>0)
                            <div class="col-lg-4 col-md-6 py-1 pr-30">
                                <select class="hero__form-input form-control custom-select mb-20" name="filterProject">
                                    <option value="">Dự Án</option>
                                    @forelse ($filterProjects as $item)
                                        <option value="{{ $item->project_id }}">{{ $item->project_name }}</option>
                                    @empty

                                    @endforelse
                                </select>
                            </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 py-1 pr-30">
                                <select class="hero__form-input form-control custom-select mb-20" name="filterPrice">
                                    <option value="">Giá Tiền</option>
                                    <option value="below">dưới 1 triệu</option>
                                    <option value="10">1 - 10 triệu</option>
                                    <option value="20">10 - 20 triệu</option>
                                    <option value="50">20 - 50 triệu</option>
                                    <option value="100">50 - 100 triệu</option>
                                    <option value="more">trên 100 triệu</option>
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-6 py-1 pr-30 ">
                                <select class="hero__form-input form-control custom-select mb-20" name="filterDirection">
                                    <option value="">Hướng Nhà</option>
                                    <option value="Đông">Đông</option>
                                    <option value="Tây">Tây</option>
                                    <option value="Nam">Nam</option>
                                    <option value="Bắc">Bắc</option>
                                    <option value="Đông Bắc">Đông Bắc</option>
                                    <option value="Đông Nam">Đông Nam</option>
                                    <option value="Tây Bắc">Tây Bắc</option>
                                    <option value="Tây Nam">Tây Nam</option>
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-6 py-1 pr-30 ">
                                <select class="hero__form-input form-control custom-select mb-20" name="filterRooms">
                                    <option value="">Số Phòng</option>
                                    <option value="1">1 phòng</option>
                                    <option value="2">2 phòng</option>
                                    <option value="3">3 phòng</option>
                                    <option value="4">4 phòng</option>
                                    <option value="5">5 phòng</option>
                                    <option value="more">Trên 5 phòng</option>
                                </select>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 py-1 pr-30">
                                <select class="hero__form-input form-control custom-select mb-20" name="filterAcreage">
                                    <option value="">Diện tích</option>
                                    <option value="below">dưới 25m²</option>
                                    <option value="25">25 - 50m²</option>
                                    <option value="50">50 - 100m²</option>
                                    <option value="more">trên 100m²</option>
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-6 py-1 pr-30">
                                <select class="hero__form-input form-control custom-select mb-20" name="filterLength">
                                    <option value="">Chiều Dài</option>
                                    <option value="below">dưới 25m</option>
                                    <option value="25">25 - 50m</option>
                                    <option value="50">50 - 100m</option>
                                    <option value="more">trên 100m</option>
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-6 py-1 pr-30">
                                <select class="hero__form-input form-control custom-select mb-20"  name="filterWidth">
                                    <option value="">Chiều Rộng</option>
                                    <option value="below">dưới 25m</option>
                                    <option value="25">25 - 50m</option>
                                    <option value="50">50 - 100m</option>
                                    <option value="more">trên 100m</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!--Form ends-->

<!--Featured Property starts-->
<div class="featured-property-section py-80 mt-140 v3">
    <div class="container-fluid no-pad-lr">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title v1">
                    {{-- <p>Check out some of our</p> --}}
                    <h2>Dự Án Nổi Bật</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="featured-property-wrap v3 swiper-container">
                    <div class="swiper-wrapper">
                        @if ($trendProjects!=null)
                            @forelse ($trendProjects as $item)
                            <div class="swiper-slide">
                                <div class="single-property-box">
                                    <div class="row">
                                        <div class="col-xl-6 col-lg-12">
                                            <div class="property-item">
                                                <a class="property-img" href="{{ url($item->link) }}">
                                                    @isset($item->image)
                                                    <img src="{{ asset('storage/'.$item->image->project_gallery_link) }}" alt="{{ $item->image->project_gallery_name }}">
                                                    @else
                                                    <img src="{{ asset('asset/clients/images/default/default-project.jpg')}}" alt="{{ $item->name }}">
                                                    @endisset
                                                </a>
                                                {{-- <div class="property-author-wrap">
                                                    <a href="#" class="property-author">
                                                        <img src="images/agents/agency_4.jpg" alt="...">
                                                        <span>Hexa Properties</span>
                                                    </a>
                                                    <ul class="save-btn">
                                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                                                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li>
                                                    </ul>
                                                </div> --}}
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-lg-12">
                                            <div class="property-title-box">
                                                <h4><a href="{{ $item->link }}">{{ $item->title }}</a></h4>
                                                <div class="property-location">
                                                    <i class="fa fa-map-marker-alt"></i>
                                                    <p>{{ $item->address }}</p>
                                                </div>
                                                {{-- <ul class="property-feature">
                                                    @isset($item->acreage)
                                                        <li> <i class="fas fa-arrows-alt"></i>
                                                            <span>{{ $item->acreage }}</span>
                                                        </li>
                                                    @endisset
                                                    @isset($item->block)
                                                        <li> <i class="fas fa-building"></i>
                                                            <span>{{ $item->block }}</span>
                                                        </li>
                                                    @endisset
                                                    @isset($item->floor)
                                                        <li> <i class="fas fa-walking"></i>
                                                            <span>{{ $item->floor }}</span>
                                                        </li>
                                                    @endisset
                                                    @isset($item->house)
                                                        <li> <i class="fas fa-home"></i>
                                                            <span>{{ $item->house }}</span>
                                                        </li>
                                                    @endisset
                                                    @isset($item->time)
                                                        <li> <i class="fas fa-calendar-alt"></i>
                                                            <span>{{ date('d/m/Y',strtotime($item->time)) }}</span>
                                                        </li>
                                                    @endisset
                                                </ul> --}}
                                                <div class="trending-bottom">
                                                    <p>
                                                        <span>{{ $item->description }}</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            @empty
                            @endforelse
                        @endif
                    </div>
                </div>
                <!--Slider Arrows-->
                <div class="slider-btn v2 feature-prev"><i class="lnr lnr-arrow-left"></i></div>
                <div class="slider-btn v2 feature-next"><i class="lnr lnr-arrow-right"></i></div>
            </div>
        </div>
    </div>
</div>
<!--Featured Property ends-->
{{-- <!--Popular City starts-->
<div class="container mt-130">
    <div class="row">
        <div class="col-md-12">
            <div class="section-title v1">
                <p>Tỉnh thành nổi bật</p>
                <h2>Tìm dự án trong tỉnh thành</h2>
            </div>
        </div>
    </div>
</div>
<div class="property-place pb-50">
    <div class="container">
        <div class="row">
            <div class="swiper-container popular-place-wrap v1">
                <div class="swiper-wrapper">
                    <div class="swiper-slide single-place-wrap">
                        <div class="single-place-image">
                            <a href="#"><img src="{{ asset('asset/clients/images/places/place_2.jpg')}}" alt="place-image"></a>
                            <a class="single-place-title" href="#">Hồ Chí Minh</a>
                        </div>
                        <div class="single-place-content">
                            <h3><span>30</span>Dự Án</h3>
                            <a href="#">Xem danh sách <i class="lnr lnr-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="swiper-slide single-place-wrap">
                        <div class="single-place-image">
                            <a href="#"><img src="{{ asset('asset/clients/images/places/place_16.jpg')}}" alt="place-image"></a>
                            <a class="single-place-title" href="#">Bình Dương</a>
                        </div>
                        <div class="single-place-content">
                            <h3><span>50</span>Dự Án</h3>
                            <a href="grid-fullwidth-map.html">Xem danh sách <i class="lnr lnr-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="swiper-slide single-place-wrap">
                        <div class="single-place-image">
                            <a href="#"><img src="{{ asset('asset/clients/images/places/place_6.jpg')}}" alt="place-image"></a>
                            <a class="single-place-title" href="#">Long An</a>
                        </div>
                        <div class="single-place-content">
                            <h3><span>145</span>Dự Án</h3>
                            <a href="grid-fullwidth-map.html">Xem danh sách <i class="lnr lnr-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="swiper-slide single-place-wrap">
                        <div class="single-place-image">
                            <a href="#"><img src="{{ asset('asset/clients/images/places/place_1.jpg')}}" alt="place-image"></a>
                            <a class="single-place-title" href="#">Tây Ninh</a>
                        </div>
                        <div class="single-place-content">
                            <h3><span>115</span>Dự Án</h3>
                            <a href="grid-fullwidth-map.html">Xem danh sách <i class="lnr lnr-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="swiper-slide single-place-wrap">
                        <div class="single-place-image">
                            <a href="#"><img src="{{ asset('asset/clients/images/places/place_4.jpg')}}" alt="place-image"></a>
                            <a class="single-place-title" href="#">New york</a>
                        </div>
                        <div class="single-place-content">
                            <h3><span>50</span>Dự Án</h3>
                            <a href="grid-fullwidth-map.html">Xem danh sách <i class="lnr lnr-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="swiper-slide single-place-wrap">
                        <div class="single-place-image">
                            <a href="#"><img src="{{ asset('asset/clients/images/places/place_5.jpg')}}" alt="place-image"></a>
                            <a class="single-place-title" href="#">Miami</a>
                        </div>
                        <div class="single-place-content">
                            <h3><span>80</span>Dự Án</h3>
                            <a href="grid-fullwidth-map.html">Xem danh sách <i class="lnr lnr-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="slider-btn v2 popular-next"><i class="lnr lnr-arrow-right"></i></div>
                <div class="slider-btn v2 popular-prev"><i class="lnr lnr-arrow-left"></i></div>
            </div>
        </div>
    </div>
</div>
<!--Popular City ends--> --}}
<!--Property Price section starts-->
<div class="category-section bg-xs bg-fixed mt-45" style="background-image: url({{ asset('asset/clients/images/bg/category-bg-3.jpg')}})">
    <div class="overlay op-5"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="category-content text-center">
                    <h3>Tìm mua hay bán bất động sản hiện có? </h3>
                    <a href="{{route('clients.house.business', ['business' => 'bat-dong-san-muon-mua']) }}" class="btn v3">Bất Động Sản Muốn Mua</a>
                    <a href="{{route('clients.house.business', ['business' => 'bat-dong-san-muon-ban']) }}" class="btn v6">Bất Động Sản Muốn Bán</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Property Price section ends-->
<!--Trending events starts-->
<div class="trending-places pt-130 v2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title v1">
                    {{-- <p>Những bất động sản được yêu thích</p> --}}
                    <h2>Bất động bản nổi bật</h2>
                </div>
            </div>
        </div>
        <div class="row">
            @forelse ($trendHouses as $item)
            @if ($loop->first)
            <div class="col-lg-7 col-md-12 mb-30">
                <div class="single-property-header v4">
                    @isset($item->image->house_gallery_link)
                        <a href="{{ $item->link }}" class="property-slider-img bg-xs" style="background-image: url({{ asset('storage/'.$item->image->house_gallery_link)}})"></a>
                    @else
                        <a href="{{ $item->link }}" class="property-slider-img bg-xs" style="background-image: url({{ asset('asset/clients/images/default/default_house.jpg')}})"></a>
                    @endisset
                    </a>
                    @isset($item->status)
                    <ul class="feature_text v2">
                        <li class="{{ $item->status_color }}"><span>{{ $item->status }}</span></li>
                    </ul>
                    @endisset
                    <ul class="save-btn v2">
                        <li data-toggle="tooltip" data-placement="top" title="Photos"><a href="#{{$item->house_id}}" class="btn-gallery"><i class="lnr lnr-camera"></i></a></li>
                        {{-- <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li> --}}
                    </ul>
                    <div id="photo-gallery{{$item->house_id}}" class="hidden">
                        @forelse ($item->galleries as $photo)

                        @isset($photo->house_gallery_link)
                            <a href="{{ asset('storage/'.$photo->house_gallery_link) }}"></a>
                         @else
                            <a href="{{ asset('asset/clients/images/default/default_house.jpg') }}"></a>
                         @endisset

                        @empty

                        @endforelse
                    </div>
                    <div class="list-details-title v2">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-7 col-lg-12 col-md-7 col-sm-12">
                                    <div class="single-listing-title float-left">
                                        <h2><a href="{{ $item->link }}">{{ $item->title }}</a></h2>
                                        <ul class="property-feature">
                                            @isset($item->acreage)
                                            <li> <i class="fas fa-arrows-alt"></i>
                                                <span>{{ $item->acreage }}</span>
                                            </li>
                                            @endisset
                                            @isset($item->length)
                                                <li> <i class="fas fa-ruler-vertical"></i>
                                                    <span>{{ $item->length }} m</span>
                                                </li>
                                            @endisset
                                            @isset($item->width)
                                                <li> <i class="fas fa-ruler-horizontal"></i>
                                                    <span>{{ $item->width }} m</span>
                                                </li>
                                            @endisset
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-xl-5 col-lg-12 col-md-5 col-sm-12">
                                    <div class="list-details-btn text-right sm-left">
                                        <div class="trend-open mb-1">
                                            <p>
                                                @if ($item->price<=0)
                                                    <strong class="text-light">Thương lượng</strong>
                                                @else
                                                    @if (strlen($item->price)>9)
                                                        {{ round($item->price/1000000000, 2).' Tỷ' }}
                                                    @elseif(strlen($item->price)>6)
                                                        {{ is_int(($item->price/1000000))?($item->price/1000000).' Triệu': number_format($item->price,0,'',',') }}
                                                    @else
                                                        {{ number_format($item->price,0,'',',') }}
                                                    @endif
                                                    <small class="text-light">VNĐ</small>
                                                @endif
                                            </p>
                                        </div>
                                        <ul class="product-rating">
                                            @php
                                                $star = $item->rating;
                                            @endphp
                                            @for ($i = 0; $i < 5; $i++)
                                                @if ($star>=1)
                                                    <li><i class="fas fa-star"></i></li>
                                                @elseif ($star>0)
                                                    <li><i class="fas fa-star-half-alt"></i></li>
                                                @else
                                                    <li><i class="far fa-star"></i></li>
                                                @endif
                                                @php
                                                    $star--;
                                                @endphp
                                            @endfor
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @elseif ($loop->last)

            <div class="col-lg-7 col-md-12 mb-30">
                <div class="single-property-header v4">
                    @isset($item->image->house_gallery_link)
                        <a href="{{ $item->link }}" class="property-slider-img bg-xs" style="background-image: url({{ asset('storage/'.$item->image->house_gallery_link)}})"></a>
                    @else
                        <a href="{{ $item->link }}" class="property-slider-img bg-xs" style="background-image: url({{ asset('asset/clients/images/default/default_house.jpg')}})"></a>
                    @endisset
                    @isset($item->status)
                    <ul class="feature_text v2">
                        <li class="{{ $item->status_color }}"><span>{{ $item->status }}</span></li>
                    </ul>
                    @endisset
                    <ul class="save-btn v2">
                        <li data-toggle="tooltip" data-placement="top" title="Photos"><a href="#{{$item->house_id}}" class="btn-gallery"><i class="lnr lnr-camera"></i></a></li>
                        {{-- <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li> --}}
                    </ul>
                    <div id="photo-gallery{{$item->house_id}}" class="hidden">
                        @forelse ($item->galleries as $photo)
                        @isset($photo->house_gallery_link)
                            <a href="{{ asset('storage/'.$photo->house_gallery_link) }}"></a>
                         @else
                            <a href="{{ asset('asset/clients/images/default/default_house.jpg') }}"></a>
                         @endisset
                        @empty

                        @endforelse
                    </div>
                    <div class="list-details-title v2">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-7 col-lg-12 col-md-7 col-sm-12">
                                    <div class="single-listing-title float-left">
                                        <h2><a href="{{ $item->link }}">{{ $item->title }}</a></h2>
                                        <ul class="property-feature">
                                            @isset($item->acreage)
                                            <li> <i class="fas fa-arrows-alt"></i>
                                                <span>{{ $item->acreage }}</span>
                                            </li>
                                            @endisset
                                            @isset($item->length)
                                                <li> <i class="fas fa-ruler-vertical"></i>
                                                    <span>{{ $item->length }} m</span>
                                                </li>
                                            @endisset
                                            @isset($item->width)
                                                <li> <i class="fas fa-ruler-horizontal"></i>
                                                    <span>{{ $item->width }} m</span>
                                                </li>
                                            @endisset
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-xl-5 col-lg-12 col-md-5 col-sm-12">
                                    <div class="list-details-btn text-right sm-left">
                                        <div class="trend-open mb-1">
                                            <p>
                                                @if ($item->price<=0)
                                                    <strong class="text-light">Thương lượng</strong>
                                                @else
                                                    @if (strlen($item->price)>9)
                                                        {{ round($item->price/1000000000, 2).' Tỷ' }}
                                                    @elseif(strlen($item->price)>6)
                                                        {{ is_int(($item->price/1000000))?($item->price/1000000).' Triệu': number_format($item->price,0,'',',') }}
                                                    @else
                                                        {{ number_format($item->price,0,'',',') }}
                                                    @endif
                                                    <strong class="text-light">VNĐ</strong>
                                                @endif
                                            </p>
                                        </div>
                                        <ul class="product-rating">
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star-half-alt"></i></li>
                                            <li><i class="fas fa-star-half-alt"></i></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @else

            <div class="col-lg-5 col-md-12 mb-30">

                <div class="single-property-header v4">
                    @isset($item->image->house_gallery_link)
                        <a href="{{ $item->link }}" class="property-slider-img bg-xs" style="background-image: url({{ asset('storage/'.$item->image->house_gallery_link)}})"></a>
                    @else
                        <a href="{{ $item->link }}" class="property-slider-img bg-xs" style="background-image: url({{ asset('asset/clients/images/default/default_house.jpg')}})"></a>
                    @endisset
                    @isset($item->status)
                    <ul class="feature_text v2">
                        <li class="{{ $item->status_color }}"><span>{{ $item->status }}</span></li>
                    </ul>
                    @endisset
                    <ul class="save-btn v2">
                        <li data-toggle="tooltip" data-placement="top" title="Photos"><a href="#{{$item->house_id}}" class="btn-gallery"><i class="lnr lnr-camera"></i></a></li>
                        {{-- <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                        <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li> --}}
                    </ul>
                    <div id="photo-gallery{{$item->house_id}}" class="hidden">
                        @forelse ($item->galleries as $photo)
                        @isset($photo->house_gallery_link)
                            <a href="{{ asset('storage/'.$photo->house_gallery_link) }}"></a>
                        @else
                            <a href="{{ asset('asset/clients/images/default/default_house.jpg') }}"></a>
                        @endisset
                        @empty

                        @endforelse
                    </div>
                    <div class="list-details-title v2">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-7 col-lg-12 col-md-7 col-sm-12">
                                    <div class="single-listing-title float-left">
                                        <h2><a href="{{ $item->link }}">{{ $item->title }}</a></h2>
                                        <ul class="property-feature">
                                            @isset($item->acreage)
                                            <li> <i class="fas fa-arrows-alt"></i>
                                                <span>{{ $item->acreage }}</span>
                                            </li>
                                            @endisset
                                            @isset($item->length)
                                                <li> <i class="fas fa-ruler-vertical"></i>
                                                    <span>{{ $item->length }} m</span>
                                                </li>
                                            @endisset
                                            @isset($item->width)
                                                <li> <i class="fas fa-ruler-horizontal"></i>
                                                    <span>{{ $item->width }} m</span>
                                                </li>
                                            @endisset
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-xl-5 col-lg-12 col-md-5 col-sm-12">
                                    <div class="list-details-btn text-right sm-left">
                                        <div class="trend-open mb-1">
                                            <p>
                                                @if ($item->price<=0)
                                                    <strong class="text-light">Thương lượng</strong>
                                                @else
                                                    @if (strlen($item->price)>9)
                                                        {{ round($item->price/1000000000, 2).' Tỷ' }}
                                                    @elseif(strlen($item->price)>6)
                                                        {{ is_int(($item->price/1000000))?($item->price/1000000).' Triệu': number_format($item->price,0,'',',') }}
                                                    @else
                                                        {{ number_format($item->price,0,'',',') }}
                                                    @endif
                                                    <strong class="text-light">VNĐ</strong>
                                                @endif
                                            </p>
                                        </div>
                                        <ul class="product-rating">
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star"></i></li>
                                            <li><i class="fas fa-star-half-alt"></i></li>
                                            <li><i class="fas fa-star-half-alt"></i></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @empty

            @endforelse
        </div>
    </div>
</div>
<!--Trending events ends-->
<!--Testimonial section starts-->
<div class="hero-client-section v2 mt-110 pt-5" style="background-image: url({{ asset('asset/clients/images/bg/category-bg-3.jpg')}})">
    <div class="overlay op-5"></div>
    <div class="container">
        @if ($webReviews!=null&&$webReviews->count()>0)
        <div class="row">
            <div class="col-lg-3 col-md-12 text-center">
                <div class="section-title v2">
                    <h2>Đánh Giá</h2>
                    <!-- Add Arrows -->
                    <div class="testimonial-btn v2">
                        <div class="slider-btn v4 testimonial-prev"><i class="lnr lnr-arrow-left"></i></div>
                        <div class="slider-btn v4 testimonial-next"><i class="lnr lnr-arrow-right"></i></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-12 mb-70">
                <div class="client-wrapper swiper-container">
                    <div class="swiper-wrapper">
                        @forelse ($webReviews as $item)

                        <div class="swiper-slide">
                            <div class="single-testimonial-item v2">
                                <div class="testimonial-img">
                                    <img src="{{ asset('storage/'.$item->web_review_image)}}" alt="{{ $item->web_review_name }}">
                                </div>
                                <div class="testimonial-text">
                                    <h3>{{ $item->web_review_name }}</h3>
                                    <span>{{ $item->web_review_address }}</span>
                                    <p>{{ $item->web_review_detail }}</p>
                                </div>
                            </div>
                        </div>
                        @empty

                        @endforelse
                    </div>
                </div>

            </div>
        </div>
        @endif
    </div>
</div>
<!--Testimonial section ends-->
<!--Team section starts-->
{{-- <div class="team-section mt-130 pb-90">
    <div class="container">
        <div class="row mt-30">
            <div class="col-md-8 offset-md-2 text-center">
                <div class="section-title v1">
                    <p>Ready to serve you the best</p>
                    <h2>Meet our Agents</h2>
                </div>
            </div>
            <div class="col-md-12">
                <div class="team-wrapper swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="single-team-member v2">
                                <a href="single-agent.html"><img src="{{ asset('asset/clients/images/agents/agent_1.jpg')}}" alt="Image"></a>
                                <div class="single-team-info">
                                    <h4><a href="single-agent.html">Tony Stark</a></h4>
                                    <span>Real Estate Agent</span>
                                    <div class="contact-info">
                                        <div class="icon">
                                            <i class="fas fa-phone-alt"></i>
                                        </div>
                                        <div class="text"><a href="tel:44078767595">+44 765 342 312</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="single-team-member v2">
                                <a href="single-agent.html">
                                    <img src="{{ asset('asset/clients/images/agents/agent_2.jpg')}}" alt="Image">
                                </a>
                                <div class="single-team-info">
                                    <h4><a href="single-agent.html">Bob Haris</a></h4>
                                    <span>Sales Executive</span>
                                    <div class="contact-info">
                                        <div class="icon">
                                            <i class="fas fa-phone-alt"></i>
                                        </div>
                                        <div class="text"><a href="tel:44078767595">+44 078 767 595</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="single-team-member v2">
                                <a href="single-agent.html"><img src="{{ asset('asset/clients/images/agents/agent_3.jpg')}}" alt="Image"></a>
                                <div class="single-team-info">
                                    <h4><a href="single-agent.html">Jim Karry</a></h4>
                                    <span>Real Estate Broker</span>
                                    <div class="contact-info">
                                        <div class="icon">
                                            <i class="fas fa-phone-alt"></i>
                                        </div>
                                        <div class="text"><a href="tel:44055847538">+44 055 847 538</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="single-team-member v2">
                                <a href="single-agent.html"><img src="{{ asset('asset/clients/images/agents/agent_4.jpg')}}" alt="Image"></a>
                                <div class="single-team-info">
                                    <h4><a href="single-agent.html">Alfred Jhon</a></h4>
                                    <span>Sales Executive</span>
                                    <div class="contact-info">
                                        <div class="icon">
                                            <i class="fas fa-phone-alt"></i>
                                        </div>
                                        <div class="text"><a href="tel:44055847538">+44 078 767 598</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="single-team-member v2">
                                <a href="single-agent.html"><img src="{{ asset('asset/clients/images/agents/agent_8.jpg')}}" alt="Image"></a>
                                <div class="single-team-info">
                                    <h4><a href="single-agent.html">Boris Baker</a></h4>
                                    <span>Real Estate Agent</span>
                                    <div class="contact-info">
                                        <div class="icon">
                                            <i class="fas fa-phone-alt"></i>
                                        </div>
                                        <div class="text"><a href="tel:44321445 678">+44 321 445 678</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-btn v2 team_next"><i class="lnr lnr-arrow-right"></i></div>
                    <div class="slider-btn v2 team_prev"><i class="lnr lnr-arrow-left"></i></div>
                </div>
            </div>
        </div>
    </div>
</div> --}}
<!--Team section starts-->

@stop

@section('javascript')
    <script>
        $(function(){
            changeType() ;
        });

        function changeType() {
            var proType = $("select[name='provincial']").children("option:selected").val();
            if(proType!='')
            {

                $('#filterDistrict').find('option').remove().end();
                // $('#filterDistrict').append($('<option>', {value: '',text : 'Chọn Quận/ Huyện'}));
                $('#filterDistrict').append('<option value="" selected>Chọn Quận/ Huyện</option>');
                $('#filterDistrict').selectpicker('refresh');

                $.ajax({
                    contentType: "application/json",
                    dataType: "json",
                    url: '/dia-chi/bat-dong-san/'+proType,
                    success: function(data){
                        $.each(data, function(i,item){
                                $('select[name="district"]').append($('<option>', {value: item,text : item}));
                                // $('#filterDistrict').append('<option value="'+item+'">'+item+'</option>');
                                $('#filterDistrict').selectpicker('refresh');
                            });
                        $('#filterDistrict').selectpicker('val', '{{Request::get('district')}}');
                        $('#filterDistrict').selectpicker('refresh');
                        }
                });

            }

        }
    </script>
@stop
