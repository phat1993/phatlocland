@extends('layouts.client_layout')
@section('title', 'Giới thiệu')

@section('content')

        <!--Breadcrumb section starts-->
        <div class="breadcrumb-section bg-h" style="background-image:url({{ asset('asset/clients/images/breadcrumb/breadcrumb_2.jpg') }})">
            <div class="overlay op-5"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        <div class="breadcrumb-menu">
                            <h2>Giới thiệu</h2>
                            <span><a href="{{ route('clients.index') }}">Trang chủ</a></span>
                            <span>Giới thiệu</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Breadcrumb section ends-->

        <!--About section starts-->
        <div class="about-section  pt-130">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <img src="{{ asset('asset/clients/images/about/about.png') }}" alt="PHÁT LỘC REAL LAND">
                        {{-- <img src="{{ asset('asset/clients/images/no_image_available.jpeg') }}" alt="PHÁT LỘC REAL LAND"> --}}
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="section-title v3">
                            <p>Thay Đổi - Khác Biệt - Nâng Tầm</p>
                            <h2 class="text-center">Công ty TNHH Đầu Tư Và Phát Triển Bất Động Sản PHÁT LỘC REAL LAND</h2>
                        </div>
                        <div class="about-text res-box">
                            <!--<span>All You Need in Just One Place</span>
                            <h2>We’ve made a life
                                that will change you</h2>-->
                            <p>&emsp;<u>Học hỏi và hoàn thiện không ngừng thông qua hành động, sáng tạo;</u><br>
                                &emsp;Học hỏi và hoàn thiện không ngừng thông qua thực tiễn kinh doanh, lấy lợi
                                ích vững bền vững của Khách hàng làm kim chỉ nam hành động là cách tốt
                                nhất để <strong class="text-dark">Phát Lộc Real Land</strong> tiến lên từng ngày với sự ủng hộ và tin yêu của
                                Khách hàng & Đối tác.</p>
                                <i class="text-dark h6" style="font-weight:bold;line-height:1.8">
                                    &emsp;Chúng tôi hiểu rằng nhu cầu “An cư lạc nghiệp” luôn được khách hàng ưu tiên
hàng đầu, một môi trường sống tốt sẽ là tiền đề để khách hàng của chúng tôi
phát triển sự nghiệp. Vì thế, chúng tôi không ngừng nỗ lực để đáp ứng tốt nhất
những yêu cầu của quý khách hàng.
                                </i>
                                <p class="pt-5">
                                    &emsp;Môi trường minh bạch, thăng tiến và tinh thần đồng đội: Sự minh bạch
                                    trong mốii quan hệ với các đối tác cũng như trong chính sách nội bộ, tạo
                                    nhiều cơ hội thăng tiến và đề cao tinh thần đồng đội giúp <strong class="text-dark">Phát Lộc Real Land </strong> thu hút được nguồn nhân lực tốt, xây dựng được đội ngũ môi giới
                                    chuyên nghiệp, tận tâm.

                                </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--About section ends-->

        <!--Promo Section starts-->
        <div class="promo-section pt-120 v2">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-12">
                        <div class="promo-desc">
                            <div class="section-title v2">
                                {{-- <p>Lorem ipsum dolor sit.</p> --}}
                                <h2>GIÁ TRỊ CỐT LÕI</h2>
                            </div>
                            <div class="promo-text">
                                <p>&emsp;Quan hệ hợp tác bền vững với các Đối tác: <strong class="text-dark">Phát Lộc Real Land </strong>
                                    tâm niệm rằng sự hợp tác bền vững với Khách hàng và đối tác là cốt
                                    lõi cho sự thành công.
                                </p>
                                <p>&emsp;<strong class="text-danger">UY TÍN:</strong> <strong class="text-dark">Phát Lộc Real Land </strong>có nguồn vốn từ các quỹ đầu tư trong và
                                    ngoài nước – có kinh nghiệm hơn 10 năm trong các lĩnh vực tài chính, bất
                                    động sản, xây dựng... Với sự uy tín của mình, những nhà đầu tư này sẽ giúp
                                    khách hàng lựa chọn những dự án và
                                    phù hợp nhất.
                                </p>

                            </div>
                            <div class="row pt-5">
                                <div class="col-sm-3 col-6">
                                    <div class="counter-text v2">
                                        <i class="lnr lnr-apartment"></i>
                                        <h6 class="counter-value" data-from="0" data-to="10" data-speed="2500">
                                        </h6>
                                        <p>Kinh Nghiệm</p>

                                    </div>
                                </div>
                                <div class="col-sm-5 col-6">
                                    <div class="counter-text v2">
                                        <i class="lnr lnr-thumbs-up"></i>
                                        <h6 class="counter-value" data-from="0" data-to="585" data-speed="2000">
                                        </h6>
                                        <p>Khách Hàng Thân Thiết</p>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-6">
                                    <div class="counter-text v2">
                                        <i class="lnr lnr-user"></i>
                                        <h6 class="counter-value" data-from="0" data-to="1" data-speed="200">
                                        </h6>
                                        <p>Đại lý</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="promo-content-wrap">
                                    <div class="promo-content text-center">
                                        <img src="{{ asset('asset/clients/images/about/teamwork.jpg') }}" alt="PHÁT LỘC REAL LAND">
                                        <h4>Team Work</h4>
                                    </div>
                                    <div class="promo-content text-center">
                                        <img src="{{ asset('asset/clients/images/about/responsible.jpg') }}" alt="PHÁT LỘC REAL LAND">
                                        <h4>Responsible</h4>
                                    </div>
                                    <div class="promo-content text-center">
                                        <img src="{{ asset('asset/clients/images/about/passion.jpg') }}" alt="PHÁT LỘC REAL LAND">
                                        <h4>Passion</h4>
                                    </div>
                                    <div class="promo-content text-center">
                                        <img src="{{ asset('asset/clients/images/about/integrity.jpg') }}" alt="PHÁT LỘC REAL LAND">
                                        <h4>Integrity</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Promo Section ends-->
        <!--Team section starts-->
        <div class="team-section pt-20">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title v1">
                            <p>Nhân lực là tài sản quý giá nhất của Công ty.</p>
                            <h2>NGUỒN NHÂN LỰC</h2>
                        </div>
                        <div class="promo-text">
                            <p>&emsp;Từ khi thành lập đến nay, <strong>Phát Lộc Real Land</strong> luôn quan niệm nguồn nhân lực
                                luôn là tài sản quý giá nhất của Công ty. Chúng tôi luôn tìm kiếm, tuyển dụng và
                                giữ chân những người giỏi nhất trong lĩnh vực Bất Động Sản. Khách hàng của
                                chúng tôi trong các lĩnh vực từ quản lý Bất Động Sản cho tới các Nhà Đầu Tư
                                cá nhân đều được hưởng lợi từ khía cạnh kinh doanh và dịch vụ mà nhân sự
                                của chúng tôi mang lại.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 d-flex justify-content-center">
                        <img src="{{ asset('asset/clients/images/about/employee.jpg') }}" alt="PHÁT LỘC REAL LAND">
                    </div>
                    <div class="col-12">
                        <div class="promo-text">
                            <p>&emsp;Chúng tôi tập trung khuyến khích trong các phương pháp hỗ trợ và hợp
                                tác giữa nhân viên: chúng tôi hiểu rằng nếu cung cấp một môi trường phù
                                hợp thì con người sẽ dễ tìm thấy được những tiềm năng của họ. Bởi vậy
                                chính Khách hàng của chúng tôi sẽ là những người thấy rõ được những lợi
                                ích đó. Vì thế chúng tôi trao cho nhân viên cơ hội để chứng tỏ bản thân, họ
                                trợ họ tạo dựng một cuộc sống cân bằng giữa công việc và đời sống cá nhân
                                và bằng việc cung cấp giá trị thông qua sự đa dạng, chúng tôi tạo nên một
                                nền văn hóa Doanh nghiệp hiện đại và nhiều cảm hứng. Kết quả đem lại là
                                mọi nhân viên của chúng tôi luôn chủ động trong công việc, suy nghĩ sáng
                                tạo và làm việc có trách nhiệm.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Testimonial Section start-->
        <div class="hero-client-section pt-130">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        <div class="section-title v1">
                            <p>Giữ vững niềm tin trong tâm trí khách hàng và đối tác.</p>
                            <h2>TẦM NHÌN - SỨ MỆNH</h2>
                        </div>
                    </div>
                    <div class="col-md-12 mb-70">
                        <div class="testimonial-wrapper swiper-container ">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="single-testimonial-item">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="testimonial-video">
                                                    <img src="{{ asset('asset/clients/images/about/feature.jpg') }}" alt="PHÁT LỘC REAL LAND">
                                                </div>
                                            </div>
                                            <div class="testimonial-quote">
                                                <p><strong>Phát Lộc Real Land</strong> đặt mục tiêu trở thành nhà môi giới bất động sản uy tín, chuyên
                                                    nghiệp và tiến đến trở thành Tập đoàn chuyên về xây dựng, quản lý, môi giới Bất Động
                                                    Sản hàng đầu tại Việt Nam.
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <br>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="single-testimonial-item">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="testimonial-video">
                                                    <img src="{{ asset('asset/clients/images/about/feature1.jpg') }}" alt="PHÁT LỘC REAL LAND">
                                                </div>
                                            </div>
                                            <div class="testimonial-quote">
                                                <p>Với tiềm lực mạnh về tài chính, vững vàng về kinh nghiệm, cùng đội ngũ
                                                    cán bộ nhân viên được đào tạo chuyên nghiệp, nhiệt tình, năng động, sáng tạo
                                                    và làm việc hiệu quả, <strong>Phát Lộc Real Land</strong> nỗ lực không ngừng trong mọi
                                                    hoàn cảnh để khẳng định và nâng cao vị thế của mình trên thị trường bất động
                                                    sản Việt Nam, giữ vững niềm tin trong tâm trí khách hàng và đối tác.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Add Arrows -->
                        <div class="client-btn-wrap">
                            <div class="client-prev"><i class="lnr lnr-arrow-left"></i></div>
                            <div class="client-next"><i class="lnr lnr-arrow-right"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Testimonial Section ends-->


    <!--Listing Details Hero starts-->
    <div class="single-property-header v3 py-100 property-carousel">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-12">
                    <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails list-gallery" data-ride="carousel">
                        <!--Slides-->
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="{{ asset('asset/clients/images/about/img1.jpg') }}" alt="slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{ asset('asset/clients/images/about/img2.jpg') }}" alt="slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{ asset('asset/clients/images/about/img3.jpg') }}" alt="slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{ asset('asset/clients/images/about/img4.jpg') }}" alt="slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{ asset('asset/clients/images/about/img5.jpg') }}" alt="slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{ asset('asset/clients/images/about/img6.jpg') }}" alt="slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{ asset('asset/clients/images/about/img7.jpg') }}" alt="slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="{{ asset('asset/clients/images/about/img8.jpg') }}" alt="slide">
                            </div>
                        </div>
                        <!--Controls starts-->
                        <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                            <span class="lnr lnr-arrow-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                            <span class="lnr lnr-arrow-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12">
                    <div class="row   list-gallery-thumb">
                        <div class="col-lg-4 col-2 mb-27">
                            <div data-target="#carousel-thumb" data-slide-to="0"><img class="img-fluid" src="{{ asset('asset/clients/images/about/img1.jpg') }}" alt="PHÁT LỘC REAL LAND"></div>
                        </div>
                        <div class="col-lg-4 col-2 mb-27">
                            <div data-target="#carousel-thumb" data-slide-to="1"><img class="img-fluid" src="{{ asset('asset/clients/images/about/img2.jpg') }}" alt="PHÁT LỘC REAL LAND"></div>
                        </div>
                        <div class="col-lg-4 col-2 mb-27">
                            <div data-target="#carousel-thumb" data-slide-to="2"><img class="img-fluid" src="{{ asset('asset/clients/images/about/img3.jpg') }}" alt="PHÁT LỘC REAL LAND"></div>
                        </div>
                        <div class="col-lg-4 col-2  mb-27">
                            <div data-target="#carousel-thumb" data-slide-to="3"><img class="img-fluid" src="{{ asset('asset/clients/images/about/img4.jpg') }}" alt="PHÁT LỘC REAL LAND"></div>
                        </div>
                        <div class="col-lg-4 col-2 mb-27">
                            <div data-target="#carousel-thumb" data-slide-to="4"><img class="img-fluid" src="{{ asset('asset/clients/images/about/img5.jpg') }}" alt="PHÁT LỘC REAL LAND"></div>
                        </div>
                        <div class="col-lg-4 col-2 mb-27">
                            <div data-target="#carousel-thumb" data-slide-to="5"><img class="img-fluid" src="{{ asset('asset/clients/images/about/img6.jpg') }}" alt="PHÁT LỘC REAL LAND"></div>
                        </div>
                        <div class="col-lg-4 col-2 mb-27">
                            <div data-target="#carousel-thumb" data-slide-to="6"><img class="img-fluid" src="{{ asset('asset/clients/images/about/img7.jpg') }}" alt="PHÁT LỘC REAL LAND"></div>
                        </div>
                        <div class="col-lg-4 col-2 mb-27">
                            <div data-target="#carousel-thumb" data-slide-to="7"><img class="img-fluid" src="{{ asset('asset/clients/images/about/img8.jpg') }}" alt="PHÁT LỘC REAL LAND"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Listing Details Hero ends-->
@stop

@section('javascript')


@stop
