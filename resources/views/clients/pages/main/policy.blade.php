@extends('layouts.client_layout')
@section('title')
    {{ $webPolicy->web_title }}
@endsection
@section('styles')
    <meta name="keywords" content="{{ $webPolicy->web_keywords }}">
    <meta name="description" content="{{ $webPolicy->web_description }}">
    <link rel="canonical" href="{{ url('chinh-sach/'.$webPolicy->web_canonical) }}">
@endsection
@section('content')

        <!--Breadcrumb section starts-->
        <div class="breadcrumb-section bg-h" style="background-image:url({{ asset('asset/clients/images/breadcrumb/breadcrumb_2.jpg') }})">
            <div class="overlay op-5"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        <div class="breadcrumb-menu">
                            <h2>Chính Sách</h2>
                            <span><a href="{{ route('clients.index') }}">Trang chủ</a></span>
                            <span>Chính Sách</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Breadcrumb section ends-->


        <!--Blog Details section starts-->
        <div class="blog-area">
            <div class="container">
                <div class="row">
                    <!--Blog post starts-->
                    <div class="col-xl-8 order-xl-12 order-xl-2 order-2 py-100">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <article class="post-single">
                                        <div class="post-content-wrap">
                                            <div class="post-content">
                                                {!! $webPolicy->web_policy_detail !!}
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Blog post ends-->
                    <!-- Right Sidebar starts-->
                    <div class="col-xl-4 order-xl-12 order-xl-1 order-1">
                        <div class="sidebar">
                            <div class="sidebar-right">
                                <div class="widget recent">
                                    <h3 class="widget-title">Danh sách</h3>
                                    <ul class="post-list">
                                        @forelse ($webPolicies as $item)
                                        <li class="row recent-list">
                                            <div class="col-lg-5 col-4">
                                                <div class="entry-img">
                                                   <a href="{{ route('clients.policy',['policy'=>$item->web_canonical]) }}">
                                                    @isset($item->web_policy_image)
                                                        <img src="{{ asset('storage/'.$item->web_policy_image) }}" alt="{{ $item->web_policy_name }}">
                                                    @else
                                                        <img src="{{ asset('asset/clients/images/default/default_policy.jpg') }}" alt="{{ $item->web_policy_name }}">
                                                    @endisset
                                                   </a>
                                                </div>
                                            </div>
                                            <div class="col-lg-7 col-8 no-pad-left">
                                                <div class="entry-text">
                                                    <h4 class="entry-title"><a href="{{ route('clients.policy',['policy'=>$item->web_canonical]) }}">{{ $item->web_policy_name }}</a></h4>
                                                    <div class="property-location">
                                                        <p>{{ date('d-m-Y',strtotime($item->updated_at)) }}</p>
                                                    </div>
                                                    {{ $item->web_policy_description }}
                                                </div>
                                            </div>
                                        </li>
                                        @empty

                                        @endforelse
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Right Sidebar ends -->
                </div>
            </div>
        </div>
        <!--Blog Details section  ends-->

@stop

@section('javascript')


@stop
