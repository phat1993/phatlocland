@extends('layouts.client_layout')
@section('title', 'Bất Động Sản')

@section('content')

        <!--error  start-->
        <div class="error-section mt-150 pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 offset-md-3  error-text text-center">
                        <div class="error-content">
                            <img src="{{ asset('asset/clients/images/others/404.png') }}" alt="404 not found"/>
                            <h4>Lỗi! Không tìm thấy trang</h4>
                            <p>Có lẽ việc tìm kiếm có thể hữu ích hoặc quay lại <a href="{{ route('clients.index') }}">Trang chủ</a> </p>
                        </div>
                        <div class="error-search-container">
                            <form>
                                <input type="text" placeholder="Tìm kiếm" name="search">
                                <button type="submit"><i class="lnr lnr-magnifier"></i></button>
                            </form>
                        </div>
                        <br>
                        <small><a class="text-grey" href="{{ route('clients.index') }}">* Phát Lộc Real Land - Nhà đất của bạn</a></small>
                    </div>
                </div>
            </div>
        </div>
        <!--error ends-->
@stop

@section('javascript')


@stop
