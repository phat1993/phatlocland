@extends('layouts.client_layout')
@section('title')
   @isset($category)
        {{ $category->web_title }}
    @else
        Dự Án
   @endisset
@endsection
@section('styles')
    @isset($category)
        <meta name="keywords" content="{{ $category->web_keywords }}">
        <meta name="description" content="{{ $category->web_description }}">
        <link rel="canonical" href="{{ url('du-an/'.$category->web_canonical) }}">
    @else
        <link rel="canonical" href="{{ url('du-an') }}">
    @endisset
@endsection
@section('content')


        <!--Breadcrumb section starts-->
        <div class="breadcrumb-section bg-h" style="background-image:url({{ asset('asset/clients/images/breadcrumb/breadcrumb_2.jpg') }})">
            <div class="overlay op-5"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        @isset($category)
                            <div class="breadcrumb-menu">
                                <h2>{{ $category->project_category_name }}</h2>
                                <span><a href="{{ route('clients.index') }}">Trang chủ</a></span>
                                <span>{{ $category->project_category_name }}</span>
                            </div>
                        @else
                            <div class="breadcrumb-menu">
                                <h2>Dự Án</h2>
                                <span><a href="{{ route('clients.index') }}">Trang chủ</a></span>
                                <span>Dự Án</span>
                            </div>
                        @endisset
                    </div>
                </div>
            </div>
        </div>
        <!--Breadcrumb section ends-->
        <!--Listing filter starts-->
        <div class="filter-wrapper style1 section-padding">
            <div class="container">
                <div class="row">
                    <!--Listing filter starts-->
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">

                                @php
                                    $action = route('clients.project.default');
                                    if(isset($category))
                                        $action = route('clients.project.list', ['category' => $category->web_canonical]);
                                @endphp
                                <form class="hero__form v1 filter listing-filter property-filter" action="{{ $action }}" method="get" name="filter-form">
                                    {{csrf_field()}}

                                    <input type="hidden" name="sort" value="{{Request::get('sort')}}">
                                    <div class="row">
                                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                                            <div class="input-search">
                                                <input type="text" placeholder="Nhập vào tên, địa chỉ ..." id="address" name="address" value="{{Request::get('address')}}">
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                                            <select class="hero__form-input  form-control custom-select"name="provincial" onchange="changeType()">
                                                <option value="">Chọn Tỉnh/ Thành Phố</option>
                                                @forelse ($provincials as $item)
                                                    <option value="{{ $item->project_provincial }}" @if ($item->project_provincial == Request::get('provincial'))
                                                        {{ __('selected') }}
                                                    @endif>{{ $item->project_provincial }}</option>
                                                @empty

                                                @endforelse
                                            </select>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                                            <select class="hero__form-input  form-control custom-select" id="filterDistrict" name="district">
                                                <option value="">Chọn Quận/ Huyện</option>
                                            </select>
                                        </div>
                                        {{-- <div class="col-xl-2 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                                            <select class="hero__form-input  form-control custom-select" id="filterCommenu" name="district">
                                                <option value="">Chọn Phường/ Xã</option>
                                            </select>
                                        </div> --}}
                                        <div class="col-xl-2 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                                            <div class="submit_btn">
                                                <button class="btn v3" type="submit">Tìm kiếm</button>
                                            </div>
                                            {{-- <div class="dropdown-filter"><span>Thêm</span></div> --}}
                                        </div>
                                        {{-- <div class="explore__form-checkbox-list full-filter">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-6 py-1 pr-30">
                                                    <select class="hero__form-input  form-control custom-select mb-20">
                                                        <option>Property Status</option>
                                                        <option>Any</option>
                                                        <option>For Rent</option>
                                                        <option>For Sale</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4 col-md-6 py-1 pr-30 pl-0 ">
                                                    <select class="hero__form-input  form-control custom-select  mb-20">
                                                        <option>Property Type</option>
                                                        <option>Residential</option>
                                                        <option>Commercial</option>
                                                        <option>Land</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4 col-md-6 py-1 pl-0">
                                                    <select class="hero__form-input  form-control custom-select  mb-20">
                                                        <option>Max rooms</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                        <option>6</option>
                                                        <option>7</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4 col-md-6 py-1 pr-30 ">
                                                    <select class="hero__form-input  form-control custom-select  mb-20">
                                                        <option>Bed</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4 col-md-6 py-1 pr-30 pl-0">
                                                    <select class="hero__form-input  form-control custom-select  mb-20">
                                                        <option>Bath</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4 col-md-6 py-1 pl-0">
                                                    <select class="hero__form-input  form-control custom-select  mb-20">
                                                        <option>Agents</option>
                                                        <option>Bob Haris</option>
                                                        <option>Ross buttler</option>
                                                        <option>Andrew Saimons</option>
                                                        <option>Steve Austin</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4 col-md-6 py-1 pr-30">
                                                    <select class="hero__form-input  form-control custom-select  mb-20">
                                                        <option>Agencies</option>
                                                        <option>Onyx Equities</option>
                                                        <option>OVG Real Estate</option>
                                                        <option>Oxford Properties*</option>
                                                        <option>Cortland</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4 col-md-6 py-1 pr-30 pl-0">
                                                    <div class="filter-sub-area style1">
                                                        <div class="filter-title mb-10">
                                                            <p>Price : <span><input type="text" id="amount_two"></span></p>
                                                        </div>
                                                        <div id="slider-range_two" class="price-range mb-30">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-6 py-1  pl-0">
                                                    <div class="filter-sub-area style1">
                                                        <div class="filter-title  mb-10">
                                                            <p>Area : <span><input type="text" id="amount_one"></span></p>
                                                        </div>
                                                        <div id="slider-range_one" class="price-range mb-30">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 py-1 pr-30">
                                                    <div class="filter-checkbox">
                                                        <p>Sort By Features</p>
                                                        <ul>
                                                            <li>
                                                                <input id="check-a" type="checkbox" name="check">
                                                                <label for="check-a">Air Condition</label>
                                                            </li>
                                                            <li>
                                                                <input id="check-b" type="checkbox" name="check">
                                                                <label for="check-b">Swimming Pool</label>
                                                            </li>
                                                            <li>
                                                                <input id="check-c" type="checkbox" name="check">
                                                                <label for="check-c">Laundry Room</label>
                                                            </li>
                                                            <li>
                                                                <input id="check-d" type="checkbox" name="check">
                                                                <label for="check-d">Free Wifi</label>
                                                            </li>
                                                            <li>
                                                                <input id="check-e" type="checkbox" name="check">
                                                                <label for="check-e">Window Covering</label>
                                                            </li>
                                                            <li>
                                                                <input id="check-f" type="checkbox" name="check">
                                                                <label for="check-f">Central Heating </label>
                                                            </li>
                                                            <li>
                                                                <input id="check-g" type="checkbox" name="check">
                                                                <label for="check-g">24 hour security</label>
                                                            </li>
                                                            <li>
                                                                <input id="check-h" type="checkbox" name="check">
                                                                <label for="check-h">Lawn</label>
                                                            </li>
                                                            <li>
                                                                <input id="check-i" type="checkbox" name="check">
                                                                <label for="check-i">Gym</label>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--Listing filter ends-->
                    <div class="col-md-12">
                        @if($projects!=null && $projects->count()>0)
                            <div class="row pt-60 align-items-center">
                                <div class="col-lg-3 col-sm-5 col-5">
                                    <div class="item-view-mode res-box">
                                        <!-- item-filter-list Menu starts -->
                                        <ul class="nav item-filter-list" role="tablist">
                                            <li><a class="active" data-toggle="tab" href="#grid-view"><i class="fas fa-th"></i></a></li>
                                            <li><a  data-toggle="tab" href="#list-view"><i class="fas fa-list"></i></a></li>
                                        </ul>
                                        <!-- item-filter-list Menu ends -->
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-7 col-7">
                                    <select class="listing-input hero__form-input form-control custom-select" name="sortObject" onchange="submitSort()">
                                        <option value="new" @if (Request::get('sort')=='new')
                                            {{ __('selected') }}
                                        @endif>Sắp xếp theo Mới nhất</option>
                                        <option value="old" @if (Request::get('sort')=='old')
                                        {{ __('selected') }}
                                    @endif>Sắp xếp theo Cũ nhất</option>
                                        <option value="trend" @if (Request::get('sort')=='trend')
                                        {{ __('selected') }}
                                    @endif>Sắp xếp theo Nổi bật</option>
                                    </select>
                                </div>
                                {{ $dbProjects->appends([
                                    'sort' => Request::get('sort'),
                                    'address' => Request::get('address'),
                                    'provincial' => Request::get('provincial'),
                                    'district' => Request::get('district')
                                ])->links('clients.parts.display_pagination') }}
                            </div>
                        @else
                            <h1 class="text-center p-5">Hiện tại không có dự án..</h1>
                        @endif
                        <div class="item-wrapper pt-40   ">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane  show active  fade property-grid" id="grid-view">
                                    <div class="row">
                                        @php
                                            $idx = 1;
                                        @endphp
                                        @forelse ($projects as $item)
                                        <div class="col-xl-4 col-md-6 col-sm-12">
                                            <div class="single-property-box">
                                                <div class="property-item">
                                                    <a class="property-img" href="{{ url($item->link) }}">
                                                        @isset($item->image)
                                                        <img src="{{ asset('storage/'.$item->image->project_gallery_link) }}" alt="{{ $item->image->project_gallery_name }}">
                                                        @else
                                                        <img src="{{ asset('asset/clients/images/default/default-project.jpg')}}" alt="{{ $item->name }}">
                                                        @endisset
                                                    </a>
                                                    @isset($item->features)
                                                        {{-- <ul class="feature_text">
                                                            <li class="feature_cb"><span> Featured</span></li>
                                                            <li class="feature_or"><span>For Sale</span></li>
                                                        </ul> --}}
                                                    @endisset
                                                    <div class="property-author-wrap">
                                                        {{-- <a href="#" class="property-author">
                                                            <img src="{{ asset('asset/clients/images/agents/agent_min_1.jpg') }}" alt="...">
                                                            <span>Tony Stark</span>
                                                        </a> --}}
                                                        <ul class="save-btn">
                                                            <li data-toggle="tooltip" data-placement="top" title="Hình Ảnh"><a href=".photo-gallery{{ $idx }}" class="btn-gallery"><i class="lnr lnr-camera"></i></a></li>
                                                            {{-- <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                                                            <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li> --}}
                                                        </ul>
                                                        <div class="hidden photo-gallery{{ $idx }}">
                                                            @forelse ($item->galleries as $photo)
                                                            <a href="{{ asset('storage/'.$photo->project_gallery_link) }}"></a>
                                                            @empty

                                                            @endforelse
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="property-title-box">
                                                    <h6><a href="{{ $item->link }}">{{ $item->title }}</a></h6>
                                                    <div class="property-location">
                                                        <i class="fa fa-map-marker-alt"></i>
                                                        <p>{{ $item->address }}</p>
                                                    </div>
                                                    <ul class="property-feature">
                                                        @isset($item->acreage)
                                                            <li> <i class="fas fa-arrows-alt"></i>
                                                                <span>{{ $item->acreage }}</span>
                                                            </li>
                                                        @endisset
                                                        @isset($item->block)
                                                            <li> <i class="fas fa-building"></i>
                                                                <span>{{ $item->block }}</span>
                                                            </li>
                                                        @endisset
                                                        @isset($item->floor)
                                                            <li> <i class="fas fa-walking"></i>
                                                                <span>{{ $item->floor }}</span>
                                                            </li>
                                                        @endisset
                                                        @isset($item->house)
                                                            <li> <i class="fas fa-home"></i>
                                                                <span>{{ $item->house }}</span>
                                                            </li>
                                                        @endisset
                                                        @isset($item->time)
                                                            <li> <i class="fas fa-calendar-alt"></i>
                                                                <span>{{ date('d/m/Y',strtotime($item->time)) }}</span>
                                                            </li>
                                                        @endisset
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        @php
                                            $idx++;
                                        @endphp
                                        @empty
                                        @endforelse
                                    </div>
                                </div>
                                <div class="tab-pane fade property-list fullwidth" id="list-view">
                                    @php
                                        $idx = 1;
                                    @endphp
                                    @forelse ($projects as $item)
                                    <div class="single-property-box">
                                        <div class="row">
                                            <div class="col-xl-5 col-lg-4 col-md-12">
                                                <div class="property-item">
                                                    <a class="property-img" href="{{ url($item->link) }}">
                                                        @isset($item->image)
                                                        <img src="{{ asset('storage/'.$item->image->project_gallery_link) }}" alt="{{ $item->image->project_gallery_name }}">
                                                        @else
                                                        <img src="{{ asset('asset/clients/images/default/default-project.jpg')}}" alt="{{ $item->name }}">
                                                        @endisset
                                                    </a>
                                                    {{-- <ul class="feature_text">
                                                        <li class="feature_cb"><span> Featured</span></li>
                                                        <li class="feature_or"><span>For Sale</span></li>
                                                    </ul> --}}
                                                    <div class="property-author-wrap">
                                                        {{-- <a href="#" class="property-author">
                                                            <img src="images/agents/agent_min_1.jpg" alt="...">
                                                            <span>Tony Stark</span>
                                                        </a> --}}
                                                        <ul class="save-btn">
                                                            <li data-toggle="tooltip" data-placement="top" title="Hình Ảnh"><a href=".photo-gallery{{ $idx }}" class="btn-gallery"><i class="lnr lnr-camera"></i></a></li>
                                                            {{-- <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                                                            <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li> --}}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-5 col-md-12">
                                                <div class="property-title-box">
                                                    <h6><a href="{{ $item->link }}">{{ $item->title }}</a></h6>
                                                    <div class="property-location">
                                                        <i class="fa fa-map-marker-alt"></i>
                                                        <p>{{ $item->address }}</p>
                                                    </div>
                                                    <ul class="property-feature">
                                                        <li> <i class="fas fa-arrows-alt"></i>
                                                            <span>{{ $item->acreage }}</span>
                                                        </li>
                                                        @isset($item->block)
                                                        <li> <i class="fas fa-building"></i>
                                                            <span>{{ $item->block }}</span>
                                                        </li>
                                                        @endisset
                                                        @isset($item->floor)
                                                            <li> <i class="fas fa-walking"></i>
                                                                <span>{{ $item->floor }}</span>
                                                            </li>
                                                        @endisset
                                                        @isset($item->house)
                                                            <li> <i class="fas fa-home"></i>
                                                                <span>{{ $item->house }}</span>
                                                            </li>
                                                        @endisset
                                                        @isset($item->time)
                                                            <li> <i class="fas fa-calendar-alt"></i>
                                                                <span>{{ date('d/m/Y',strtotime($item->time)) }}</span>
                                                            </li>
                                                        @endisset
                                                    </ul>
                                                    <div class="trending-bottom">
                                                        <div class="trend-left float-left">
                                                            <p class="list-date"><i class="lnr lnr-calendar-full"></i>đăng
                                                                {{ $item->before_day>0?$item->before_day.' ngày trước':$item->before_hour.' giờ trước' }}</p>
                                                        </div>
                                                        {{-- <div class="trend-right float-right">
                                                            <ul class="product-rating">
                                                                <li><i class="fas fa-star"></i></li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                <li><i class="fas fa-star-half-alt"></i></li>
                                                                <li><i class="fas fa-star-half-alt"></i></li>
                                                            </ul>
                                                        </div> --}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-3 col-md-12">
                                                <div class="row list-extra">
                                                    <div class="col-md-12 col-7 sm-left">
                                                        {{-- <div class="trend-open">
                                                            <p><span class="per_sale">starts from</span>$25000</p>
                                                        </div> --}}
                                                    </div>
                                                    <div class="col-md-12 col-5">
                                                        <a href="{{ url($item->link) }}" class="btn v7">Chi tiết</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                        $idx++;
                                    @endphp
                                    @empty

                                    @endforelse
                                </div>

                                <!--pagination starts-->
                                <div class="post-nav nav-res pt-50">
                                    {{ $dbProjects->appends([
                                        'sort' => Request::get('sort'),
                                        'address' => Request::get('address'),
                                        'provincial' => Request::get('provincial'),
                                        'district' => Request::get('district')
                                        ])->links('clients.parts.pagination') }}

                                </div>
                                <!--pagination ends-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Listing filter ends-->
@stop

@section('javascript')
    <script>
        $(function(){
            var cUrl = window.location.href;
            if(cUrl.includes('?page=')||cUrl.includes('?_token=')){
                $('html, body').scrollTop(500);}

            changeType() ;
        });

        function changeType() {
            var proType = $("select[name='provincial']").children("option:selected").val();
            if(proType!='')
            {

                $('#filterDistrict').find('option').remove().end();
                // $('#filterDistrict').append($('<option>', {value: '',text : 'Chọn Quận/ Huyện'}));
                $('#filterDistrict').append('<option value="" selected>Chọn Quận/ Huyện</option>');
                $('#filterDistrict').selectpicker('refresh');

                $.ajax({
                    contentType: "application/json",
                    dataType: "json",
                    url: '/dia-chi/du-an/'+proType,
                    success: function(data){
                        $.each(data, function(i,item){
                                $('select[name="district"]').append($('<option>', {value: item,text : item}));
                                // $('#filterDistrict').append('<option value="'+item+'">'+item+'</option>');
                                $('#filterDistrict').selectpicker('refresh');
                            });
                        $('#filterDistrict').selectpicker('val', '{{Request::get('district')}}');
                        $('#filterDistrict').selectpicker('refresh');
                        }
                });

            }
        }

        function submitSort() {
            var sort = $('select[name="sortObject"]').children("option:selected").val();
            $('input[name="sort"]').val(sort);
            $('form[name="filter-form"]').submit();
        }
    </script>
@stop
