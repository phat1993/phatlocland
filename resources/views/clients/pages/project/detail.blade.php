@extends('layouts.client_layout')
@section('title')
    {{ $project->web_title }}
@endsection
@section('styles')
    <meta name="keywords" content="{{ $project->web_keywords }}">
    <meta name="description" content="{{ $project->web_description }}">
    @isset($category)
        <link rel="canonical" href="{{ url('du-an/'.$category->web_canonical.'/'.$project->web_canonical) }}">
    @else
        <link rel="canonical" href="{{ url('du-an/'.$project->web_canonical) }}">
    @endisset
@endsection
@section('content')

<!--Header ends-->
<div class="property-details-wrap bg-cb">
    <!--Listing Details Hero starts-->
    <div class="single-property-header v1 bg-h mt-60"
    style="background-image:
        @isset($banner)
            url({{ asset('storage/'.$banner->project_gallery_link)}})
        @else
            url({{ asset('asset/clients/images/default/default-project-banner.jpg')}})
        @endisset
    ;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="list-details-title v1">
                        <div class="row">
                            <div class="col-lg-7 col-md-8 col-sm-12">
                                <div class="single-listing-title float-left">
                                    <h2>{{ $project->project_name }}
                                        {{-- <span class="btn v5">For Sale</span> --}}
                                    </h2>
                                    <p><i class="fa fa-map-marker-alt"></i>
                                        {{ strlen($project->project_address) > 1 ? $project->project_address . ', ' : '' }}
                                        {{ $project->project_commune . ', ' . $project->project_district . ', ' . $project->project_provincial }}
                                        </p>
                                    {{-- <a href="#" class="property-author">
                                        <img src="{{ asset('asset/clients/images/agents/agent_min_1.jpg')}}" alt="...">
                                        <span>Tony Stark</span>
                                    </a> --}}
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-4 col-sm-12">
                                <div class="list-details-btn text-right sm-left">
                                    {{-- <div class="trend-open">
                                        <p><span class="per_sale">starts from</span>$85000</p>
                                    </div>
                                    <div class="list-ratings">
                                        <span class="fas fa-star"></span>
                                        <span class="fas fa-star"></span>
                                        <span class="fas fa-star"></span>
                                        <span class="fas fa-star"></span>
                                        <span class="fas fa-star-half-alt"></span>
                                    </div> --}}
                                    <ul class="list-btn">
                                        {{-- <li class="share-btn">
                                            <a href="#" class="btn v2" data-toggle="tooltip" title="Share">Chia Sẻ</a>
                                            <ul class="social-share">
                                                <li class="bg-fb"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                                <li class="bg-tt"><a href="#"><i class="fab fa-twitter"></i></a></li>
                                                <li class="bg-ig"><a href="#"><i class="fab fa-instagram"></i></a></li>
                                            </ul>
                                        </li>
                                        <li class="save-btn">
                                            <a href="#" class="btn v2" data-toggle="tooltip" title="Save">Lưu</a>
                                        </li> --}}
                                        <li class="print-btn">
                                            <a href="#" class="btn btn-danger" data-toggle="tooltip" title="Liên Hệ Ngay">Liên Hệ Ngay</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Listing Details Hero ends-->
    <!--Listing Details Info starts-->
    <div class="single-property-details v1 mt-120">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-12">
                    <div class="listing-desc-wrap mr-30">
                        <div id="list-menu" class="list_menu d-none d-md-block">
                            <ul class="list-details-tab fixed_nav">
                                <li class="nav-item active"><a href="#description" class="active">Mô tả</a></li>
                                @if ($galleries!=null && $galleries->count()>0)
                                <li class="nav-item"><a href="#gallery">Hình Ảnh</a></li>
                                @endif
                                {{-- <li class="nav-item"><a href="#video">Video</a></li> --}}
                                <li class="nav-item"><a href="#details">Chi tiết</a></li>
                                {{-- <li class="nav-item"><a href="#floor_plan">Bản vẽ</a></li>
                                <li class="nav-item"><a href="#nearby">Lân cận</a></li>
                                <li class="nav-item"><a href="#reviews">Reviews</a></li> --}}
                            </ul>
                        </div>
                        <!--Listing Details starts-->
                        <div class="list-details-wrap">
                            <div id="description" class="list-details-section">
                                <h4>Mô Tả</h4>
                                <div class="overview-content">
                                    <p class="mb-10">{{ $project->project_description }}</p>
                                </div>
                                <div class="mt-40">
                                    <h4 class="list-subtitle">Vị trí</h4>
                                    <a target="_blank" href="{{$project->project_map_location}}" class="location-map">Xem Bản Đồ<i class="fa fa-map-marker-alt"></i></a>
                                    <ul class="listing-address">
                                        @if (strlen($project->project_address) > 1)
                                            <li>Địa chỉ : <span>{{ $project->project_address }}</span></li>
                                        @endif
                                        <li>Phường/Xã : <span>{{ $project->project_commune }}</span></li>
                                        <li>Quận/Huyện : <span>{{ $project->project_district }}</span></li>
                                        <li>Tỉnh/Thành Phố : <span>{{ $project->project_provincial }}</span></li>
                                        <li>Mã Bưu Điện : <span>{{ $project->project_postal_code }}</span></li>
                                    </ul>
                                </div>
                            </div>
                            @if ($galleries!=null && $galleries->count()>0)
                            <div id="gallery" class="list-details-section">
                                <h4>Hình Ảnh</h4>
                                <!--Carousel Wrapper-->
                                <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails list-gallery pt-2" data-ride="carousel">
                                    <!--Slides-->
                                    <div class="carousel-inner" role="listbox">
                                        @php
                                            $first = true;
                                        @endphp
                                        @foreach ($galleries as $item)
                                        @if($item->project_gallery_banner != 1)
                                                @if ($first)
                                                    <div class="carousel-item active">
                                                        <img class="d-block w-100" src="{{ asset('storage/'.$item->project_gallery_link)}}" alt="{{ $item->project_gallery_name }}">
                                                    </div>
                                                    @php
                                                         $first = false;
                                                    @endphp
                                                @else
                                                    <div class="carousel-item">
                                                        <img class="d-block w-100" src="{{ asset('storage/'.$item->project_gallery_link)}}" alt="{{ $item->project_gallery_name }}">
                                                    </div>
                                                @endif
                                            @endif
                                        @endforeach
                                    </div>
                                    <!--Controls starts-->
                                    <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                                        <span class="lnr lnr-arrow-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                                        <span class="lnr lnr-arrow-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                    <!--Controls ends-->
                                    <ol class="carousel-indicators  list-gallery-thumb">
                                        @php
                                            $idx = 0;
                                        @endphp
                                        @forelse ($galleries as $item)
                                            @if($item->project_gallery_banner != 1)
                                            <li data-target="#carousel-thumb" data-slide-to="{{ $idx }}">
                                                <img class="img-fluid d-block w-100" src="{{ asset('storage/'.$item->project_gallery_link)}}" alt="{{ $item->project_gallery_name }}">
                                            </li>
                                            @endif
                                        @empty
                                        @endforelse
                                    </ol>
                                </div>
                                <!--/.Carousel Wrapper-->
                            </div>
                            @endif
                            {{-- <div id="video" class="list-details-section">
                                <h4>Video</h4>
                                <div class="popup-vid pt-2">
                                    <img src="{{ asset('asset/clients/images/bg/property-vid.jpg')}}" alt="..." class="popup-img">
                                    <a class="popup-yt hvr-ripple-out" href="https://www.youtube.com/watch?v=v_ATnE02qFs">
                                        <i class="fas fa-play"></i>
                                    </a>
                                </div>
                            </div> --}}
                            <div id="details" class="list-details-section">
                                <div class="mb-40">
                                    <h4>Chi tiết</h4>
                                    <br>
                                    <div>
                                        {!! $project->project_detail !!}
                                    </div>
                                </div>
                                @if($amenities!=null)
                                <div class="mb-40">
                                    <h4>Tiện nghi</h4>
                                    <ul class="listing-features">
                                        @forelse ($amenities as $item)
                                            <li><i class="{{ $item->amenity_icon }}"></i> {{ $item->amenity_name }}</li>
                                        @empty
                                        <li>Không có tiện nghi</li>
                                        @endforelse
                                    </ul>
                                </div>
                                @endif
                                {{-- <div>
                                    <h4>Property Documents</h4>
                                    <ul class="listing-features pp_docs">
                                        <li><a target="_blank" href="images/property-file/demo.docx"><i class="lnr lnr-file-empty"></i>Sample Property Document </a></li>
                                    </ul>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-12">
                    <div id="list-sidebar" class="listing-sidebar">
                        <div class="widget">
                            <h3 class="widget-title">Đã xem gần đây</h3>
                            @forelse ($addeds as $item)
                            <li class="row recent-list">
                                <div class="col-lg-5 col-4">
                                    <div class="entry-img">
                                        @isset($item->image)
                                            <img src="{{ asset('storage/'.$item->image->project_gallery_link)}}" alt="{{ $item->image->project_gallery_name }}">
                                        @else
                                            <img src="{{ asset('asset/clients/images/default/default-project.jpg')}}" alt="{{ $item->name }}">
                                        @endisset
                                    </div>
                                </div>
                                <div class="col-lg-7 col-8 no-pad-left">
                                    <div class="entry-text">
                                        <h6 class="entry-title"><a href="{{ url($item->link) }}">{{ $item->name }}</a></h6>
                                        <div class="property-location">
                                            <i class="fa fa-map-marker-alt"></i>
                                            <p>{{ $item->address }}</p>
                                        </div>
                                        <div class="trend-open">
                                            <i class="lnr lnr-calendar-full"></i> đăng
                                            {{ $item->before_day>0?$item->before_day.' ngày trước':$item->before_hour.' giờ trước' }}
                                        </div>
                                    </div>
                                </div>
                            </li>
                            @empty

                            @endforelse
                        </div>
                        <div class="widget">
                            <h3 class="widget-title">Nổi Bật</h3>
                            <div class="swiper-container single-featured-list">
                                <div class="swiper-wrapper">
                                    @forelse ($features as $item)
                                    <div class="swiper-slide single-property-box">
                                        <div class="property-item">
                                            <a class="property-img" href="{{ url($item->link) }}">
                                                @isset($item->image)
                                                    <img src="{{ asset('storage/'.$item->image->project_gallery_link)}}" alt="{{ $item->image->project_gallery_name }}">
                                                @else
                                                    <img src="{{ asset('asset/clients/images/default/default-project.jpg')}}" alt="{{ $item->name }}">
                                                @endisset
                                            </a>
                                            {{-- <ul class="feature_text">
                                                <li class="feature_cb"><span> Featured</span></li>
                                                <li class="feature_or"><span>For Sale</span></li>
                                            </ul> --}}
                                            <div class="property-author-wrap">
                                                <h5><a href="{{ url($item->link) }}" class="property-author">
                                                    {{ $item->name }}
                                                    </a>
                                                </h5>
                                                <div class="featured-price">
                                                    {{ $item->acreage }}
                                                    {{-- <p><span class="per_sale">starts from</span>$12000</p> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @empty

                                    @endforelse
                                </div>
                                <div class="slider-btn v3 single-featured-next"><i class="lnr lnr-arrow-right"></i></div>
                                <div class="slider-btn v3 single-featured-prev"><i class="lnr lnr-arrow-left"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Listing Details Info ends-->
    <!--Similar Listing starts-->
    @if($vieweds!=null&&$vieweds->count()>0)
    <div class="similar-listing-wrap pb-70 mt-70">
        <div class="container">
            <div class="col-md-12 px-0">
                <div class="similar-listing">
                    <div class="section-title v2">
                        <h2>Danh sách đã xem</h2>
                    </div>
                    <div class="swiper-container similar-list-wrap">
                        <div class="swiper-wrapper">
                            @php
                                $idx = 1;
                            @endphp
                            @forelse ($vieweds as $item)

                            <div class="swiper-slide">
                                <div class="single-property-box">
                                    <div class="property-item">
                                        <a class="property-img" href="{{ url($item->link) }}">
                                            @isset($item->image)
                                                <img src="{{ asset('storage/'.$item->image->project_gallery_link)}}" alt="{{ $item->image->project_gallery_name }}">
                                            @else
                                                <img src="{{ asset('asset/clients/images/default/default-project.jpg')}}" alt="{{ $item->name }}">
                                            @endisset
                                        </a>

                                        @isset($item->features)
                                            {{--
                                            <ul class="feature_text">
                                                <li class="feature_cb"><span>New</span></li>
                                                <li class="feature_or"><span>For Sale</span></li>
                                            </ul> --}}
                                        @endisset
                                        <div class="property-author-wrap">
                                            {{-- <a href="#" class="property-author">
                                                <img src="images/agents/agent_min_1.jpg" alt="...">
                                                <span>Tony Stark</span>
                                            </a> --}}
                                            <ul class="save-btn">
                                                <li data-toggle="tooltip" data-placement="top" title="Hình Ảnh"><a href=".photo-gallery{{ $idx }}" class="btn-gallery"><i class="lnr lnr-camera"></i></a></li>
                                                {{-- <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                                                <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li> --}}
                                            </ul>
                                            <div class="hidden photo-gallery{{ $idx }}">
                                                @forelse ($item->galleries as $photo)
                                                <a href="{{ asset('storage/'.$photo->project_gallery_link) }}"></a>
                                                @empty

                                                @endforelse
                                            </div>
                                        </div>
                                    </div>
                                    <div class="property-title-box">
                                        <h6 clas><a href="{{ $item->link }}">{{ $item->title }}</a></h6>
                                        <div class="property-location">
                                            <i class="fa fa-map-marker-alt"></i>
                                            <p>{{ $item->address }}</p>
                                        </div>
                                        <ul class="property-feature">
                                            <li> <i class="fas fa-arrows-alt"></i>
                                                <span>{{ $item->acreage }}</span>
                                            </li>
                                            @isset($item->block)
                                                <li> <i class="fas fa-building"></i>
                                                    <span>{{ $item->block }}</span>
                                                </li>
                                            @endisset
                                            @isset($item->floor)
                                                <li> <i class="fas fa-walking"></i>
                                                    <span>{{ $item->floor }}</span>
                                                </li>
                                            @endisset
                                            @isset($item->house)
                                                <li> <i class="fas fa-home"></i>
                                                    <span>{{ $item->house }}</span>
                                                </li>
                                            @endisset
                                            @isset($item->time)
                                                <li> <i class="fas fa-calendar-alt"></i>
                                                    <span>{{ date('d/m/Y',strtotime($item->time)) }}</span>
                                                </li>
                                            @endisset
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            @php
                                $idx++;
                            @endphp
                            @empty

                            @endforelse
                        </div>
                    </div>
                    <div class="slider-btn v2 similar-next"><i class="lnr lnr-arrow-right"></i></div>
                    <div class="slider-btn v2 similar-prev"><i class="lnr lnr-arrow-left"></i></div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <!--Similar Listing ends-->
</div>
<!--Agent Chat box starts-->
{{-- <div class="chatbox-wrapper">
    <div class="chat-popup chat-bounce" data-toggle="tooltip" title="Have any query? Ask Me !" data-placement="top">
        <i class="fas fa-comment-alt"></i>
    </div>
    <div class="agent-chat-form v1">
        <div class="container">
            <div class="row">
                <div class="col-4">
                    <img class="agency-chat-img" src="{{ asset('asset/clients/images/agents/agent_min_1.jpg')}}" alt="...">
                </div>
                <div class="col-8 pl-0">
                    <ul class="agent-chat-info">
                        <li><i class="fas fa-user"></i>Tony Stark</li>
                        <li><i class="fas fa-phone-alt"></i>+440 3456 345</li>
                        <li><a href="single-agent.html">View Listings</a></li>
                    </ul>
                    <span class="chat-close"><i class="lnr lnr-cross"></i></span>
                </div>
            </div>
            <div class="row mt-1">
                <div class="col-md-12">
                    <form action="#" method="POST">
                        <div class="chat-group mt-1">
                            <input class="chat-field" type="text" name="chat-name" id="chat-name" placeholder="Your name">
                        </div>
                        <div class="chat-group mt-1">
                            <input class="chat-field" type="text" name="chat-phone" id="chat-phone" placeholder="Phone">
                        </div>
                        <div class="chat-group mt-1">
                            <input class="chat-field" type="text" name="chat-email" id="chat-email" placeholder="Email">
                        </div>
                        <div class="chat-group mt-1">
                            <textarea class="form-control chat-msg" name="message" rows="4" placeholder="Description">Hello, I am interested in [Luxury apartment bay view]</textarea>
                        </div>
                        <div class="chat-button mt-3">
                            <a class="chat-btn" data-toggle="modal" data-target="#mortgage_result">Send Message</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
<!--Agent Chat box ends-->

@stop

@section('javascript')


@stop
