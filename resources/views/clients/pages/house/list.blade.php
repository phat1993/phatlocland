@extends('layouts.client_layout')
@section('title')
    @if($category!=null)
        {{ $category->web_title }}
    @elseif($business!=null)
        {{ $business->web_title }}
    @else
        Bất Động Sản
   @endif
@endsection
@section('styles')
    @if($category!=null)
    <meta name="keywords" content="{{ $category->web_keywords }}">
    <meta name="description" content="{{ $category->web_description }}">
    <link rel="canonical" href="{{ url('bat-dong-san/'.$category->web_canonical) }}">
    @elseif($business!=null)
        <meta name="keywords" content="{{ $business->web_keywords }}">
        <meta name="description" content="{{ $business->web_description }}">
        <link rel="canonical" href="{{ url('bat-dong-san/'.$business->web_canonical) }}">
    @else
        <link rel="canonical" href="{{ url('bat-dong-san') }}">
    @endif
@endsection
@section('content')


        <!--Breadcrumb section starts-->
        <div class="breadcrumb-section bg-h" style="background-image:url({{ asset('asset/clients/images/breadcrumb/breadcrumb_2.jpg') }})">
            <div class="overlay op-5"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        @if($category!=null&&$business!=null)
                            <div class="breadcrumb-menu">
                                <h2>{{ $category->house_category_name }}</h2>
                                <span><a href="{{ route('clients.index') }}">Trang chủ</a></span>
                                <span>{{ $business->house_business_name }}</span>
                                <span>{{ $category->house_category_name }}</span>
                            </div>
                        @elseif($business!=null)
                            <div class="breadcrumb-menu">
                                <h2>{{ $business->house_business_name }}</h2>
                                <span><a href="{{ route('clients.index') }}">Trang chủ</a></span>
                                <span>{{ $business->house_business_name }}</span>
                            </div>
                        @else
                            <div class="breadcrumb-menu">
                                <h2>Bất Động Sản</h2>
                                <span><a href="{{ route('clients.index') }}">Trang chủ</a></span>
                                <span>Bất Động Sản</span>
                            </div>
                            @endif
                    </div>
                </div>
            </div>
        </div>
        <!--Breadcrumb section ends-->
        <!--Listing filter starts-->
        <div class="filter-wrapper style1 section-padding">
            <div class="container">
                <div class="row">
                    <!--Listing filter starts-->
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="hero__form v1 filter listing-filter property-filter" action="{{ route('clients.house.default') }}" method="get" name="filter-form">
                                    {{csrf_field()}}

                                    <input type="hidden" name="sort" value="{{Request::get('sort')}}">
                                    <div class="row">
                                        <div class="col-xl-5 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                                            <div class="input-search">
                                                <input type="text" placeholder="Nhập vào tên, địa chỉ ..." id="address" name="address" value="{{Request::get('address')}}">
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                                            <select class="hero__form-input  form-control custom-select"name="provincial" onchange="changeType()">
                                                <option value="">Chọn Tỉnh/ Thành Phố</option>
                                                @forelse ($provincials as $item)
                                                    <option value="{{ $item->house_provincial }}" @if ($item->house_provincial == Request::get('provincial'))
                                                        {{ __('selected') }}
                                                    @endif>{{ $item->house_provincial }}</option>
                                                @empty

                                                @endforelse
                                            </select>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                                            <select class="hero__form-input  form-control custom-select" id="filterDistrict" name="district">
                                                <option value="">Chọn Quận/ Huyện</option>
                                            </select>
                                        </div>
                                        {{-- <div class="col-xl-2 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                                            <select class="hero__form-input  form-control custom-select" id="filterCommenu" name="district">
                                                <option value="">Chọn Phường/ Xã</option>
                                            </select>
                                        </div> --}}
                                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                                            <div class="submit_btn">
                                                <button class="btn v3" type="submit">Tìm kiếm</button>
                                            </div>
                                            <div class="dropdown-filter"><span>Thêm</span></div>
                                        </div>
                                        <div class="explore__form-checkbox-list full-filter">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-6 py-1 pr-30">
                                                    <select class="hero__form-input  form-control custom-select mb-20" name="filterBusiness">
                                                        <option value="">Kinh Doanh</option>
                                                        @forelse ($filterBusinesses as $item)
                                                            <option value="{{ $item->house_business_id }}"@if ($item->house_business_id == Request::get('filterBusiness'))
                                                                {{ __('selected') }}
                                                            @endif>{{ $item->house_business_name }}</option>
                                                        @empty

                                                        @endforelse
                                                    </select>
                                                </div>
                                                <div class="col-lg-4 col-md-6 py-1 pr-30">
                                                    <select class="hero__form-input  form-control custom-select mb-20" name="filterCategory">
                                                        <option value="">Danh Mục</option>
                                                        @forelse ($filterCategories as $item)
                                                            <option value="{{ $item->house_category_id }}"@if ($item->house_category_id == Request::get('filterCategory'))
                                                                {{ __('selected') }}
                                                            @endif>{{ $item->house_category_name }}</option>
                                                        @empty

                                                        @endforelse
                                                    </select>
                                                </div>
                                                @if($filterProjects!=null && $filterProjects->count()>0)
                                                <div class="col-lg-4 col-md-6 py-1 pr-30">
                                                    <select class="hero__form-input form-control custom-select mb-20" name="filterProject">
                                                        <option value="">Dự Án</option>
                                                        @forelse ($filterProjects as $item)
                                                            <option value="{{ $item->project_id }}"@if ($item->project_id == Request::get('filterProject'))
                                                                {{ __('selected') }}
                                                            @endif>{{ $item->project_name }}</option>
                                                        @empty

                                                        @endforelse
                                                    </select>
                                                </div>
                                                @endif
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 py-1 pr-30">
                                                    <select class="hero__form-input form-control custom-select mb-20" name="filterPrice">
                                                        <option value="" @if (Request::get('filterPrice'=='')){{ __('selected') }}@endif>Giá Tiền</option>
                                                        <option value="below" @if (Request::get('filterPrice'=='below')){{ __('selected') }}@endif>dưới 1 triệu</option>
                                                        <option value="10" @if (Request::get('filterPrice'=='10')){{ __('selected') }}@endif>1 - 10 triệu</option>
                                                        <option value="20" @if (Request::get('filterPrice'=='20')){{ __('selected') }}@endif>10 - 20 triệu</option>
                                                        <option value="50" @if (Request::get('filterPrice'=='50')){{ __('selected') }}@endif>20 - 50 triệu</option>
                                                        <option value="100" @if (Request::get('filterPrice'=='100')){{ __('selected') }}@endif>50 - 100 triệu</option>
                                                        <option value="more" @if (Request::get('filterPrice'=='more')){{ __('selected') }}@endif>trên 100 triệu</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3 col-md-6 py-1 pr-30 ">
                                                    <select class="hero__form-input form-control custom-select mb-20" name="filterDirection">
                                                        <option value="" @if (Request::get('filterDirection'=='')){{ __('selected') }}@endif>Hướng Nhà</option>
                                                        <option value="Đông" @if (Request::get('filterDirection'=='Đông')){{ __('selected') }}@endif>Đông</option>
                                                        <option value="Tây" @if (Request::get('filterDirection'=='Tây')){{ __('selected') }}@endif>Tây</option>
                                                        <option value="Nam" @if (Request::get('filterDirection'=='Nam')){{ __('selected') }}@endif>Nam</option>
                                                        <option value="Bắc" @if (Request::get('filterDirection'=='Bắc')){{ __('selected') }}@endif>Bắc</option>
                                                        <option value="Đông Bắc" @if (Request::get('filterDirection'=='Đông Bắc')){{ __('selected') }}@endif>Đông Bắc</option>
                                                        <option value="Đông Nam" @if (Request::get('filterDirection'=='Đông Nam')){{ __('selected') }}@endif>Đông Nam</option>
                                                        <option value="Tây Bắc" @if (Request::get('filterDirection'=='Tây Bắc')){{ __('selected') }}@endif>Tây Bắc</option>
                                                        <option value="Tây Nam" @if (Request::get('filterDirection'=='Tây Nam')){{ __('selected') }}@endif>Tây Nam</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3 col-md-6 py-1 pr-30 ">
                                                    <select class="hero__form-input form-control custom-select mb-20" name="filterRooms">
                                                        <option value="" @if (Request::get('filterRooms'=='')){{ __('selected') }}@endif>Số Phòng</option>
                                                        <option value="1" @if (Request::get('filterRooms'=='1')){{ __('selected') }}@endif>1 phòng</option>
                                                        <option value="2" @if (Request::get('filterRooms'=='2')){{ __('selected') }}@endif>2 phòng</option>
                                                        <option value="3" @if (Request::get('filterRooms'=='3')){{ __('selected') }}@endif>3 phòng</option>
                                                        <option value="4" @if (Request::get('filterRooms'=='4')){{ __('selected') }}@endif>4 phòng</option>
                                                        <option value="5" @if (Request::get('filterRooms'=='5')){{ __('selected') }}@endif>5 phòng</option>
                                                        <option value="more" @if (Request::get('filterRooms'=='more')){{ __('selected') }}@endif>Trên 5 phòng</option>
                                                    </select>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 py-1 pr-30">
                                                    <select class="hero__form-input form-control custom-select mb-20" name="filterAcreage">
                                                        <option value="">Diện tích</option>
                                                        <option value="below" @if (Request::get('filterAcreage'=='below')){{ __('selected') }}@endif>dưới 25m²</option>
                                                        <option value="25" @if (Request::get('filterAcreage'=='25')){{ __('selected') }}@endif>25 - 50m²</option>
                                                        <option value="50" @if (Request::get('filterAcreage'=='50')){{ __('selected') }}@endif>50 - 100m²</option>
                                                        <option value="more" @if (Request::get('filterAcreage'=='more')){{ __('selected') }}@endif>trên 100m²</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3 col-md-6 py-1 pr-30">
                                                    <select class="hero__form-input form-control custom-select mb-20" name="filterLength">
                                                        <option value="">Chiều Dài</option>
                                                        <option value="below" @if (Request::get('filterLength'=='below')){{ __('selected') }}@endif>dưới 25m</option>
                                                        <option value="25" @if (Request::get('filterLength'=='25')){{ __('selected') }}@endif>25 - 50m</option>
                                                        <option value="50" @if (Request::get('filterLength'=='50')){{ __('selected') }}@endif>50 - 100m</option>
                                                        <option value="more" @if (Request::get('filterLength'=='more')){{ __('selected') }}@endif>trên 100m</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3 col-md-6 py-1 pr-30">
                                                    <select class="hero__form-input form-control custom-select mb-20"  name="filterWidth">
                                                        <option value="" @if (Request::get('filterWidth'=='')){{ __('selected') }}@endif>Chiều Rộng</option>
                                                        <option value="below" @if (Request::get('filterWidth'=='below')){{ __('selected') }}@endif>dưới 25m</option>
                                                        <option value="25" @if (Request::get('filterWidth'=='25')){{ __('selected') }}@endif>25 - 50m</option>
                                                        <option value="50" @if (Request::get('filterWidth'=='50')){{ __('selected') }}@endif>50 - 100m</option>
                                                        <option value="more" @if (Request::get('filterWidth'=='more')){{ __('selected') }}@endif>trên 100m</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--Listing filter ends-->
                    <div class="col-md-12">
                        @if($houses!=null && $houses->count()>0)
                            <div class="row pt-60 align-items-center">
                                <div class="col-lg-3 col-sm-5 col-5">
                                    <div class="item-view-mode res-box">
                                        <!-- item-filter-list Menu starts -->
                                        <ul class="nav item-filter-list" role="tablist">
                                            <li><a class="active" data-toggle="tab" href="#grid-view"><i class="fas fa-th"></i></a></li>
                                            <li><a data-toggle="tab" href="#list-view"><i class="fas fa-list"></i></a></li>
                                        </ul>
                                        <!-- item-filter-list Menu ends -->
                                    </div>
                                </div>
                                <div class="col-lg-4 col-sm-7 col-7">
                                    <select class="listing-input hero__form-input form-control custom-select" name="sortObject" onchange="submitSort()">
                                        <option value="new" @if (Request::get('sort')=='new')
                                            {{ __('selected') }}
                                        @endif>Sắp xếp theo Mới nhất</option>
                                        <option value="old" @if (Request::get('sort')=='old')
                                        {{ __('selected') }}
                                    @endif>Sắp xếp theo Cũ nhất</option>
                                        <option value="trend" @if (Request::get('sort')=='trend')
                                        {{ __('selected') }}
                                    @endif>Sắp xếp theo Nổi bật</option>
                                    </select>
                                </div>
                                {{ $dbHouses->appends([
                                    'sort' => Request::get('sort'),
                                    'address' => Request::get('address'),
                                    'provincial' => Request::get('provincial'),
                                    'district' => Request::get('district'),
                                    'filterBusiness' => Request::get('filterBusiness'),
                                    'filterCategory' => Request::get('filterCategory'),
                                    'filterProject' => Request::get('filterProject'),
                                    'filterPrice' => Request::get('filterPrice'),
                                    'filterPrice' => Request::get('filterPrice'),
                                    'filterDirection' => Request::get('filterDirection'),
                                    'filterRooms' => Request::get('filterRooms'),
                                    'filterAcreage' => Request::get('filterAcreage'),
                                    'filterLength' => Request::get('filterLength'),
                                    'filterWidth' => Request::get('filterWidth')
                                ])->links('clients.parts.display_pagination') }}
                            </div>
                        @else
                            <h1 class="text-center p-5">Hiện tại không có bất động sản..</h1>
                        @endif
                        <div class="item-wrapper pt-40">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane show active fade property-grid" id="grid-view">
                                    <div class="row">
                                        @php
                                            $idx = 1;
                                        @endphp
                                        @forelse ($houses as $item)
                                        <div class="col-xl-4 col-md-6 col-sm-12">
                                            <div class="single-property-box">
                                                <div class="property-item">
                                                    <a class="property-img" href="{{ url($item->link) }}">
                                                        @isset($item->image)
                                                            <img src="{{ asset('storage/'.$item->image->house_gallery_link) }}" alt="{{ $item->image->house_gallery_name }}">
                                                        @else
                                                            <img src="{{ asset('asset/clients/images/default/default_house.jpg')}}" alt="{{ $item->name!=''?$item->name:'' }}">
                                                        @endisset
                                                    </a>
                                                    @isset($item->status)
                                                    <ul class="feature_text">
                                                        <li class="{{ $item->status_color }}"><span>{{ $item->status }}</span></li>
                                                    </ul>
                                                    @endisset
                                                    <div class="property-author-wrap">
                                                        {{-- <a href="#" class="property-author">
                                                            <img src="{{ asset('asset/clients/images/agents/agent_min_1.jpg') }}" alt="...">
                                                            <span>Tony Stark</span>
                                                        </a> --}}
                                                        <ul class="save-btn">
                                                            <li data-toggle="tooltip" data-placement="top" title="Hình Ảnh"><a href=".photo-gallery{{ $idx }}" class="btn-gallery"><i class="lnr lnr-camera"></i></a></li>
                                                            {{-- <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                                                            <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li> --}}
                                                        </ul>
                                                        <div class="hidden photo-gallery{{ $idx }}">
                                                            @forelse ($item->galleries as $photo)
                                                            <a href="{{ asset('storage/'.$photo->house_gallery_link) }}"></a>
                                                            @empty

                                                            @endforelse
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="property-title-box">
                                                    <h6 class="text-limit">
                                                        <a href="{{ $item->link }}"><p style="overflow: hidden;text-overflow: ellipsis;max-width: 75ch;">{{ $item->title }}</p></a>
                                                    </h6>
                                                    <div class="property-location">
                                                        <i class="fa fa-map-marker-alt"></i>
                                                        <p>{{ $item->address }}</p>
                                                    </div>
                                                    <ul class="property-feature">
                                                        @isset($item->acreage)
                                                            <li> <i class="fas fa-arrows-alt"></i>
                                                                <span>{{ $item->acreage }}</span>
                                                            </li>
                                                        @endisset
                                                        @isset($item->length)
                                                            <li> <i class="fas fa-ruler-vertical"></i>
                                                                <span>{{ $item->length }} m</span>
                                                            </li>
                                                        @endisset
                                                        @isset($item->width)
                                                            <li> <i class="fas fa-ruler-horizontal"></i>
                                                                <span>{{ $item->width }} m</span>
                                                            </li>
                                                        @endisset
                                                        @isset($item->direction)
                                                            <li> <i class="fas fa-compass"></i>
                                                                <span>Hướng {{ $item->direction }}</span>
                                                            </li>
                                                        @endisset
                                                        {{-- @isset($item->rooms)
                                                            <li> <i class="fas fa-home"></i>
                                                                <span>{{ $item->rooms }} Phòng</span>
                                                            </li>
                                                        @endisset
                                                        @isset($item->time)
                                                            <li> <i class="fas fa-calendar-alt"></i>
                                                                <span>{{ date('d/m/Y',strtotime($item->time)) }}</span>
                                                            </li>
                                                        @endisset --}}
                                                    </ul>
                                                    <div class="trending-bottom">
                                                        <div class="trend-left float-left">
                                                            <ul class="product-rating">
                                                                @php
                                                                    $star = $item->rating;
                                                                @endphp
                                                                @for ($i = 0; $i < 5; $i++)
                                                                    @if ($star>=1)
                                                                        <li><i class="fas fa-star"></i></li>
                                                                    @elseif ($star>0)
                                                                        <li><i class="fas fa-star-half-alt"></i></li>
                                                                    @else
                                                                        <li><i class="far fa-star"></i></li>
                                                                    @endif
                                                                    @php
                                                                        $star--;
                                                                    @endphp
                                                                @endfor
                                                            </ul>
                                                        </div>
                                                        <a class="trend-right float-right">
                                                            <div class="trend-open">
                                                                <p>
                                                                    @if ($item->price<=0)
                                                                        <strong class="text-danger">Thương lượng</strong>
                                                                    @else
                                                                        @if (strlen($item->price)>9)
                                                                            {{ round($item->price/1000000000, 2).' Tỷ' }}
                                                                        @elseif(strlen($item->price)>6)
                                                                            {{ is_int(($item->price/1000000))?($item->price/1000000).' Triệu': number_format($item->price,0,'',',') }}
                                                                        @else
                                                                            {{ number_format($item->price,0,'',',') }}
                                                                        @endif
                                                                        <sup class="text-danger">vnđ</sup>
                                                                    @endif
                                                                </p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @php
                                            $idx++;
                                        @endphp
                                        @empty
                                        @endforelse
                                    </div>
                                </div>
                                <div class="tab-pane fade property-list fullwidth" id="list-view">
                                    @php
                                        $idx = 1;
                                    @endphp
                                    @forelse ($houses as $item)
                                    <div class="single-property-box">
                                        <div class="row">
                                            <div class="col-xl-5 col-lg-4 col-md-12">
                                                <div class="property-item">
                                                    <a class="property-img" href="{{ url($item->link) }}">
                                                    @isset($item->image)
                                                        <img src="{{ asset('storage/'.$item->image->house_gallery_link) }}" alt="{{ $item->image->house_gallery_name }}">
                                                    @else
                                                        <img src="{{ asset('asset/clients/images/default/default_house.jpg')}}" alt="{{ $item->name!=''?$item->name:'' }}">
                                                    @endisset
                                                    </a>
                                                    {{-- <ul class="feature_text">
                                                        <li class="feature_cb"><span> Featured</span></li>
                                                        <li class="feature_or"><span>For Sale</span></li>
                                                    </ul> --}}
                                                    <div class="property-author-wrap">
                                                        {{-- <a href="#" class="property-author">
                                                            <img src="images/agents/agent_min_1.jpg" alt="...">
                                                            <span>Tony Stark</span>
                                                        </a> --}}
                                                        <ul class="save-btn">
                                                            <li data-toggle="tooltip" data-placement="top" title="Hình Ảnh"><a href=".photo-gallery{{ $idx }}" class="btn-gallery"><i class="lnr lnr-camera"></i></a></li>
                                                            {{-- <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                                                            <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li> --}}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-4 col-lg-5 col-md-12">
                                                <div class="property-title-box">
                                                    <h6><a href="{{ $item->link }}">{{ $item->title }}</a></h6>
                                                    <div class="property-location">
                                                        <i class="fa fa-map-marker-alt"></i>
                                                        <p>{{ $item->address }}</p>
                                                    </div>
                                                    <ul class="property-feature">
                                                        @isset($item->acreage)
                                                            <li> <i class="fas fa-arrows-alt"></i>
                                                                <span>{{ $item->acreage }}</span>
                                                            </li>
                                                        @endisset
                                                        @isset($item->length)
                                                            <li> <i class="fas fa-ruler-vertical"></i>
                                                                <span>{{ $item->length }} m</span>
                                                            </li>
                                                        @endisset
                                                        @isset($item->width)
                                                            <li> <i class="fas fa-ruler-horizontal"></i>
                                                                <span>{{ $item->width }} m</span>
                                                            </li>
                                                        @endisset
                                                        @isset($item->direction)
                                                            <li> <i class="fas fa-compass"></i>
                                                                <span>Hướng {{ $item->direction }}</span>
                                                            </li>
                                                        @endisset
                                                        @isset($item->rooms)
                                                            <li> <i class="fas fa-home"></i>
                                                                <span>{{ $item->rooms }} Phòng</span>
                                                            </li>
                                                        @endisset
                                                        @isset($item->time)
                                                            <li> <i class="fas fa-calendar-alt"></i>
                                                                <span>{{ date('d/m/Y',strtotime($item->time)) }}</span>
                                                            </li>
                                                        @endisset
                                                    </ul>
                                                    <div class="trending-bottom">

                                                        <div class="trend-left float-left">
                                                            <p class="list-date"><i class="lnr lnr-calendar-full"></i>đăng
                                                                {{ $item->before_day>0?$item->before_day.' ngày trước':$item->before_hour.' giờ trước' }}</p>
                                                        </div>
                                                        <div class="trend-right float-right">
                                                            <ul class="product-rating">
                                                                <li><i class="fas fa-star"></i></li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                <li><i class="fas fa-star"></i></li>
                                                                <li><i class="fas fa-star-half-alt"></i></li>
                                                                <li><i class="fas fa-star-half-alt"></i></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-lg-3 col-md-12">
                                                <div class="row list-extra">
                                                    <div class="col-md-12 col-7 sm-left">
                                                        <a class="trend-right float-right">
                                                            <div class="trend-open">
                                                                <p>
                                                                    @if ($item->price<=0)
                                                                        <strong class="text-danger">Thương lượng</strong>
                                                                    @else
                                                                        @if (strlen($item->price)>9)
                                                                            {{ round($item->price/1000000000, 2).' Tỷ' }}
                                                                        @elseif(strlen($item->price)>6)
                                                                            {{ is_int(($item->price/1000000))?($item->price/1000000).' Triệu': number_format($item->price,0,'',',') }}
                                                                        @else
                                                                            {{ number_format($item->price,0,'',',') }}
                                                                        @endif
                                                                        <sup class="text-danger">vnđ</sup>
                                                                    @endif
                                                                </p>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-12 col-5">
                                                        <a href="{{ url($item->link) }}" class="btn v7">Chi tiết</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @php
                                        $idx++;
                                    @endphp
                                    @empty

                                    @endforelse
                                </div>


                                <!--pagination starts-->
                                <div class="post-nav nav-res pt-50">
                                    {{ $dbHouses->appends([
                                        'sort' => Request::get('sort'),
                                        'address' => Request::get('address'),
                                        'provincial' => Request::get('provincial'),
                                        'district' => Request::get('district'),
                                        'filterBusiness' => Request::get('filterBusiness'),
                                        'filterCategory' => Request::get('filterCategory'),
                                        'filterProject' => Request::get('filterProject'),
                                        'filterPrice' => Request::get('filterPrice'),
                                        'filterPrice' => Request::get('filterPrice'),
                                        'filterDirection' => Request::get('filterDirection'),
                                        'filterRooms' => Request::get('filterRooms'),
                                        'filterAcreage' => Request::get('filterAcreage'),
                                        'filterLength' => Request::get('filterLength'),
                                        'filterWidth' => Request::get('filterWidth')
                                        ])->links('clients.parts.pagination') }}

                                </div>
                                <!--pagination ends-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Listing filter ends-->
@stop

@section('javascript')
    <script>
        $(function(){
            var cUrl = window.location.href;
            if(cUrl.includes('?page=')||cUrl.includes('?_token=')){
                $('html, body').scrollTop(500);}

            changeType() ;
        });

        function changeType() {
            var proType = $("select[name='provincial']").children("option:selected").val();
            if(proType!='')
            {

                $('#filterDistrict').find('option').remove().end();
                // $('#filterDistrict').append($('<option>', {value: '',text : 'Chọn Quận/ Huyện'}));
                $('#filterDistrict').append('<option value="" selected>Chọn Quận/ Huyện</option>');
                $('#filterDistrict').selectpicker('refresh');

                $.ajax({
                    contentType: "application/json",
                    dataType: "json",
                    url: '/dia-chi/bat-dong-san/'+proType,
                    success: function(data){
                        $.each(data, function(i,item){
                                $('select[name="district"]').append($('<option>', {value: item,text : item}));
                                // $('#filterDistrict').append('<option value="'+item+'">'+item+'</option>');
                                $('#filterDistrict').selectpicker('refresh');
                            });
                        $('#filterDistrict').selectpicker('val', '{{Request::get('district')}}');
                        $('#filterDistrict').selectpicker('refresh');
                        }
                });

            }

        }

        function submitSort() {
            var sort = $('select[name="sortObject"]').children("option:selected").val();
            $('input[name="sort"]').val(sort);
            $('form[name="filter-form"]').submit();
        }
    </script>
@stop
