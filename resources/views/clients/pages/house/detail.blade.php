@extends('layouts.client_layout')
@section('title')
    {{ $house->web_title }}
@endsection
@section('styles')
    <meta name="keywords" content="{{ $house->web_keywords }}">
    <meta name="description" content="{{ $house->web_description }}">
    @isset($category)
        <link rel="canonical" href="{{ url('du-an/'.$category->web_canonical.'/'.$house->web_canonical) }}">
    @else
        <link rel="canonical" href="{{ url('du-an/'.$house->web_canonical) }}">
    @endisset
    <style>
        .contact-form .is-invalid{
            border-color: red;
        }
    </style>
@endsection
@section('content')

<!--Header ends-->
<div class="property-details-wrap bg-cb">
    <!--Listing Details Hero starts-->
    <div class="single-property-header v1 bg-h mt-60"
    style="background-image:
        @isset($banner)
            url({{ asset('storage/'.$banner->house_gallery_link)}});
        @else
            url({{ asset('asset/clients/images/default/default_house_banner.jpg')}});
        @endisset">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="list-details-title v1">
                        <div class="row">
                            <div class="col-lg-7 col-md-8 col-sm-12">
                                <div class="single-listing-title float-left">
                                    <h2>{{ $house->house_name }}
                                        {{-- <span class="btn v5">For Sale</span> --}}
                                    </h2>
                                    <p><i class="fa fa-map-marker-alt"></i>
                                        {{ strlen($house->house_address) > 1 ? $house->house_address . ', ' : '' }}
                                        {{ $house->house_commune . ', ' . $house->house_district . ', ' . $house->house_provincial }}
                                        </p>
                                    {{-- <a href="#" class="property-author">
                                        <img src="{{ asset('asset/clients/images/agents/agent_min_1.jpg')}}" alt="...">
                                        <span>Tony Stark</span>
                                    </a> --}}
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-4 col-sm-12">
                                <div class="list-details-btn text-right sm-left">
                                    <div class="trend-open">
                                        <p>
                                            @if ($house->house_price<=0)
                                                <strong class="text-danger">Thương lượng</strong>
                                            @else
                                                @if (strlen($house->house_price)>9)
                                                    {{ round($house->house_price/1000000000, 2).' Tỷ' }}
                                                @elseif(strlen($house->house_price)>6)
                                                    {{ is_int(($house->house_price/1000000))?($house->house_price/1000000).' Triệu': number_format($house->house_price,0,'',',') }}
                                                @else
                                                    {{ number_format($house->house_price,0,'',',') }}
                                                @endif
                                                <small class="text-danger">vnđ</small>
                                            @endif
                                        </p>
                                    </div>
                                    <div class="list-ratings">
                                        @php
                                            $star = $house->house_rating;
                                        @endphp
                                        @for ($i = 0; $i < 5; $i++)
                                            @if ($star>=1)
                                                <span class="fas fa-star"></span>
                                            @elseif ($star>0)
                                                <span class="fas fa-star-half-alt"></span>
                                            @else
                                                <span class="far fa-star"></span>
                                            @endif
                                            @php
                                                $star--;
                                            @endphp
                                        @endfor
                                    </div>
                                    <ul class="list-btn">
                                        {{-- <li class="share-btn">
                                            <a href="#" class="btn v2" data-toggle="tooltip" title="Share">Chia Sẻ</a>
                                            <ul class="social-share">
                                                <li class="bg-fb"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                                <li class="bg-tt"><a href="#"><i class="fab fa-twitter"></i></a></li>
                                                <li class="bg-ig"><a href="#"><i class="fab fa-instagram"></i></a></li>
                                            </ul>
                                        </li>
                                        <li class="save-btn">
                                            <a href="#" class="btn v2" data-toggle="tooltip" title="Save">Lưu</a>
                                        </li> --}}
                                        <li class="print-btn">
                                            <a href="#" class="btn btn-danger" data-toggle="tooltip" title="Liên Hệ Ngay">Liên Hệ Ngay</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Listing Details Hero ends-->
    <!--Listing Details Info starts-->
    <div class="single-property-details v1 mt-120">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-12">
                    <div class="listing-desc-wrap mr-30">
                        <div id="list-menu" class="list_menu d-none d-md-block">
                            <ul class="list-details-tab fixed_nav">
                                <li class="nav-item active"><a href="#description" class="active">Mô tả</a></li>
                                @if($galleries!=null&&$galleries->count()>0)
                                <li class="nav-item"><a href="#gallery">Hình Ảnh</a></li>
                                @endif
                                {{-- @isset($video)
                                    <li class="nav-item"><a href="#floor_plan">Bản vẽ</a></li>
                                @endisset --}}
                                <li class="nav-item"><a href="#details">Chi tiết</a></li>
                                @if($floorPlans!=null && $floorPlans->count()>0)
                                    <li class="nav-item"><a href="#floor_plan">Bản vẽ</a></li>
                                @endif
                                @if($locationNearbies!=null && $locationNearbies->count()>0)
                                    <li class="nav-item"><a href="#nearby">Lân cận</a></li>
                                @endif
                                <li class="nav-item"><a href="#reviews">Đánh Giá</a></li>

                            </ul>
                        </div>
                        <!--Listing Details starts-->
                        <div class="list-details-wrap">
                            <div id="description" class="list-details-section">
                                <h4>Mô Tả</h4>
                                <div class="overview-content">
                                    <p class="mb-10">{{ $house->house_description }}</p>
                                </div>
                                <div class="mt-40">
                                    <h4 class="list-subtitle">Vị trí</h4>
                                    <a target="_blank" href="{{$house->house_map_location}}" class="location-map">Xem Bản Đồ<i class="fa fa-map-marker-alt"></i></a>
                                    <ul class="listing-address">
                                        @if (strlen($house->house_address) > 1)
                                            <li>Địa chỉ : <span>{{ $house->house_address }}</span></li>
                                        @endif
                                        <li>Phường/Xã : <span>{{ $house->house_commune }}</span></li>
                                        <li>Quận/Huyện : <span>{{ $house->house_district }}</span></li>
                                        <li>Tỉnh/Thành Phố : <span>{{ $house->house_provincial }}</span></li>
                                        <li>Mã Bưu Điện : <span>{{ $house->house_postal_code }}</span></li>
                                    </ul>
                                </div>
                            </div>
                            @if($galleries!=null&&$galleries->count()>0)
                            <div id="gallery" class="list-details-section">
                                <h4>Hình Ảnh</h4>
                                <!--Carousel Wrapper-->
                                <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails list-gallery pt-2" data-ride="carousel">
                                    <!--Slides-->
                                    <div class="carousel-inner" role="listbox">
                                        @php
                                            $firstLoop = true;
                                        @endphp
                                        @foreach ($galleries as $item)
                                            @if($item->house_gallery_banner == 1)
                                                <div class="carousel-item {{  $firstLoop?'active':'' }}">
                                                    <img class="d-block w-100" src="{{ asset('storage/'.$item->house_gallery_link)}}" alt="{{ $item->house_gallery_name }}">
                                                </div>
                                                @php
                                                    $firstLoop = false;
                                                @endphp
                                            @else
                                                <div class="carousel-item">
                                                    <img class="d-block w-100" src="{{ asset('storage/'.$item->house_gallery_link)}}" alt="{{ $item->house_gallery_name }}">
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                    <!--Controls starts-->
                                    <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                                        <span class="lnr lnr-arrow-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                                        <span class="lnr lnr-arrow-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                    <!--Controls ends-->
                                    <ol class="carousel-indicators  list-gallery-thumb">
                                        @php
                                            $idx = 0;
                                        @endphp
                                        @forelse ($galleries as $item)
                                            <li data-target="#carousel-thumb" data-slide-to="{{ $idx }}">
                                                <img class="img-fluid d-block w-100" src="{{ asset('storage/'.$item->house_gallery_link)}}" alt="{{ $item->house_gallery_name }}">
                                            </li>
                                        @empty
                                        @endforelse
                                    </ol>
                                </div>
                                <!--/.Carousel Wrapper-->
                            </div>
                            @endif
                            {{-- <div id="video" class="list-details-section">
                                <h4>Video</h4>
                                <div class="popup-vid pt-2">
                                    <img src="{{ asset('asset/clients/images/bg/property-vid.jpg')}}" alt="..." class="popup-img">
                                    <a class="popup-yt hvr-ripple-out" href="https://www.youtube.com/watch?v=v_ATnE02qFs">
                                        <i class="fas fa-play"></i>
                                    </a>
                                </div>
                            </div> --}}
                            <div id="details" class="list-details-section">
                                <div class="mb-40">
                                    <h4>Chi tiết</h4>
                                    <br>
                                    <div>
                                        {!! $house->house_detail !!}
                                    </div>
                                </div>
                                @if($amenities!=null)
                                <div class="mb-40">
                                    <h4>Tiện nghi</h4>
                                    <ul class="listing-features">
                                        @forelse ($amenities as $item)
                                            <li><i class="{{ $item->amenity_icon }}"></i> {{ $item->amenity_name }}</li>
                                        @empty
                                        <li>Không có tiện nghi</li>
                                        @endforelse
                                    </ul>
                                </div>
                                @endif
                                {{-- <div>
                                    <h4>Property Documents</h4>
                                    <ul class="listing-features pp_docs">
                                        <li><a target="_blank" href="images/property-file/demo.docx"><i class="lnr lnr-file-empty"></i>Sample Property Document </a></li>
                                    </ul>
                                </div> --}}
                            </div>
                            @if($floorPlans!=null && $floorPlans->count()>0)
                            <div id="floor_plan" class="list-details-section">
                                <h4>Bản Vẽ</h4>
                                <div id="accordion10" role="tablist" class="pt-2">
                                    @forelse ($floorPlans as $item)
                                    <div class="card">
                                        <div class="card-header" role="tab" id="headingOne">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion10" href="#collapse{{ $item->floor_plan_id  }}" aria-expanded="true" aria-controls="collapse{{ $item->floor_plan_id  }}">
                                                <p>{{ $item->floor_plan_name }}</p>
                                                <i class="fas fa-angle-down"></i>
                                                <ul class="floor-list">

                                                    @isset($item->floor_plan_size)
                                                        <li>Kích thước : <span>{{ $item->floor_plan_size }}</span></li>
                                                    @endisset
                                                    @isset($item->floor_plan_rooms)
                                                    <li>Phòng : <span>{{ $item->floor_plan_rooms }}</span></li>
                                                    @endisset
                                                    @isset($item->floor_plan_barths)
                                                    <li>Phòng tắm: <span>{{ $item->floor_plan_barths }}</span></li>
                                                    @endisset
                                                    @isset($item->house_price)
                                                    <li>Giá : <span>
                                                        @if (strlen($house->house_price)>9)
                                                            {{ round($house->house_price/1000000000, 2).' Tỷ' }}
                                                        @elseif(strlen($house->house_price)>6)
                                                            {{ is_int(($house->house_price/1000000))?($house->house_price/1000000).' Triệu': number_format($house->house_price,0,'',',') }}
                                                        @else
                                                            {{ number_format($house->house_price,0,'',',') }}
                                                        @endif <small class="text-danger">vnđ</small></span></li>
                                                    @endisset
                                                </ul>
                                            </a>
                                        </div>
                                        <div id="collapse{{ $item->floor_plan_id  }}" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne">
                                            <div class="card-body">
                                                <a href="{{ asset('storage/'.$item->floor_plan_image) }}" data-lightbox="single-1">
                                                    <img src="{{ asset('storage/'.$item->floor_plan_image) }}" alt="{{ $item->floor_plan_name }}">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    @empty

                                    @endforelse
                                </div>
                            </div>
                            @endif
                            @if($locationNearbies!=null && $locationNearbies->count()>0)
                            <div id="nearby" class="list-details-section">
                                <div class="container no-pad-lr">
                                    <h4>Lân Cận</h4>
                                    <div class="nearby-wrap pt-2">
                                        <h4><span><i class="fas fa-utensils"></i></span>Thực Phẩm</h4>
                                        <div class="row align-items-center">
                                            <div class="col-md-6 col-6">
                                                <p class="nearby-place">KFC (0.03mile)</p>
                                            </div>
                                            <div class="col-md-6 col-6">
                                                <ul class="product-rating float-right nearby-rating">
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star-half-alt"></i></li>
                                                    <li><i class="fas fa-star-half-alt"></i></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-md-6 col-6">
                                                <p class="nearby-place">KFC (0.03mile)</p>
                                            </div>
                                            <div class="col-md-6 col-6">
                                                <ul class="product-rating float-right nearby-rating">
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star-half-alt"></i></li>
                                                    <li><i class="fas fa-star-half-alt"></i></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="nearby-wrap">
                                        <h4><span><i class="fas fa-university"></i></span>Giáo Dục</h4>
                                        <div class="row align-items-center">
                                            <div class="col-md-6 col-7">
                                                <p class="nearby-place">Oakleaf School(0.01mile)</p>
                                            </div>
                                            <div class="col-md-6 col-5">
                                                <ul class="product-rating float-right nearby-rating">
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star-half-alt"></i></li>
                                                    <li><i class="fas fa-star-half-alt"></i></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-md-6 col-7">
                                                <p class="nearby-place">Frozen Lake Academy (0.1mile)</p>
                                            </div>
                                            <div class="col-md-6 col-5">
                                                <ul class="product-rating float-right nearby-rating">
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star-half-alt"></i></li>
                                                    <li><i class="fas fa-star-half-alt"></i></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-md-6 col-7">
                                                <p class="nearby-place">Spring Gardens Elementary(0.05mile)</p>
                                            </div>
                                            <div class="col-md-6 col-5">
                                                <ul class="product-rating float-right nearby-rating">
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star-half-alt"></i></li>
                                                    <li><i class="fas fa-star-half-alt"></i></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="nearby-wrap">
                                        <h4><span><i class="fas fa-medkit"></i></span>Bệnh Viện</h4>
                                        <div class="row align-items-center">
                                            <div class="col-md-6 col-7">
                                                <p class="nearby-place">Basuda Health Care (0.03mile)</p>
                                            </div>
                                            <div class="col-md-6 col-5">
                                                <ul class="product-rating float-right nearby-rating">
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star"></i></li>
                                                    <li><i class="fas fa-star-half-alt"></i></li>
                                                    <li><i class="fas fa-star-half-alt"></i></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div id="reviews" class="list-details-section mt-10">
                                <div class="review-box comment-box">
                                    @if($comments!=null && $comments->count()>0)
                                        <h4 class="list-details-title">Nội Dung</h4>
                                        <hr>
                                        <ul class="review_wrap" id="review_form">
                                            @forelse ($comments as $item)
                                            <li>
                                                <div class="customer-review_wrap">
                                                    <div class="reviewer-img">
                                                        @isset($item->house_review_avatar)
                                                            <img src="{{ asset('storage/'.$item->house_review_avatar)}}" class="img-fluid" alt="{{ $item->house_review_name }}">
                                                        @else
                                                            <div class="border border-warning p-2"><i class="fas fa-user-tie" style="font-size: 9rem"></i></div>
                                                            {{-- <img src="{{ asset('asset/clients/images/others/client_4.jpg')}}" class="img-fluid" alt="{{ $item->house_review_name }}"> --}}
                                                        @endisset
                                                        <p>{{ $item->house_review_name }}</p>
                                                    </div>
                                                    <div class="customer-content-wrap">
                                                        <div class="customer-content">
                                                            <div class="customer-review">
                                                                <h6>{{ $item->house_review_email }}</h6>
                                                                <p class="comment-date">
                                                                    {{ date('H:i d/m/Y', strtotime($item->created_at)) }}
                                                                </p>
                                                                <ul class="product-rating">
                                                                    @for ($i = 0; $i < $item->house_review_point; $i++)
                                                                        <li><i class="fas fa-star"></i></li>
                                                                    @endfor
                                                                    @if ($item->house_review_point<5)
                                                                        @for ($i = 0; $i < 5-$item->house_review_point; $i++)
                                                                            <li><i class="far fa-star"></i></li>
                                                                        @endfor
                                                                    @endif
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <p class="customer-text">{{ $item->house_review_message }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </li>
                                            @empty

                                            @endforelse
                                        </ul>
                                        <nav aria-label="Comment navigation">
                                            {{ $comments->appends([])->links('clients.parts.comment_pagination') }}
                                        </nav>
                                        <br>
                                    @endif
                                    <form class="contact-form" id="leave-review" method="POST" action="{{ route('clients.house.review') }}" name="rating-form">
                                        {{csrf_field()}}
                                        <input type="hidden" name="rating_code" value="{{ $house->house_code }}">
                                        <h4 class="contact-form__title">
                                            Đánh giá
                                        </h4>

                                        @if(session('success'))
                                            <div class="alert alert-success alert-dismissible notifyHidden">
                                                <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                                                <p>{{session('success')}}</p>
                                                <br>
                                            </div>
                                        @endif
                                        @if(count($errors) > 0)
                                            <div class="alert alert-danger alert-dismissible notifyHidden">
                                                <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                                                @foreach($errors->all() as $error)
                                                    <p>{{$error}}</p>
                                                @endforeach
                                            </div>
                                        @endif
                                        <div class="form-group row">
                                            <div class="col-md-6 col-sm-7 col-12">
                                                {{-- <p class="contact-form__rate">
                                                    Đánh giá:
                                                </p> --}}
                                                <p class="contact-form__rate-bx">
                                                    <i class="fas fa-star selected"></i>
                                                    <i class="fas fa-star selected"></i>
                                                    <i class="fas fa-star selected"></i>
                                                    <i class="fas fa-star selected"></i>
                                                    <i class="fas fa-star selected"></i>
                                                </p>
                                                <p class="contact-form__rate-bx-show">
                                                    <span class="rate-actual">5</span> / 5
                                                </p>
                                                <input type="hidden" name="rating_star" class="rate-actual" value="5"/>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-4">
                                            <div class="col-md-6">
                                                <input class="contact-form__input-text" type="text" name="rating_name" id="rating_name" value="{{ old('rating_name') }}" placeholder="Họ Tên:">
                                            </div>
                                            <div class="col-md-6">
                                                <input class="contact-form__input-text" type="text" name="rating_mail" id="rating_mail" value="{{ old('rating_mail') }}" placeholder="Email:">
                                            </div>
                                        </div>
                                        <div class="form-group row mb-4">
                                            <div class="col-12">
                                                <textarea class="contact-form__textarea" name="rating_comment" id="rating_comment" placeholder="Nội Dung">{{ old('rating_comment') }}</textarea>
                                            </div>
                                        </div>
                                        <input class="btn v3" type="submit" name="submit-contact" value="Gửi">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-12">
                    <div id="list-sidebar" class="listing-sidebar">
                        <div class="widget">
                            <h3 class="widget-title">Đã xem gần đây</h3>
                            @forelse ($addeds as $item)
                            <li class="row recent-list">
                                <div class="col-lg-5 col-4">
                                    <div class="entry-img">
                                        @isset($item->image)
                                            <img src="{{ asset('storage/'.$item->image->house_gallery_link)}}" alt="{{ $item->image->house_gallery_name }}">
                                        @else
                                            <img src="{{ asset('asset/clients/images/default/default_house.jpg')}}" alt="{{ $item->name!=''?$item->name:'' }}">
                                        @endisset
                                    </div>
                                </div>
                                <div class="col-lg-7 col-8 no-pad-left">
                                    <div class="entry-text">
                                        <h6 class="entry-title"><a href="{{ url($item->link) }}">
                                            <p style="overflow: hidden;
                                            text-overflow: ellipsis;
                                            max-width: 75ch;">
                                            {{ $item->name }}</p>
                                        </a></h6>
                                        <div class="property-location">
                                            <i class="fa fa-map-marker-alt"></i>
                                            <p>{{ $item->address }}</p>
                                        </div>
                                        <div class="trend-open">
                                            <p>
                                                @if ($house->house_price<=0)
                                                    <strong class="text-danger">Thương lượng</strong>
                                                @else
                                                    @if (strlen($house->house_price)>9)
                                                        {{ round($house->house_price/1000000000, 2).' Tỷ' }}
                                                    @elseif(strlen($house->house_price)>6)
                                                        {{ is_int(($house->house_price/1000000))?($house->house_price/1000000).' Triệu': number_format($house->house_price,0,'',',') }}
                                                    @else
                                                        {{ number_format($house->house_price,0,'',',') }}
                                                    @endif
                                                    <small class="text-danger">vnđ</small>
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            @empty

                            @endforelse
                        </div>
                        <div class="widget">
                            <h3 class="widget-title">Nổi Bật</h3>
                            <div class="swiper-container single-featured-list">
                                <div class="swiper-wrapper">
                                    @forelse ($features as $item)
                                    <div class="swiper-slide single-property-box">
                                        <div class="property-item">
                                            <a class="property-img" href="{{ url($item->link) }}">
                                                @isset($item->image)
                                                    <img src="{{ asset('storage/'.$item->image->house_gallery_link)}}" alt="{{ $item->image->house_gallery_name }}">
                                                @else
                                                    <img src="{{ asset('asset/clients/images/default/default_house.jpg')}}" alt="{{ $house->house_name }}">
                                                @endisset
                                            </a>
                                            @isset($item->status)
                                            <ul class="feature_text">
                                                <li class="{{ $item->status_color }}"><span>{{ $item->status }}</span></li>
                                            </ul>
                                            @endisset
                                            <div class="property-author-wrap">
                                                <h5><a href="{{ url($item->link) }}" class="property-author">
                                                    <p style="overflow: hidden;
                                                    text-overflow: ellipsis;
                                                    max-width: 75ch;">{{ $item->name }}</p>
                                                    </a>
                                                </h5>
                                                <div class="featured-price">
                                                    <p>
                                                        @if ($house->house_price<=0)
                                                            <strong class="text-danger">Thương lượng</strong>
                                                        @else
                                                            @if (strlen($house->house_price)>9)
                                                                {{ round($house->house_price/1000000000, 2).' Tỷ' }}
                                                            @elseif(strlen($house->house_price)>6)
                                                                {{ is_int(($house->house_price/1000000))?($house->house_price/1000000).' Triệu': number_format($house->house_price,0,'',',') }}
                                                            @else
                                                                {{ number_format($house->house_price,0,'',',') }}
                                                            @endif
                                                            <small class="text-danger">vnđ</small>
                                                        @endif
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @empty

                                    @endforelse
                                </div>
                                <div class="slider-btn v3 single-featured-next"><i class="lnr lnr-arrow-right"></i></div>
                                <div class="slider-btn v3 single-featured-prev"><i class="lnr lnr-arrow-left"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Listing Details Info ends-->
    <!--Similar Listing starts-->
    @if($vieweds!=null&&$vieweds->count()>0)
    <div class="similar-listing-wrap pb-70 mt-70">
        <div class="container">
            <div class="col-md-12 px-0">
                <div class="similar-listing">
                    <div class="section-title v2">
                        <h2>Danh sách đã xem</h2>
                    </div>
                    <div class="swiper-container similar-list-wrap">
                        <div class="swiper-wrapper">
                            @php
                                $idx = 1;
                            @endphp
                            @forelse ($vieweds as $item)

                            <div class="swiper-slide">
                                <div class="single-property-box">
                                    <div class="property-item">
                                        <a class="property-img" href="{{ url($item->link) }}">
                                            @isset($item->image)
                                                <img src="{{ asset('storage/'.$item->image->house_gallery_link)}}" alt="{{ $item->image->house_gallery_name }}">
                                            @else
                                                <img src="{{ asset('asset/clients/images/default/default_house.jpg')}}" alt="{{ $house->house_name }}">
                                            @endisset
                                        </a>

                                        @isset($item->status)
                                        <ul class="feature_text">
                                            <li class="{{ $item->status_color }}"><span>{{ $item->status }}</span></li>
                                        </ul>
                                        @endisset
                                        <div class="property-author-wrap">
                                            {{-- <a href="#" class="property-author">
                                                <img src="images/agents/agent_min_1.jpg" alt="...">
                                                <span>Tony Stark</span>
                                            </a> --}}
                                            <ul class="save-btn">
                                                <li data-toggle="tooltip" data-placement="top" title="Hình Ảnh"><a href=".photo-gallery{{ $idx }}" class="btn-gallery"><i class="lnr lnr-camera"></i></a></li>
                                                {{-- <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                                                <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li> --}}
                                            </ul>
                                            <div class="hidden photo-gallery{{ $idx }}">
                                                @forelse ($item->galleries as $photo)
                                                <a href="{{ asset('storage/'.$photo->house_gallery_link) }}"></a>
                                                @empty

                                                @endforelse
                                            </div>
                                        </div>
                                    </div>
                                    <div class="property-title-box">
                                        <h6 class="text-limit"><a href="{{ $item->link }}">{{ $item->title }}</a></h6>
                                        <div class="property-location">
                                            <i class="fa fa-map-marker-alt"></i>
                                            <p>{{ $item->address }}</p>
                                        </div>
                                        <ul class="property-feature">
                                            @isset($item->acreage)
                                            <li> <i class="fas fa-arrows-alt"></i>
                                                <span>{{ $item->acreage }}</span>
                                            </li>
                                            @endisset
                                            @isset($item->block)
                                                <li> <i class="fas fa-building"></i>
                                                    <span>{{ $item->block }}</span>
                                                </li>
                                            @endisset
                                            @isset($item->floor)
                                                <li> <i class="fas fa-walking"></i>
                                                    <span>{{ $item->floor }}</span>
                                                </li>
                                            @endisset
                                            @isset($item->house)
                                                <li> <i class="fas fa-home"></i>
                                                    <span>{{ $item->house }}</span>
                                                </li>
                                            @endisset
                                            @isset($item->time)
                                                <li> <i class="fas fa-calendar-alt"></i>
                                                    <span>{{ date('d/m/Y',strtotime($item->time)) }}</span>
                                                </li>
                                            @endisset
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            @php
                                $idx++;
                            @endphp
                            @empty

                            @endforelse
                        </div>
                    </div>
                    <div class="slider-btn v2 similar-next"><i class="lnr lnr-arrow-right"></i></div>
                    <div class="slider-btn v2 similar-prev"><i class="lnr lnr-arrow-left"></i></div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <!--Similar Listing ends-->
</div>
<!--Agent Chat box starts-->
{{-- <div class="chatbox-wrapper">
    <div class="chat-popup chat-bounce" data-toggle="tooltip" title="Have any query? Ask Me !" data-placement="top">
        <i class="fas fa-comment-alt"></i>
    </div>
    <div class="agent-chat-form v1">
        <div class="container">
            <div class="row">
                <div class="col-4">
                    <img class="agency-chat-img" src="{{ asset('asset/clients/images/agents/agent_min_1.jpg')}}" alt="...">
                </div>
                <div class="col-8 pl-0">
                    <ul class="agent-chat-info">
                        <li><i class="fas fa-user"></i>Tony Stark</li>
                        <li><i class="fas fa-phone-alt"></i>+440 3456 345</li>
                        <li><a href="single-agent.html">View Listings</a></li>
                    </ul>
                    <span class="chat-close"><i class="lnr lnr-cross"></i></span>
                </div>
            </div>
            <div class="row mt-1">
                <div class="col-md-12">
                    <form action="#" method="POST">
                        <div class="chat-group mt-1">
                            <input class="chat-field" type="text" name="chat-name" id="chat-name" placeholder="Your name">
                        </div>
                        <div class="chat-group mt-1">
                            <input class="chat-field" type="text" name="chat-phone" id="chat-phone" placeholder="Phone">
                        </div>
                        <div class="chat-group mt-1">
                            <input class="chat-field" type="text" name="chat-email" id="chat-email" placeholder="Email">
                        </div>
                        <div class="chat-group mt-1">
                            <textarea class="form-control chat-msg" name="message" rows="4" placeholder="Description">Hello, I am interested in [Luxury apartment bay view]</textarea>
                        </div>
                        <div class="chat-button mt-3">
                            <a class="chat-btn" data-toggle="modal" data-target="#mortgage_result">Send Message</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
<!--Agent Chat box ends-->

@stop

@section('javascript')

<script src="{{ asset('asset/clients/js/validate.min.js')}}"></script>
<script>
    $( document ).ready( function () {
        // initialSetup();
        $( "form[name=rating-form]" ).validate({
            rules: {
                rating_name: {
                    required: true,
                    maxlength: 30
                },
                rating_mail: {
                    required: true,
                    email: true,
                    maxlength: 50
                },
                rating_comment: {
                    required: true,
                    maxlength: 500
                }
            },
            messages: {
                rating_name:  {
                    required: "Hãy nhập vào họ tên.",
                    maxlength: "Bạn chỉ được nhập tối đa 30 ký tự."
                },
                rating_mail:  {
                    required: "Hãy nhập vào email.",
                    email: "Hãy nhập vào chính xác địa chỉ email.",
                    maxlength: "Bạn chỉ được nhập tối đa 50 ký tự."
                },
                rating_comment:  {
                    required: "Hãy nhập vào Nội Dung.",
                    maxlength: "Bạn chỉ được nhập tối đa 500 ký tự."
                },
            },
            errorElement: "span",
            errorPlacement: function ( error, element ) {
                // Add the `invalid-feedback` class to the error element
                error.addClass("form-text").addClass( "text-danger" );
                error.insertAfter( element );
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
            }
        });
    });

</script>

@stop
