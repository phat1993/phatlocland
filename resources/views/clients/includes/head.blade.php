<!-- Links -->
{{-- <link rel="icon" type="icon/shorticon" href="{{ asset('asset/clients/image/png')}}" /> --}}
<link rel="icon" href="{{ asset('asset/clients/images/logo-blue.png') }}" type="image/x-icon" />
<!-- google fonts-->
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;display=swap" rel="stylesheet">
<!-- Plugins CSS -->
<link href="{{ asset('asset/clients/css/plugin.css')}}" rel="stylesheet" />
<!-- style CSS -->
<link href="{{ asset('asset/clients/css/style.css')}}" rel="stylesheet" />
