{{-- /*
1. Jquery min js
2. Popper min js
3. Jquery UI js
4. Bootstrap min js
5. Counting Up js
6. Appear js
7. Bootstrap select js
8. Swiper js
9. jQuery ytplayer js
10.Magnific Popup js
11.Bootstrap Datepicker js
12.Preloadinator js
13.bootstrap validator js
14.Validator js

*/ --}}

    <!--plugin js-->
    <script src="{{ asset('asset/clients/js/jqurery-v3.3.1.js')}}"></script>
    <script src="{{ asset('asset/clients/js/popper.js')}}"></script>
    <script src="{{ asset('asset/clients/js/jqurery-ui.js')}}"></script>
    <script src="{{ asset('asset/clients/js/bootstrap.js')}}"></script>
    <script src="{{ asset('asset/clients/js/couting-up.js')}}"></script>
    <script src="{{ asset('asset/clients/js/appear.js')}}"></script>
    <script src="{{ asset('asset/clients/js/sticky-sidebar.js')}}"></script>
    <script src="{{ asset('asset/clients/js/bootstrap-select.js')}}"></script>
    <script src="{{ asset('asset/clients/js/swiper.min.js')}}"></script>
    <script src="{{ asset('asset/clients/js/magnific-popup.js')}}"></script>
    <script src="{{ asset('asset/clients/js/ytplayer.js')}}"></script>
    <script src="{{ asset('asset/clients/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{ asset('asset/clients/js/preloadinator.js')}}"></script>
    <script src="{{ asset('asset/clients/js/wow.js')}}"></script>
    <!--google maps-->
    {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_8C7p0Ws2gUu7wo0b6pK9Qu7LuzX2iWY&amp;libraries=places&amp;callback=initAutocomplete"></script> --}}
    <!--Main js-->
    <script src="{{ asset('asset/clients/js/main.js')}}"></script>
    <!--Scripts ends-->


