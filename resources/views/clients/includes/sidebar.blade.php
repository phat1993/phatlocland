
<div class="site-navbar-wrap v2 style2">
    <div class="container">
        <div class="site-navbar">
            <div class="row align-items-center">
                <div class="col-lg-2 col-md-6 col-9 order-2 order-xl-1 order-lg-1 order-md-1">
                    <a class="navbar-brand" href="{{ route('clients.index') }}"><img src="{{ asset('asset/clients/images/logo-blue-head.png') }}" style="width: 116px;height:50px" alt="Phat Loc Land" class="img-fluid"></a>
                </div>
                <div class="col-lg-6 col-md-1 col-3  order-3 order-xl-2 order-lg-2 order-md-3 pl-xs-0">
                    <nav class="site-navigation text-right">
                        <div class="container">
                            <ul class="site-menu js-clone-nav d-none d-lg-block" id="mainMenu">
                                @forelse ($menuTops as $menuLv1)
                                    @if ($menuLv1['has_child1']==true)
                                        <li class="has-children">
                                            <a href="{{ url($menuLv1['menu_url']) }}">{{ $menuLv1['menu_name'] }}</a>
                                            <ul class="dropdown @if ($menuLv1['has_child2']==false) {{ __('sub-menu') }} @endif">
                                                @forelse ($menuLv1['menu_list'] as $menuLv2)
                                                    @if ($menuLv2['has_child']==true)
                                                        <li class="has-children">
                                                            <a href="{{ url($menuLv2['menu_url']) }}">{{ $menuLv2['menu_name'] }}</a>
                                                            <ul class="dropdown sub-menu">
                                                                @forelse ($menuLv2['menu_list'] as $menuLv3)
                                                                    <li>
                                                                        <a href="{{ url($menuLv3['menu_url']) }}">{{ $menuLv3['menu_name'] }}</a>
                                                                    </li>
                                                                @empty
                                                                @endforelse
                                                            </ul>
                                                        </li>
                                                    @else
                                                        <li>
                                                            <a href="{{ url($menuLv2['menu_url']) }}">{{ $menuLv2['menu_name'] }}</a>
                                                        </li>
                                                    @endif
                                                @empty
                                                @endforelse
                                            </ul>
                                        </li>
                                    @else
                                        <li class="has-children">
                                            <a href="{{ url($menuLv1['menu_url']) }}">{{ $menuLv1['menu_name'] }}</a>
                                        </li>
                                    @endif
                                @empty
                                @endforelse
                            </ul>
                        </div>
                    </nav>
                    <div class="d-lg-none sm-right">
                        <a href="#" class="mobile-bar js-menu-toggle">
                            <span class="lnr lnr-menu"></span>
                        </a>
                    </div>
                    <!--mobile-menu starts -->
                    <div class="site-mobile-menu">
                        <div class="site-mobile-menu-header">
                            <div class="site-mobile-menu-close  js-menu-toggle">
                                <span class="lnr lnr-cross"></span> </div>
                        </div>
                        <div class="site-mobile-menu-body"></div>
                    </div>
                    <!--mobile-menu ends-->
                </div>
                <div class="col-lg-4 col-md-5 col-12 order-1 order-xl-3 order-lg-3 order-md-2 text-right pr-xs-0">
                    {{-- <div class="menu-btn">
                        <ul class="user-btn">
                            <li><a href="#"><i class="fas fa-arrows-alt-h"></i></a>
                                <span>0</span>
                            </li>
                            <li><a href="#"><i class="lnr lnr-heart"></i></a>
                                <span>0</span>
                            </li>
                            <li><a href="#" data-toggle="modal" data-target="#user-login-popup"><i class="lnr lnr-user"></i></a>
                            </li>
                        </ul>
                        <div class="add-list float-right ml-3">
                            <a class="btn v6" href="#"><i class="lnr lnr-home"></i>Thêm yêu yêu thích</a>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>
