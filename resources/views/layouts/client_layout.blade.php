<!DOCTYPE html>
<html dir="ltr" lang="vi">
<head>
    <!-- Metas -->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Document Title -->
    <title>PHÁT LỘC REAL LAND - @yield('title')</title>
    @include('clients.includes.head')
    @yield('styles')
</head>

<body>
    <!--Preloader starts-->
    {{-- <div class="preloader js-preloader">
        <img src="{{ asset('asset/clients/images/preloader.gif') }}" alt="...">
    </div> --}}
    @include('clients.includes.header')
    <!--Preloader ends-->
    <!--Page Wrapper starts-->
    <div class="page-wrapper">
        <!--header starts-->
        <header class="header transparent scroll-hide">
            <!--Main Menu starts-->
            @include('clients.includes.sidebar')
            <!--Main Menu ends-->
        </header>
        <!--Header ends-->
        @yield('content')
        <!-- Scroll to top starts-->
        <span class="scrolltotop"><i class="lnr lnr-arrow-up"></i></span>
        <!-- Scroll to top ends-->
    </div>
    <!--Page Wrapper ends-->
    <!--Footer Starts-->

    <div class="footer-wrapper v1">

        <div class="footer-top-area">
            <div class="container">
                <div class="row nav-folderized">
                    <div class="col-lg-4 col-md-12">
                        <div class="footer-logo">
                            <a href="{{ route('clients.index') }}"> <img src="{{ asset('asset/clients/images/logo-blue.png') }}" alt="Phat Loc Land"></a>
                            <div class="company-desc">
                                <p>
                                    {{ $webInfo->web_info_special_note }}
                                </p>
                                <ul class="list footer-list mt-20">
                                    <li>
                                        <div class="contact-info">
                                            <div class="icon">
                                                <i class="fa fa-map-marker-alt"></i>
                                            </div>
                                            <div class="text">{{ $webInfo->web_info_address }}</div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="contact-info">
                                            <div class="icon">
                                                <i class="fas fa-envelope"></i>
                                            </div>
                                            <div class="text"><a href="mailto:{{ $webInfo->web_info_mail }}">{{ $webInfo->web_info_mail }}</a></div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="contact-info">
                                            <div class="icon">
                                                <i class="fas fa-phone-alt"></i>
                                            </div>
                                            <div class="text">{{ $webInfo->web_info_phone }}</div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-12 text-center sm-left">
                        <div class="footer-content nav">
                            <h2 class="title">Kinh doanh</h2>
                            <ul class="list res-list">
                                @forelse ($menuBots as $item)
                                    <li><a class="link-hov style2" href="{{ url('bat-dong-san/'.$item->web_canonical) }}">{{ $item->house_business_name }}</a></li>
                                @empty
                                @endforelse
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12 text-center sm-left">
                        <div class="footer-content nav">
                            <h2 class="title">Thông Tin</h2>
                            <ul class="list res-list">
                                <li><a class="link-hov style2" href="{{ route('clients.about') }}">Giới thiệu</a></li>
                                <li><a class="link-hov style2" href="{{ route('clients.contact') }}">Liên Hệ</a></li>
                                <li><a class="link-hov style2" href="{{ route('clients.policy') }}">Chính sách</a></li>
                                <li><a class="link-hov style2" href="{{ route('clients.advice') }}">Tư Vấn</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        <div class="footer-content">
                            <h4 class="title">Đăng ký nhận tin tức</h4>
                            <div class="value-input-wrap newsletter">
                                <form action="#">
                                    <input type="text" placeholder="Địa chỉ email của bạn ..">
                                    <button type="submit">Đăng ký</button>
                                </form>
                            </div>
                            <div class="footer-social-wrap">
                                <p>Theo dõi chúng tôi tại </p>
                                <ul class="social-buttons style2">
                                    @isset($webInfo->web_info_facebook)
                                        <li><a href="{{ $webInfo->web_info_facebook }}" target="_blank"><i class="fab fa-facebook-square" style="font-size: 3rem"></i></a></li>
                                    @endisset
                                    @isset($webInfo->web_info_instagram)
                                        <li><a href="{{ $webInfo->web_info_instagram }}" target="_blank"><i class="fab fa-instagram" style="font-size: 3rem"></i></a></li>
                                    @endisset
                                    @isset($webInfo->web_info_youtube)
                                        <li><a href="{{ $webInfo->web_info_youtube }}" target="_blank"><i class="fab fa-youtube" style="font-size: 3rem"></i></a></li>
                                    @endisset
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom-area">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-8 offset-md-2">
                        <p>
                            © PhatLocRealLand 2021. Bản quyền thuộc PhatLocRealLand. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Footer ends-->
    <!--login Modal starts-->
    <div class="modal fade" id="user-login-popup">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="lnr lnr-cross"></i></span></button>
                </div>
                <div class="modal-body">
                    <!--User Login section starts-->
                    <div class="user-login-section">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="ui-list nav nav-tabs mb-30" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#login" role="tab" aria-selected="true">Login</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#register" role="tab" aria-selected="false">Register</a>
                                        </li>
                                    </ul>
                                    <div class="login-wrapper">
                                        <div class="ui-dash tab-content">
                                            <div class="tab-pane fade show active" id="login" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <form id="login-form" action="#" method="post">
                                                            <div class="form-group">
                                                                <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="email" value="" required="">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" autocomplete="off">
                                                            </div>
                                                            <div class="row mt-20">
                                                                <div class="col-md-6 col-12 text-left">
                                                                    <div class="res-box">
                                                                        <input id="check-l" type="checkbox" name="check">
                                                                        <label for="check-l">Remember me</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-12 text-right">
                                                                    <div class="res-box sm-left">
                                                                        <a href="#" tabindex="5" class="forgot-password">Forgot Password?</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="res-box text-center mt-30">
                                                                <button type="submit" class="btn v8"><span class="lnr lnr-exit"></span> Log In</button>
                                                            </div>
                                                        </form>
                                                        <div class="social-profile-login  text-left mt-20">
                                                            <h5>or Login with</h5>
                                                            <ul class="social-btn">
                                                                <li class="bg-fb"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                                                <li class="bg-tt"><a href="#"><i class="fab fa-twitter"></i></a></li>
                                                                <li class="bg-ig"><a href="#"><i class="fab fa-instagram"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="signup-wrapper">
                                                            <img src="{{ asset('asset/clients/images/others/login-1.png') }}" alt="...">
                                                            <p>Welcome Back! Please Login to your Account for latest property listings.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="register" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <form id="register-form" action="#" method="post">
                                                            <div class="form-group">
                                                                <input type="text" name="user_name" id="user_name" tabindex="1" class="form-control" placeholder="Username" value="">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="password" name="user_password" id="user_password" tabindex="2" class="form-control" placeholder="Password" autocomplete="off">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="password" name="confirm-password" id="confirm-password" tabindex="2" class="form-control" placeholder="Confirm Password" autocomplete="off">
                                                            </div>
                                                            <div class="res-box text-left">
                                                                <input type="checkbox" tabindex="3" class="" name="remember" id="remember">
                                                                <label for="remember">I've read and accept terms &amp; conditions</label>
                                                            </div>
                                                            <div class="res-box text-center mt-30">
                                                                <button type="submit" class="btn v8"><i class="lnr lnr-enter"></i>Sign Up</button>
                                                            </div>
                                                        </form>
                                                        <div class="social-profile-login  text-left mt-20">
                                                            <h5>or Sign Up with</h5>
                                                            <ul class="social-btn">
                                                                <li class="bg-fb"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                                                <li class="bg-tt"><a href="#"><i class="fab fa-twitter"></i></a></li>
                                                                <li class="bg-ig"><a href="#"><i class="fab fa-instagram"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="signup-wrapper">
                                                            <img src="{{ asset('asset/clients/images/others/login-1.png') }}" alt="...">
                                                            <p>Create an account to find the best Property for you.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--User login section ends-->
                </div>
            </div>
        </div>
    </div>
    <!--login Modal ends-->

    <!--Scripts starts-->

    @include('clients.includes.footer')

    @yield('javascript')

    <!--Scripts ends-->

</body>
</html>
