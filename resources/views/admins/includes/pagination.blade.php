@if ($paginator->hasPages())

<div class="card-footer d-flex justify-content-center">
    <div class="dataTables_paginate">

        @if ($paginator->onFirstPage())
            <a class="paginate_button disabled" href="#" tabindex="-1">←</a>
        @else
            <a class="paginate_button" href="{{ $paginator->previousPageUrl() }}" tabindex="-1">←</a>
        @endif

        @php
            $tabIndex = -1;
        @endphp
        @foreach ($elements as $element)
            @php
                $tabIndex++;
            @endphp
            @if (is_string($element))
                <span>
                    <a class="paginate_button current">{{ $element }}</a>
                </span>
            @endif

            @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <span>
                                <a class="paginate_button current" tabindex="{{ $page }}">{{ $page }}</a>
                            </span>
                        @else
                            <span>
                                <a class="paginate_button" href="{{ $url }}" tabindex="{{ $page }}">{{ $page }}</a>
                            </span>
                        @endif
                    @endforeach
            @endif

        @endforeach


    @if ($paginator->hasMorePages())
            <a class="paginate_button" href="{{ $paginator->nextPageUrl() }}" tabindex="{{ $tabIndex }}">→</a>
    @else
            <a class="paginate_button disabled">→</a>
    @endif
    </div>
</div>

@endif

