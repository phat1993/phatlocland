
<div class="card card-sidebar-mobile" id="sidebarCollapse">
    <ul class="nav nav-sidebar" data-nav-type="accordion" id="mainMenu">

        <li class="nav-item">
            <a href="{{ route('admins.dashboard.index') }}" class="nav-link">
                <i class="icon-home4"></i>
                <span>
                    Tổng Quan
                </span>
            </a>
        </li>
        <!-- Main -->
        <li class="nav-item-header">
            <div class="text-uppercase font-size-xs line-height-xs">Thông Tin Chung</div> <i class="icon-menu" title="Cài Đặt Chung"></i></li>
        <li class="nav-item nav-item-submenu">
            <a href="#" class="nav-link"><i class="icon-stack"></i> <span>Thống Kê</span></a>

            <ul class="nav nav-group-sub" data-submenu-title="Thống Kê">
                <li class="nav-item"><a href="#" class="nav-link disabled">Theo Tuần <span class="badge bg-transparent align-self-center ml-auto">Sắp ra mắt</span></a></li>
                <li class="nav-item"><a href="#" class="nav-link disabled">Theo Tháng <span class="badge bg-transparent align-self-center ml-auto">Sắp ra mắt</span></a></li>
                <li class="nav-item"><a href="#" class="nav-link disabled">Theo Năm <span class="badge bg-transparent align-self-center ml-auto">Sắp ra mắt</span></a></li>
                <li class="nav-item"><a href="#" class="nav-link disabled">Theo Thu Chi <span class="badge bg-transparent align-self-center ml-auto">Sắp ra mắt</span></a></li>
                <li class="nav-item"><a href="#" class="nav-link disabled">Theo Lượt Xem<span class="badge bg-transparent align-self-center ml-auto">Sắp ra mắt</span></a></li>
            </ul>
        </li>
        {{-- <li class="nav-item">
            <a href="changelog.html" class="nav-link">
                <i class="icon-list-unordered"></i>
                <span>Changelog</span>
                <span class="badge bg-blue-400 align-self-center ml-auto">2.3</span>
            </a>
        </li> --}}

        <li class="nav-item nav-item-submenu">
            <a href="#" class="nav-link"><i class="icon-cog3"></i> <span>Cài Đặt Chung</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="Cài Đặt Chung">

                <li class="nav-item"><a href="{{ route('admins.webmenu.index') }}" class="nav-link"><i class="icon-menu6"></i> <span>Danh Mục Web</span></a></li>
                <li class="nav-item"><a href="#" class="nav-link disabled"><i class="icon-versions"></i> <span>Băng Rôn</span> <span class="badge bg-transparent align-self-center ml-auto">Sắp ra mắt</span></a></li>
                <li class="nav-item"><a href="{{ route('admins.webinfo.index') }}" class="nav-link"><i class="icon-sphere"></i>Thông Tin Web</a></li>
                <li class="nav-item"><a href="{{ route('admins.webreview.index') }}" class="nav-link"><i class="icon-stack-star"></i>Đánh Giá Web</a></li>
                <li class="nav-item"><a href="{{ route('admins.webpolicy.index') }}" class="nav-link"><i class="icon-certificate"></i>Chính Sách Web</a></li>

            </ul>
        </li> <!-- /main -->
        <li class="nav-item nav-item-submenu">
            <a href="#" class="nav-link"><i class="icon-cog3"></i> <span>Thư Viện</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="Thư Viện">
                <li class="nav-item"><a href="{{ route('admins.mediasupport.index') }}" class="nav-link"><i class="icon-folder5"></i> Thư Viện </a></li>
                <li class="nav-item"><a href="{{ route('admins.mediasupport.ckfinder') }}" target="_blank" class="nav-link"><i class="icon-images3"></i>CKFinder</a></li>

            </ul>
        </li> <!-- /main -->
        <li class="nav-item">
            <a href="{{ route('admins.webfeedback.index') }}" class="nav-link">
                <i class="icon-comment-discussion"></i>
                <span>Góp Ý</span>
                <span class="badge bg-danger-400 align-self-center ml-auto" id="feedBackCount">0</span>
            </a>
        </li>
        <!-- Forms -->
        <li class="nav-item-header">
            <div class="text-uppercase font-size-xs line-height-xs">Bất Động Sản</div> <i class="icon-menu" title="Bất Động Sản"></i></li>
        <li class="nav-item nav-item-submenu">
            <a href="#" class="nav-link"><i class="icon-city"></i> <span>Dự Án</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="Dự Án">
                <li class="nav-item"><a href="{{ route('admins.procategory.index') }}" class="nav-link"><i class="icon-menu6"></i> Danh mục</a></li>
                <li class="nav-item"><a href="{{ route('admins.project.index') }}" class="nav-link"><i class="icon-list-numbered"></i> Danh sách</a></li>
                <li class="nav-item"><a href="{{ route('admins.project.create') }}" class="nav-link"><i class="icon-plus-circle2"></i> Tạo Mới</a></li>
            </ul>
        </li>
        <li class="nav-item nav-item-submenu">
            <a href="#" class="nav-link"><i class="icon-office"></i> <span>Bất Động Sản</span></a>
            <ul class="nav nav-group-sub" data-submenu-title="Bất Động Sản">
                <li class="nav-item"><a href="{{ route('admins.houbusiness.index') }}" class="nav-link"><i class="icon-cash"></i> Kinh doanh</a></li>
                <li class="nav-item"><a href="{{ route('admins.houcategory.index') }}" class="nav-link"><i class="icon-menu6"></i> Danh mục</a></li>
                <li class="nav-item"><a href="{{ route('admins.house.index') }}" class="nav-link"><i class="icon-list-numbered"></i> Danh sách</a></li>
                <li class="nav-item"><a href="{{ route('admins.house.create') }}" class="nav-link"><i class="icon-plus-circle2"></i> Tạo Mới</a></li>
                {{-- <li class="nav-item"><a href="{{ route('admins.housereview.index') }}" class="nav-link"><i class="icon-stars"></i> Đánh giá</a></li> --}}

            </ul>
        </li>

        <li class="nav-item"><a href="{{ route('admins.amenity.index') }}" class="nav-link"><i class="icon-cup2"></i>Tiện Nghi</a></li>
        <li class="nav-item">
            <a href="{{ route('admins.housereview.index') }}" class="nav-link">
                <i class="icon-stars"></i>
                <span>Đánh giá</span>
                <span class="badge bg-danger-400 align-self-center ml-auto" id="houseReviewCount">0</span>
            </a>
        </li>
        <!-- /forms -->

        <!-- Forms -->
        <li class="nav-item-header">
            <div class="text-uppercase font-size-xs line-height-xs">Nhân Lực</div> <i class="icon-menu" title="Nhân Lực"></i></li>
        <li class="nav-item"><a href="{{ route('admins.webhire.index') }}" class="nav-link"><i class="icon-user-tie"></i> Tuyển Dụng </a></li>
        <!-- /forms -->
    </ul>
</div>
