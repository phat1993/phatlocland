
    <!-- Global stylesheets -->
    <link rel="icon" href="{{ asset('asset/clients/images/logo-blue.png') }}" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/admins/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/admins/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/admins/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/admins/css/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/admins/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/admins/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
