
    <!-- Core JS files -->
    <script src="{{ asset('asset/admins/js/main/jquery.min.js') }}"></script>
    <script src="{{ asset('asset/admins/js/main/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('asset/admins/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <script>
        $.ajax({
            contentType: "application/json",
            dataType: "json",
            type: "GET",
            url: "{{route('admins.housereview.count')}}",
            success: function(rdata){
                $('#houseReviewCount').empty().append(rdata);
            }
        })
        $.ajax({
            contentType: "application/json",
            dataType: "json",
            type: "GET",
            url: "{{route('admins.webfeedback.count')}}",
            success: function(rdata){
                $('#feedBackCount').empty().append(rdata);
            }
        })


    </script>
    <!-- Theme JS files -->
    {{-- <script src="{{ asset('asset/admins/js/plugins/visualization/d3/d3.min.js') }}"></script>
    <script src="{{ asset('asset/admins/js/plugins/visualization/d3/d3_tooltip.js') }}"></script>
    <script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script src="{{ asset('asset/admins/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('asset/admins/js/plugins/pickers/daterangepicker.js') }}"></script>

    <script src="{{ asset('asset/admins/js/pages/dashboard.js') }}"></script>
    <script src="{{ asset('asset/admins/js/charts/pages/dashboard/light/streamgraph.js') }}"></script>
    <script src="{{ asset('asset/admins/js/charts/pages/dashboard/light/sparklines.js') }}"></script>
    <script src="{{ asset('asset/admins/js/charts/pages/dashboard/light/lines.js') }}"></script>
    <script src="{{ asset('asset/admins/js/charts/pages/dashboard/light/areas.js') }}"></script>
    <script src="{{ asset('asset/admins/js/charts/pages/dashboard/light/donuts.js') }}"></script>
    <script src="{{ asset('asset/admins/js/charts/pages/dashboard/light/bars.js') }}"></script>
    <script src="{{ asset('asset/admins/js/charts/pages/dashboard/light/progress.js') }}"></script>
    <script src="{{ asset('asset/admins/js/charts/pages/dashboard/light/heatmaps.js') }}"></script>
    <script src="{{ asset('asset/admins/js/charts/pages/dashboard/light/pies.js') }}"></script>
    <script src="{{ asset('asset/admins/js/charts/pages/dashboard/light/bullets.js') }}"></script> --}}
    <!-- /theme JS files -->
