@extends('layouts.admin_layout')
@section('title', 'Tiện Nghi')
@section('styles')
<!-- Plugins CSS -->
<link href="{{ asset('asset/clients/css/font-awesome.min.css')}}" rel="stylesheet" />
<link href="{{ asset('asset/clients/css/linearicon.min.css')}}" rel="stylesheet" />
@endsection
@section('topnavdetail')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Tiện Nghi</span> - Cập Nhật</h4>
        <a href="{{ route('admins.project.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.project.index') }}" class="breadcrumb-item"><i class="icon-city mr-2"></i> Dự Án</a>
        <a href="{{ route('admins.amenity.index') }}" class="breadcrumb-item">Tiện Nghi</a>
        <span class="breadcrumb-item active">Cập Nhật</span>
    </div>
    <a href="{{ route('admins.project.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
        <h5>{{session('success')}}</h5>
        <br>
        <a href="{{ route('admins.amenity.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
    </div>
    @endif

    @if(count($errors) > 0)
         <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
            @foreach($errors->all() as $error)
            <h5>{{$error}}</h5>
            @endforeach
            <br>
            <a href="{{ route('admins.amenity.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
        </div>
    @endif
    <!-- /Notifications Set  -->
    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Cập Nhật</strong>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="reload"></a>
                </div>
            </div>
        </div>

        <div class="card-body">

            <form action="{{ route('admins.amenity.update') }}" method="POST" name="amenity-form">
                {{csrf_field()}}
                <div class="form-group">
                    <input type="hidden" class="form-control" name="amenity_id" value="{{$amenity->amenity_id}}">
                </div>
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Thông Tin Cơ Bản</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="amenity_icon">Icon<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào ký hiệu icon"  id="amenity_icon" name="amenity_icon" value="{{ old('amenity_icon', $amenity->amenity_icon) }}">
                            <div class="card card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <div style="width: 100px;height:100px" class="border border-warning p-2 text-center">
                                            <i id="viewIcon" class="{{ old('amenity_icon', $amenity->amenity_icon) }}" style="font-size: 5rem"></i>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <span>Tham khảo:</span><br>
                                        <a href="https://fontawesome.com" target="_blank">fontawesome</a><kbd><kbd>fas fa-user </kbd> = <kbd><i class="fas fa-user"></i></kbd></kbd><br>
                                        <a href="https://linearicons.com/free" target="_blank">linearicons</a> <kbd><kbd>lnr lnr-user </kbd> = <kbd><i class="lnr lnr-user"></i></kbd></kbd>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="amenity_name">Tiêu đề<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào tiêu đề tiện nghi"  id="amenity_name" name="amenity_name" value="{{ old('amenity_name', $amenity->amenity_name) }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="amenity_detail">Mô tả:</label>
                        <div class="col-lg-10">
                            <textarea rows="3" cols="3" class="form-control" maxlength="200" id="amenity_detail" name="amenity_detail" placeholder="Nhập vào mô tả">{{ old('amenity_detail', $amenity->amenity_detail) }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="active_status">Trạng Thái:</label>
                        <div class="col-lg-10">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" id="active_status" name="active_status" class="form-check-input-styled-primary" value="1"
                                    @if(old('active_status', $amenity->active_status)!=null && old('active_status', $amenity->active_status) == 1)
                                        checked
                                    @endif data-fouc>
                                    Kích Hoạt
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>

                <div class="text-center">
                    <button type="submit" class="btn btn-primary"><i class="icon-download4 mr-2"> Lưu Lại</i></button>
                    <a href="{{ route('admins.amenity.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở Về Danh Sách</a>
                </div>
            </form>
        </div>
    </div>
    <!-- /restore column visibility -->

@stop

@section('javascript')

	<!-- Theme JS files -->
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tagsinput.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tokenfield.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/ui/prism.min.js')}}"></script>

	<script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
	<script src="{{ asset('asset/admins/js/plugins/forms/styling/switch.min.js')}}"></script>


    <script src="{{ asset('asset/admins/js/plugins/forms/validation/validate.min.js') }}"></script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_tags_input.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_checkboxes_radios.js')}}"></script>
	<!-- /theme JS files -->


    <script>

        $("input[name='amenity_icon']").on({
            keyup: function() {
                var input_val = $(this).val();

                // don't validate empty input
                if (input_val === "") { return; }

                $("#viewIcon").removeAttr('class');
                $("#viewIcon").addClass(input_val);

            }
        });


        function initialSetup() {
           $hidden = $(".notifyHidden");
            if ($hidden != null) {
                $hidden.delay(10000).fadeOut('slow');
            }
        }
        initialSetup();


        $( document ).ready( function () {
        $( "form[name=amenity-form]" ).validate( {
            rules: {
                amenity_icon: {
                    required: true,
                    maxlength: 100
                },
                amenity_name: {
                    required: true,
                    maxlength: 100
                }
            },
            messages: {
                amenity_icon: {
                    required: "Hãy nhập vào tiêu đề.",
                    maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                },
                amenity_name: {
                    required: "Hãy nhập vào tiêu đề.",
                    maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                },
            },
            errorElement: "span",
            errorPlacement: function ( error, element ) {
                // Add the `invalid-feedback` class to the error element
                error.addClass("form-text").addClass( "text-danger" );

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.next( "label" ) );
                } else {
                    error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
            }
        } );

    } );
    </script>
@stop
