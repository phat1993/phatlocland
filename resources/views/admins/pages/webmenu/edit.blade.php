@extends('layouts.admin_layout')
@section('title', 'Danh mục website')
@section('topnavdetail')
<div class="page-title d-flex">
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Danh Mục Website</span> - Cập nhật</h4>
    <a href="#{{ route('admins.webmenu.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
</div>
@endsection
@section('topnavigation')
<div class="breadcrumb">
    <a href="{{ route('admins.webmenu.index') }}" class="breadcrumb-item"><i class="icon-menu6 mr-2"></i> Danh Mục Website</a>
    <span class="breadcrumb-item active">Cập nhật</span>
</div>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
        <h5>{{session('success')}}</h5>
        <br>
        <a href="{{ route('admins.webmenu.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
    </div>
    @endif

    @if(count($errors) > 0)
         <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
            @foreach($errors->all() as $error)
            <h5>{{$error}}</h5>
            @endforeach
            <br>
            <a href="{{ route('admins.webmenu.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
        </div>
    @endif
    <!-- /Notifications Set  -->
    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Cập Nhật</strong>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="reload"></a>
                </div>
            </div>
        </div>

        <div class="card-body">

            <form action="{{ route('admins.webmenu.update') }}" method="POST" name="web-menu-form">
                {{csrf_field()}}
                <fieldset class="mb-3">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="menu_id" value="{{$webMenu->menu_id}}">
                    </div>
                    <legend class="text-uppercase font-size-sm font-weight-bold">Thông Tin Cơ Bản</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="menu_name">Tên Danh Mục<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào tên danh mục"  id="menu_name" name="menu_name" value="{{ old('menu_name', $webMenu->menu_name) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="menu_lv1">Danh Mục Cấp 1:</label>
                        <div class="col-lg-10">
                            <select data-placeholder="Chọn danh mục cấp 1" onchange="changeType()" class="form-control select-search" id="menu_lv1" name="menu_lv1" data-fouc>
                                <optgroup label="Danh Mục Cấp 1">
                                    <option value="" selected>Để Trống</option>
                                    @foreach ($menuA as $item)
                                        <option value="{{ $item->menu_id }}" @if ($item->menu_id == $webMenu->menu_lv1) selected="selected" @endif>{{ $item->menu_name }}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="menu_lv2">Danh Mục Cấp 2:</label>
                        <div class="col-lg-10">
                            <select data-placeholder="Chọn danh mục cấp 2" class="form-control select-search" id="menu_lv2" name="menu_lv2" disabled="disabled" data-fouc>
                                <optgroup label="Danh Mục Cấp 2">
                                   {{--  <option value="" selected>Để Trống</option>
                                    <option value="AZ">Arizona</option>
                                    <option value="CO">Colorado</option>
                                    <option value="ID">Idaho</option>
                                    <option value="WY">Wyoming</option> --}}
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="menu_order">Thứ Tự<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="number" class="form-control"  maxlength="3" placeholder="Nhập vào thứ tự"  id="menu_order" name="menu_order" value="{{ old('menu_order', $webMenu->menu_order) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="menu_url">Đường Dẫn<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào đường dẫn"  id="menu_url" name="menu_url" value="{{ old('menu_url', $webMenu->menu_url) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="active_status">Trạng Thái:</label>
                        <div class="col-lg-10">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" id="active_status" name="active_status" class="form-check-input-styled-primary"
                                    @if(old('active_status', $webMenu->active_status)!=null && old('active_status', $webMenu->active_status) == 1)
                                        checked
                                    @endif data-fouc>
                                    Kích Hoạt
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>

                <div class="text-center">
                    <button type="submit" class="btn btn-primary"><i class="icon-download4 mr-2"> Lưu Lại</i></button>
                    <a href="{{ route('admins.webmenu.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở Về Danh Sách</a>
                </div>
            </form>
        </div>
    </div>
    <!-- /restore column visibility -->

@stop

@section('javascript')

	<!-- Theme JS files -->
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tagsinput.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tokenfield.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/ui/prism.min.js')}}"></script>

	<script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
	<script src="{{ asset('asset/admins/js/plugins/forms/styling/switch.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/selects/select2.min.js')}}"></script>

    <script src="{{ asset('asset/admins/js/plugins/forms/validation/validate.min.js') }}"></script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_tags_input.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_checkboxes_radios.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_select2.js')}}"></script>
	<!-- /theme JS files -->


    <script>
        function initialSetup() {
           $hidden = $(".notifyHidden");
            if ($hidden != null) {
                $hidden.delay(10000).fadeOut('slow');
            }
        }

        function changeType() {
            var proType = $("#menu_lv1").children().children("option:selected").val();
            if(proType == '')
            {
                $("select[name='menu_lv2']").attr('disabled','disabled').empty();
            }else{
                $.ajax({
                    contentType: "application/json",
                    dataType: "json",
                    type: "GET",
                    url: "/admins/web-menu/change/{{$webMenu->menu_id}}/"+proType,
                    success: function(rdata){
                        var selectElment = $("select[name='menu_lv2']");
                        selectElment.removeAttr('disabled','disabled');
                        selectElment.empty().append('<optgroup label="Danh Mục Cấp 2"></optgroup>');
                        selectElment.children().append($('<option>', {value: "",text : "Để Trống"}));
                        $.each(rdata, function(i,item){
                            if(item['menu_id']=='{{$webMenu->menu_lv2}}'){
                                selectElment.children().append($('<option>', {value: item['menu_id'], selected: true, text : item['menu_name']}));
                            }
                            else{
                                selectElment.children().append($('<option>', {value: item['menu_id'],text : item['menu_name']}));
                            }
                        });
                    }
                })
            }
        }

        $( document ).ready( function () {

            initialSetup();
            changeType();

            $( "form[name=web-menu-form]" ).validate( {
                rules: {
                    menu_name: {
                        required: true,
                        maxlength: 100
                    },
                    menu_order: {
                        required: true,
                        maxlength: 2
                    },
                    menu_url: {
                        required: true,
                        maxlength: 300
                    }
                },
                messages: {
                    menu_name:  {
                        required: "Hãy nhập vào tên danh mục.",
                        maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    },
                    menu_order:  {
                        required: "Hãy nhập vào thứ tự.",
                        maxlength: "Bạn chỉ được nhập tối đa 2 ký tự."
                    },
                    menu_url:  {
                        required: "Hãy nhập vào đường dẫn.",
                        maxlength: "Bạn chỉ được nhập tối đa 300 ký tự."
                    },
                },
                errorElement: "span",
                errorPlacement: function ( error, element ) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("form-text").addClass( "text-danger" );

                    if ( element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( element.next( "label" ) );
                    } else {
                        error.insertAfter( element );
                    }
                },
                highlight: function ( element, errorClass, validClass ) {
                    $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
                },
                unhighlight: function (element, errorClass, validClass) {
                    $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
                }
            } );

        } );
    </script>
@stop
