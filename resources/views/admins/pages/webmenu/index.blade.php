@extends('layouts.admin_layout')
@section('title', 'Danh mục website')

@section('topnavdetail')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Danh Mục Website</span> - Danh sách</h4>
        <a href="{{ route('admins.webmenu.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.webmenu.index') }}" class="breadcrumb-item"><i class="icon-menu6 mr-2"></i> Danh Mục Website</a>
        <span class="breadcrumb-item active">Danh sách</span>
    </div>
    <a href="{{ route('admins.webmenu.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h6 class="alert-heading font-weight-semibold mb-1">Thông Báo</h6>
        <span>{{session('success')}}</span>
    </div>
    @endif

    @if(count($errors) > 0)
        <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h6 class="alert-heading font-weight-semibold mb-1">Thông Báo</h6>
            @foreach($errors->all() as $error)
            <p>{{$error}}</p>
            @endforeach
        </div>
    @endif
    <!-- /Notifications Set  -->

    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Tìm Kiếm</strong>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form action="{{route('admins.webmenu.index')}}" method="get">
                {{csrf_field()}}
                <div class="form-group">
                    <div class="row align-items-center">
                        <div class="col-sm-2">
                            <select data-placeholder="Chọn danh mục cấp 1" onchange="changeType()" class="form-control select-search" id="menu_lv1" name="menu_lv1" data-fouc>
                                <optgroup label="Danh Mục Cấp 1">
                                    <option value="" selected>Để Trống</option>
                                    @foreach ($menuA as $item)
                                        <option value="{{ $item->menu_id }}" {{ $item->menu_id == Request::get('menu_lv1')?'selected':'' }}>{{ $item->menu_name }}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <select data-placeholder="Chọn danh mục cấp 2" class="form-control select-search" id="menu_lv2" name="menu_lv2" disabled="disabled" data-fouc>
                                <optgroup label="Danh Mục Cấp 2">
                                   {{--  <option value="" selected>Để Trống</option>
                                    <option value="AZ">Arizona</option>
                                    <option value="CO">Colorado</option>
                                    <option value="ID">Idaho</option>
                                    <option value="WY">Wyoming</option> --}}
                                </optgroup>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="menu_name" value="{{Request::get('menu_name')}}" placeholder="Tìm kiếm theo danh mục cấp 3">
                        </div>
                        <div class="col-sm-1">
                            <input type="number" class="form-control" name="menu_order" value="{{Request::get('menu_order')}}" placeholder="Thứ tự">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="menu_url" value="{{Request::get('menu_url')}}" placeholder="Tìm kiếm theo đường dẫn">
                        </div>
                        <div class="col-sm-1">
                            <select class="form-control" name='active_status'>
                                <option value="0" {{Request::get('active_status') == 0 ? 'selected': ''}}>Kích hoạt</option>
                                <option value="1" {{Request::get('active_status') == 1 ? 'selected': ''}}>Bật</option>
                                <option value="2" {{Request::get('active_status') == 2 ? 'selected': ''}}>Tắt</option>
                            </select>
                        </div>
                        <div class="col-sm-1">
                            <input type="submit" class="btn btn-primary btn-lg active" value="Tìm Kiếm">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /restore column visibility -->
    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Danh Sách</strong>
                <a href="{{ route('admins.webmenu.create') }}" class="btn btn-outline-danger"><i class="icon-plus-circle2 mr-2"></i> Thêm</a>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <table class="table datatable-colvis-restore">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Danh Mục Cấp 1</th>
                        <th>Danh Mục Cấp 2</th>
                        <th>Danh Mục Cấp 3</th>
                        <th>Thứ Tự</th>
                        <th>Đường Dẫn</th>
                        <th>Kích Hoạt</th>
                        <th class="text-right">Chức Năng</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $idx = $perPage*($currentPage-1); ?>
                    @forelse ($webMenus as $menu)
                        <tr>
                            <?php $idx++; ?>
                            <td>{{$idx}}</td>
                            @if (empty($menu->menu_name_lv1)&&empty($menu->menu_name_lv2))
                                <td>{{$menu->menu_name}}</td>
                                <td>____________________</td>
                                <td>____________________</td>
                            @elseif(isset($menu->menu_name_lv1)&&empty($menu->menu_name_lv2))
                                <td>{{ $menu->menu_name_lv1 }}</td>
                                <td>{{$menu->menu_name}}</td>
                                <td>____________________</td>
                            @else
                                <td>{{ $menu->menu_name_lv1 }}</td>
                                <td>{{ $menu->menu_name_lv2 }}</td>
                                <td>{{$menu->menu_name}}</td>
                            @endif
                            <td>{{ $menu->menu_order }}</td>
                            <td>{{ $menu->menu_url }}</td>
                            <td>@if($menu->active_status == 1)<span class="badge badge-success">BẬT</span>  @else <span class="badge badge-danger">TẮT</span> @endif </td>
                            <td class="text-right">
                                <div class="list-icons">
                                    <a href="javascript:void(0)" onclick="form_action('view', {{$menu->menu_id}});" role="button" data-popup="tooltip" title="Chi Tiết" data-placement="bottom" class="list-icons-item text-info-600"><i class="icon-file-eye2"></i></a>
                                    <a href="javascript:void(0)" onclick="form_action('edit', {{$menu->menu_id}});" role="button" data-popup="tooltip" title="Cập Nhập" data-placement="bottom" class="list-icons-item text-warning-600"><i class="icon-pencil7"></i></a>
                                    <a  href="javascript:void(0)" onclick="form_action('delete', {{$menu->menu_id}});" role="button" data-popup="tooltip" title="Xóa" data-placement="bottom" class="list-icons-item text-danger-600"><i class="icon-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        @empty
                    @endforelse
                </tbody>
            </table>
        </div>
        {{ $webMenus->appends([
            'menu_lv1' => Request::get('menu_lv1'),
            'menu_lv2' => Request::get('menu_lv2'),
            'menu_name' => Request::get('menu_name'),
            'menu_order' => Request::get('menu_order'),
            'menu_url' => Request::get('menu_url'),
            'active_status' => Request::get('active_status'),
        ])->links('admins.includes.pagination') }}
    </div>
    <!-- /restore column visibility -->



    <form name="form1" action="" method="">
        {{csrf_field()}}
        <div class="form-group ">
            <input type="hidden" class="form-control" name="menu_id" value="">
        </div>
    </form>
@stop

@section('javascript')
	<!-- Theme JS files -->
	<script src="{{ asset('asset/admins/js/plugins/tables/datatables/datatables.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/selects/select2.min.js')}}"></script> --}}
	{{-- <script src="{{ asset('asset/admins/js/pages/datatables_extension_colvis.js')}}"></script> --}}
	<script src="{{ asset('asset/admins/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/selects/select2.min.js')}}"></script>


	<script src="{{ asset('asset/admins/js/plugins/notifications/sweet_alert.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
    <script>

        var DatatableColumnVisibility = function() {
            var _componentDatatableColumnVisibility = function() {
                if (!$().DataTable) {
                    console.warn('Warning - datatables.min.js is not loaded.');
                    return;
                }

                // Setting datatable defaults
                $.extend($.fn.dataTable.defaults, {
                    paging:false,
                    autoWidth: false,
                    dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Tìm kiếm:</span> _INPUT_',
                        searchPlaceholder: 'Nhập từ khóa...',
                        lengthMenu: '<span>Hiển thị:</span> _MENU_',
                        paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                    }
                });

                // Restore column visibility
                $('.datatable-colvis-restore').DataTable({
                    buttons: [{
                        extend: 'colvis',
                        text: '<i class="icon-grid7"></i>',
                        className: 'btn bg-teal-400 btn-icon dropdown-toggle',
                        postfixButtons: ['colvisRestore']
                    }],
                    columnDefs: [
                        {targets: -1, orderable: false,},
                        // { targets: [0, 1, 2, 3, 8], visible: true},
                        // { targets: '_all', visible: false }
                    ]
                });

            };

            return {
                init: function() {
                    _componentDatatableColumnVisibility();
                }
            }
        }();

        function form_action (mode, menu_id) {
           let frm = document.form1;
            frm.menu_id.value = menu_id
           if(mode == 'edit'){
               frm.action = "{{route('admins.webmenu.edit')}}";
               frm.method = 'get';
            frm.submit();
           }else if(mode == 'view'){
               frm.action = "{{route('admins.webmenu.show')}}";
               frm.method = 'get';
               frm.submit();
           }else if(mode == 'delete'){

                // Defaults
                var swalInit = swal.mixin({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-light'
                });
                 swalInit.fire({
                    title: 'Bạn có chắc muốn xóa đối tượng này?',
                    text: "Bạn không thể hoàn tác lại thao tác này!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Vâng, Xóa đối tượng!',
                    cancelButtonText: 'Thoát!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function(result) {
                    if (result.value) {
                        frm.action = "{{route('admins.webmenu.delete')}}";
                        frm.method = 'post';
                        frm.submit();
                    }
                });
            }
        }

        function changeType() {
            var proType = $("#menu_lv1").children().children("option:selected").val();
            if(proType == '')
            {
                $("select[name='menu_lv2']").attr('disabled','disabled').empty();
            }else{
                $.ajax({
                    contentType: "application/json",
                    dataType: "json",
                    type: "GET",
                    url: "/admins/web-menu/change/{{Request::get('menu_lv1')}}/"+proType,
                    success: function(rdata){
                        var selectElment = $("select[name='menu_lv2']");
                        selectElment.removeAttr('disabled','disabled');
                        selectElment.empty().append('<optgroup label="Danh Mục Cấp 2"></optgroup>');
                        selectElment.children().append($('<option>', {value: "",text : "Để Trống"}));
                        $.each(rdata, function(i,item){
                            if(item['menu_id']=="{{Request::get('menu_lv2')}}"){
                                selectElment.children().append($('<option>', {value: item['menu_id'], selected: true, text : item['menu_name']}));
                            }
                            else{
                                selectElment.children().append($('<option>', {value: item['menu_id'],text : item['menu_name']}));
                            }
                        });
                    }
                })
            }
        }
        function initialSetup() {
           $hidden = $(".notifyHidden");
            if ($hidden != null) {
                $hidden.delay(3000).fadeOut('slow');
            }
        }

        // Initialize module
        // ------------------------------
        document.addEventListener('DOMContentLoaded', function() {
            DatatableColumnVisibility.init();
            initialSetup();
            changeType();
        });
    </script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_select2.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/components_popups.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/pages/extra_sweetalert.js')}}"></script> --}}
	<!-- /theme JS files -->

@stop
