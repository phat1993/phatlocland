<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>PHÁT LỘC AD - Đăng Nhập</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/admins/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/admins/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/admins/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/admins/css/layout.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/admins/css/components.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('asset/admins/css/colors.min.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

</head>

<body class="bg-slate-800">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content d-flex justify-content-center align-items-center">

                <!-- Login card -->
                <form class="login-form" name="login-form" action="{{ route('admins.login') }}" method="POST">
                    {{csrf_field()}}
                    <div class="card mb-0">
                        <div class="card-body">
                            <div class="text-center mb-3">
                                <i class="icon-people icon-2x text-warning-400 border-warning-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
                                <h5 class="mb-0">ĐĂNG NHẬP</h5>
                                <span class="d-block text-muted">Tài Khoản Của Bạn</span>
                            </div>

                            @if(count($errors) > 0)
                                @foreach($errors->all() as $error)
                                <div class="alert bg-danger text-white alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                                    <span class="font-weight-semibold">{{$error}}</a>
                                </div>
                                @endforeach
                            @endif

                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="text" class="form-control" name="user_name" placeholder="Tài Khoản" value="trung001">
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="password" name="password" class="form-control" placeholder="Mật Khẩu" value="123">
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group d-flex align-items-center">
                                <div class="form-check mb-0">
                                    <label class="form-check-label">
										<input type="checkbox" name="remember" class="form-input-styled" checked data-fouc>
										Ghi Nhớ
									</label>
                                </div>

                                <a href="{{ route('admins.forget') }}" class="ml-auto">Quên mật khẩu?</a>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Đăng Nhập <i class="icon-circle-right2 ml-2"></i></button>
                            </div>

                            <span class="form-text text-center text-muted">
                                Để tiếp tục, Bạn xác nhận rằng bạn đã đọc <a href="#">Điều khoản &amp; Điều kiện</a> và <a href="#">Chính sách</a> của chúng tôi
                            </span>
                        </div>
                    </div>
                </form>
                <!-- /login card -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
     <!-- Core JS files -->
    <script src="{{ asset('asset/admins/js/main/jquery.min.js') }}"></script>
    <script src="{{ asset('asset/admins/js/main/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('asset/admins/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script src="{{ asset('asset/admins/js/plugins/forms/validation/validate.min.js') }}"></script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
    <script src="{{ asset('asset/admins/js/pages/login_validation.js') }}"></script>
    <!-- /theme JS files -->
</body>

</html>
