@extends('layouts.admin_layout')
@section('title', 'Lỗi 403')

@section('topnavigation')
<div class="breadcrumb">
    <a href="index-2.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dự Án</a>
    <span class="breadcrumb-item active">Danh Mục</span>
</div>
@endsection
@section('content')

<div class="flex-fill">

    <!-- Error title -->
    <div class="text-center mb-3">
        <h1 class="error-title">405</h1>
        <h5>{{ $message }}</h5>
    </div>
    <!-- /error title -->


    <!-- Error content -->
    <div class="row">
        <div class="col-xl-4 offset-xl-4 col-md-8 offset-md-2">

            <!-- Search -->
            <form action="#">
                <div class="input-group mb-3">
                    <input type="text" class="form-control form-control-lg" placeholder="Search">

                    <div class="input-group-append">
                        <button type="submit" class="btn bg-slate-600 btn-icon btn-lg"><i class="icon-search4"></i></button>
                    </div>
                </div>
            </form>
            <!-- /search -->


            <!-- Buttons -->
            <div class="row">
                <div class="col-sm-6">
                    <a href="#" class="btn btn-primary btn-block"><i class="icon-home4 mr-2"></i> Dashboard</a>
                </div>

                <div class="col-sm-6">
                    <a href="#" class="btn btn-light btn-block mt-3 mt-sm-0"><i class="icon-menu7 mr-2"></i> Advanced search</a>
                </div>
            </div>
            <!-- /buttons -->

        </div>
    </div>
    <!-- /error wrapper -->

</div>
@stop

@section('javascript')
<!-- Theme JS files -->
<script src="{{ asset('asset/admins/js/app.js') }}"></script>
<!-- /theme JS files -->

@stop
