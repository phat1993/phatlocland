@extends('layouts.admin_layout')
@section('title', 'Hình ảnh bất động sản')

@section('topnavdetail')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i>
            <span class="font-weight-semibold">Hình Ảnh</span> <span class="text-primary">{{ $house->house_name }}</span>
            - Danh sách</h4>
        <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.house.index') }}" class="breadcrumb-item"><i class="icon-city mr-2"></i> Bất Động Sản</a>
        <a href="{{ route('admins.house.show', ['house_id'=>$house->house_id]) }}" class="breadcrumb-item"> {{ $house->house_name }}</a>
        <a href="{{ route('admins.housegallery.index', ['id'=>$house->house_id]) }}" class="breadcrumb-item"> Danh sách</a>
        <span class="breadcrumb-item active">Cập Nhật</span>
    </div>
    <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
        <h5>{{session('success')}}</h5>
        <br>
        <a href="{{ route('admins.housegallery.index', ['id'=>$house->house_id]) }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
    </div>
    @endif

    @if(count($errors) > 0)
         <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
            @foreach($errors->all() as $error)
            <h5>{{$error}}</h5>
            @endforeach
            <br>
            <a href="{{ route('admins.housegallery.index', ['id'=>$house->house_id]) }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
        </div>
    @endif
    <!-- /Notifications Set  -->
    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Cập Nhật</strong>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="reload"></a>
                </div>
            </div>
        </div>

        <div class="card-body">

            <form action="{{ route('admins.housegallery.update', ['id'=>$house->house_id]) }}" method="POST" enctype="multipart/form-data"  name="house-gallery-form" >
                {{csrf_field()}}
                <fieldset class="mb-3">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="house_id" value="{{$houseGallery->house_id}}">
                        <input type="hidden" class="form-control" name="house_gallery_id" value="{{$houseGallery->house_gallery_id}}">
                        <input type="hidden" class="form-control" name="house_gallery_link" value="{{$houseGallery->house_gallery_link}}">
                    </div>
                    <legend class="text-uppercase font-size-sm font-weight-bold">Thông Tin Cơ Bản</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_gallery_banner">Thuộc băng rôn<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" id="house_gallery_banner" name="house_gallery_banner" value="1" class="form-check-input-styled-danger"
                                    @if(old('house_gallery_banner', $houseGallery->house_gallery_banner) == 1) checked @endif data-fouc>
                                    Băng Rôn(1400x700)
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" id="house_gallery_banner" name="house_gallery_banner" value="2" class="form-check-input-styled-warning"
                                    @if(old('house_gallery_banner', $houseGallery->house_gallery_banner) == 2) checked @endif data-fouc>
                                    Hình Đại diện(750x500)
                                </label>
                            </div>


                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" id="house_gallery_banner" name="house_gallery_banner" value="0" class="form-check-input-styled-custom"
                                    @if(old('house_gallery_banner', $houseGallery->house_gallery_banner) == 0) checked @endif data-fouc>
                                    Hình Ảnh
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_gallery_name">Tên<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào tên hình ảnh"  id="house_gallery_name" name="house_gallery_name" value="{{ old('house_gallery_name', $houseGallery->house_gallery_name) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="file-input">Hình ảnh:</label>
                        <div class="col-lg-10">
                            <input type="file" class="file-input" id="file_input" name="file_input" data-show-caption="false" data-show-upload="false" data-fouc>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_gallery_order">Thứ Tự<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="number" class="form-control"  maxlength="3" placeholder="Nhập vào thứ tự"  id="house_gallery_order" name="house_gallery_order" value="{{ old('house_gallery_order', $houseGallery->house_gallery_order) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="active_status">Trạng Thái:</label>
                        <div class="col-lg-10">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" id="active_status" name="active_status" class="form-check-input-styled-primary" value="1"
                                    @if(old('active_status', $houseGallery->active_status)!=null && old('active_status', $houseGallery->active_status) == 1)
                                        checked
                                    @endif data-fouc>
                                    Kích Hoạt
                                </label>
                            </div>
                        </div>
                    </div>

                </fieldset>


                <div class="text-center">
                    <button type="submit" class="btn btn-primary"><i class="icon-download4 mr-2"> Lưu Lại</i></button>
                    <a href="{{ route('admins.housegallery.index', ['id'=>$house->house_id]) }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở Về Danh Sách</a>
                </div>
            </form>
        </div>
    </div>
    <!-- /restore column visibility -->

@stop

@section('javascript')

	<!-- Theme JS files -->
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tagsinput.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tokenfield.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/ui/prism.min.js')}}"></script>

	<script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
	<script src="{{ asset('asset/admins/js/plugins/forms/styling/switch.min.js')}}"></script>

	{{-- <script src="{{ asset('asset/admins/js/plugins/uploaders/fileinput/plugins/purify.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/uploaders/fileinput/plugins/sortable.min.js')}}"></script> --}}
	<script src="{{ asset('asset/admins/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>


    <script src="{{ asset('asset/admins/js/plugins/forms/validation/validate.min.js') }}"></script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_tags_input.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_checkboxes_radios.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/pages/uploader_bootstrap.js')}}"></script> --}}
	<!-- /theme JS files -->

    <script>
        function initialSetup() {
           $hidden = $(".notifyHidden");
            if ($hidden != null) {
                $hidden.delay(10000).fadeOut('slow');
            }
        }



        var FileUpload = function() {


                //
                // Setup module components
                //

                // Bootstrap file upload
                var _componentFileUpload = function() {
                    if (!$().fileinput) {
                        console.warn('Warning - fileinput.min.js is not loaded.');
                        return;
                    }

                    //
                    // Define variables
                    //

                    // Modal template
                    var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
                        '  <div class="modal-content">\n' +
                        '    <div class="modal-header align-items-center">\n' +
                        '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
                        '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
                        '    </div>\n' +
                        '    <div class="modal-body">\n' +
                        '      <div class="floating-buttons btn-group"></div>\n' +
                        '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
                        '    </div>\n' +
                        '  </div>\n' +
                        '</div>\n';

                    // Buttons inside zoom modal
                    var previewZoomButtonClasses = {
                        toggleheader: 'btn btn-light btn-icon btn-header-toggle btn-sm',
                        fullscreen: 'btn btn-light btn-icon btn-sm',
                        borderless: 'btn btn-light btn-icon btn-sm',
                        close: 'btn btn-light btn-icon btn-sm'
                    };

                    // Icons inside zoom modal classes
                    var previewZoomButtonIcons = {
                        prev: '<i class="icon-arrow-left32"></i>',
                        next: '<i class="icon-arrow-right32"></i>',
                        toggleheader: '<i class="icon-menu-open"></i>',
                        fullscreen: '<i class="icon-screen-full"></i>',
                        borderless: '<i class="icon-alignment-unalign"></i>',
                        close: '<i class="icon-cross2 font-size-base"></i>'
                    };

                    // File actions
                    var fileActionSettings = {
                        zoomClass: '',
                        zoomIcon: '<i class="icon-zoomin3"></i>',
                        dragClass: 'p-2',
                        dragIcon: '<i class="icon-three-bars"></i>',
                        removeClass: '',
                        removeErrorClass: 'text-danger',
                        // removeIcon: '<i class="icon-bin"></i>',
                        indicatorNew: '<i class="icon-file-plus text-success"></i>',
                        indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
                        indicatorError: '<i class="icon-cross2 text-danger"></i>',
                        indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
                    };


                    //
                    // Basic example
                    //

                    $('.file-input').fileinput({
                        browseLabel: 'Chọn Hình Ảnh',
                        browseIcon: '<i class="icon-file-plus mr-2"></i>',
                        uploadIcon: '<i class="icon-file-upload2 mr-2"></i>',
                        removeIcon: '<i class="icon-cross2 font-size-base mr-2"></i>',
                        layoutTemplates: {
                            icon: '<i class="icon-file-check"></i>',
                            modal: modalTemplate
                        },

                            initialPreview: [
                            "{{ asset('storage/'.$houseGallery->house_gallery_link) }}",

                        ],
                        initialPreviewAsData: true,
                        allowedFileExtensions: ["jpg", "png", "jpeg"],
                        initialCaption: "Không có hình ảnh được chọn",
                        previewZoomButtonClasses: previewZoomButtonClasses,
                        previewZoomButtonIcons: previewZoomButtonIcons,
                        fileActionSettings: fileActionSettings
                    });
                };


                //
                // Return objects assigned to module
                //

                return {
                init: function() {
                    _componentFileUpload();
                }
                }
            }();


                // Initialize module
                // ------------------------------

                document.addEventListener('DOMContentLoaded', function() {
                FileUpload.init();
                });

        $( document ).ready( function () {
            initialSetup();
            $( "form[name=house-gallery-form]" ).validate( {
                rules: {
                    house_gallery_name: {
                        required: true,
                        maxlength: 100
                    },
                    house_gallery_order: {
                        required: true,
                        maxlength: 3
                    },
                },
                messages: {
                    house_gallery_name: {
                        required: "Hãy nhập vào tên hình ảnh.",
                        maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    },
                    house_gallery_order: {
                        required: "Hãy nhập vào vị trí.",
                        maxlength: "Bạn chỉ được nhập tối đa 3 ký tự."
                    },
                },
                errorElement: "span",
                errorPlacement: function ( error, element ) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("form-text").addClass( "text-danger" );

                    if ( element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( element.next( "label" ) );
                    } else {
                        error.insertAfter( element );
                    }
                },
                highlight: function ( element, errorClass, validClass ) {
                    $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
                },
                unhighlight: function (element, errorClass, validClass) {
                    $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
                }
            } );

        } );
    </script>
@stop
