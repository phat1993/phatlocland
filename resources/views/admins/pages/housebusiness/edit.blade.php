@extends('layouts.admin_layout')
@section('title', 'Hạng mục kinh doanh')

@section('topnavdetail')
<div class="page-title d-flex">
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Hạng mục kinh doanh</span> - Cập Nhật</h4>
    <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
</div>
@endsection
@section('topnavigation')
<div class="breadcrumb">
    <a href="{{ route('admins.house.index') }}" class="breadcrumb-item"><i class="icon-city mr-2"></i> Dự Án</a>
    <a href="{{ route('admins.houbusiness.index') }}" class="breadcrumb-item">Hạng mục kinh doanh</a>
    <span class="breadcrumb-item active">Cập Nhật</span>
</div>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
        <h5>{{session('success')}}</h5>
        <br>
        <a href="{{ route('admins.houbusiness.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
    </div>
    @endif

    @if(count($errors) > 0)
         <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
            @foreach($errors->all() as $error)
            <h5>{{$error}}</h5>
            @endforeach
            <br>
            <a href="{{ route('admins.houbusiness.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
        </div>
    @endif
    <!-- /Notifications Set  -->
    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Cập Nhật</strong>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="reload"></a>
                </div>
            </div>
        </div>

        <div class="card-body">

            <form action="{{ route('admins.houbusiness.update') }}" method="POST" name="house-business-form">
                {{csrf_field()}}
                <fieldset class="mb-3">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="house_business_id" value="{{$houseBusiness->house_business_id}}">
                    </div>
                    <legend class="text-uppercase font-size-sm font-weight-bold">Thông Tin Cơ Bản</legend>
                    <div class="form-group form-group-feedback form-group-feedback-right row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_business_name">Tiêu đề<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào tiêu đề danh mục"  id="house_business_name" name="house_business_name" value="{{ old('house_business_name', $houseBusiness->house_business_name) }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_business_description">Mô tả:</label>
                        <div class="col-lg-10">
                            <textarea rows="3" cols="3" class="form-control" maxlength="200" id="house_business_description" name="house_business_description" placeholder="Nhập vào mô tả">{{ old('house_business_description', $houseBusiness->house_business_description) }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_description">Trạng Thái:</label>
                        <div class="col-lg-10">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" id="active_status" name="active_status" class="form-check-input-styled-primary" value="1"
                                    @if(old('active_status', $houseBusiness->active_status)!=null && old('active_status', $houseBusiness->active_status) == 1)
                                        checked
                                    @endif data-fouc>
                                    Kích Hoạt
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Tối Ưu Quảng Cáo</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_title">Tiêu đề Website:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="20" placeholder="Nhập vào tiêu đề hiển thị website" id="web_title" name="web_title" value="{{ old('web_title', $houseBusiness->web_title) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_keywords">Từ khóa tìm kiếm:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control tokenfield" id="web_keywords" placeholder="Từ khóa" maxlength="200" name="web_keywords"
                            value="{{ old('web_keywords', $houseBusiness->web_keywords) }}" data-fouc>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_description">Mô Tả Website:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="145" placeholder="Nhập vào mô tả website"  id="web_description" name="web_description" value="{{ old('web_description', $houseBusiness->web_description) }}">
                        </div>
                    </div>
                </fieldset>


                <div class="text-center">
                    <button type="submit" class="btn btn-primary"><i class="icon-download4 mr-2"> Lưu Lại</i></button>
                    <a href="{{ route('admins.houbusiness.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở Về Danh Sách</a>
                </div>
            </form>
        </div>
    </div>
    <!-- /restore column visibility -->

@stop

@section('javascript')

	<!-- Theme JS files -->
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tagsinput.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tokenfield.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/ui/prism.min.js')}}"></script>

	<script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
	<script src="{{ asset('asset/admins/js/plugins/forms/styling/switch.min.js')}}"></script>


    <script src="{{ asset('asset/admins/js/plugins/forms/validation/validate.min.js') }}"></script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_tags_input.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_checkboxes_radios.js')}}"></script>
	<!-- /theme JS files -->


    <script>
        function initialSetup() {
           $hidden = $(".notifyHidden");
            if ($hidden != null) {
                $hidden.delay(10000).fadeOut('slow');
            }
        }
        initialSetup();


        $( document ).ready( function () {
        $( "form[name=house-business-form]" ).validate( {
            rules: {
                house_business_name: {
                    required: true,
                    maxlength: 100
                }
            },
            messages: {
                house_business_name:  {
                    required: "Hãy nhập vào tiêu đề.",
                    maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                },
            },
            errorElement: "span",
            errorPlacement: function ( error, element ) {
                // Add the `invalid-feedback` class to the error element
                error.addClass("form-text").addClass( "text-danger" );

                if ( element.prop( "type" ) === "checkbox" ) {
                    error.insertAfter( element.next( "label" ) );
                } else {
                    error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
            }
        } );

    } );
    </script>
@stop
