@extends('layouts.admin_layout')
@section('title', 'Đánh giá bất động sản')

@section('topnavdetail')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Đánh Giá</span> - Danh sách</h4>
        <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.house.index') }}" class="breadcrumb-item"><i class="icon-city mr-2"></i> Bất Động Sản</a>
        <span class="breadcrumb-item active">Đánh Giá</span>
    </div>
    <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h6 class="alert-heading font-weight-semibold mb-1">Thông Báo</h6>
        <span>{{session('success')}}</span>
    </div>
    @endif

    @if(count($errors) > 0)
        <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h6 class="alert-heading font-weight-semibold mb-1">Thông Báo</h6>
            @foreach($errors->all() as $error)
            <p>{{$error}}</p>
            @endforeach
        </div>
    @endif
    <!-- /Notifications Set  -->
    <!-- /Notifications Set  -->
    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Danh Sách</strong>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <table class="table datatable-colvis-restore">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Hình Ảnh</th>
                    <th>Email</th>
                    <th>IP</th>
                    <th>Đánh Giá</th>
                    <th>Họ Tên</th>
                    <th>Nội Dung</th>
                    <th>Kích Hoạt</th>
                    <th class="text-right">Chức Năng</th>
                </tr>
            </thead>
            <tbody>
                <?php $idx = 0; ?>
                @forelse ($houseReviews as $review)
                    <tr>
                        <?php $idx++; ?>
                        <td>{{$idx}}</td>
                        <td>
                            @isset($review->house_review_avatar)
                            <img src="{{ asset('storage/'.$review->house_review_avatar) }}" width="75" alt="">
                            @else
                            <i class="icon-user-tie" style="font-size: 5rem"></i>
                            @endisset
                        </td>
                        <td>{{$review->house_review_email}}</td>
                        <td>{{$review->house_review_ip}}</td>
                        <td>
                            <div style="width:5.9rem;color:orange">
                                @for ($i = 0; $i < $review->house_review_point; $i++)
                                    <i class="icon-star-full2"></i>
                                @endfor
                                @if ($review->house_review_point<5)
                                    @for ($i = 0; $i < 5-$review->house_review_point; $i++)
                                        <i class="icon-star-empty3"></i>
                                    @endfor
                                @endif
                            </div>
                        </td>
                        <td>{{$review->house_review_name}}</td>
                        <td>{{ $review->house_review_message }}</td>
                        <td>
                            @if($review->active_status == 1)<span class="badge badge-success">BẬT</span>
                            @else <span class="badge badge-danger">TẮT</span>
                            @endif
                        </td>

                        <td class="text-right">
                            <div class="list-icons">
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#detail{{ $review->house_review_id  }}" role="button" data-popup="tooltip" title="Chi Tiết" data-placement="bottom" class="list-icons-item text-info-600"><i class="icon-file-eye2"></i></a>
                                <a href="javascript:void(0)" onclick="form_action('edit', {{$review->house_review_id}});" role="button" data-popup="tooltip" title="Cập Nhập" data-placement="bottom" class="list-icons-item text-warning-600"><i class="icon-checkmark4"></i></a>
                                <a  href="javascript:void(0)" onclick="form_action('delete', {{$review->house_review_id}});" role="button" data-popup="tooltip" title="Xóa" data-placement="bottom" class="list-icons-item text-danger-600"><i class="icon-trash"></i></a>
                            </div>
                        </td>
                    </tr>
                    @empty
                @endforelse
            </tbody>
        </table>
    </div>
    <!-- /restore column visibility -->
    <!-- Large modal -->

    @forelse ($houseReviews as $review)

    <div id="detail{{ $review->house_review_id  }}" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Chi Tiết Đánh Giá</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body">
                    <table class="table">
                        <tr>
                            <th width="20%" scope="col">Kinh Doanh</th>
                            <td width="80%">{{$review->house_business_name}}</td>
                        </tr>
                        <tr>
                            <th scope="col">Danh Mục</th>
                            <td>{{$review->house_category_name}}</td>
                        </tr>
                        @isset($review->project_name)
                        <tr>
                            <th scope="col">Dự Án</th>
                            <td>{{$review->project_name}}</td>
                        </tr>
                        @endisset
                        <tr>
                            <th scope="col">Bất Động Sản</th>
                            <td>{{$review->house_name}}</td>
                        </tr>
                        <tr>
                            <th>Đánh Giá</th>
                            <td>
                                <div style="width:5.9rem;color:orange">
                                    @for ($i = 0; $i < $review->house_review_point; $i++)
                                        <i class="icon-star-full2"></i>
                                    @endfor
                                    @if ($review->house_review_point<5)
                                        @for ($i = 0; $i < 5-$review->house_review_point; $i++)
                                            <i class="icon-star-empty3"></i>
                                        @endfor
                                    @endif
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>IP</th>
                            <td>{{$review->house_review_ip}}</td>
                        </tr>
                        <tr>
                            <th>Họ Tên</th>
                            <td>{{$review->house_review_name}}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{$review->house_review_email}}</td>
                        </tr>
                        <tr>
                            <th>Nội Dung</th>
                            <td>{{$review->house_review_message}}</td>
                        </tr>
                        <tr>
                            <th>Thời Gian</th>
                            <td class="font-weight-bold text-danger">{{date('H:i d-m-Y', strtotime($review->created_at))}}</td>
                        </tr>
                    </table>
                    <hr class="border-danger">
                </div>

                <div class="modal-footer">
                    <a href="javascript:void(0)" onclick="form_action('edit', {{$review->house_review_id}});"
                        role="button" data-popup="tooltip" title="Cập Nhập" data-placement="bottom"
                        class="btn btn-outline-success"><i class="icon-checkmark4"></i></a>
                    <a  href="javascript:void(0)" onclick="form_action('delete', {{$review->house_review_id}});"
                        role="button" data-popup="tooltip" title="Xóa" data-placement="bottom"
                         class="btn btn-outline-danger"><i class="icon-trash"></i></a>
                    <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
    @empty
    @endforelse
    <!-- /large modal -->


    <form name="form1" action="" method="">
        {{csrf_field()}}
        <div class="form-group ">
            <input type="hidden" class="form-control" name="house_review_id" value="">
        </div>
    </form>
@stop

@section('javascript')
	<!-- Theme JS files -->
	<script src="{{ asset('asset/admins/js/plugins/tables/datatables/datatables.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/selects/select2.min.js')}}"></script> --}}
	{{-- <script src="{{ asset('asset/admins/js/pages/datatables_extension_colvis.js')}}"></script> --}}


	<script src="{{ asset('asset/admins/js/plugins/notifications/sweet_alert.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
    <script>

        var DatatableColumnVisibility = function() {
            var _componentDatatableColumnVisibility = function() {
                if (!$().DataTable) {
                    console.warn('Warning - datatables.min.js is not loaded.');
                    return;
                }

                // Setting datatable defaults
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Tìm kiếm:</span> _INPUT_',
                        searchPlaceholder: 'Nhập từ khóa...',
                        lengthMenu: '<span>Hiển thị:</span> _MENU_',
                        paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                    }
                });

                // Restore column visibility
                $('.datatable-colvis-restore').DataTable({
                    buttons: [{
                        extend: 'colvis',
                        text: '<i class="icon-grid7"></i>',
                        className: 'btn bg-teal-400 btn-icon dropdown-toggle',
                        postfixButtons: ['colvisRestore']
                    }],
                    columnDefs: [
                        {targets: -1, orderable: false,}
                    ]
                });

            };

            return {
                init: function() {
                    _componentDatatableColumnVisibility();
                }
            }
        }();

        function form_action (mode, house_review_id) {
           let frm = document.form1;
            frm.house_review_id.value = house_review_id
           if(mode == 'edit'){
               frm.action = "{{route('admins.housereview.update')}}";
               frm.method = 'post';
            frm.submit();
           }else if(mode == 'delete'){

                // Defaults
                var swalInit = swal.mixin({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-light'
                });
                 swalInit.fire({
                    title: 'Bạn có chắc muốn xóa đối tượng này?',
                    text: "Bạn không thể hoàn tác lại thao tác này!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Vâng, Xóa đối tượng!',
                    cancelButtonText: 'Thoát!',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function(result) {
                    if (result.value) {
                        frm.action = "{{route('admins.housereview.delete')}}";
                        frm.method = 'post';
                        frm.submit();
                    }
                });
            }
        }

        function initialSetup() {
           $hidden = $(".notifyHidden");
            if ($hidden != null) {
                $hidden.delay(3000).fadeOut('slow');
            }
        }

        // Initialize module
        // ------------------------------
        document.addEventListener('DOMContentLoaded', function() {
            DatatableColumnVisibility.init();
            initialSetup();
        });
    </script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<script src="{{ asset('asset/admins/js/pages/components_popups.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/pages/extra_sweetalert.js')}}"></script> --}}
	<!-- /theme JS files -->

@stop
