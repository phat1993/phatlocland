@extends('layouts.admin_layout')
@section('title', 'Chính Sách Web')

@section('topnavdetail')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i>
            <span class="font-weight-semibold">Chính Sách Web</span> - Cập nhật</h4>
        <a href="{{ route('admins.webpolicy.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.webpolicy.index') }}" class="breadcrumb-item"> Danh sách</a>
        <span class="breadcrumb-item active">Cập nhật</span>
    </div>
    <a href="{{ route('admins.webpolicy.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
        <h5>{{session('success')}}</h5>
        <br>
        <a href="{{ route('admins.webpolicy.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
    </div>
    @endif
    @if(count($errors) > 0)
         <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
            @foreach($errors->all() as $error)
            <h5>{{$error}}</h5>
            @endforeach
            <br>
            <a href="{{ route('admins.webpolicy.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
        </div>
    @endif
    <!-- /Notifications Set  -->
    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Cập Nhật</strong>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="reload"></a>
                </div>
            </div>
        </div>

        <div class="card-body">

            <form action="{{ route('admins.webpolicy.update') }}" method="POST" enctype="multipart/form-data"  name="web-policy-form" >
                {{csrf_field()}}
                <div class="form-group">
                    <input type="hidden" class="form-control" name="web_policy_id" value="{{$webPolicy->web_policy_id}}">
                    <input type="hidden" class="form-control" name="web_policy_image" value="{{$webPolicy->web_policy_image}}">
                </div>
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Thông Tin Cơ Bản</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_policy_name">Tiêu đề<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="200" placeholder="Nhập vào tiêu đề"  id="web_policy_name" name="web_policy_name" value="{{ old('web_policy_name', $webPolicy->web_policy_name) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="file-input">Hình ảnh(750x500):</label>
                        <div class="col-lg-10">
                            <input type="file" class="file-input" id="file_input" name="file_input" data-show-caption="false" data-show-upload="false" data-fouc>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_policy_description">Mô tả<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <textarea rows="6" class="form-control" maxlength="500" id="web_policy_description" name="web_policy_description" placeholder="Nhập vào mô tả">{{ old('web_policy_description', $webPolicy->web_policy_description) }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2" for="web_policy_detail">Chi Tiết<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <textarea class="form-control" id="web_policy_detail" name="web_policy_detail" >
                                {{ old('web_policy_detail', $webPolicy->web_policy_detail) }}
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="active_status">Trạng Thái:</label>
                        <div class="col-lg-10">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" id="active_status" name="active_status" class="form-check-input-styled-primary" value="1"
                                    @if(old('active_status', $webPolicy->active_status)!=null && old('active_status', $webPolicy->active_status) == 1)
                                        checked
                                    @endif data-fouc>
                                    Kích Hoạt
                                </label>
                            </div>
                        </div>
                    </div>

                </fieldset>


                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Tối Ưu Quảng Cáo</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_title">Tiêu đề Website:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="20" placeholder="Nhập vào tiêu đề hiển thị website" id="web_title" name="web_title" value="{{ old('web_title', $webPolicy->web_title) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_keywords">Từ khóa tìm kiếm:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control tokenfield" id="web_keywords" placeholder="Từ khóa" maxlength="200" name="web_keywords"  value="{{ old('web_keywords', $webPolicy->web_keywords) }}" data-fouc>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_description">Mô Tả Website:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="145" placeholder="Nhập vào mô tả website"  id="web_description" name="web_description" value="{{ old('web_description', $webPolicy->web_description) }}">
                        </div>
                    </div>
                </fieldset>

                <div class="text-center">
                    <button type="submit" class="btn btn-primary"><i class="icon-download4 mr-2"> Lưu Lại</i></button>
                    <a href="{{ route('admins.webpolicy.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở Về Danh Sách</a>
                </div>
            </form>
        </div>
    </div>
    <!-- /restore column visibility -->

@stop

@section('javascript')

	<!-- Theme JS files -->
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tagsinput.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tokenfield.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/ui/prism.min.js')}}"></script>

	<script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
	<script src="{{ asset('asset/admins/js/plugins/forms/styling/switch.min.js')}}"></script>

	{{-- <script src="{{ asset('asset/admins/js/plugins/uploaders/fileinput/plugins/purify.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/uploaders/fileinput/plugins/sortable.min.js')}}"></script> --}}
	<script src="{{ asset('asset/admins/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>
    <script src="{{ asset('asset/admins/js/plugins/editors/ckeditor/ckeditor.js')}}"></script>


    <script src="{{ asset('asset/admins/js/plugins/forms/validation/validate.min.js') }}"></script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_tags_input.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_checkboxes_radios.js')}}"></script>
	<!-- /theme JS files -->

    <script>
        var FileUpload = function() {


        //
        // Setup module components
        //

        // Bootstrap file upload
        var _componentFileUpload = function() {
            if (!$().fileinput) {
                console.warn('Warning - fileinput.min.js is not loaded.');
                return;
            }

            //
            // Define variables
            //

            // Modal template
            var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
                '  <div class="modal-content">\n' +
                '    <div class="modal-header align-items-center">\n' +
                '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
                '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
                '    </div>\n' +
                '    <div class="modal-body">\n' +
                '      <div class="floating-buttons btn-group"></div>\n' +
                '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
                '    </div>\n' +
                '  </div>\n' +
                '</div>\n';

            // Buttons inside zoom modal
            var previewZoomButtonClasses = {
                toggleheader: 'btn btn-light btn-icon btn-header-toggle btn-sm',
                fullscreen: 'btn btn-light btn-icon btn-sm',
                borderless: 'btn btn-light btn-icon btn-sm',
                close: 'btn btn-light btn-icon btn-sm'
            };

            // Icons inside zoom modal classes
            var previewZoomButtonIcons = {
                prev: '<i class="icon-arrow-left32"></i>',
                next: '<i class="icon-arrow-right32"></i>',
                toggleheader: '<i class="icon-menu-open"></i>',
                fullscreen: '<i class="icon-screen-full"></i>',
                borderless: '<i class="icon-alignment-unalign"></i>',
                close: '<i class="icon-cross2 font-size-base"></i>'
            };

            // File actions
            var fileActionSettings = {
                zoomClass: '',
                zoomIcon: '<i class="icon-zoomin3"></i>',
                dragClass: 'p-2',
                dragIcon: '<i class="icon-three-bars"></i>',
                removeClass: '',
                removeErrorClass: 'text-danger',
                // removeIcon: '<i class="icon-bin"></i>',
                indicatorNew: '<i class="icon-file-plus text-success"></i>',
                indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
                indicatorError: '<i class="icon-cross2 text-danger"></i>',
                indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
            };


            //
            // Basic example
            //

            $('.file-input').fileinput({
                browseLabel: 'Chọn Hình Ảnh',
                browseIcon: '<i class="icon-file-plus mr-2"></i>',
                uploadIcon: '<i class="icon-file-upload2 mr-2"></i>',
                removeIcon: '<i class="icon-cross2 font-size-base mr-2"></i>',
                layoutTemplates: {
                    icon: '<i class="icon-file-check"></i>',
                    modal: modalTemplate
                },

                    initialPreview: [
                    "{{ asset('storage/'.$webPolicy->web_policy_image) }}",

                ],
                initialPreviewAsData: true,
                allowedFileExtensions: ["jpg", "png", "jpeg"],
                initialCaption: "Không có hình ảnh được chọn",
                previewZoomButtonClasses: previewZoomButtonClasses,
                previewZoomButtonIcons: previewZoomButtonIcons,
                fileActionSettings: fileActionSettings
            });
        };


        //
        // Return objects assigned to module
        //

                return {
                    init: function() {
                        _componentFileUpload();
                    }
                }
            }();


            // Initialize module
            // ------------------------------

            document.addEventListener('DOMContentLoaded', function() {
            FileUpload.init();
        });

            function initialSetup() {
            $hidden = $(".notifyHidden");
                if ($hidden != null) {
                    $hidden.delay(10000).fadeOut('slow');
                }
            }


            $( document ).ready( function () {
                initialSetup();
                CKEDITOR.replace( 'web_policy_detail' );
                $( "form[name=web-policy-form]" ).validate( {
                    rules: {
                        web_policy_name: {
                            required: true,
                            maxlength: 200
                        },
                        web_policy_description: {
                            required: true,
                            maxlength: 500
                        },
                        web_policy_detail: {
                            required: true,
                        }
                    },
                    messages: {
                        web_policy_name: {
                            required: "Hãy nhập vào tiêu đề.",
                            maxlength: "Bạn chỉ được nhập tối đa 200 ký tự."
                        },
                        web_policy_description: {
                            required: "Hãy nhập vào mô tả.",
                            maxlength: "Bạn chỉ được nhập tối đa 500 ký tự."
                        },
                        web_policy_detail: {
                            required: "Hãy nhập vào chi tiết.",
                        }
                    },
                    errorElement: "span",
                    errorPlacement: function ( error, element ) {
                        // Add the `invalid-feedback` class to the error element
                        error.addClass("form-text").addClass( "text-danger" );

                        if ( element.prop( "type" ) === "checkbox" ) {
                            error.insertAfter( element.next( "label" ) );
                        } else {
                            error.insertAfter( element );
                        }
                    },
                    highlight: function ( element, errorClass, validClass ) {
                        $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
                    }
                } );

            } );
    </script>
@stop
