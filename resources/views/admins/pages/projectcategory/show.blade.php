@extends('layouts.admin_layout')
@section('title', 'Danh mục dự án')

@section('topnavdetail')
<div class="page-title d-flex">
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Danh Mục</span> - Chi tiết</h4>
    <a href="{{ route('admins.project.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
</div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.project.index') }}" class="breadcrumb-item"><i class="icon-city mr-2"></i> Dự Án</a>
        <a href="{{ route('admins.procategory.index') }}" class="breadcrumb-item">Danh Mục</a>
        <span class="breadcrumb-item active">Chi Tiết</span>
    </div>
    <a href="{{ route('admins.project.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection

@section('content')


    <!-- Inner container -->
    <div class="d-flex align-items-start flex-column flex-md-row">

        <!-- Left content -->
        <div class="w-100 overflow-auto order-2 order-md-1">

            <!-- Course overview -->
            <div class="card">
                <div class="card-header header-elements-md-inline">
                    <h5 class="card-title">{{ $projectCategory->project_category_name }}</h5>

                    {{-- <div class="header-elements">
                        <ul class="list-inline list-inline-dotted mb-0 mt-2 mt-md-0">
                            <li class="list-inline-item">Rating: <span class="font-weight-semibold">4.85</span></li>
                            <li class="list-inline-item">
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <span class="text-muted ml-1">(439)</span>
                            </li>
                        </ul>
                    </div> --}}
                </div>

                <div class="nav-tabs-responsive bg-light border-top">
                    <ul class="nav nav-tabs nav-tabs-bottom flex-nowrap mb-0">
                        <li class="nav-item"><a href="#course-overview" class="nav-link active" data-toggle="tab"><i class="icon-menu7 mr-2"></i> Thông Tin</a></li>

                    </ul>
                </div>

                <div class="tab-content">
                        <div class="tab-pane fade show active" id="course-overview">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <th width="20%" scope="col">Tiêu đề</th>
                                            <td width="80%">{{$projectCategory->project_category_name}}</td>
                                        </tr>
                                        <tr>
                                            <th scope="col">Mô tả</th>
                                            <td>{{$projectCategory->project_category_description}}</td>
                                        </tr>
                                        <tr>
                                            <th>Tiêu đề website</th>
                                            <td>{{$projectCategory->web_title}}</td>
                                        </tr>
                                        <tr>
                                            <th>Từ khóa tìm kiếm</th>
                                            <td>{{$projectCategory->web_keywords}}</td>
                                        </tr>
                                        <tr>
                                            <th>Mô tả website</th>
                                            <td>{{$projectCategory->web_description}}</td>
                                        </tr>
                                        <tr>
                                            <th>Đường dẫn</th>
                                            <td><a href="{{url('').'/'.$projectCategory->web_canonical}}">{{url('').'/'.$projectCategory->web_canonical}}</a></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- /course overview -->

        </div>
        <!-- /left content -->


        <!-- Right sidebar component -->
        <div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right wmin-350 border-0 shadow-0 order-1 order-md-2 sidebar-expand-md">

            <!-- Sidebar content -->
            <div class="sidebar-content">
                <!-- Task details -->
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="card-title font-weight-semibold">Cơ bản</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>

                    <table class="table table-borderless table-xs my-2">
                        <tbody>
                            <tr>
                                <td><i class="icon-user-tie mr-2"></i> Quản trị viên:</td>
                                <td class="text-right"><a href="#">{{ $projectCategory->admin_full_name }}</a></td>
                            </tr>
                            <tr>
                                <td><i class="icon-circles2 mr-2"></i> Kích hoạt:</td>
                                <td class="text-right">
                                    @if($projectCategory->active_status == 1)<span class="badge badge-success">bật</span>
                                        @else <span class="badge badge-danger">tắt</span> @endif
                                </td>
                            </tr>
                            <tr>
                                <td><i class="icon-alarm-add mr-2"></i> Cập nhật:</td>
                                <td class="text-right text-muted">{{date('H:i - d/m/Y', strtotime($projectCategory->updated_at))}}</td>
                            </tr>
                            <tr>
                                <td><i class="icon-alarm-check mr-2"></i> Tạo mới:</td>
                                <td class="text-right text-muted">{{date('H:i - d/m/Y', strtotime($projectCategory->created_at))}}</td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="card-footer d-flex align-items-center">
                        <ul class="list-inline list-inline-condensed mb-0">
                            <li class="list-inline-item">
                                <a href="javascript:void(0)" onclick="form_action('edit', {{$projectCategory->project_category_id}});" role="button"
                                    data-popup="tooltip" title="Cập Nhập" data-placement="bottom"
                                    class="text-default"><i class="icon-pencil7"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript:void(0)" onclick="form_action('delete', {{$projectCategory->project_category_id}});" role="button"
                                    data-popup="tooltip" title="Xóa" data-placement="bottom"
                                    class="text-default"><i class="icon-bin"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="{{ route('admins.procategory.index') }}"
                                    data-popup="tooltip" title="Trở về danh sách" data-placement="bottom"
                                    class="text-default"><i class="icon-reply"></i></a>
                            </li>
                        </ul>

                    </div>
                </div>
                <!-- /task details -->

            </div>
            <!-- /sidebar content -->

        </div>
        <!-- /right sidebar component -->

    </div>
    <!-- /inner container -->

    <form name="form1" action="" method="">
        {{csrf_field()}}
        <div class="form-group ">
            <input type="hidden" class="form-control" name="project_category_id" value="">
        </div>
    </form>
@stop

@section('javascript')


<script src="{{ asset('asset/admins/js/plugins/notifications/sweet_alert.min.js')}}"></script>
{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{ asset('asset/admins/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
<script>
    function form_action (mode, project_category_id) {
       let frm = document.form1;
        frm.project_category_id.value = project_category_id
       if(mode == 'edit'){
           frm.action = "{{route('admins.procategory.edit')}}";
           frm.method = 'get';
        frm.submit();
       }else if(mode == 'view'){
           frm.action = "{{route('admins.procategory.show')}}";
           frm.method = 'get';
           frm.submit();
       }else if(mode == 'delete'){

            // Defaults
            var swalInit = swal.mixin({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-light'
            });
             swalInit.fire({
                title: 'Bạn có chắc muốn xóa đối tượng này?',
                text: "Bạn không thể hoàn tác lại thao tác này!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Vâng, Xóa đối tượng!',
                cancelButtonText: 'Thoát',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function(result) {
                if (result.value) {
                    frm.action = "{{route('admins.procategory.delete')}}";
                    frm.method = 'post';
                    frm.submit();
                }
            });
        }
    }

    function initialSetup() {
       $hidden = $(".notifyHidden");
        if ($hidden != null) {
            $hidden.delay(3000).fadeOut('slow');
        }
    }
    // Initialize module
    // ------------------------------
    document.addEventListener('DOMContentLoaded', function() {
        initialSetup();
    });
</script>

<script src="{{ asset('asset/admins/js/app.js') }}"></script>
<script src="{{ asset('asset/admins/js/pages/components_popups.js')}}"></script>
<!-- /theme JS files -->

@stop


