@extends('layouts.admin_layout')
@section('title', 'Tuyển Dụng')

@section('topnavdetail')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i>
            <span class="font-weight-semibold">Tuyển Dụng</span> - Tạo mới</h4>
        <a href="{{ route('admins.webhire.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.webhire.index') }}" class="breadcrumb-item"> Danh sách</a>
        <span class="breadcrumb-item active">Tạo mới</span>
    </div>
    <a href="{{ route('admins.webhire.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
        <h5>{{session('success')}}</h5>
        <br>
        <a href="{{ route('admins.webhire.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
    </div>
    @endif

    @if(count($errors) > 0)
         <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
            @foreach($errors->all() as $error)
            <h5>{{$error}}</h5>
            @endforeach
            <br>
            <a href="{{ route('admins.webhire.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
        </div>
    @endif
    <!-- /Notifications Set  -->
    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Tạo Mới</strong>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="reload"></a>
                </div>
            </div>
        </div>

        <div class="card-body">

            <form action="{{ route('admins.webhire.store') }}" method="POST" enctype="multipart/form-data"  name="web-hire-form" >
                {{csrf_field()}}
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Thông Tin Cơ Bản</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_hire_name">Tiêu đề<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="200" placeholder="Nhập vào tiêu đề"  id="web_hire_name" name="web_hire_name" value="{{ old('web_hire_name', $webHire->web_hire_name) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="file-input">Hình ảnh<span class="text-danger">(*1024x450)</span>:</label>
                        <div class="col-lg-10">
                            <input type="file" class="file-input" id="file_input" name="file_input" data-show-caption="false" data-show-upload="false" data-fouc>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_hire_description">Mô tả<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <textarea rows="6" class="form-control" maxlength="500" id="web_hire_description" name="web_hire_description" placeholder="Nhập vào mô tả">{{ old('web_hire_description', $webHire->web_hire_description) }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2" for="web_hire_detail">Chi Tiết<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <textarea class="form-control" id="web_hire_detail" name="web_hire_detail" >
                                {{ old('web_hire_detail', $webHire->web_hire_detail) }}
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="active_status">Trạng Thái:</label>
                        <div class="col-lg-10">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" id="active_status" name="active_status" class="form-check-input-styled-primary" value="1"
                                    @if(old('active_status', $webHire->active_status)!=null && old('active_status', $webHire->active_status) == 1)
                                        checked
                                    @endif data-fouc>
                                    Kích Hoạt
                                </label>
                            </div>
                        </div>
                    </div>

                </fieldset>


                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Tối Ưu Quảng Cáo</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_title">Tiêu đề Website:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="20" placeholder="Nhập vào tiêu đề hiển thị website" id="web_title" name="web_title" value="{{ old('web_title', $webHire->web_title) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_keywords">Từ khóa tìm kiếm:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control tokenfield" id="web_keywords" placeholder="Từ khóa" maxlength="200" name="web_keywords"  value="{{ old('web_keywords', $webHire->web_keywords) }}" data-fouc>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_description">Mô Tả Website:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="145" placeholder="Nhập vào mô tả website"  id="web_description" name="web_description" value="{{ old('web_description', $webHire->web_description) }}">
                        </div>
                    </div>
                </fieldset>

                <div class="text-center">
                    <button type="submit" class="btn btn-primary"><i class="icon-download4 mr-2"> Lưu Lại</i></button>
                    <a href="{{ route('admins.webhire.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở Về Danh Sách</a>
                </div>
            </form>
        </div>
    </div>
    <!-- /restore column visibility -->

@stop

@section('javascript')

	<!-- Theme JS files -->
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tagsinput.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tokenfield.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/ui/prism.min.js')}}"></script>

	<script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
	<script src="{{ asset('asset/admins/js/plugins/forms/styling/switch.min.js')}}"></script>

	{{-- <script src="{{ asset('asset/admins/js/plugins/uploaders/fileinput/plugins/purify.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/uploaders/fileinput/plugins/sortable.min.js')}}"></script> --}}
	<script src="{{ asset('asset/admins/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>
    <script src="{{ asset('asset/admins/js/plugins/editors/ckeditor/ckeditor.js')}}"></script>

    <script src="{{ asset('asset/admins/js/plugins/forms/validation/validate.min.js') }}"></script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_tags_input.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_checkboxes_radios.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/uploader_bootstrap.js')}}"></script>
	<!-- /theme JS files -->

    <script>
        function initialSetup() {
           $hidden = $(".notifyHidden");
            if ($hidden != null) {
                $hidden.delay(10000).fadeOut('slow');
            }
        }


        $( document ).ready( function () {
            initialSetup();
            CKEDITOR.replace( 'web_hire_detail' );
            $( "form[name=web-hire-form]" ).validate( {
                rules: {
                    web_hire_name: {
                        required: true,
                        maxlength: 200
                    },
                    web_hire_description: {
                        required: true,
                        maxlength: 500
                    },
                    web_hire_detail: {
                        required: true,
                    }
                },
                messages: {
                    web_hire_name: {
                        required: "Hãy nhập vào tiêu đề.",
                        maxlength: "Bạn chỉ được nhập tối đa 200 ký tự."
                    },
                    web_hire_description: {
                        required: "Hãy nhập vào mô tả.",
                        maxlength: "Bạn chỉ được nhập tối đa 500 ký tự."
                    },
                    web_hire_detail: {
                        required: "Hãy nhập vào chi tiết.",
                    }
                },
                errorElement: "span",
                errorPlacement: function ( error, element ) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("form-text").addClass( "text-danger" );

                    if ( element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( element.next( "label" ) );
                    } else {
                        error.insertAfter( element );
                    }
                },
                highlight: function ( element, errorClass, validClass ) {
                    $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
                },
                unhighlight: function (element, errorClass, validClass) {
                    $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
                }
            } );

        } );
    </script>
@stop
