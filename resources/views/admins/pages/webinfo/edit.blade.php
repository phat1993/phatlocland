@extends('layouts.admin_layout')
@section('title', 'Thông Tin Web')

@section('topnavdetail')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i>
            <span class="font-weight-semibold">Thông Tin Web</span> - Cập nhật</h4>
        <a href="{{ route('admins.webinfo.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.webinfo.index') }}" class="breadcrumb-item"> Danh sách</a>
        <span class="breadcrumb-item active">Cập nhật</span>
    </div>
    <a href="{{ route('admins.webinfo.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
        <h5>{{session('success')}}</h5>
        <br>
        <a href="{{ route('admins.webinfo.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
    </div>
    @endif

    @if(count($errors) > 0)
         <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
            @foreach($errors->all() as $error)
            <h5>{{$error}}</h5>
            @endforeach
            <br>
            <a href="{{ route('admins.webinfo.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
        </div>
    @endif
    <!-- /Notifications Set  -->
    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Cập Nhật</strong>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="reload"></a>
                </div>
            </div>
        </div>
        <div class="card-body">

            <form action="{{ route('admins.webinfo.update') }}" method="POST" name="web-info-form" >
                {{csrf_field()}}
                <div class="form-group">
                    <input type="hidden" class="form-control" name="web_info_id" value="{{$webInfo->web_info_id}}">
                </div>
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Thông Tin Cơ Bản</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_info_title">Tiêu đề<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào tiêu đề"  id="web_info_title" name="web_info_title" value="{{ old('web_info_title', $webInfo->web_info_title) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_info_phone">Điện Thoại<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="50" placeholder="Nhập vào điện thoại"  id="web_info_phone" name="web_info_phone" value="{{ old('web_info_phone', $webInfo->web_info_phone) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_info_mail">Mail<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="50" placeholder="Nhập vào mail"  id="web_info_mail" name="web_info_mail" value="{{ old('web_info_mail', $webInfo->web_info_mail) }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_info_address">Địa Chỉ<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="200" placeholder="Nhập vào địa chỉ"  id="web_info_address" name="web_info_address" value="{{ old('web_info_address', $webInfo->web_info_address) }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="active_status">Trạng Thái:</label>
                        <div class="col-lg-10">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" id="active_status" name="active_status" class="form-check-input-styled-primary" value="1"
                                    @if(old('active_status', $webInfo->active_status)!=null && old('active_status', $webInfo->active_status) == 1)
                                        checked
                                    @endif data-fouc>
                                    Kích Hoạt
                                </label>
                            </div>
                        </div>
                    </div>

                </fieldset>

                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Chi Tiết</legend>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_info_special_note">Thông Điệp<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <textarea rows="3" class="form-control" maxlength="500" id="web_info_special_note" name="web_info_special_note" placeholder="Nhập vào thông điệp">{{ old('web_info_special_note', $webInfo->web_info_special_note) }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_info_work_time">Giờ Làm Việc<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <textarea type="text" id="web_info_work_time" name="web_info_work_time" maxlength="1000">
                                {{ old('web_info_work_time', $webInfo->web_info_work_time) }}
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_info_map_location">Vị Trí Map<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <input type="text" class="form-control border-right-0" maxlength="5000" placeholder="Nhập vào đường dẫn map" id="web_info_map_location" name="web_info_map_location" value="{{ old('web_info_map_location', $webInfo->web_info_map_location) }}">
                                <span class="input-group-append">
                                    <button type="button" class="btn bg-teal" onclick="changeSource()" data-toggle="modal" data-target="#exampleModal">
                                        <i class="icon-map5"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Tối Ưu Quảng Cáo</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_title">Tiêu đề Website<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="20" placeholder="Nhập vào tiêu đề hiển thị website" id="web_title" name="web_title" value="{{ old('web_title', $webInfo->web_title) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_keywords">Từ khóa tìm kiếm<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control tokenfield" id="web_keywords" placeholder="Từ khóa" maxlength="200" name="web_keywords"  value="{{ old('web_keywords', $webInfo->web_keywords) }}" data-fouc>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_canonical">Đường dẫn quảng cáo<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="145" placeholder="Nhập vào mô tả website"  id="web_canonical" name="web_canonical" value="{{ old('web_canonical', $webInfo->web_canonical) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_canonical">Mô Tả Website<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="200" placeholder="Nhập vào mô tả website"  id="web_description" name="web_description" value="{{ old('web_description', $webInfo->web_description) }}">
                        </div>
                    </div>
                </fieldset>
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Liên Kết</legend>
                    {{-- <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_info_zalo">Zalo:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="300" placeholder="Nhập vào đường dẫn zalo" id="web_info_zalo" name="web_info_zalo" value="{{ old('web_info_zalo', $webInfo->web_info_zalo) }}">
                        </div>
                    </div> --}}
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_info_facebook">Facebook:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="300" placeholder="Nhập vào đường dẫn facebook" id="web_info_facebook" name="web_info_facebook" value="{{ old('web_info_facebook', $webInfo->web_info_facebook) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_info_youtube">Youtube:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="300" placeholder="Nhập vào đường dẫn youtube"  id="web_info_youtube" name="web_info_youtube" value="{{ old('web_info_youtube', $webInfo->web_info_youtube) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_info_instagram">Instagram:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="300" placeholder="Nhập vào đường dẫn instagram"  id="web_info_instagram" name="web_info_instagram" value="{{ old('web_info_instagram', $webInfo->web_info_instagram) }}">
                        </div>
                    </div>
                </fieldset>

                <div class="text-center">
                    <button type="submit" class="btn btn-primary"><i class="icon-download4 mr-2"> Lưu Lại</i></button>
                    <a href="{{ route('admins.webhire.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở Về Danh Sách</a>
                </div>
            </form>
        </div>
    </div>
    <!-- /restore column visibility -->

    <div class="modal modal-info fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <!-- Map Item -->
                        <div class="col-sm-12">
                            <div class="widget-area-2 lorvens-box-shadow pb-3">
                                <h3 class="widget-title">Xem bản đồ</h3>
                                <div class="map-box">
                                    <iframe id='frameMap'
                                    width="100%" height="500px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                                </div>
                                <hr/>
                                <button type="button" class="btn btn-secondary" style="float: right" data-dismiss="modal">Đóng</button>
                                <br>
                                <br>
                            </div>
                        </div>
                        <!-- /Map Item -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')

	<!-- Theme JS files -->
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tagsinput.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tokenfield.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/ui/prism.min.js')}}"></script>

	<script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
	<script src="{{ asset('asset/admins/js/plugins/forms/styling/switch.min.js')}}"></script>

    <script src="{{ asset('asset/admins/js/plugins/editors/ckeditor/ckeditor.js')}}"></script>
    <script src="{{ asset('asset/admins/js/plugins/forms/validation/validate.min.js') }}"></script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_tags_input.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_checkboxes_radios.js')}}"></script>
	<!-- /theme JS files -->

    <script>
        function initialSetup() {
           $hidden = $(".notifyHidden");
            if ($hidden != null) {
                $hidden.delay(10000).fadeOut('slow');
            }
        }


        function changeSource() {
            var link = $("input[name='web_info_map_location'").val();
            $('#frameMap').attr('src',  link);
        }
        $( document ).ready( function () {
            initialSetup();
            CKEDITOR.replace( 'web_info_work_time',
                {
                    height: '100px',
                    toolbar:[
                        { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source' ] },
                        { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
                        { name: 'editing', groups: [ 'find', 'selection'], items: [ 'Find', 'Replace', '-', 'SelectAll' ] },
                        { name: 'insert', items: [ 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar'] },
                        '/',
                        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
                        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
                        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
                    ],
                    toolbarGroups:[],
                    removePlugins : 'easyimage, cloudservices, exportpdf'
                }
             );
            $( "form[name=web-info-form]" ).validate( {
                rules: {
                    web_info_title: {
                        required: true,
                        maxlength: 100
                    },
                    web_info_phone: {
                        required: true,
                        maxlength: 50
                    },
                    web_info_mail: {
                        required: true,
                        email: true,
                        maxlength: 50
                    },
                    web_info_address: {
                        required: true,
                        maxlength: 200
                    },
                    web_info_work_time: {
                        required: true,
                        maxlength: 1000
                    },
                    web_info_map_location: {
                        required: true,
                        maxlength: 5000
                    },

                    web_info_special_note: {
                        required: true,
                        maxlength: 500
                    },
                    web_title: {
                        required: true,
                        maxlength: 20
                    },
                    web_keywords: {
                        required: true,
                        maxlength: 200
                    },
                    web_description: {
                        required: true,
                        maxlength: 145
                    },
                    web_canonical: {
                        required: true,
                        maxlength: 200
                    },

                },
                messages: {
                    web_info_title: {
                        required: "Hãy nhập vào tiêu đề.",
                        maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    },
                    web_info_phone: {
                        required: "Hãy nhập vào số điện thoại.",
                        maxlength: "Bạn chỉ được nhập tối đa 50 ký tự."
                    },
                    web_info_mail: {
                        required: "Hãy nhập vào email.",
                        email: "Hãy nhập vào chính xác địa chỉ email.",
                        maxlength: "Bạn chỉ được nhập tối đa 50 ký tự."
                    },
                    web_info_address: {
                        required: "Hãy nhập vào địa chỉ.",
                        maxlength: "Bạn chỉ được nhập tối đa 200 ký tự."
                    },
                    web_info_work_time: {
                        required: "Hãy nhập vào giờ làm việc.",
                        maxlength: "Bạn chỉ được nhập tối đa 1000 ký tự."
                    },
                    web_info_map_location: {
                        required: "Hãy nhập vào đường dẫn map.",
                        maxlength: "Bạn chỉ được nhập tối đa 5000 ký tự."
                    },

                    web_info_special_note: {
                        required: "Hãy nhập vào thông điệp.",
                        maxlength: "Bạn chỉ được nhập tối đa 500 ký tự."
                    },
                    web_title: {
                        required: "Hãy nhập vào tiêu đề website.",
                        maxlength: "Bạn chỉ được nhập tối đa 20 ký tự."
                    },
                    web_keywords: {
                        required: "Hãy nhập vào từ khóa tìm kiếm.",
                        maxlength: "Bạn chỉ được nhập tối đa 200 ký tự."
                    },
                    web_description: {
                        required: "Hãy nhập vào mô tả website.",
                        maxlength: "Bạn chỉ được nhập tối đa 145 ký tự."
                    },
                    web_canonical: {
                        required: "Hãy nhập vào đường dẫn quảng cáo.",
                        maxlength: "Bạn chỉ được nhập tối đa 200 ký tự."
                    },

                },
                errorElement: "span",
                errorPlacement: function ( error, element ) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("form-text").addClass( "text-danger" );

                    if ( element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( element.next( "label" ) );
                    } else {
                        error.insertAfter( element );
                    }
                },
                highlight: function ( element, errorClass, validClass ) {
                    $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
                },
                unhighlight: function (element, errorClass, validClass) {
                    $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
                }
            } );

        } );
    </script>
@stop
