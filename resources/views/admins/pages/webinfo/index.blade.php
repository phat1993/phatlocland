@extends('layouts.admin_layout')
@section('title', 'Tuyển Dụng')

@section('topnavdetail')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i>
            <span class="font-weight-semibold">Tuyển Dụng</span> - Danh sách</h4>
        <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <span class="breadcrumb-item active">Tuyển Dụng</span>
    </div>
    <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h6 class="alert-heading font-weight-semibold mb-1">Thông Báo</h6>
        <span>{{session('success')}}</span>
    </div>
    @endif

    @if(count($errors) > 0)
        <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h6 class="alert-heading font-weight-semibold mb-1">Thông Báo</h6>
            @foreach($errors->all() as $error)
            <p>{{$error}}</p>
            @endforeach
        </div>
    @endif
    <!-- /Notifications Set  -->

    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Tìm Kiếm</strong>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form action="{{route('admins.webinfo.index')}}" method="get">
                {{csrf_field()}}
                <div class="form-group">
                    <div class="row align-items-center">
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="web_info_title" value="{{Request::get('web_info_title')}}" placeholder="Tìm kiếm theo tiêu đề">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="web_info_phone" value="{{Request::get('web_info_phone')}}" placeholder="Tìm kiếm theo điện thoại">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="web_info_mail" value="{{Request::get('web_info_mail')}}" placeholder="Tiềm kiếm theo email">
                        </div>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="web_info_address" value="{{Request::get('web_info_address')}}" placeholder="Tiềm kiếm theo địa chỉ">
                        </div>
                        <div class="col-sm-1">
                            <select class="form-control" name='active_status'>
                                <option value="0" {{Request::get('active_status') == 0 ? 'selected': ''}}>Kích hoạt</option>
                                <option value="1" {{Request::get('active_status') == 1 ? 'selected': ''}}>Bật</option>
                                <option value="2" {{Request::get('active_status') == 2 ? 'selected': ''}}>Tắt</option>
                            </select>
                        </div>
                        <div class="col-sm-1">
                            <input type="submit" class="btn btn-primary btn-lg active" value="Tìm Kiếm">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /restore column visibility -->
    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Danh Sách</strong>
                <a href="{{ route('admins.webinfo.create') }}" class="btn btn-outline-danger"><i class="icon-plus-circle2 mr-2"></i> Thêm</a>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <table class="table datatable-colvis-restore">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tiêu đề</th>
                        <th>Điện Thoại</th>
                        <th>Email</th>
                        <th>Địa chỉ</th>
                        <th>Kích Hoạt</th>
                        <th class="text-right">Chức Năng</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $idx = $perPage*($currentPage-1); ?>
                    @forelse ($webInfos as $webInfo)
                        <tr>
                            <?php $idx++; ?>
                            <td>{{$idx}}</td>
                            <td>{{$webInfo->web_info_title}}</td>
                            <td>{{$webInfo->web_info_phone}}</td>
                            <td>{{$webInfo->web_info_mail}}</td>
                            <td>{{$webInfo->web_info_address}}</td>
                            <td>@if($webInfo->active_status == 1)<span class="badge badge-success">BẬT</span>  @else <span class="badge badge-danger">TẮT</span> @endif </td>
                            <td class="text-right">
                                <div class="list-icons">
                                    <a href="javascript:void(0)" onclick="form_action('view', {{$webInfo->web_info_id}});" role="button" data-popup="tooltip" title="Chi Tiết" data-placement="bottom" class="list-icons-item text-info-600"><i class="icon-file-eye2"></i></a>
                                    <a href="javascript:void(0)" onclick="form_action('edit', {{$webInfo->web_info_id}});" role="button" data-popup="tooltip" title="Cập Nhập" data-placement="bottom" class="list-icons-item text-warning-600"><i class="icon-pencil7"></i></a>
                                    <a  href="javascript:void(0)" onclick="form_action('delete', {{$webInfo->web_info_id}});" role="button" data-popup="tooltip" title="Xóa" data-placement="bottom" class="list-icons-item text-danger-600"><i class="icon-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        @empty
                    @endforelse
                </tbody>
            </table>
        </div>


        {{ $webInfos->appends([
            'web_info_title' => Request::get('web_info_title'),
            'web_info_phone' => Request::get('web_info_phone'),
            'web_info_mail' => Request::get('web_info_mail'),
            'web_info_address' => Request::get('web_info_address'),
            'active_status' => Request::get('active_status'),
        ])->links('admins.includes.pagination') }}
    </div>
    <!-- /restore column visibility -->



    <form name="form1" action="" method="">
        {{csrf_field()}}
        <div class="form-group ">
            <input type="hidden" class="form-control" name="web_info_id" value="">
        </div>
    </form>
@stop

@section('javascript')
	<!-- Theme JS files -->
	<script src="{{ asset('asset/admins/js/plugins/tables/datatables/datatables.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/selects/select2.min.js')}}"></script> --}}
	{{-- <script src="{{ asset('asset/admins/js/pages/datatables_extension_colvis.js')}}"></script> --}}


	<script src="{{ asset('asset/admins/js/plugins/notifications/sweet_alert.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
    <script>
        var DatatableColumnVisibility = function() {
            var _componentDatatableColumnVisibility = function() {
                if (!$().DataTable) {
                    console.warn('Warning - datatables.min.js is not loaded.');
                    return;
                }

                // Setting datatable defaults
                $.extend($.fn.dataTable.defaults, {
                    paging: false,
                    autoWidth: false,
                    dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Tìm kiếm:</span> _INPUT_',
                        searchPlaceholder: 'Nhập từ khóa...',
                        lengthMenu: '<span>Hiển thị:</span> _MENU_',
                        paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                    }
                });

                // Restore column visibility
                $('.datatable-colvis-restore').DataTable({
                    buttons: [{
                        extend: 'colvis',
                        text: '<i class="icon-grid7"></i>',
                        className: 'btn bg-teal-400 btn-icon dropdown-toggle',
                        postfixButtons: ['colvisRestore']
                    }],
                    columnDefs: [
                        {targets: -1, orderable: false,},
                    ]
                });

            };

            return {
                init: function() {
                    _componentDatatableColumnVisibility();
                }
            }
        }();

        function form_action (mode, web_info_id) {
           let frm = document.form1;
            frm.web_info_id.value = web_info_id
           if(mode == 'edit'){
               frm.action = "{{route('admins.webinfo.edit')}}";
               frm.method = 'get';
            frm.submit();
           }else if(mode == 'view'){
               frm.action = "{{route('admins.webinfo.show')}}";
               frm.method = 'get';
               frm.submit();
           }else if(mode == 'delete'){

                // Defaults
                var swalInit = swal.mixin({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-light'
                });
                 swalInit.fire({
                    title: 'Bạn có chắc muốn xóa đối tượng này?',
                    text: "Bạn không thể hoàn tác lại thao tác này!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Vâng, Xóa đối tượng!',
                    cancelButtonText: 'Thoát',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function(result) {
                    if (result.value) {
                        frm.action = "{{route('admins.webinfo.delete')}}";
                        frm.method = 'post';
                        frm.submit();
                    }
                });
            }
        }

        function initialSetup() {
           $hidden = $(".notifyHidden");
            if ($hidden != null) {
                $hidden.delay(3000).fadeOut('slow');
            }
        }

        // Initialize module
        // ------------------------------
        document.addEventListener('DOMContentLoaded', function() {
            DatatableColumnVisibility.init();
            initialSetup();
        });
    </script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<script src="{{ asset('asset/admins/js/pages/components_popups.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/pages/extra_sweetalert.js')}}"></script> --}}
	<!-- /theme JS files -->

@stop
