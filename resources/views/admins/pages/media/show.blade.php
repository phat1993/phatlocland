@extends('layouts.admin_layout')
@section('title', 'Thư Viện')

@section('topnavdetail')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i>
            <span class="font-weight-semibold">Thư Viện</span> - Chi tiết</h4>
        <a href="{{ route('admins.mediasupport.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.mediasupport.index') }}" class="breadcrumb-item"> Danh sách</a>
        <span class="breadcrumb-item active">Chi tiết</span>
    </div>
    <a href="{{ route('admins.mediasupport.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection

@section('content')


    <!-- Inner container -->
    <div class="d-flex align-items-start flex-column flex-md-row">

        <!-- Left content -->
        <div class="w-100 overflow-auto order-2 order-md-1">

            <!-- Course overview -->
            <div class="card">
                <div class="card-header header-elements-md-inline">
                    <h5 class="card-title">{{ $mediaSupport->media_support_name }}</h5>
                </div>

                <div class="nav-tabs-responsive bg-light border-top">
                    <ul class="nav nav-tabs nav-tabs-bottom flex-nowrap mb-0">
                        <li class="nav-item"><a href="#course-overview" class="nav-link active" data-toggle="tab"><i class="icon-menu7 mr-2"></i> Thông Tin</a></li>

                    </ul>
                </div>

                <div class="tab-content">
                        <div class="tab-pane fade show active" id="course-overview">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <th width="20%" scope="col">Tiêu Đề</th>
                                            <td width="80%" scope="col" colspan="2">{{$mediaSupport->media_support_name}}</td>
                                        </tr>
                                        <tr>
                                            <th scope="col">Hình Ảnh</th>
                                            {{-- File Hinh Anh --}}
                                            @if ($mediaSupport->media_support_extension == 'jpg'||$mediaSupport->media_support_extension == 'jpeg'
                                            ||$mediaSupport->media_support_extension == 'png'||$mediaSupport->media_support_extension == 'gif')
                                            <td scope="col" colspan="2"><img src="{{ asset('storage/'.$mediaSupport->media_support_url) }}" alt="{{ $mediaSupport->media_support_name }}" width="100" height="100"></td>
                                            {{-- File powerpoint --}}
                                            @elseif ($mediaSupport->media_support_extension == 'pptx'||$mediaSupport->media_support_extension == 'pptm'
                                            ||$mediaSupport->media_support_extension == 'ppt'||$mediaSupport->media_support_extension == 'ppsx'||$mediaSupport->media_support_extension == 'ppsm')
                                            <td scope="col" colspan="2"><i class="icon-file-presentation display-1"></i></td>
                                            {{-- File Excel --}}
                                            @elseif ($mediaSupport->media_support_extension == 'xltx'||$mediaSupport->media_support_extension == 'xltm'
                                            ||$mediaSupport->media_support_extension == 'xlt'||$mediaSupport->media_support_extension == 'xlsx'||$mediaSupport->media_support_extension == 'xlsm')
                                            <td scope="col" colspan="2"><i class="icon-file-spreadsheet display-1"></i></td>
                                            {{-- File pdf --}}
                                            @elseif ($mediaSupport->media_support_extension == 'pdf')
                                            <td scope="col" colspan="2"><i class="icon-file-pdf display-1"></i></td>
                                            {{-- File zip --}}
                                            @elseif ($mediaSupport->media_support_extension == 'zip'||$mediaSupport->media_support_extension == 'rar')
                                            <td scope="col" colspan="2"><i class="icon-file-zip display-1"></i></td>
                                            {{-- File music --}}
                                            @elseif ($mediaSupport->media_support_extension == 'mp3')
                                            <td scope="col" colspan="2"><i class="icon-file-music display-1"></i></td>
                                            {{-- File video --}}
                                            @elseif ($mediaSupport->media_support_extension == 'mp4')
                                            <td scope="col" colspan="2"><i class="icon-file-video display-1"></i></td>
                                            @else
                                            <td scope="col" colspan="2"><i class="icon-file-check display-1"></i></td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <th scope="col">Đường Dẫn</th>
                                            <td width="80%"><span id="cpLink{{$mediaSupport->media_support_id}}">{{ asset('storage/'.$mediaSupport->media_support_url) }}</span></td>
                                            <td width="5%"><button class="btn btn-outline-dark" onclick="copyToClipboard({{$mediaSupport->media_support_id}})"><i class="icon-copy3"></i></button></td>
                                        </tr>
                                        <tr>
                                            <th scope="col">Mô Tả</th>
                                            <td scope="col" colspan="2">{{$mediaSupport->media_support_description}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- /course overview -->

        </div>
        <!-- /left content -->


        <!-- Right sidebar component -->
        <div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right wmin-350 border-0 shadow-0 order-1 order-md-2 sidebar-expand-md">

            <!-- Sidebar content -->
            <div class="sidebar-content">
                <!-- Task details -->
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="card-title font-weight-semibold">Cơ bản</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>

                    <table class="table table-borderless table-xs my-2">
                        <tbody>
                            <tr>
                                <td><i class="icon-user-tie mr-2"></i> Quản trị viên:</td>
                                <td class="text-right"><a href="#">{{ $mediaSupport->admin_full_name }}</a></td>
                            </tr>
                            <tr>
                                <td><i class="icon-alarm-add mr-2"></i> Cập nhật:</td>
                                <td class="text-right text-muted">{{date('H:i - d/m/Y', strtotime($mediaSupport->updated_at))}}</td>
                            </tr>
                            <tr>
                                <td><i class="icon-alarm-check mr-2"></i> Chi tiết:</td>
                                <td class="text-right text-muted">{{date('H:i - d/m/Y', strtotime($mediaSupport->created_at))}}</td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="card-footer d-flex align-items-center">
                        <ul class="list-inline list-inline-condensed mb-0">
                            <li class="list-inline-item">
                                <a href="javascript:void(0)" onclick="form_action('edit', {{$mediaSupport->media_support_id}});" role="button"
                                    data-popup="tooltip" title="Cập Nhập" data-placement="bottom"
                                    class="text-default"><i class="icon-pencil7"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript:void(0)" onclick="form_action('delete', {{$mediaSupport->media_support_id}});" role="button"
                                    data-popup="tooltip" title="Xóa" data-placement="bottom"
                                    class="text-default"><i class="icon-bin"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="{{ route('admins.mediasupport.index') }}"
                                    data-popup="tooltip" title="Trở về danh sách" data-placement="bottom"
                                    class="text-default"><i class="icon-reply"></i></a>
                            </li>
                        </ul>

                    </div>
                </div>
                <!-- /task details -->

            </div>
            <!-- /sidebar content -->

        </div>
        <!-- /right sidebar component -->

    </div>
    <!-- /inner container -->

    <form name="form1" action="" method="">
        {{csrf_field()}}
        <div class="form-group ">
            <input type="hidden" class="form-control" name="media_support_id" value="">
        </div>
    </form>
@stop

@section('javascript')


<script src="{{ asset('asset/admins/js/plugins/notifications/sweet_alert.min.js')}}"></script>
{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{ asset('asset/admins/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
<script>
    function form_action (mode, media_support_id) {
       let frm = document.form1;
        frm.media_support_id.value = media_support_id
       if(mode == 'edit'){
           frm.action = "{{route('admins.mediasupport.edit')}}";
           frm.method = 'get';
        frm.submit();
       }else if(mode == 'view'){
           frm.action = "{{route('admins.mediasupport.show')}}";
           frm.method = 'get';
           frm.submit();
       }else if(mode == 'delete'){

            // Defaults
            var swalInit = swal.mixin({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-light'
            });
             swalInit.fire({
                title: 'Bạn có chắc muốn xóa đối tượng này?',
                text: "Bạn không thể hoàn tác lại thao tác này!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Vâng, Xóa đối tượng!',
                cancelButtonText: 'Thoát',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function(result) {
                if (result.value) {
                    frm.action = "{{route('admins.mediasupport.destroy')}}";
                    frm.method = 'post';
                    frm.submit();
                }
            });
        }
    }

    function copyToClipboard(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($('#cpLink'+element).text()).select();
            document.execCommand("copy");
            $temp.remove();
        }
    function initialSetup() {
       $hidden = $(".notifyHidden");
        if ($hidden != null) {
            $hidden.delay(3000).fadeOut('slow');
        }
    }
    // Initialize module
    // ------------------------------
    document.addEventListener('DOMContentLoaded', function() {
        initialSetup();
    });
</script>

<script src="{{ asset('asset/admins/js/app.js') }}"></script>
<script src="{{ asset('asset/admins/js/pages/components_popups.js')}}"></script>
<!-- /theme JS files -->

@stop


