<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="{{ asset('asset/clients/images/logo-blue.png') }}" type="image/x-icon" />
	{{-- <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no"> --}}
	<title>PHÁT LỘC AD - Thư Viện</title>
</head>
<body>
@include('ckfinder::setup')
<script>
	var finder;

	CKFinder.start( {
		onInit: function( instance ) {
			finder = instance;
		},language: 'vi',
	} );
</script>

</body>
</html>
