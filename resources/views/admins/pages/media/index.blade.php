@extends('layouts.admin_layout')
@section('title', 'Thư Viện')

@section('topnavdetail')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i>
            <span class="font-weight-semibold">Thư Viện</span> - Danh sách</h4>
        <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <span class="breadcrumb-item active">Thư Viện</span>
    </div>
    <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h6 class="alert-heading font-weight-semibold mb-1">Thông Báo</h6>
        <span>{{session('success')}}</span>
    </div>
    @endif

    @if(count($errors) > 0)
        <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h6 class="alert-heading font-weight-semibold mb-1">Thông Báo</h6>
            @foreach($errors->all() as $error)
            <p>{{$error}}</p>
            @endforeach
        </div>
    @endif
    <!-- /Notifications Set  -->

    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Tìm Kiếm</strong>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form action="{{route('admins.mediasupport.index')}}" method="get">
                {{csrf_field()}}
                <div class="form-group">
                    <div class="row align-items-center">
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="media_support_name" value="{{Request::get('media_support_name')}}" placeholder="Tìm kiếm theo tiêu đề">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="media_support_size" value="{{Request::get('media_support_size')}}" placeholder="Tìm kiếm theo kích thước">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="media_support_extension" value="{{Request::get('media_support_extension')}}" placeholder="Tìm kiếm theo đuôi file">
                        </div>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="media_support_url" value="{{Request::get('media_support_url')}}" placeholder="Tìm kiếm theo đường dẫn">
                        </div>
                        <div class="col-sm-1">
                            <input type="submit" class="btn btn-primary btn-lg active" value="Tìm Kiếm">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /restore column visibility -->
    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Danh Sách</strong>
                <a href="{{ route('admins.mediasupport.create') }}" class="btn btn-outline-danger"><i class="icon-plus-circle2 mr-2"></i> Thêm</a>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">

        <table class="table datatable-colvis-restore">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Hình Ảnh</th>
                    <th>Tiêu Đề</th>
                    <th>Kích Thước</th>
                    <th>Extension</th>
                    <th>Đường Dẫn</th>
                    <th class="text-right">Chức Năng</th>
                </tr>
            </thead>
            <tbody>
                <?php $idx = $perPage*($currentPage-1); ?>
                @forelse ($mediaSupports as $media)
                    <tr>
                        <?php $idx++; ?>
                        <td>{{$idx}}</td>
                        {{-- File Hinh Anh --}}
                        @if ($media->media_support_extension == 'jpg'||$media->media_support_extension == 'jpeg'
                        ||$media->media_support_extension == 'png'||$media->media_support_extension == 'gif')
                        <td><img src="{{ asset('storage/'.$media->media_support_url) }}" alt="{{ $media->media_support_name }}" width="100" height="100"></td>
                        {{-- File powerpoint --}}
                        @elseif ($media->media_support_extension == 'pptx'||$media->media_support_extension == 'pptm'
                        ||$media->media_support_extension == 'ppt'||$media->media_support_extension == 'ppsx'||$media->media_support_extension == 'ppsm')
                        <td><i class="icon-file-presentation display-1"></i></td>
                        {{-- File Excel --}}
                        @elseif ($media->media_support_extension == 'xltx'||$media->media_support_extension == 'xltm'
                        ||$media->media_support_extension == 'xlt'||$media->media_support_extension == 'xlsx'||$media->media_support_extension == 'xlsm')
                        <td><i class="icon-file-spreadsheet display-1"></i></td>
                        {{-- File pdf --}}
                        @elseif ($media->media_support_extension == 'pdf')
                        <td><i class="icon-file-pdf display-1"></i></td>
                        {{-- File zip --}}
                        @elseif ($media->media_support_extension == 'zip'||$media->media_support_extension == 'rar')
                        <td><i class="icon-file-zip display-1"></i></td>
                        {{-- File music --}}
                        @elseif ($media->media_support_extension == 'mp3')
                        <td><i class="icon-file-music display-1"></i></td>
                        {{-- File video --}}
                        @elseif ($media->media_support_extension == 'mp4')
                        <td><i class="icon-file-video display-1"></i></td>
                        @else
                        <td><i class="icon-file-check display-1"></i></td>
                        @endif
                        <td>{{$media->media_support_name}}</td>
                        <td><span class="badge badge-danger">{{$media->media_support_size}}KB</span></td>
                        <td><span class="badge badge-warning">{{$media->media_support_extension}}</span></td>
                        <td><span id="cpLink{{$media->media_support_id}}">{{ asset('storage/'.$media->media_support_url) }}</span> <button class="btn btn-link" onclick="copyToClipboard({{$media->media_support_id}})"><i class="icon-copy3"></i></button></td>
                        <td class="text-right">
                            <div class="list-icons">
                                <a href="javascript:void(0)" onclick="form_action('view', {{$media->media_support_id}});" role="button" data-popup="tooltip" title="Chi Tiết" data-placement="bottom" class="list-icons-item text-info-600"><i class="icon-file-eye2"></i></a>
                                <a href="javascript:void(0)" onclick="form_action('edit', {{$media->media_support_id}});" role="button" data-popup="tooltip" title="Cập Nhập" data-placement="bottom" class="list-icons-item text-warning-600"><i class="icon-pencil7"></i></a>
                                <a  href="javascript:void(0)" onclick="form_action('delete', {{$media->media_support_id}});" role="button" data-popup="tooltip" title="Xóa" data-placement="bottom" class="list-icons-item text-danger-600"><i class="icon-trash"></i></a>
                            </div>
                        </td>
                    </tr>
                    @empty
                @endforelse
            </tbody>
        </table>
        </div>

        {{ $mediaSupports->appends([
            'media_support_name' => Request::get('media_support_name'),
            'media_support_size' => Request::get('media_support_size'),
            'media_support_extension' => Request::get('media_support_extension'),
            'media_support_url' => Request::get('media_support_url'),
        ])->links('admins.includes.pagination') }}
    </div>
    <!-- /restore column visibility -->



    <form name="form1" action="" method="">
        {{csrf_field()}}
        <div class="form-group ">
            <input type="hidden" class="form-control" name="media_support_id" value="">
        </div>
    </form>
@stop

@section('javascript')
	<!-- Theme JS files -->
	<script src="{{ asset('asset/admins/js/plugins/tables/datatables/datatables.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/selects/select2.min.js')}}"></script> --}}
	{{-- <script src="{{ asset('asset/admins/js/pages/datatables_extension_colvis.js')}}"></script> --}}


	<script src="{{ asset('asset/admins/js/plugins/notifications/sweet_alert.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
    <script>
        var DatatableColumnVisibility = function() {
            var _componentDatatableColumnVisibility = function() {
                if (!$().DataTable) {
                    console.warn('Warning - datatables.min.js is not loaded.');
                    return;
                }

                // Setting datatable defaults
                $.extend($.fn.dataTable.defaults, {
                    paging: false,
                    autoWidth: false,
                    dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Tìm kiếm:</span> _INPUT_',
                        searchPlaceholder: 'Nhập từ khóa...',
                        lengthMenu: '<span>Hiển thị:</span> _MENU_',
                        paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                    }
                });

                // Restore column visibility
                $('.datatable-colvis-restore').DataTable({
                    buttons: [{
                        extend: 'colvis',
                        text: '<i class="icon-grid7"></i>',
                        className: 'btn bg-teal-400 btn-icon dropdown-toggle',
                        postfixButtons: ['colvisRestore']
                    }],
                    columnDefs: [
                        {targets: -1, orderable: false,},
                    ]
                });

            };

            return {
                init: function() {
                    _componentDatatableColumnVisibility();
                }
            }
        }();
        function copyToClipboard(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($('#cpLink'+element).text()).select();
            document.execCommand("copy");
            $temp.remove();
        }
        function form_action (mode, media_support_id) {
           let frm = document.form1;
            frm.media_support_id.value = media_support_id
           if(mode == 'edit'){
               frm.action = "{{route('admins.mediasupport.edit')}}";
               frm.method = 'get';
            frm.submit();
           }else if(mode == 'view'){
               frm.action = "{{route('admins.mediasupport.show')}}";
               frm.method = 'get';
               frm.submit();
           }else if(mode == 'delete'){

                // Defaults
                var swalInit = swal.mixin({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-light'
                });
                 swalInit.fire({
                    title: 'Bạn có chắc muốn xóa đối tượng này?',
                    text: "Bạn không thể hoàn tác lại thao tác này!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Vâng, Xóa đối tượng!',
                    cancelButtonText: 'Thoát',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function(result) {
                    if (result.value) {
                        frm.action = "{{route('admins.mediasupport.destroy')}}";
                        frm.method = 'post';
                        frm.submit();
                    }
                });
            }
        }

        function initialSetup() {
           $hidden = $(".notifyHidden");
            if ($hidden != null) {
                $hidden.delay(3000).fadeOut('slow');
            }
        }

        // Initialize module
        // ------------------------------
        document.addEventListener('DOMContentLoaded', function() {
            DatatableColumnVisibility.init();
            initialSetup();
        });
    </script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<script src="{{ asset('asset/admins/js/pages/components_popups.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/pages/extra_sweetalert.js')}}"></script> --}}
	<!-- /theme JS files -->

@stop
