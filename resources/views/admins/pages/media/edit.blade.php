@extends('layouts.admin_layout')
@section('title', 'Thư Viện')

@section('topnavdetail')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i>
            <span class="font-weight-semibold">Thư Viện</span> - Cập nhật</h4>
        <a href="{{ route('admins.mediasupport.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.mediasupport.index') }}" class="breadcrumb-item"> Danh sách</a>
        <span class="breadcrumb-item active">Cập Nhật</span>
    </div>
    <a href="{{ route('admins.mediasupport.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
        <h5>{{session('success')}}</h5>
        <br>
        <a href="{{ route('admins.mediasupport.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
    </div>
    @endif

    @if(count($errors) > 0)
         <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
            @foreach($errors->all() as $error)
            <h5>{{$error}}</h5>
            @endforeach
            <br>
            <a href="{{ route('admins.mediasupport.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
        </div>
    @endif
    <!-- /Notifications Set  -->
    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Cập Nhật</strong>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="reload"></a>
                </div>
            </div>
        </div>
        @php
            $showPreview = '';
            if (Str::endsWith($mediaSupport->media_support_url, '.jpg')||Str::endsWith($mediaSupport->media_support_url, '.jpeg')
            ||Str::endsWith($mediaSupport->media_support_url, '.png')||Str::endsWith($mediaSupport->media_support_url, '.gif'))
            $showPreview = '<img src="'.asset('storage/'.$mediaSupport->media_support_url).'" class="kv-preview-data file-preview-image" alt="'. $mediaSupport->media_support_name.'">';

            elseif (Str::endsWith($mediaSupport->media_support_url, '.pptx')||Str::endsWith($mediaSupport->media_support_url, '.pptm')
            ||Str::endsWith($mediaSupport->media_support_url, '.ppt')||Str::endsWith($mediaSupport->media_support_url, '.ppsx')||Str::endsWith($mediaSupport->media_support_url, '.ppsm'))
            $showPreview = '<span class="kv-preview-data file-preview-image"><i class="icon-file-presentation display-1"></i></span>';

            elseif (Str::endsWith($mediaSupport->media_support_url, '.xltx')||Str::endsWith($mediaSupport->media_support_url, '.xltm')
            ||Str::endsWith($mediaSupport->media_support_url, '.xlt')||Str::endsWith($mediaSupport->media_support_url, '.xlsx')||Str::endsWith($mediaSupport->media_support_url, '.xlsm'))
            $showPreview = '<span class="kv-preview-data file-preview-image"><i class="icon-file-spreadsheet display-1"></i></span>';

            elseif (Str::endsWith($mediaSupport->media_support_url, '.pdf'))
            $showPreview = '<span class="kv-preview-data file-preview-image"><i class="icon-file-pdf display-1"></i></span>';

            elseif (Str::endsWith($mediaSupport->media_support_url, '.zip')||Str::endsWith($mediaSupport->media_support_url, '.rar'))
            $showPreview = '<span class="kv-preview-data file-preview-image"><i class="icon-file-zip display-1"></i></span>';

            elseif (Str::endsWith($mediaSupport->media_support_url, '.mp3'))
            $showPreview = '<span class="kv-preview-data file-preview-image"><i class="icon-file-music display-1"></i></span>';

            elseif (Str::endsWith($mediaSupport->media_support_url, '.mp4'))
            $showPreview = '<span class="kv-preview-data file-preview-image"><i class="icon-file-video display-1"></i></span>';

            else
            $showPreview = '<span class="kv-preview-data file-preview-image"><i class="icon-file-check display-1"></i></span>';
        @endphp

        <div class="card-body">
            <form action="{{ route('admins.mediasupport.update') }}" method="POST" enctype="multipart/form-data"  name="media-support-form" >
                {{csrf_field()}}
                <fieldset class="mb-3">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="media_support_id" value="{{$mediaSupport->media_support_id }}">
                        <input type="hidden" class="form-control" name="media_support_url" value="{{$mediaSupport->media_support_url}}">
                    </div>
                    <legend class="text-uppercase font-size-sm font-weight-bold">Thông Tin Cơ Bản</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="media_support_name">Tiêu Đề<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="50" placeholder="Nhập vào tiêu đề"  id="media_support_name" name="media_support_name" value="{{ old('media_support_name', $mediaSupport->media_support_name) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="file-input">File<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="file" class="file-input" id="file_input" name="file_input" data-show-caption="true" data-show-upload="false" data-fouc>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="media_support_description">Mô Tả:</label>
                        <div class="col-lg-10">
                            <textarea rows="6" class="form-control" maxlength="200" id="media_support_description" name="media_support_description" placeholder="Nhập vào mô tả">{{ old('media_support_description', $mediaSupport->media_support_description) }}</textarea>
                        </div>
                    </div>
                </fieldset>

                <div class="text-center">
                    <button type="submit" class="btn btn-primary"><i class="icon-download4 mr-2"> Lưu Lại</i></button>
                    <a href="{{ route('admins.mediasupport.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở Về Danh Sách</a>
                </div>
            </form>
        </div>
    </div>
    <!-- /restore column visibility -->

@stop

@section('javascript')

	<!-- Theme JS files -->}
	<script src="{{ asset('asset/admins/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>

    <script src="{{ asset('asset/admins/js/plugins/forms/validation/validate.min.js') }}"></script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<!-- /theme JS files -->

    <script>

        var FileUpload = function() {

            //
            // Setup module components
            //

            // Bootstrap file upload
            var _componentFileUpload = function() {
                if (!$().fileinput) {
                    console.warn('Warning - fileinput.min.js is not loaded.');
                    return;
                }

                //
                // Define variables
                //

                // Modal template
                var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
                    '  <div class="modal-content">\n' +
                    '    <div class="modal-header align-items-center">\n' +
                    '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
                    '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
                    '    </div>\n' +
                    '    <div class="modal-body">\n' +
                    '      <div class="floating-buttons btn-group"></div>\n' +
                    '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
                    '    </div>\n' +
                    '  </div>\n' +
                    '</div>\n';

                // Buttons inside zoom modal
                var previewZoomButtonClasses = {
                    toggleheader: 'btn btn-light btn-icon btn-header-toggle btn-sm',
                    fullscreen: 'btn btn-light btn-icon btn-sm',
                    borderless: 'btn btn-light btn-icon btn-sm',
                    close: 'btn btn-light btn-icon btn-sm'
                };

                // Icons inside zoom modal classes
                var previewZoomButtonIcons = {
                    prev: '<i class="icon-arrow-left32"></i>',
                    next: '<i class="icon-arrow-right32"></i>',
                    toggleheader: '<i class="icon-menu-open"></i>',
                    fullscreen: '<i class="icon-screen-full"></i>',
                    borderless: '<i class="icon-alignment-unalign"></i>',
                    close: '<i class="icon-cross2 font-size-base"></i>'
                };

                // File actions
                var fileActionSettings = {
                    zoomClass: '',
                    zoomIcon: '<i class="icon-zoomin3"></i>',
                    dragClass: 'p-2',
                    dragIcon: '<i class="icon-three-bars"></i>',
                    // removeClass: '',
                    removeErrorClass: 'text-danger',
                    // removeIcon: '<i class="icon-bin"></i>',
                    indicatorNew: '<i class="icon-file-plus text-success"></i>',
                    indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
                    indicatorError: '<i class="icon-cross2 text-danger"></i>',
                    indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
                };


                //
                // Basic example
                //

                $('.file-input').fileinput({
                    browseLabel: 'Chọn File',
                    browseIcon: '<i class="icon-file-plus mr-2"></i>',
                    uploadIcon: '<i class="icon-file-upload2 mr-2"></i>',
                    // removeIcon: '<i class="icon-cross2 font-size-base mr-2"></i>',
                    layoutTemplates: {
                        icon: '<i class="icon-file-check"></i>',
                        modal: modalTemplate
                    },
                    initialPreview: [
                        '{!! $showPreview !!}'
                    ],
                    showRemove:false,
                    initialPreviewAsData: false, // allows you to set a raw markup
                    initialPreviewFileType: 'image', // image is the default and can be overridden in config below
                    initialPreviewConfig: [
                        {caption: "{{$mediaSupport->media_support_name}}", size: {{$mediaSupport->media_support_size}}},//type: "image",
                    ],
                    initialCaption: "Không có file được chọn",
                    maxFileSize: 5120,
                    previewZoomButtonClasses: previewZoomButtonClasses,
                    previewZoomButtonIcons: previewZoomButtonIcons,
                    fileActionSettings: fileActionSettings
                });
            };


            //
            // Return objects assigned to module
            //

            return {
                init: function() {
                    _componentFileUpload();
                }
            }
        }();


        // Initialize module
        // ------------------------------

        document.addEventListener('DOMContentLoaded', function() {
            FileUpload.init();
        });

    </script>
    <script>
        function initialSetup() {
           $hidden = $(".notifyHidden");
            if ($hidden != null) {
                $hidden.delay(10000).fadeOut('slow');
            }
        }


        $( document ).ready( function () {
            initialSetup();
            $( "form[name=media-support-form]" ).validate( {
                rules: {
                    media_support_name: {
                        required: true,
                        maxlength: 50
                    },
                    media_support_description: {
                        maxlength: 200
                    }
                },
                messages: {
                    media_support_name: {
                        required: "Hãy nhập vào tiêu đề.",
                        maxlength: "Bạn chỉ được nhập tối đa 50 ký tự."
                    },
                    media_support_description: {
                        maxlength: "Bạn chỉ được nhập tối đa 200 ký tự."
                    }
                },
                errorElement: "span",
                errorPlacement: function ( error, element ) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("form-text").addClass( "text-danger" );

                    if ( element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( element.next( "label" ) );
                    } else {
                        error.insertAfter( element );
                    }
                },
                highlight: function ( element, errorClass, validClass ) {
                    $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
                },
                unhighlight: function (element, errorClass, validClass) {
                    $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
                }
            } );

        } );
    </script>
@stop
