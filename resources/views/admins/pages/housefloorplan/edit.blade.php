@extends('layouts.admin_layout')
@section('title', 'Bản vẽ kỹ thuật bất động sản')

@section('topnavdetail')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i>
            <span class="font-weight-semibold">Bản Vẽ Kỹ Thuật</span> <span class="text-primary">{{ $house->house_name }}</span>
            - cập nhật</h4>
        <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.house.index') }}" class="breadcrumb-item"><i class="icon-city mr-2"></i> Bất Động Sản</a>
        <a href="{{ route('admins.house.show', ['house_id'=>$house->house_id]) }}" class="breadcrumb-item"> {{ $house->house_name }}</a>
        <a href="{{ route('admins.housefloorplan.index', ['id'=>$house->house_id]) }}" class="breadcrumb-item"> Danh sách</a>
        <span class="breadcrumb-item active">Cập nhật</span>
    </div>
    <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
        <h5>{{session('success')}}</h5>
        <br>
        <a href="{{ route('admins.housefloorplan.index', ['id'=>$house->house_id]) }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
    </div>
    @endif

    @if(count($errors) > 0)
         <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
            @foreach($errors->all() as $error)
            <h5>{{$error}}</h5>
            @endforeach
            <br>
            <a href="{{ route('admins.housefloorplan.index', ['id'=>$house->house_id]) }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
        </div>
    @endif
    <!-- /Notifications Set  -->
    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Cập Nhật</strong>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="reload"></a>
                </div>
            </div>
        </div>

        <div class="card-body">

            <form action="{{ route('admins.housefloorplan.update', ['id'=>$house->house_id]) }}" method="POST" enctype="multipart/form-data"  name="house-form" >
                {{csrf_field()}}
                <fieldset class="mb-3">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="house_id" value="{{$floorPlan->house_id}}">
                        <input type="hidden" class="form-control" name="floor_plan_id" value="{{$floorPlan->floor_plan_id}}">
                        <input type="hidden" class="form-control" name="floor_plan_image" value="{{$floorPlan->floor_plan_image}}">
                    </div>
                    <legend class="text-uppercase font-size-sm font-weight-bold">Thông Tin Cơ Bản</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="floor_plan_name">Tên<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào tên bản vẽ"  id="floor_plan_name" name="floor_plan_name" value="{{ old('floor_plan_name', $floorPlan->floor_plan_name) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="file-input">Bản vẽ<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="file" class="file-input" id="file_input" name="file_input" data-show-caption="false" data-show-upload="false" data-fouc>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="floor_plan_size">Kích Thước:</label>
                        <div class="col-lg-10">
                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="text" class="form-control"  maxlength="10" placeholder="Nhập vào tổng kích thước"  id="floor_plan_size" name="floor_plan_size" value="{{ old('floor_plan_size', $floorPlan->floor_plan_size) }}">
                                 <div class="form-control-feedback">
                                    <i class="icon-rulers"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="floor_plan_rooms">Phòng:</label>
                        <div class="col-lg-10">
                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="text" class="form-control"  maxlength="10" placeholder="Nhập vào kích thước phòng"  id="floor_plan_rooms" name="floor_plan_rooms" value="{{ old('floor_plan_rooms', $floorPlan->floor_plan_rooms) }}">
                                 <div class="form-control-feedback">
                                    <i class="icon-rulers"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="floor_plan_barths">Phòng Tắm:</label>
                        <div class="col-lg-10">
                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="text" class="form-control"  maxlength="10" placeholder="Nhập vào kích thước phòng tắm"  id="floor_plan_barths" name="floor_plan_barths" value="{{ old('floor_plan_barths', $floorPlan->floor_plan_barths) }}">
                                 <div class="form-control-feedback">
                                    <i class="icon-rulers"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="floor_plan_price">Giá Tiền:</label>
                        <div class="col-lg-10">
                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="text" data-type='currency' class="form-control" maxlength="100" placeholder="Nhập vào số tiền"  id="floor_plan_price" name="floor_plan_price" value="{{ old('floor_plan_price', $floorPlan->floor_plan_price) }}">
                                <div class="form-control-feedback">
                                    <i class="icon-cash3"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="active_status">Trạng Thái:</label>
                        <div class="col-lg-10">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" id="active_status" name="active_status" class="form-check-input-styled-primary" value="1"
                                    @if(old('active_status', $floorPlan->active_status)!=null && old('active_status', $floorPlan->active_status) == 1)
                                        checked
                                    @endif data-fouc>
                                    Kích Hoạt
                                </label>
                            </div>
                        </div>
                    </div>

                </fieldset>


                <div class="text-center">
                    <button type="submit" class="btn btn-primary"><i class="icon-download4 mr-2"> Lưu Lại</i></button>
                    <a href="{{ route('admins.housefloorplan.index', ['id'=>$house->house_id]) }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở Về Danh Sách</a>
                </div>
            </form>
        </div>
    </div>
    <!-- /restore column visibility -->

@stop

@section('javascript')

	<!-- Theme JS files -->
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tagsinput.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tokenfield.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/ui/prism.min.js')}}"></script>

	<script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
	<script src="{{ asset('asset/admins/js/plugins/forms/styling/switch.min.js')}}"></script>

	{{-- <script src="{{ asset('asset/admins/js/plugins/uploaders/fileinput/plugins/purify.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/uploaders/fileinput/plugins/sortable.min.js')}}"></script> --}}
	<script src="{{ asset('asset/admins/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>


    <script src="{{ asset('asset/admins/js/plugins/forms/validation/validate.min.js') }}"></script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_tags_input.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_checkboxes_radios.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/pages/uploader_bootstrap.js')}}"></script> --}}
	<!-- /theme JS files -->

    <script>
        var FileUpload = function() {


        //
        // Setup module components
        //

        // Bootstrap file upload
        var _componentFileUpload = function() {
            if (!$().fileinput) {
                console.warn('Warning - fileinput.min.js is not loaded.');
                return;
            }

            //
            // Define variables
            //

            // Modal template
            var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
                '  <div class="modal-content">\n' +
                '    <div class="modal-header align-items-center">\n' +
                '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
                '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
                '    </div>\n' +
                '    <div class="modal-body">\n' +
                '      <div class="floating-buttons btn-group"></div>\n' +
                '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
                '    </div>\n' +
                '  </div>\n' +
                '</div>\n';

            // Buttons inside zoom modal
            var previewZoomButtonClasses = {
                toggleheader: 'btn btn-light btn-icon btn-header-toggle btn-sm',
                fullscreen: 'btn btn-light btn-icon btn-sm',
                borderless: 'btn btn-light btn-icon btn-sm',
                close: 'btn btn-light btn-icon btn-sm'
            };

            // Icons inside zoom modal classes
            var previewZoomButtonIcons = {
                prev: '<i class="icon-arrow-left32"></i>',
                next: '<i class="icon-arrow-right32"></i>',
                toggleheader: '<i class="icon-menu-open"></i>',
                fullscreen: '<i class="icon-screen-full"></i>',
                borderless: '<i class="icon-alignment-unalign"></i>',
                close: '<i class="icon-cross2 font-size-base"></i>'
            };

            // File actions
            var fileActionSettings = {
                zoomClass: '',
                zoomIcon: '<i class="icon-zoomin3"></i>',
                dragClass: 'p-2',
                dragIcon: '<i class="icon-three-bars"></i>',
                removeClass: '',
                removeErrorClass: 'text-danger',
                // removeIcon: '<i class="icon-bin"></i>',
                indicatorNew: '<i class="icon-file-plus text-success"></i>',
                indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
                indicatorError: '<i class="icon-cross2 text-danger"></i>',
                indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
            };


            //
            // Basic example
            //

            $('.file-input').fileinput({
                browseLabel: 'Chọn Bản Vẽ',
                browseIcon: '<i class="icon-file-plus mr-2"></i>',
                uploadIcon: '<i class="icon-file-upload2 mr-2"></i>',
                removeIcon: '<i class="icon-cross2 font-size-base mr-2"></i>',
                layoutTemplates: {
                    icon: '<i class="icon-file-check"></i>',
                    modal: modalTemplate
                },

                    initialPreview: [
                    "{{ asset('storage/'.$floorPlan->floor_plan_image) }}",

                ],
                initialPreviewAsData: true,
                allowedFileExtensions: ["jpg", "png", "jpeg"],
                initialCaption: "Không có bản vẽ được chọn",
                previewZoomButtonClasses: previewZoomButtonClasses,
                previewZoomButtonIcons: previewZoomButtonIcons,
                fileActionSettings: fileActionSettings
            });
        };


        //
        // Return objects assigned to module
        //

        return {
        init: function() {
            _componentFileUpload();
        }
        }
        }();


        // Initialize module
        // ------------------------------

        document.addEventListener('DOMContentLoaded', function() {
        FileUpload.init();
        });


        function initialSetup() {
           $hidden = $(".notifyHidden");
            if ($hidden != null) {
                $hidden.delay(10000).fadeOut('slow');
            }
        }

        $("input[data-type='currency']").on({
            keyup: function() {
                formatCurrency($(this));
            },
            blur: function() {
                formatCurrency($(this), "blur");
            },
        });

        function formatNumber(n) {
        // format number 1000000 to 1,234,567
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }


        function formatCurrency(input, blur) {
        // appends $ to value, validates decimal side
        // and puts cursor back in right position.

        // get input value
        var input_val = input.val();

        // don't validate empty input
        if (input_val === "") { return; }

        // original length
        var original_len = input_val.length;

        // initial caret position
        var caret_pos = input.prop("selectionStart");

        // check for decimal
        if (input_val.indexOf(".") >= 0) {

            // // get position of first decimal
            // // this prevents multiple decimals from
            // // being entered
            // var decimal_pos = input_val.indexOf(".");

            // split number by decimal point
            var left_side = input_val.substring(0, decimal_pos);
            // var right_side = input_val.substring(decimal_pos);

            // add commas to left side of number
            left_side = formatNumber(left_side);

            // // validate right side
            // right_side = formatNumber(right_side);

            // // On blur make sure 2 numbers after decimal
            // if (blur === "blur") {
            // right_side += "00";
            // }

            // // Limit decimal to only 2 digits
            // right_side = right_side.substring(0, 2);

            // join number by .
            input_val = left_side + "." + right_side;

        } else {
            // no decimal entered
            // add commas to number
            // remove all non-digits
            input_val = formatNumber(input_val);
            input_val = input_val;

            // // final formatting
            // if (blur === "blur") {
            // input_val += ".00";
            // }
        }

        // send updated string to input
        input.val(input_val);

        // put caret back in the right position
        var updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
        }


        $( document ).ready( function () {
            initialSetup();
            formatCurrency($("input[name='floor_plan_price']"));
            $( "form[name=house-form]" ).validate( {
                rules: {
                    floor_plan_name: {
                        required: true,
                        maxlength: 100
                    }
                },
                messages: {
                    floor_plan_name: {
                        required: "Hãy nhập vào tên bản vẽ.",
                        maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    }
                },
                errorElement: "span",
                errorPlacement: function ( error, element ) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("form-text").addClass( "text-danger" );

                    if ( element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( element.next( "label" ) );
                    } else {
                        error.insertAfter( element );
                    }
                },
                highlight: function ( element, errorClass, validClass ) {
                    $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
                },
                unhighlight: function (element, errorClass, validClass) {
                    $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
                }
            } );

        } );
    </script>
@stop
