@extends('layouts.admin_layout')
@section('title', 'Bản vẽ kỹ thuật bất động sản')

@section('topnavdetail')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i>
            <span class="font-weight-semibold">Bản Vẽ Kỹ Thuật</span> <span class="text-primary">{{ $house->house_name }}</span>
            - Danh sách</h4>
        <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.house.index') }}" class="breadcrumb-item"><i class="icon-city mr-2"></i> Bất Động Sản</a>
        <a href="{{ route('admins.house.show', ['house_id'=>$house->house_id]) }}" class="breadcrumb-item"> {{ $house->house_name }}</a>
        <span class="breadcrumb-item active">Bản Vẽ Kỹ Thuật</span>
    </div>
    <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h6 class="alert-heading font-weight-semibold mb-1">Thông Báo</h6>
        <span>{{session('success')}}</span>
    </div>
    @endif

    @if(count($errors) > 0)
        <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h6 class="alert-heading font-weight-semibold mb-1">Thông Báo</h6>
            @foreach($errors->all() as $error)
            <p>{{$error}}</p>
            @endforeach
        </div>
    @endif
    <!-- /Notifications Set  -->

    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Danh Sách</strong>
                <a href="{{ route('admins.housefloorplan.create', ['id' => $house->house_id]) }}" class="btn btn-outline-danger"><i class="icon-plus-circle2 mr-2"></i> Thêm</a>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body"></div>

        <table class="table datatable-colvis-restore">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Bản Vẽ Kỹ Thuật</th>
                    <th>Tên</th>
                    <th>Kích Thước</th>
                    <th>Kích Thước Phòng</th>
                    <th>Kích Thước Phòng Tắm</th>
                    <th>Giá Tiền</th>
                    <th>Kích Hoạt</th>
                    <th class="text-right">Chức Năng</th>
                </tr>
            </thead>
            <tbody>
                <?php $idx = 0; ?>
                @forelse ($floorPlans as $category)
                    <tr>
                        <?php $idx++; ?>
                        <td>{{$idx}}</td>
                        <td><img src="{{ asset('storage/'.$category->floor_plan_image) }}" alt="{{ $category->floor_plan_name }}" width="100" height="100"></td>
                        <td>{{$category->floor_plan_name}}</td>
                        <td>{{ $category->floor_plan_size }}</td>
                        <td>{{ $category->floor_plan_rooms }}</td>
                        <td>{{ $category->floor_plan_barths }}</td>
                        <td>
                            <span>
                                @if (Str::length($category->floor_plan_price)>9)
                                    {{ is_int(($category->floor_plan_price/1000000000))?($category->floor_plan_price/1000000000).' Tỷ': number_format($category->floor_plan_price,0,'',',') }}
                                @elseif(Str::length($category->floor_plan_price)>6)
                                    {{ is_int(($category->floor_plan_price/1000000))?($category->floor_plan_price/1000000).' Triệu': number_format($category->floor_plan_price,0,'',',') }}
                                @else
                                    {{ number_format($category->floor_plan_price,0,'',',') }}
                                @endif
                                <sup class="text-danger-600">vnđ</sup>
                            </span>
                        </td>
                        <td>@if($category->active_status == 1)<span class="badge badge-success">BẬT</span>  @else <span class="badge badge-danger">TẮT</span> @endif </td>
                        <td class="text-right">
                            <div class="list-icons">
                                {{-- <a href="javascript:void(0)" onclick="form_action('view', {{$category->floor_plan_id}});" role="button" data-popup="tooltip" title="Chi Tiết" data-placement="bottom" class="list-icons-item text-info-600"><i class="icon-file-eye2"></i></a> --}}
                                <a href="javascript:void(0)" onclick="form_action('edit', {{$category->floor_plan_id}});" role="button" data-popup="tooltip" title="Cập Nhập" data-placement="bottom" class="list-icons-item text-warning-600"><i class="icon-pencil7"></i></a>
                                <a  href="javascript:void(0)" onclick="form_action('delete', {{$category->floor_plan_id}});" role="button" data-popup="tooltip" title="Xóa" data-placement="bottom" class="list-icons-item text-danger-600"><i class="icon-trash"></i></a>
                            </div>
                        </td>
                    </tr>
                    @empty
                @endforelse
            </tbody>
        </table>
    </div>
    <!-- /restore column visibility -->



    <form name="form1" action="" method="">
        {{csrf_field()}}
        <div class="form-group ">
            <input type="hidden" class="form-control" name="floor_plan_id" value="">
        </div>
    </form>
@stop

@section('javascript')
	<!-- Theme JS files -->
	<script src="{{ asset('asset/admins/js/plugins/tables/datatables/datatables.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/selects/select2.min.js')}}"></script> --}}
	{{-- <script src="{{ asset('asset/admins/js/pages/datatables_extension_colvis.js')}}"></script> --}}


	<script src="{{ asset('asset/admins/js/plugins/notifications/sweet_alert.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
    <script>
        var DatatableColumnVisibility = function() {
            var _componentDatatableColumnVisibility = function() {
                if (!$().DataTable) {
                    console.warn('Warning - datatables.min.js is not loaded.');
                    return;
                }

                // Setting datatable defaults
                $.extend($.fn.dataTable.defaults, {
                    autoWidth: false,
                    dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Tìm kiếm:</span> _INPUT_',
                        searchPlaceholder: 'Nhập từ khóa...',
                        lengthMenu: '<span>Hiển thị:</span> _MENU_',
                        paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                    }
                });

                // Restore column visibility
                $('.datatable-colvis-restore').DataTable({
                    buttons: [{
                        extend: 'colvis',
                        text: '<i class="icon-grid7"></i>',
                        className: 'btn bg-teal-400 btn-icon dropdown-toggle',
                        postfixButtons: ['colvisRestore']
                    }],
                    columnDefs: [
                        {targets: -1, orderable: false,},
                    ]
                });

            };

            return {
                init: function() {
                    _componentDatatableColumnVisibility();
                }
            }
        }();

        function form_action (mode, floor_plan_id) {
           let frm = document.form1;
            frm.floor_plan_id.value = floor_plan_id
           if(mode == 'edit'){
               frm.action = "{{route('admins.housefloorplan.edit', ['id' => $house->house_id])}}";
               frm.method = 'get';
            frm.submit();
           }else if(mode == 'view'){
               frm.action = "{{route('admins.housefloorplan.show', ['id' => $house->house_id])}}";
               frm.method = 'get';
               frm.submit();
           }else if(mode == 'delete'){

                // Defaults
                var swalInit = swal.mixin({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-light'
                });
                 swalInit.fire({
                    title: 'Bạn có chắc muốn xóa đối tượng này?',
                    text: "Bạn không thể hoàn tác lại thao tác này!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Vâng, Xóa đối tượng!',
                    cancelButtonText: 'Thoát',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function(result) {
                    if (result.value) {
                        frm.action = "{{route('admins.housefloorplan.destroy', ['id' => $house->house_id])}}";
                        frm.method = 'post';
                        frm.submit();
                    }
                });
            }
        }

        function initialSetup() {
           $hidden = $(".notifyHidden");
            if ($hidden != null) {
                $hidden.delay(3000).fadeOut('slow');
            }
        }

        // Initialize module
        // ------------------------------
        document.addEventListener('DOMContentLoaded', function() {
            DatatableColumnVisibility.init();
            initialSetup();
        });
    </script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<script src="{{ asset('asset/admins/js/pages/components_popups.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/pages/extra_sweetalert.js')}}"></script> --}}
	<!-- /theme JS files -->

@stop
