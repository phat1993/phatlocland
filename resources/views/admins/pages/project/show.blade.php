@extends('layouts.admin_layout')
@section('title', 'Dự án')
@section('styles')
<!-- Plugins CSS -->
<link href="{{ asset('asset/clients/css/font-awesome.min.css')}}" rel="stylesheet" />
<link href="{{ asset('asset/clients/css/linearicon.min.css')}}" rel="stylesheet" />
@endsection
@section('topnavdetail')
<div class="page-title d-flex">
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dự Án</span> - Chi tiết</h4>
    <a href="{{ route('admins.project.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
</div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.project.index') }}" class="breadcrumb-item"><i class="icon-city mr-2"></i> Dự Án</a>
        <span class="breadcrumb-item active">Chi Tiết</span>
    </div>
    <a href="{{ route('admins.project.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection

@section('content')


    <!-- Inner container -->
    <div class="d-flex align-items-start flex-column flex-md-row">

        <!-- Left content -->
        <div class="w-100 overflow-auto order-2 order-md-1">

            <!-- Course overview -->

            <div class="card">
                <div class="card-header header-elements-md-inline">
                    <h5 class="card-title">{{ $project->project_name }}</h5>

                    {{-- <div class="header-elements">
                        <ul class="list-inline list-inline-dotted mb-0 mt-2 mt-md-0">
                            <li class="list-inline-item">Rating: <span class="font-weight-semibold">4.85</span></li>
                            <li class="list-inline-item">
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <span class="text-muted ml-1">(439)</span>
                            </li>
                        </ul>
                    </div> --}}
                </div>

                <div class="nav-tabs-responsive bg-light border-top">
                    <ul class="nav nav-tabs nav-tabs-bottom flex-nowrap mb-0">
                        <li class="nav-item"><a href="#course-overview" class="nav-link active" data-toggle="tab"><i class="icon-menu7 mr-2"></i> Thông Tin</a></li>
                        <li class="nav-item"><a href="#course-attendees" class="nav-link" data-toggle="tab"><i class="icon-cup2 mr-2"></i> Tiện Nghi</a></li>
                        <li class="nav-item"><a href="#course-schedule" class="nav-link" data-toggle="tab"><i class="icon-image3 mr-2"></i> Hình Ảnh</a></li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade show active" id="course-overview">
                        <div class="card-body">
                            <div class="table-responsive">
                                <h6 class="font-weight-semibold">Cơ bản</h6>
                                <table class="table">
                                    <tr>
                                        <th width="20%" scope="col">Danh mục</th>
                                        <td width="80%">{{$project->project_category_name}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="col">Tiêu đề</th>
                                        <td>{{$project->project_name}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="col">Mô tả</th>
                                        <td>{{$project->project_description}}</td>
                                    </tr>
                                    <tr>
                                        <th>Chi tiết</th>
                                        <td><div class="card card-body">{!!$project->project_detail!!}</div></td>
                                    </tr>
                                </table>
                            </div>
                            <hr class="border-danger">
                            <div class="table-responsive">
                                <h6 class="font-weight-semibold">Chi tiết</h6>
                                <table class="table">
                                    <tr>
                                        <th width="20%" scope="col">Chủ sở hữu</th>
                                        <td width="80%">{{$project->project_own}}</td>
                                    </tr>
                                    <tr>
                                        <th>Thời gian xây dựng</th>
                                        <td>{{isset($project->project_build_time)!=''?date('d/m/Y',strtotime($project->project_build_time)):''}}</td>
                                    </tr>
                                    <tr>
                                        <th>Diện Tích</th>
                                        <td>{{$project->project_acreage}}</td>
                                    </tr>
                                    <tr>
                                        <th>Số Tòa Nhà</th>
                                        <td>{{$project->project_block}}</td>
                                    </tr>
                                    <tr>
                                        <th>Số Tầng</th>
                                        <td>{{$project->project_floor}}</td>
                                    </tr>
                                    <tr>
                                        <th>Số Bất Động Sản</th>
                                        <td>{{$project->project_house}}</td>
                                    </tr>
                                    <tr>
                                        <th>Diện Tích Bất Động Sản</th>
                                        <td>{{$project->project_house_acreage}}</td>
                                    </tr>
                                </table>
                            </div>
                            <hr class="border-danger">
                            <div class="table-responsive">
                                <h6 class="font-weight-semibold">Vị Trí Địa Lý</h6>
                                <table class="table">
                                    <tr>
                                        <th width="20%" scope="col">Mã bưu điện</th>
                                        <td width="80%">{{$project->project_name}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="col">Tỉnh/Thành Phố</th>
                                        <td>{{$project->project_provincial}}</td>
                                    </tr>
                                    <tr>
                                        <th>Quận/ Huyện</th>
                                        <td>{{$project->project_district}}</td>
                                    </tr>
                                    <tr>
                                        <th>Phường/ Xã</th>
                                        <td>{{$project->project_commune}}</td>
                                    </tr>
                                    <tr>
                                        <th>Địa chỉ</th>
                                        <td>{{$project->project_address}}</td>
                                    </tr>
                                    <tr>
                                        <th>Định vị bản đồ</th>
                                        <td>{{$project->project_map_location}}</td>
                                    </tr>
                                </table>
                            </div>
                            <hr class="border-danger">
                            <div class="table-responsive">
                                <h6 class="font-weight-semibold">Tối Ưu SEO</h6>
                                <table class="table">
                                    <tr>
                                        <th width="20%" scope="col">Tiêu đề website</th>
                                        <td width="80%">{{$project->web_title}}</td>
                                    </tr>
                                    <tr>
                                        <th>Từ khóa tìm kiếm</th>
                                        <td>{{$project->web_keywords}}</td>
                                    </tr>
                                    <tr>
                                        <th>Mô tả website</th>
                                        <td>{{$project->web_description}}</td>
                                    </tr>
                                    <tr>
                                        <th>Đường dẫn</th>
                                        <td><a href="{{url('du-an').'/'.$project->category_web_canonical.'/'.$project->web_canonical}}">{{url('').'/'.$project->category_web_canonical.'/'.$project->web_canonical}}</a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="course-attendees">
                        <div class="card-body">
                            <div class="row">
                                @forelse ($amenities as $item)
                                <div class="col-xl-3 col-md-6">
                                    <div class="card card-body">
                                        <div class="media">
                                            <div class="mr-3">
                                                <a href="#" class="">
                                                    <i class="{{ $item->amenity_icon }}" style="font-size:2.8rem"></i>
                                                    {{-- <img src="../../../../global_assets/images/demo/users/face11.jpg" class="rounded-circle" width="42" height="42" alt=""> --}}
                                                </a>
                                            </div>

                                            <div class="media-body">
                                                <h6 class="mb-0">{{ $item->amenity_name }}</h6>
                                                <span class="text-muted"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @empty
                                <h1 class="ml-5 p-5">Hiện tại, Dự án chưa có tiện nghi!</h1>
                                @endforelse
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="course-schedule">
                        <div class="card-body">

                            <div class="row">
                                @forelse ($galleries as $item)


                                <div class="col-xl-3 col-sm-6">
                                    <div class="card">
                                        <div class="card-img-actions mx-1 mt-1">
                                            <img class="card-img img-fluid" src="{{ asset('storage/'.$item->project_gallery_link) }}" alt="">
                                            <div class="card-img-actions-overlay card-img">
                                                <a href="{{ asset('storage/'.$item->project_gallery_link) }}" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round" data-popup="lightbox" rel="group">
                                                    <i class="icon-zoomin3"></i>
                                                </a>

                                                {{-- <a href="#" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round ml-2">
                                                    <i class="icon-download"></i>
                                                </a> --}}
                                            </div>
                                        </div>

                                        <div class="card-body">
                                            <div class="d-flex align-items-start flex-wrap">
                                                <div class="font-weight-semibold">{{ $item->project_gallery_name }}</div>
                                                <span class="font-size-sm text-muted ml-auto">
                                                    {{-- @if($item->project_gallery_banner == 1)<span class="badge badge-warning">BĂNG RÔN</span>
                                                    @else <span class="badge badge-dark">HÌNH ẢNH</span> @endif --}}

                                                    @if($item->project_gallery_banner == 1)
                                                    <span class="badge badge-danger">BĂNG RÔN</span>
                                                    @elseif($item->project_gallery_banner == 2)
                                                    <span class="badge badge-warning">ĐẠI DIỆN</span>
                                                    @else
                                                     <span class="badge badge-dark">HÌNH ẢNH</span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @empty
                                <h1 class="ml-5 p-5">Hiện tại, Dự án chưa có hình ảnh!</h1>
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /course overview -->

        </div>
        <!-- /left content -->


        <!-- Right sidebar component -->
        <div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right wmin-350 border-0 shadow-0 order-1 order-md-2 sidebar-expand-md">

            <!-- Sidebar content -->
            <div class="sidebar-content">
                <!-- Task details -->
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="card-title font-weight-semibold">Cơ bản</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>

                    <table class="table table-borderless table-xs my-2">
                        <tbody>
                            <tr>
                                <td><i class="icon-user-tie mr-2"></i> Quản trị viên:</td>
                                <td class="text-right"><a href="#">{{ $project->admin_full_name }}</a></td>
                            </tr>
                            <tr>
                                <td><i class="icon-circles2 mr-2"></i> Kích hoạt:</td>
                                <td class="text-right">
                                    @if($project->active_status == 1)<span class="badge badge-success">bật</span>
                                        @else <span class="badge badge-danger">tắt</span> @endif
                                </td>
                            </tr>
                            <tr>
                                <td><i class="icon-users4 mr-2"></i> Lượt Xem:</td>
                                <td class="text-right text-muted">{{$project->project_viewed}} lượt</td>
                            </tr>
                            <tr>
                                <td><i class="icon-alarm-add mr-2"></i> Cập nhật:</td>
                                <td class="text-right text-muted">{{date('H:i - d/m/Y', strtotime($project->updated_at))}}</td>
                            </tr>
                            <tr>
                                <td><i class="icon-alarm-check mr-2"></i> Tạo mới:</td>
                                <td class="text-right text-muted">{{date('H:i - d/m/Y', strtotime($project->created_at))}}</td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="card-footer d-flex align-items-center">
                        <ul class="list-inline list-inline-condensed mb-0">
                            <li class="list-inline-item">
                                <a href="{{ route('admins.projectgallery.index', ['id' => $project->project_id]) }}"
                                    data-popup="tooltip" title="Thêm Hình Ảnh" data-placement="bottom"
                                    class="text-default"><i class="icon-images3"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript:void(0)" onclick="form_action('edit', {{$project->project_id}});" role="button"
                                    data-popup="tooltip" title="Cập Nhập" data-placement="bottom"
                                    class="text-default"><i class="icon-pencil7"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript:void(0)" onclick="form_action('delete', {{$project->project_id}});" role="button"
                                    data-popup="tooltip" title="Xóa" data-placement="bottom"
                                    class="text-default"><i class="icon-bin"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="{{ route('admins.project.index') }}"
                                    data-popup="tooltip" title="Trở về danh sách" data-placement="bottom"
                                    class="text-default"><i class="icon-reply"></i></a>
                            </li>
                        </ul>

                    </div>
                </div>
                <!-- /task details -->

            </div>
            <!-- /sidebar content -->

        </div>
        <!-- /right sidebar component -->

    </div>
    <!-- /inner container -->

    <form name="form1" action="" method="">
        {{csrf_field()}}
        <div class="form-group ">
            <input type="hidden" class="form-control" name="project_id" value="">
        </div>
    </form>
@stop

@section('javascript')


<script src="{{ asset('asset/admins/js/plugins/notifications/sweet_alert.min.js')}}"></script>
{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{ asset('asset/admins/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
<script>
    function form_action (mode, project_id) {
       let frm = document.form1;
        frm.project_id.value = project_id
       if(mode == 'edit'){
           frm.action = "{{route('admins.project.edit')}}";
           frm.method = 'get';
        frm.submit();
       }else if(mode == 'view'){
           frm.action = "{{route('admins.project.show')}}";
           frm.method = 'get';
           frm.submit();
       }else if(mode == 'delete'){

            // Defaults
            var swalInit = swal.mixin({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-light'
            });
             swalInit.fire({
                title: 'Bạn có chắc muốn xóa đối tượng này?',
                text: "Bạn không thể hoàn tác lại thao tác này!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Vâng, Xóa đối tượng!',
                cancelButtonText: 'Thoát',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function(result) {
                if (result.value) {
                    frm.action = "{{route('admins.project.delete')}}";
                    frm.method = 'post';
                    frm.submit();
                }
            });
        }
    }

    function initialSetup() {
       $hidden = $(".notifyHidden");
        if ($hidden != null) {
            $hidden.delay(3000).fadeOut('slow');
        }
    }
    // Initialize module
    // ------------------------------
    document.addEventListener('DOMContentLoaded', function() {
        initialSetup();
    });
</script>

<script src="{{ asset('asset/admins/js/app.js') }}"></script>
<script src="{{ asset('asset/admins/js/pages/components_popups.js')}}"></script>
<!-- /theme JS files -->

@stop


