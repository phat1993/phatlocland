@extends('layouts.admin_layout')
@section('title', 'Dự án')
@section('styles')
<link href="{{ asset('asset/admins/css/summernote.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('topnavdetail')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dự Án</span> - Tạo mới</h4>
        <a href="{{ route('admins.project.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.project.index') }}" class="breadcrumb-item"><i class="icon-city mr-2"></i> Dự Án</a>
        <span class="breadcrumb-item active">Tạo mới</span>
    </div>
    <a href="{{ route('admins.project.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
        <h5>{{session('success')}}</h5>
        <br>
        <a href="{{ route('admins.project.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
    </div>
    @endif

    @if(count($errors) > 0)
         <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
            @foreach($errors->all() as $error)
            <h5>{{$error}}</h5>
            @endforeach
            <br>
            <a href="{{ route('admins.project.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
        </div>
    @endif
    <!-- /Notifications Set  -->
    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Tạo Mới</strong>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="reload"></a>
                </div>
            </div>
        </div>

        <div class="card-body">

            <form action="{{ route('admins.project.store') }}" method="POST" enctype="multipart/form-data" name="project-form">
                {{csrf_field()}}
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Thông Tin Cơ Bản</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="menu_lv1">Danh Mục:</label>
                        <div class="col-lg-10">
                            <select data-placeholder="Chọn danh mục cho dự án" class="form-control select-search" id="project_category_id" name="project_category_id" data-fouc>
                                <optgroup label="Danh Mục">
                                    @foreach ($categories as $item)
                                        <option value="{{ $item->project_category_id }}" {{ $item->project_category_id == old('project_category_id',$project->project_category_id)?'selected':''  }}>{{ $item->project_category_name }}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_name">Tiêu đề<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="500" placeholder="Nhập vào tiêu đề"  id="project_name" name="project_name" value="{{ old('project_name', $project->project_name) }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_description">Mô tả<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <textarea rows="6" class="form-control" maxlength="1000" id="project_description" name="project_description" placeholder="Nhập vào mô tả">{{ old('project_description', $project->project_description) }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2" for="project_detail">Chi Tiết<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <textarea class="form-control" id="project_detail" name="project_detail" >
                                {{ old('project_detail', $project->project_detail) }}
                            </textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_amenities">Tiện Nghi:</label>
                        <div class="col-lg-10">
                            <select multiple="multiple" class="form-control listbox-tall" id="project_amenities" name="project_amenities[]" data-fouc>
                                @foreach ($amenities as $item)
                                    <option value="{{ $item->amenity_id }}">{{ $item->amenity_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="active_status">Trạng Thái:</label>
                        <div class="col-lg-10">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" id="active_status" name="active_status" class="form-check-input-styled-primary" value="1"
                                    @if(old('active_status', $project->active_status)!=null && old('active_status', $project->active_status) == 1)
                                        checked
                                    @endif data-fouc>
                                    Kích Hoạt
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>

                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Thông Tin Chi Tiết</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_own">Chủ Sở Hữu:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào chủ sở hữu" id="project_own" name="project_own" value="{{ old('project_own', $project->project_own) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_build_time">Thời Gian Xây Dựng:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control pickadate-translated" placeholder="Nhập vào thời gian xây dựng" id="project_build_time"
                            name="project_build_time" value="{{ old('project_build_time', $project->project_build_time) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_block">Số Tòa Nhà:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="50" placeholder="Nhập vào số tòa nhà"  id="project_block" name="project_block" value="{{ old('project_block', $project->project_block) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_acreage">Tổng Diện Tích:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="50" placeholder="Nhập vào tổng diện tích"  id="project_acreage" name="project_acreage" value="{{ old('project_acreage', $project->project_acreage) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_floor">Số Tầng Trung Bình:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="50" placeholder="Nhập vào số tầng trung bình"  id="project_floor" name="project_floor" value="{{ old('project_floor', $project->project_floor	) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_house">Số Căn Hộ:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="50" placeholder="Nhập vào diện tích nhà trung bình"  id="project_house" name="project_house" value="{{ old('project_house', $project->project_house	) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_house_acreage">Diện Tích Căn Hộ:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="50" placeholder="Nhập vào diện tích nhà trung bình"  id="project_house_acreage" name="project_house_acreage" value="{{ old('project_house_acreage', $project->project_house_acreage	) }}">
                        </div>
                    </div>
                </fieldset>

                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Vị Trí Địa Lý</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_postal_code">Mã Bưu Biện<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào mã bưu biện" id="project_postal_code" name="project_postal_code" value="{{ old('project_postal_code', $project->project_postal_code) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_provincial">Tỉnh/Thành Phố<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào tỉnh/thành phố" id="project_provincial" name="project_provincial" value="{{ old('project_provincial', $project->project_provincial) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_district">Quận/Huyện<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào quận/huyện" id="project_district" name="project_district" value="{{ old('project_district', $project->project_district) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_commune">Xã/Phường<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào xã/phường" id="project_commune" name="project_commune" value="{{ old('project_commune', $project->project_commune) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_address">Địa Chỉ<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào địa chỉ" id="project_address" name="project_address" value="{{ old('project_address', $project->project_address) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_map_location">Định Vị Bản Đồ<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="1000" placeholder="Nhập vào Định Vị Bản Đồ" id="project_map_location" name="project_map_location" value="{{ old('project_map_location', $project->project_map_location) }}">
                        </div>
                    </div>
                </fieldset>



                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold">Tối Ưu Quảng Cáo</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_title">Tiêu đề Website:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="20" placeholder="Nhập vào tiêu đề hiển thị website" id="web_title" name="web_title" value="{{ old('web_title', $project->web_title) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_keywords">Từ khóa tìm kiếm:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control tokenfield" id="web_keywords" placeholder="Từ khóa" maxlength="200" name="web_keywords"  value="{{ old('web_keywords', $project->web_keywords) }}" data-fouc>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_description">Mô Tả Website:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="145" placeholder="Nhập vào mô tả website"  id="web_description" name="web_description" value="{{ old('web_description', $project->web_description) }}">
                        </div>
                    </div>
                </fieldset>


                <div class="text-center">
                    <button type="submit" class="btn btn-primary"><i class="icon-download4 mr-2"> Lưu Lại</i></button>
                    <a href="{{ route('admins.project.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở Về Danh Sách</a>
                </div>
            </form>
        </div>
    </div>
    <!-- /restore column visibility -->

@stop

@section('javascript')

	<!-- Theme JS files -->
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tagsinput.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tokenfield.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/ui/prism.min.js')}}"></script>

	<script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
	<script src="{{ asset('asset/admins/js/plugins/forms/styling/switch.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/selects/select2.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/inputs/duallistbox/duallistbox.min.js')}}"></script>


	<script src="{{ asset('asset/admins/js/plugins/ui/moment/moment.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/pickers/daterangepicker.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/pickers/anytime.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/pickers/pickadate/picker.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/pickers/pickadate/picker.date.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/pickers/pickadate/picker.time.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/pickers/pickadate/legacy.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/notifications/jgrowl.min.js')}}"></script>
    <script src="{{ asset('asset/admins/js/plugins/editors/ckeditor/ckeditor.js')}}"></script>

    <script src="{{ asset('asset/admins/js/plugins/forms/validation/validate.min.js') }}"></script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_tags_input.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_checkboxes_radios.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_select2.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_dual_listboxes.js')}}"></script>

	{{-- <script src="{{ asset('asset/admins/js/pages/editor_ckeditor_default.js')}}"></script> --}}
	{{-- <script src="{{ asset('asset/admins/js/pages/picker_date.js')}}"></script> --}}


	<!-- /theme JS files -->

    <script>
        CKEDITOR.replace( 'project_detail', {
            filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',

        } );
    </script>
    @include('ckfinder::setup')

    <script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
    <script>CKFinder.config( { connectorPath: '/ckfinder/connector' } );</script>


    <script>
        function initialSetup() {
           $hidden = $(".notifyHidden");
            if ($hidden != null) {
                $hidden.delay(10000).fadeOut('slow');
            }
        }


        $( document ).ready( function () {
            initialSetup();
            // Localization
            $('.pickadate-translated').pickadate({
                singleDatePicker: true,
                selectYears: true,
                selectMonths: true,
                monthsFull: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
                weekdaysShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
                today: 'Hôm Nay',
                clear: 'Xóa',
                format: 'dd/mm/yyyy',
            });
            $( "form[name=project-form]" ).validate( {
                rules: {
                    project_name: {
                        required: true,
                        maxlength: 500
                    },
                    project_description: {
                        required: true,
                        maxlength: 1000
                    },
                    // project_detail: {
                    //     required: true
                    // },
                    project_postal_code: {
                        required: true,
                        maxlength: 100
                    },
                    project_provincial: {
                        required: true,
                        maxlength: 100
                    },
                    project_district: {
                        required: true,
                        maxlength: 100
                    },
                    project_commune: {
                        required: true,
                        maxlength: 100
                    },
                    project_address: {
                        required: true,
                        maxlength: 200
                    },
                    project_map_location: {
                        required: true,
                        maxlength: 1000
                    },
                },
                messages: {
                    project_name: {
                        required: "Bạn không được để trống.",
                        maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    },
                    project_description: {
                        required: "Bạn không được để trống.",
                        maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    },
                    // project_detail: {
                    //     required: "Bạn không được để trống.",
                    //     maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    // },
                    project_postal_code: {
                        required: "Bạn không được để trống.",
                        maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    },
                    project_provincial: {
                        required: "Bạn không được để trống.",
                        maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    },
                    project_district: {
                        required: "Bạn không được để trống.",
                        maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    },
                    project_commune: {
                        required: "Bạn không được để trống.",
                        maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    },
                    project_address: {
                        required: "Bạn không được để trống.",
                        maxlength: "Bạn chỉ được nhập tối đa 200 ký tự."
                    },
                    project_map_location: {
                        required: "Bạn không được để trống.",
                        maxlength: "Bạn chỉ được nhập tối đa 1000 ký tự."
                    },
                },
                errorElement: "span",
                errorPlacement: function ( error, element ) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("form-text").addClass( "text-danger" );

                    if ( element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( element.next( "label" ) );
                    } else {
                        error.insertAfter( element );
                    }
                },
                highlight: function ( element, errorClass, validClass ) {
                    $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
                },
                unhighlight: function (element, errorClass, validClass) {
                    $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
                }
            } );

        } );
    </script>
@stop
