@extends('layouts.admin_layout')
@section('title', 'Dự án')

@section('topnavdetail')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Dự Án</span> - Danh sách</h4>
        <a href="{{ route('admins.project.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.project.index') }}" class="breadcrumb-item"><i class="icon-city mr-2"></i> Dự Án</a>
        <span class="breadcrumb-item active">Danh sách</span>
    </div>
    <a href="{{ route('admins.project.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h6 class="alert-heading font-weight-semibold mb-1">Thông Báo</h6>
        <span>{{session('success')}}</span>
    </div>
    @endif

    @if(count($errors) > 0)
        <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h6 class="alert-heading font-weight-semibold mb-1">Thông Báo</h6>
            @foreach($errors->all() as $error)
            <p>{{$error}}</p>
            @endforeach
        </div>
    @endif
    <!-- /Notifications Set  -->

    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Tìm Kiếm</strong>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form action="{{route('admins.project.index')}}" method="get">
                {{csrf_field()}}
                <div class="form-group">
                    <div class="row align-items-center">
                        <div class="col-sm-2">
                            <select data-placeholder="Chọn danh mục cho dự án" class="form-control select-search" id="project_category_id" name="project_category_id" data-fouc>
                                <optgroup label="Danh Mục">
                                    <option value="" selected>Để Trống</option>
                                    @foreach ($categories as $item)
                                        <option value="{{ $item->project_category_id }}" {{ $item->project_category_id == Request::get('project_category_id')?'selected':'' }}>{{ $item->project_category_name }}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="project_name" value="{{Request::get('project_name')}}" placeholder="Tìm kiếm theo Tiêu đề">
                        </div>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" name="project_own" value="{{Request::get('project_own')}}" placeholder="Chủ dự án">
                        </div>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" name="project_acreage" value="{{Request::get('project_acreage')}}" placeholder="Tổng diện tích">
                        </div>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" name="project_block" value="{{Request::get('project_block')}}" placeholder="Số tòa nhà">
                        </div>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" name="project_floor" value="{{Request::get('project_floor')}}" placeholder="Số Tầng">
                        </div>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" name="project_house" value="{{Request::get('project_house')}}" placeholder="Số bất động sản">
                        </div>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" name="project_house_acreage" value="{{Request::get('project_house_acreage')}}" placeholder="Diện tích bất động sản">
                        </div>
                        <div class="col-sm-1">
                            <select class="form-control" name='active_status'>
                                <option value="0" {{Request::get('active_status') == 0 ? 'selected': ''}}>Kích hoạt</option>
                                <option value="1" {{Request::get('active_status') == 1 ? 'selected': ''}}>Bật</option>
                                <option value="2" {{Request::get('active_status') == 2 ? 'selected': ''}}>Tắt</option>
                            </select>
                        </div>
                        <div class="col-sm-1">
                            <input type="submit" class="btn btn-primary btn-lg active" value="Tìm Kiếm">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /restore column visibility -->
    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Danh Sách</strong>
                <a href="{{ route('admins.project.create') }}" class="btn btn-outline-danger"><i class="icon-plus-circle2 mr-2"></i> Thêm</a>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>

        <div class="card-body">
            <table class="table datatable-colvis-restore">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Danh mục</th>
                        <th>Tiêu Đề</th>
                        <th>Chủ Sở Hữu</th>
                        <th>Thời Gian Xây Dựng</th>
                        <th>Diện Tích</th>
                        <th>Số Tòa Nhà</th>
                        <th>Số Tầng</th>
                        <th>Số Bất Động Sản</th>
                        <th>Diện Tích Bất Động Sản</th>
                        <th>Kích Hoạt</th>
                        <th>Tiêu đề website</th>
                        <th>Từ khóa tìm kiếm</th>
                        <th>Mô tả website</th>
                        <th>Đường dẫn</th>
                        <th class="text-right">Chức Năng</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $idx = $perPage*($currentPage-1); ?>
                    @forelse ($projects as $project)
                        <tr>
                            <?php $idx++; ?>
                            <td>{{$idx}}</td>
                            <td>{{$project->project_category_name}}</td>
                            <td>{{$project->project_name}}</td>
                            <td>{{$project->project_own}}</td>
                            <td>@isset($project->project_build_time)
                                {{date('d/m/Y', strtotime($project->project_build_time))}}
                            @endisset</td>
                            <td>{{$project->project_acreage}}</td>
                            <td>{{$project->project_block}}</td>
                            <td>{{$project->project_floor}}</td>
                            <td>{{$project->project_house}}</td>
                            <td>{{$project->project_house_acreage}}</td>
                            <td>@if($project->active_status == 1)<span class="badge badge-success">BẬT</span>  @else <span class="badge badge-danger">TẮT</span> @endif </td>
                            <td>{{ $project->web_title }}</td>
                            <td>{{ $project->web_keywords }}</td>
                            <td>{{ $project->web_description }}</td>
                            <td>{{ url('').'/'.$project->category_web_canonical.'/'.$project->web_canonical }}</td>
                            <td class="text-right">
                                <div class="list-icons">
                                    <a href="javascript:void(0)" onclick="form_action('view', {{$project->project_id}});" role="button" data-popup="tooltip" title="Chi Tiết" data-placement="bottom" class="list-icons-item text-info-600"><i class="icon-file-eye2"></i></a>
                                    <a href="{{ route('admins.projectgallery.index', ['id' => $project->project_id]) }}" data-popup="tooltip" title="Thêm Hình Ảnh" data-placement="bottom" class="list-icons-item text-warning-400"><i class="icon-images3"></i></a>
                                    <a href="javascript:void(0)" onclick="form_action('edit', {{$project->project_id}});" role="button" data-popup="tooltip" title="Cập Nhập" data-placement="bottom" class="list-icons-item text-warning-600"><i class="icon-pencil7"></i></a>
                                    <a  href="javascript:void(0)" onclick="form_action('delete', {{$project->project_id}});" role="button" data-popup="tooltip" title="Xóa" data-placement="bottom" class="list-icons-item text-danger-800"><i class="icon-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        @empty
                    @endforelse
                </tbody>
            </table>
        </div>

        {{ $projects->appends([
            'project_category_id' => Request::get('project_category_id'),
            'project_name' => Request::get('project_name'),
            'project_own' => Request::get('project_own'),
            'project_acreage' => Request::get('project_acreage'),
            'project_block' => Request::get('project_block'),
            'project_floor' => Request::get('project_floor'),
            'project_house' => Request::get('project_house'),
            'project_house_acreage' => Request::get('project_house_acreage'),
            'active_status' => Request::get('active_status'),
        ])->links('admins.includes.pagination') }}
    </div>
    <!-- /restore column visibility -->



    <form name="form1" action="" method="">
        {{csrf_field()}}
        <div class="form-group ">
            <input type="hidden" class="form-control" name="project_id" value="">
        </div>
    </form>
@stop

@section('javascript')
	<!-- Theme JS files -->
	<script src="{{ asset('asset/admins/js/plugins/tables/datatables/datatables.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/selects/select2.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/selects/select2.min.js')}}"></script> --}}
	{{-- <script src="{{ asset('asset/admins/js/pages/datatables_extension_colvis.js')}}"></script> --}}


	<script src="{{ asset('asset/admins/js/plugins/notifications/sweet_alert.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
    <script>
        var DatatableColumnVisibility = function() {
            var _componentDatatableColumnVisibility = function() {
                if (!$().DataTable) {
                    console.warn('Warning - datatables.min.js is not loaded.');
                    return;
                }

                // Setting datatable defaults
                $.extend($.fn.dataTable.defaults, {
                    paging: false,
                    autoWidth: false,
                    dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Tìm kiếm:</span> _INPUT_',
                        searchPlaceholder: 'Nhập từ khóa...',
                        lengthMenu: '<span>Hiển thị:</span> _MENU_',
                        paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                    }
                });

                // Restore column visibility
                $('.datatable-colvis-restore').DataTable({
                    buttons: [{
                        extend: 'colvis',
                        text: '<i class="icon-grid7"></i>',
                        className: 'btn bg-teal-400 btn-icon dropdown-toggle',
                        postfixButtons: ['colvisRestore']
                    }],
                    columnDefs: [
                        {targets: -1, orderable: false,},
                        { targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 15], visible: true},
                        { targets: '_all', visible: false }
                    ]
                });

            };

            return {
                init: function() {
                    _componentDatatableColumnVisibility();
                }
            }
        }();

        function form_action (mode, project_id) {
           let frm = document.form1;
            frm.project_id.value = project_id
           if(mode == 'edit'){
               frm.action = "{{route('admins.project.edit')}}";
               frm.method = 'get';
            frm.submit();
           }else if(mode == 'view'){
               frm.action = "{{route('admins.project.show')}}";
               frm.method = 'get';
               frm.submit();
           }else if(mode == 'delete'){

                // Defaults
                var swalInit = swal.mixin({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-light'
                });
                 swalInit.fire({
                    title: 'Bạn có chắc muốn xóa đối tượng này?',
                    text: "Bạn không thể hoàn tác lại thao tác này!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Vâng, Xóa đối tượng!',
                    cancelButtonText: 'Thoát',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function(result) {
                    if (result.value) {
                        frm.action = "{{route('admins.project.delete')}}";
                        frm.method = 'post';
                        frm.submit();
                    }
                });
            }
        }

        function initialSetup() {
           $hidden = $(".notifyHidden");
            if ($hidden != null) {
                $hidden.delay(3000).fadeOut('slow');
            }
        }

        // Initialize module
        // ------------------------------
        document.addEventListener('DOMContentLoaded', function() {
            DatatableColumnVisibility.init();
            initialSetup();
        });
    </script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<script src="{{ asset('asset/admins/js/pages/components_popups.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_select2.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/pages/extra_sweetalert.js')}}"></script> --}}
	<!-- /theme JS files -->

@stop
