@extends('layouts.admin_layout')
@section('title', 'Bất động sản')

@section('topnavdetail')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Bất động sản</span> - Danh sách</h4>
        <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.house.index') }}" class="breadcrumb-item"><i class="icon-city mr-2"></i> Bất động sản</a>
        <span class="breadcrumb-item active">Danh sách</span>
    </div>
    <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h6 class="alert-heading font-weight-semibold mb-1">Thông Báo</h6>
        <span>{{session('success')}}</span>
    </div>
    @endif

    @if(count($errors) > 0)
        <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h6 class="alert-heading font-weight-semibold mb-1">Thông Báo</h6>
            @foreach($errors->all() as $error)
            <p>{{$error}}</p>
            @endforeach
        </div>
    @endif
    <!-- /Notifications Set  -->

    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Tìm Kiếm</strong>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form action="{{route('admins.house.index')}}" method="get">
                {{csrf_field()}}
                <div class="form-group">
                    <div class="row align-items-center mb-2">
                        <div class="col-sm-2">
                            <select data-placeholder="Chọn hạng mục kinh doanh cho bất động sản" class="form-control select-search" id="house_business_id" name="house_business_id" data-fouc>
                                <optgroup label="Hạng Mục Kinh Doanh">
                                    <option value="">Không Có</option>
                                    @foreach ($businesses as $item)
                                        <option value="{{ $item->house_business_id }}" {{ $item->house_business_id == Request::get('house_business_id')? 'selected':'' }}>
                                            {{ $item->house_business_name }}
                                        </option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <select data-placeholder="Chọn hạng mục kinh doanh cho bất động sản" class="form-control select-search" id="house_category_id" name="house_category_id" data-fouc>
                                <optgroup label="Danh Mục">
                                    <option value="">Không Có</option>
                                    @foreach ($categories as $item)
                                        <option value="{{ $item->house_category_id }}" {{ $item->house_category_id == Request::get('house_category_id')? 'selected':'' }}>
                                            {{ $item->house_category_name }}
                                        </option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <select data-placeholder="Chọn dự án cho bất động sản" class="form-control select-search" id="project_id" name="project_id" data-fouc>
                                <optgroup label="Dự Án">
                                    <option value="">Không Có</option>
                                    @foreach ($projects as $item)
                                        <option value="{{ $item->project_id }}" {{ $item->project_id == Request::get('project_id')? 'selected':'' }}>
                                            {{ $item->project_name }}
                                        </option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <input type="text"  class="form-control" name="house_name" value="{{Request::get('house_name')}}" placeholder="Tìm kiếm theo Tiêu đề">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" data-type='currency' class="form-control" name="house_price" value="{{Request::get('house_price')}}" placeholder="Tìm kiếm theo Giá tiền">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" data-type='currency' class="form-control" name="house_price_sub" value="{{Request::get('house_price_sub')}}" placeholder="Tìm kiếm theo Khuyến mãi">
                        </div>
                    </div>
                    <div class="row align-items-center mb-2">
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="house_postal_code" value="{{Request::get('house_postal_code')}}" placeholder="Tìm kiếm theo mã bưu điện">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="house_provincial" value="{{Request::get('house_provincial')}}" placeholder="Tìm kiếm theo Tỉnh/Thành Phố">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="house_district" value="{{Request::get('house_district')}}" placeholder="Tìm kiếm theo Quận/Huyện">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="house_commune" value="{{Request::get('house_commune')}}" placeholder="Tìm kiếm theo Phường/Xã">
                        </div>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="house_address" value="{{Request::get('house_address')}}" placeholder="Tìm kiếm theo Địa Chỉ">
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="house_own" value="{{Request::get('house_own')}}" placeholder="Chủ dự án">
                        </div>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" name="house_acreage" value="{{Request::get('house_acreage')}}" placeholder="Diện tích">
                        </div>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" name="house_length" value="{{Request::get('house_length')}}" placeholder="Chiều dài">
                        </div>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" name="house_width" value="{{Request::get('house_width')}}" placeholder="Chiều rộng">
                        </div>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" name="house_direction" value="{{Request::get('house_direction')}}" placeholder="Hướng nhà">
                        </div>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" name="house_rooms" value="{{Request::get('house_rooms')}}" placeholder="Số phòng">
                        </div>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" name="house_bath_rooms" value="{{Request::get('house_bath_rooms')}}" placeholder="Phòng tắm">
                        </div>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" name="house_garages" value="{{Request::get('house_garages')}}" placeholder="Bãi đỗ xe">
                        </div>
                        <div class="col-sm-1">
                            <select class="form-control" name='active_status'>
                                <option value="0" {{Request::get('active_status') == 0 ? 'selected': ''}}>Kích hoạt</option>
                                <option value="1" {{Request::get('active_status') == 1 ? 'selected': ''}}>Bật</option>
                                <option value="2" {{Request::get('active_status') == 2 ? 'selected': ''}}>Tắt</option>
                            </select>
                        </div>
                        <div class="col-sm-1">
                            <input type="submit" class="btn btn-primary btn-lg active" value="Tìm Kiếm">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /restore column visibility -->
    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Danh Sách</strong>
                <a href="{{ route('admins.house.create') }}" class="btn btn-outline-danger"><i class="icon-plus-circle2 mr-2"></i> Thêm</a>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="collapse"></a>
                    <a class="list-icons-item" data-action="reload"></a>
                    <a class="list-icons-item" data-action="remove"></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table class="table datatable-colvis-restore">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Kinh Doanh</th>
                        <th>Danh mục</th>
                        <th>Dự Án</th>
                        <th>Tiêu Đề</th>
                        <th>Giá Tiền</th>
                        <th>Khuyến Mãi</th>
                        <th>Chủ Sở Hữu</th>
                        <th>Xây Dựng</th>
                        <th>Diện Tích</th>
                        <th>Chiều Dài</th>
                        <th>Chiều Rộng</th>
                        <th>Hướng Nhà</th>
                        <th>Số Phòng</th>
                        <th>Nhà Tắm</th>
                        <th>Nhà Xe</th>
                        <th>Mã Bưu Điện</th>
                        <th>Tỉnh/Thành Phố</th>
                        <th>Quận/Huyện</th>
                        <th>Phường/Xã</th>
                        <th>Địa Chỉ</th>
                        <th>Kích Hoạt</th>
                        <th class="text-right">Chức Năng</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $idx = 0; ?>
                    @forelse ($houses as $house)
                        <tr>
                            <?php $idx++; ?>
                            <td>{{$idx}}</td>
                            <td>{{$house->house_business_name}}</td>
                            <td>{{$house->house_category_name}}</td>
                            <td>{{$house->project_name}}</td>
                            <td>{{$house->house_name}}</td>
                            <td>
                                <span>
                                    @if ($house->house_price<=0)
                                        <span class="badge badge-warning">Thương lượng</span>
                                    @else
                                    <span
                                        @if (isset($house->house_price_sub)&&$house->house_price_sub>0) class="text-danger-600" style="text-decoration:line-through" @endif>
                                            @if (strlen($house->house_price)>9)
                                            {{ round($house->house_price/1000000000, 2).' Tỷ' }}
                                        @elseif(strlen($house->house_price)>6)
                                            {{ is_int(($house->house_price/1000000))?($house->house_price/1000000).' Triệu': number_format($house->house_price,0,'',',') }}
                                        @else
                                            {{ number_format($house->house_price,0,'',',') }}
                                        @endif
                                        </span>
                                        <small class="text-danger-600">vnđ</small></span>
                                    @endif
                                    </span>
                            </td>
                            <td>
                                @if (isset($house->house_price_sub)&&$house->house_price_sub>0)
                                <span>
                                    @if (strlen($house->house_price_sub)>9)
                                        {{ round($house->house_price_sub/1000000000, 2).' Tỷ' }}
                                    @elseif(strlen($house->house_price_sub)>6)
                                        {{ is_int(($house->house_price_sub/1000000))?($house->house_price_sub/1000000).' Triệu': number_format($house->house_price_sub,0,'',',') }}
                                    @else
                                        {{ number_format($house->house_price_sub,0,'',',') }}
                                        @endif
                                    <small class="text-danger-600">vnđ</small>
                                </span>
                                @endif
                            </td>
                            <td>{{$house->house_own}}</td>
                            <td>@isset($house->house_build_time)
                                {{date('d/m/Y', strtotime($house->house_build_time))}}
                            @endisset</td>

                            <td>{{$house->house_acreage}}</td>
                            <td>{{$house->house_length}}m</td>
                            <td>{{$house->house_width}}m</td>
                            <td>{{$house->house_direction}}</td>
                            <td>{{$house->house_rooms}}</td>
                            <td>{{$house->house_bath_rooms}}</td>
                            <td>{{$house->house_garages}}</td>


                            <td>{{$house->house_postal_code}}</td>
                            <td>{{$house->house_provincial}}</td>
                            <td>{{$house->house_district}}</td>
                            <td>{{$house->house_commune}}</td>
                            <td>{{$house->house_address}}</td>


                            <td>@if($house->active_status == 1)<span class="badge badge-success">BẬT</span>  @else <span class="badge badge-danger">TẮT</span> @endif </td>
                            <td class="text-right">
                                <div class="list-icons">
                                    <a href="javascript:void(0)" onclick="form_action('view', {{$house->house_id}});" role="button" data-popup="tooltip" title="Chi Tiết" data-placement="bottom" class="list-icons-item text-info-600"><i class="icon-file-eye2"></i></a>
                                    <a href="{{ route('admins.housegallery.index', ['id' => $house->house_id]) }}" data-popup="tooltip" title="Thêm Hình Ảnh" data-placement="bottom" class="list-icons-item text-warning-400"><i class="icon-file-picture2"></i></a>
                                    <a href="{{ route('admins.housefloorplan.index', ['id' => $house->house_id]) }}" data-popup="tooltip" title="Thêm Bản Vẽ" data-placement="bottom" class="list-icons-item text-warning-400"><i class="icon-file-presentation2"></i></a>
                                    <a href="javascript:void(0)" onclick="form_action('edit', {{$house->house_id}});" role="button" data-popup="tooltip" title="Cập Nhập" data-placement="bottom" class="list-icons-item text-warning-600"><i class="icon-pencil7"></i></a>
                                    <a  href="javascript:void(0)" onclick="form_action('delete', {{$house->house_id}});" role="button" data-popup="tooltip" title="Xóa" data-placement="bottom" class="list-icons-item text-danger-800"><i class="icon-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                        @empty
                    @endforelse
                </tbody>
            </table>
        </div>



        {{ $houses->appends([
            'house_category_id' => Request::get('house_category_id'),
            'house_business_id' => Request::get('house_business_id'),
            'project_id' => Request::get('project_id'),
            'house_name' => Request::get('house_name'),
            'house_price' => Request::get('house_price'),
            'house_price_sub' => Request::get('house_price_sub'),

            'house_own' => Request::get('house_own'),
            'house_acreage' => Request::get('house_acreage'),
            'house_length' => Request::get('house_length'),
            'house_width' => Request::get('house_width'),
            'house_direction' => Request::get('house_direction'),
            'house_rooms' => Request::get('house_rooms'),
            'house_bath_rooms' => Request::get('house_bath_rooms'),
            'house_garages' => Request::get('house_garages'),

            'house_postal_code' => Request::get('house_postal_code'),
            'house_provincial' => Request::get('house_provincial'),
            'house_district' => Request::get('house_district'),
            'house_address' => Request::get('house_address'),

            'active_status' => Request::get('active_status'),
        ])->links('admins.includes.pagination') }}
    </div>
    <!-- /restore column visibility -->



    <form name="form1" action="" method="">
        {{csrf_field()}}
        <div class="form-group ">
            <input type="hidden" class="form-control" name="house_id" value="">
        </div>
    </form>
@stop

@section('javascript')
	<!-- Theme JS files -->
	<script src="{{ asset('asset/admins/js/plugins/tables/datatables/datatables.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/selects/select2.min.js')}}"></script> --}}
	{{-- <script src="{{ asset('asset/admins/js/pages/datatables_extension_colvis.js')}}"></script> --}}
	<script src="{{ asset('asset/admins/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/selects/select2.min.js')}}"></script>


	<script src="{{ asset('asset/admins/js/plugins/notifications/sweet_alert.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
    <script>
        var DatatableColumnVisibility = function() {
            var _componentDatatableColumnVisibility = function() {
                if (!$().DataTable) {
                    console.warn('Warning - datatables.min.js is not loaded.');
                    return;
                }

                // Setting datatable defaults
                $.extend($.fn.dataTable.defaults, {
                    paging: false,
                    autoWidth: false,
                    dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                    language: {
                        search: '<span>Tìm kiếm:</span> _INPUT_',
                        searchPlaceholder: 'Nhập từ khóa...',
                        lengthMenu: '<span>Hiển thị:</span> _MENU_',
                        paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                    }
                });

                // Restore column visibility
                $('.datatable-colvis-restore').DataTable({
                        buttons: {
                            buttons: [
                                {
                                    extend: 'colvisGroup',
                                    className: 'btn btn-light',
                                    text: 'Chi Tiết',
                                    show: [0, 1, 2, 3, 4, 7, 8, 9, 10, 11, 12, 13, 14, 15, 22],
                                    hide: [5, 6, 16, 17, 18, 19, 20, 21, 21]
                                },
                                {
                                    extend: 'colvisGroup',
                                    className: 'btn btn-light',
                                    text: 'Địa Lý',
                                    show: [0, 1, 2, 3, 4, 16, 17, 18, 19, 20, 22],
                                    hide: [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 21]
                                },
                                {
                                    extend: 'colvisGroup',
                                    text: 'Cơ bản',
                                    className: 'btn btn-light',
                                    show: [0, 1, 2, 3, 4, 5, 21, 22],
                                    hide: [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21]
                                },
                                {
                                    extend: 'colvisGroup',
                                    className: 'btn btn-light',
                                    text: 'Tất Cả',
                                    show: ':hidden'
                                },
                                {
                                    extend: 'colvis',
                                    text: '<i class="icon-grid7"></i>',
                                    className: 'btn bg-teal-400 btn-icon dropdown-toggle',
                                    postfixButtons: ['colvisRestore']
                                },
                            ],
                        },
                        columnDefs: [
                            {targets: -1, orderable: false,},
                            { targets: [0, 1, 2, 3, 4, 5, 6, 21, 22], visible: true},
                            { targets: '_all', visible: false }
                        ]
                    });
                };

            return {
                init: function() {
                    _componentDatatableColumnVisibility();
                }
            }
        }();

        function form_action (mode, house_id) {
           let frm = document.form1;
            frm.house_id.value = house_id
           if(mode == 'edit'){
               frm.action = "{{route('admins.house.edit')}}";
               frm.method = 'get';
            frm.submit();
           }else if(mode == 'view'){
               frm.action = "{{route('admins.house.show')}}";
               frm.method = 'get';
               frm.submit();
           }else if(mode == 'delete'){

                // Defaults
                var swalInit = swal.mixin({
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-light'
                });
                 swalInit.fire({
                    title: 'Bạn có chắc muốn xóa đối tượng này?',
                    text: "Bạn không thể hoàn tác lại thao tác này!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Vâng, Xóa đối tượng!',
                    cancelButtonText: 'Thoát',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                }).then(function(result) {
                    if (result.value) {
                        frm.action = "{{route('admins.house.delete')}}";
                        frm.method = 'post';
                        frm.submit();
                    }
                });
            }
        }

        function initialSetup() {
           $hidden = $(".notifyHidden");
            if ($hidden != null) {
                $hidden.delay(3000).fadeOut('slow');
            }
        }

        // Initialize module
        // ------------------------------
        document.addEventListener('DOMContentLoaded', function() {
            DatatableColumnVisibility.init();
            initialSetup();
        });
    </script>
    <script>
        $("input[data-type='currency']").on({
            keyup: function() {
                formatCurrency($(this));
            },
            blur: function() {
                formatCurrency($(this), "blur");
            },
        });


        function formatNumber(n) {
            // format number 1000000 to 1,234,567
            return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            }


            function formatCurrency(input, blur) {
                // appends $ to value, validates decimal side
                // and puts cursor back in right position.

                // get input value
                var input_val = input.val();

                // don't validate empty input
                if (input_val === "") { return; }

                // original length
                var original_len = input_val.length;

                // initial caret position
                var caret_pos = input.prop("selectionStart");

                // check for decimal
                if (input_val.indexOf(".") >= 0) {

                    // // get position of first decimal
                    // // this prevents multiple decimals from
                    // // being entered
                    // var decimal_pos = input_val.indexOf(".");

                    // split number by decimal point
                    var left_side = input_val.substring(0, decimal_pos);
                    // var right_side = input_val.substring(decimal_pos);

                    // add commas to left side of number
                    left_side = formatNumber(left_side);

                    // // validate right side
                    // right_side = formatNumber(right_side);

                    // // On blur make sure 2 numbers after decimal
                    // if (blur === "blur") {
                    // right_side += "00";
                    // }

                    // // Limit decimal to only 2 digits
                    // right_side = right_side.substring(0, 2);

                    // join number by .
                    input_val = left_side + "." + right_side;

                } else {
                    // no decimal entered
                    // add commas to number
                    // remove all non-digits
                    input_val = formatNumber(input_val);
                    input_val = input_val;

                    // // final formatting
                    // if (blur === "blur") {
                    // input_val += ".00";
                    // }
                }

                // send updated string to input
                input.val(input_val);

                // put caret back in the right position
                var updated_len = input_val.length;
                caret_pos = updated_len - original_len + caret_pos;
                input[0].setSelectionRange(caret_pos, caret_pos);
            }
    </script>
    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<script src="{{ asset('asset/admins/js/pages/components_popups.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_select2.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/pages/extra_sweetalert.js')}}"></script> --}}
	<!-- /theme JS files -->

@stop
