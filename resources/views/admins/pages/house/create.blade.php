@extends('layouts.admin_layout')
@section('title', 'Bất động bản')
@section('topnavdetail')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Bất Động Sản</span> - Tạo mới</h4>
        <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.house.index') }}" class="breadcrumb-item"><i class="icon-city mr-2"></i> Bất Động Sản</a>
        <span class="breadcrumb-item active">Tạo mới</span>
    </div>
    <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
        <h5>{{session('success')}}</h5>
        <br>
        <a href="{{ route('admins.house.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
    </div>
    @endif

    @if(count($errors) > 0)
         <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
            @foreach($errors->all() as $error)
            <h5>{{$error}}</h5>
            @endforeach
            <br>
            <a href="{{ route('admins.house.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
        </div>
    @endif
    <!-- /Notifications Set  -->
    <!-- Restore column visibility -->


    <div class="card">
        <div class="card-header header-elements-inline">
            <h3 class="card-title">
                <strong class="mr-4">Tạo Mới</strong>
            </h3>
        </div>
    </div>


    <form action="{{ route('admins.house.store') }}" method="POST" name="house-form">
        {{csrf_field()}}
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">
                    <strong class="mr-4">Thông Tin Cơ Bản</strong>
                </h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold"></legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="menu_lv1">Kinh Doanh<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <select data-placeholder="Chọn hạng mục kinh doanh cho bất động sản" class="form-control select-search" id="house_business_id" name="house_business_id" data-fouc>
                                <optgroup label="Hạng Mục Kinh Doanh">
                                    @foreach ($businesses as $item)
                                        <option value="{{ $item->house_business_id }}" {{ $item->house_business_id= old('house_business_id',$house->house_business_id)?'selected':''}}>
                                            {{ $item->house_business_name }}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="menu_lv1">Danh Mục<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <select data-placeholder="Chọn hạng mục kinh doanh cho bất động sản" class="form-control select-search" id="house_category_id" name="house_category_id" data-fouc>
                                <optgroup label="Danh Mục">
                                    @foreach ($categories as $item)
                                        <option value="{{ $item->house_category_id }}" {{ $item->house_category_id== old('house_category_id',$house->house_category_id)?'selected':''}}>
                                            {{ $item->house_category_name }}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="menu_lv1">Dự Án:</label>
                        <div class="col-lg-10">
                            <select data-placeholder="Chọn dự án cho bất động sản" class="form-control select-search" id="project_id" name="project_id" data-fouc>
                                <optgroup label="Dự Án">
                                    <option value="">Không Có</option>
                                    @foreach ($projects as $item)
                                        <option value="{{ $item->project_id }}" {{ $item->project_id=  old('project_id',$house->project_id)?'selected':''}}>
                                            {{ $item->project_name }}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_name">Tiêu đề<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="500" placeholder="Nhập vào tiêu đề"  id="house_name" name="house_name" value="{{ old('house_name', $house->house_name) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_price">Giá:</label>
                        <div class="col-lg-10">
                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="text" data-type='currency' class="form-control" maxlength="100" placeholder="Nhập vào số tiền"  id="house_price" name="house_price" value="{{ old('house_price', $house->house_price) }}">
                                <div class="form-control-feedback">
                                    <i class="icon-cash3"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_price_sub">Khuyến Mãi:</label>
                        <div class="col-lg-10">
                            <div class="form-group form-group-feedback form-group-feedback-left">
                                <input type="text" data-type='currency' class="form-control" maxlength="100" placeholder="Nhập vào giá khuyến mãi"  id="house_price_sub" name="house_price_sub" value="{{ old('house_price_sub', $house->house_price_sub) }}">
                                <div class="form-control-feedback">
                                    <i class="icon-gift"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_description">Mô tả<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <textarea rows="3" cols="3" class="form-control" maxlength="1000" id="house_description" name="house_description" placeholder="Nhập vào mô tả">{{ old('house_description', $house->house_description) }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_detail">Chi Tiết<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <textarea rows="3" cols="3" class="form-control" id="house_detail" name="house_detail" placeholder="Nhập vào chi tiết">{{ old('house_detail', $house->house_detail) }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="active_status">Trạng Thái:</label>
                        <div class="col-lg-10">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" id="active_status" name="active_status" class="form-check-input-styled-primary" value="1"
                                    {{ old('active_status', $house->active_status)==1?'checked':'' }}  data-fouc>
                                    Kích Hoạt
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">
                    <strong class="mr-4">Tiện Nghi</strong>
                </h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold"></legend>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_amenities">Tiện Nghi:</label>
                        <div class="col-lg-10">
                            <select multiple="multiple" class="form-control listbox-tall" id="house_amenities" name="house_amenities[]" data-fouc>
                                @foreach ($amenities as $item)
                                    <option value="{{ $item->amenity_id }}">{{ $item->amenity_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">
                    <strong class="mr-4">Trạng Thái Quảng Cáo</strong>
                </h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold"></legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_title">Đánh Giá<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <select data-placeholder="Chọn sao cho sản phẩm" class="form-control select" id="house_rating" name="house_rating" data-fouc>
                            <optgroup label="Số Sao Hiển Thị">
                                <option value="1" {{ old('house_rating', $house->house_rating)==1?'selected':''}}>1 Sao</option>
                                <option value="2" {{ old('house_rating', $house->house_rating)==2?'selected':''}}>2 Sao</option>
                                <option value="3" {{ old('house_rating', $house->house_rating)==3?'selected':''}}>3 Sao</option>
                                <option value="4" {{ old('house_rating', $house->house_rating)==4?'selected':''}}>4 Sao</option>
                                <option value="5" {{ old('house_rating', $house->house_rating)==5?'selected':''}}>5 Sao</option>
                            </optgroup>
                        </select>
                    </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_status">Trạng Thái Quảng Cáo:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="20" placeholder="Nhập vào trạng thái quảng cáo như Bán, Thuê, Hot..." id="house_status" name="house_status" value="{{ old('house_status', $house->house_status) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_status_color">Màu Trạng Thái:</label>
                        <div class="col-lg-10">
                            <select data-placeholder="Chọn Loại sản phẩm" class="form-control select" id="house_status_color" name="house_status_color" data-fouc>
                                <optgroup label="Chọn Màu Trạng Thái">
                                    <option value="bg-danger" {{ old('house_status_color', $house->house_status_color)=='bg-danger'?'selected':''}}>Màu Đỏ</option>
                                    <option value="bg-primary" {{ old('house_status_color', $house->house_status_color)=='bg-primary'?'selected':''}}><span class="text-info">Màu Xanh Đậm</span></option>
                                    <option value="bg-info" {{ old('house_status_color', $house->house_status_color)=='bg-info'?'selected':''}}><span class="text-info">Màu Xanh Nhạt</span></option>
                                    <option value="bg-warning" {{ old('house_status_color', $house->house_status_color)=='bg-warning'?'selected':''}}><span class="text-warning">Màu Vàng</span></option>
                                    <option value="bg-success" {{ old('house_status_color', $house->house_status_color)=='bg-success'?'selected':''}}><span class="text-success">Màu Xanh Lá Cây</span></option>
                                    <option value="bg-secondary" {{ old('house_status_color', $house->house_status_color)=='bg-secondary'?'selected':''}}><span class="text-dark">Màu Xám</span></option>
                                    <option value="bg-dark" {{ old('house_status_color', $house->house_status_color)=='bg-dark'?'selected':''}}><span class="text-dark">Màu Đen</span></option>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">
                    <strong class="mr-4">Thông Tin Chi Tiết</strong>
                </h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold"></legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_own">Chủ Sở Hữu:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào chủ sở hữu" id="house_own" name="house_own" value="{{ old('house_own', $house->house_own) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_build_time">Thời Gian Xây Dựng:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control pickadate-translated" placeholder="Nhập vào thời gian xây dựng" id="house_build_time"
                            name="house_build_time" value="{{ old('house_build_time', $house->house_build_time) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_acreage">Diện Tích:</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text"><i class="icon-move-alt1"></i></span>
                                </span>
                                <input type="" class="form-control" maxlength="50" placeholder="Nhập vào tổng diện tích"  id="house_acreage" name="house_acreage" value="{{ old('house_acreage', $house->house_acreage) }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_length">Chiều Dài:</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text"><i class="icon-width"></i></span>
                                </span>
                                <input type="number" class="form-control" maxlength="20" placeholder="Nhập vào chiều dài"  id="house_length" name="house_length" value="{{ old('house_length', $house->house_length) }}">
                                <span class="input-group-append">
                                    <span class="input-group-text">m</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_width">Chiều Rộng:</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text"><i class="icon-height2"></i></span>
                                </span>
                                <input type="number" class="form-control" maxlength="20" placeholder="Nhập vào chiều rộng"  id="house_width" name="house_width" value="{{ old('house_width', $house->house_width	) }}">
                                <span class="input-group-append">
                                    <span class="input-group-text">m</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_direction">Hướng Nhà:</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text"><i class="icon-direction"></i></span>
                                </span>
                                <input type="text" class="form-control" maxlength="50" placeholder="Nhập vào hướng nhà"  id="house_direction" name="house_direction" value="{{ old('house_direction', $house->house_direction	) }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_rooms">Số Phòng:</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text"><i class="icon-home7"></i></span>
                                </span>
                                <input type="number" class="form-control" maxlength="5" placeholder="Nhập vào số phòng"  id="house_rooms" name="house_rooms" value="{{ old('house_rooms', $house->house_rooms	) }}">
                                <span class="input-group-append">
                                    <span class="input-group-text">căn</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_bath_rooms">Nhà Tắm:</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text"><i class="icon-home7"></i></span>
                                </span>
                                <input type="number" class="form-control" maxlength="5" placeholder="Nhập vào số phòng tắm"  id="house_bath_rooms" name="house_bath_rooms" value="{{ old('house_bath_rooms', $house->house_bath_rooms	) }}">
                                <span class="input-group-append">
                                    <span class="input-group-text">căn</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_garages">Nhà Xe:</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <span class="input-group-text"><i class="icon-home7"></i></span>
                                </span>
                                <input type="number" class="form-control" maxlength="5" placeholder="Nhập vào số nhà để xe"  id="house_garages" name="house_garages" value="{{ old('house_garages', $house->house_garages	) }}">
                                <span class="input-group-append">
                                    <span class="input-group-text">căn</span>
                                </span>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">
                    <strong class="mr-4">Vị Trí Địa Lý</strong>
                </h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold"></legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_postal_code">Mã Bưu Biện<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào mã bưu biện" id="house_postal_code" name="house_postal_code" value="{{ old('house_postal_code', $house->house_postal_code) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_provincial">Tỉnh/Thành Phố<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào tỉnh/thành phố" id="house_provincial" name="house_provincial" value="{{ old('house_provincial', $house->house_provincial) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_district">Quận/Huyện<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào quận/huyện" id="house_district" name="house_district" value="{{ old('house_district', $house->house_district) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_commune">Xã/Phường<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào xã/phường" id="house_commune" name="house_commune" value="{{ old('house_commune', $house->house_commune) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_address">Địa Chỉ<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào địa chỉ" id="house_address" name="house_address" value="{{ old('house_address', $house->house_address) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="house_map_location">Định Vị Bản Đồ<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="1000" placeholder="Nhập vào Định Vị Bản Đồ" id="house_map_location" name="house_map_location" value="{{ old('house_map_location', $house->house_map_location) }}">
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">
                    <strong class="mr-4">Tối Ưu Quảng Cáo</strong>
                </h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <fieldset class="mb-3">
                    <legend class="text-uppercase font-size-sm font-weight-bold"></legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_title">Tiêu đề Website:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="20" placeholder="Nhập vào tiêu đề hiển thị website" id="web_title" name="web_title" value="{{ old('web_title', $house->web_title) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_keywords">Từ khóa tìm kiếm:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control tokenfield" id="web_keywords" placeholder="Từ khóa" maxlength="200" name="web_keywords"  value="{{ old('web_keywords', $house->web_keywords) }}" data-fouc>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="web_description">Mô Tả Website:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="145" placeholder="Nhập vào mô tả website"  id="web_description" name="web_description" value="{{ old('web_description', $house->web_description) }}">
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>

        <div class="card card-body">
            <div class="text-center">
                <button type="submit" class="btn btn-primary"><i class="icon-download4 mr-2"> Lưu Lại</i></button>
                <a href="{{ route('admins.house.index') }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở Về Danh Sách</a>
            </div>
        </div>
    </form>

    <!-- /restore column visibility -->

@stop

@section('javascript')

	<!-- Theme JS files -->
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tagsinput.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tokenfield.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/ui/prism.min.js')}}"></script>

	<script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
	<script src="{{ asset('asset/admins/js/plugins/forms/styling/switch.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/selects/select2.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/inputs/duallistbox/duallistbox.min.js')}}"></script>


	<script src="{{ asset('asset/admins/js/plugins/ui/moment/moment.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/pickers/daterangepicker.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/pickers/anytime.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/pickers/pickadate/picker.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/pickers/pickadate/picker.date.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/pickers/pickadate/picker.time.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/pickers/pickadate/legacy.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/notifications/jgrowl.min.js')}}"></script>
    <script src="{{ asset('asset/admins/js/plugins/editors/ckeditor/ckeditor.js')}}"></script>

    <script src="{{ asset('asset/admins/js/plugins/forms/validation/validate.min.js') }}"></script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_tags_input.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_checkboxes_radios.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_select2.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_dual_listboxes.js')}}"></script>

	{{-- <script src="{{ asset('asset/admins/js/pages/picker_date.js')}}"></script> --}}


	<!-- /theme JS files -->
    <script>
        CKEDITOR.replace( 'house_detail', {
            filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',

        } );
    </script>
    @include('ckfinder::setup')

    <script type="text/javascript" src="/js/ckfinder/ckfinder.js"></script>
    <script>CKFinder.config( { connectorPath: '/ckfinder/connector' } );</script>

    <script>
        function initialSetup() {
           $hidden = $(".notifyHidden");
            if ($hidden != null) {
                $hidden.delay(10000).fadeOut('slow');
            }
        }

        $("input[data-type='currency']").on({
            keyup: function() {
                formatCurrency($(this));
            },
            blur: function() {
                formatCurrency($(this), "blur");
            },
        });


        $( document ).ready( function () {
            initialSetup();
            formatCurrency($("input[name='house_price']"));

            // Localization
            $('.pickadate-translated').pickadate({
                singleDatePicker: true,
                selectYears: true,
                selectMonths: true,
                monthsFull: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
                weekdaysShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
                today: 'Hôm Nay',
                clear: 'Xóa',
                format: 'dd/mm/yyyy',
            });
            $( "form[name=house-form]" ).validate( {
                rules: {
                    house_name: {
                        required: true,
                        maxlength: 500
                    },
                    house_description: {
                        required: true,
                        maxlength: 1000
                    },
                    // house_price: {
                    //     required: true
                    // },
                    house_detail: {
                        required: true
                    },
                    house_postal_code: {
                        required: true,
                        maxlength: 100
                    },
                    house_provincial: {
                        required: true,
                        maxlength: 100
                    },
                    house_district: {
                        required: true,
                        maxlength: 100
                    },
                    house_commune: {
                        required: true,
                        maxlength: 100
                    },
                    house_address: {
                        required: true,
                        maxlength: 200
                    },
                    house_map_location: {
                        required: true,
                        maxlength: 1000
                    },
                },
                messages: {
                    house_name: {
                        required: "Bạn không được để trống.",
                        maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    },
                    house_description: {
                        required: "Bạn không được để trống.",
                        maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    },
                    // house_price: {
                    //     required: "Bạn không được để trống.",
                    // },
                    house_detail: {
                        required: "Bạn không được để trống.",
                        maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    },
                    house_postal_code: {
                        required: "Bạn không được để trống.",
                        maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    },
                    house_provincial: {
                        required: "Bạn không được để trống.",
                        maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    },
                    house_district: {
                        required: "Bạn không được để trống.",
                        maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    },
                    house_commune: {
                        required: "Bạn không được để trống.",
                        maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    },
                    house_address: {
                        required: "Bạn không được để trống.",
                        maxlength: "Bạn chỉ được nhập tối đa 200 ký tự."
                    },
                    house_map_location: {
                        required: "Bạn không được để trống.",
                        maxlength: "Bạn chỉ được nhập tối đa 1000 ký tự."
                    },
                },
                errorElement: "span",
                errorPlacement: function ( error, element ) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("form-text").addClass( "text-danger" );

                    if ( element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( element.next( "label" ) );
                    } else {
                        error.insertAfter( element );
                    }
                },
                highlight: function ( element, errorClass, validClass ) {
                    $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
                },
                unhighlight: function (element, errorClass, validClass) {
                    $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
                }
            } );

        } );
        function formatNumber(n) {
        // format number 1000000 to 1,234,567
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }


        function formatCurrency(input, blur) {
            // appends $ to value, validates decimal side
            // and puts cursor back in right position.

            // get input value
            var input_val = input.val();

            // don't validate empty input
            if (input_val === "") { return; }

            // original length
            var original_len = input_val.length;

            // initial caret position
            var caret_pos = input.prop("selectionStart");

            // check for decimal
            if (input_val.indexOf(".") >= 0) {

                // // get position of first decimal
                // // this prevents multiple decimals from
                // // being entered
                // var decimal_pos = input_val.indexOf(".");

                // split number by decimal point
                var left_side = input_val.substring(0, decimal_pos);
                // var right_side = input_val.substring(decimal_pos);

                // add commas to left side of number
                left_side = formatNumber(left_side);

                // // validate right side
                // right_side = formatNumber(right_side);

                // // On blur make sure 2 numbers after decimal
                // if (blur === "blur") {
                // right_side += "00";
                // }

                // // Limit decimal to only 2 digits
                // right_side = right_side.substring(0, 2);

                // join number by .
                input_val = left_side + "." + right_side;

            } else {
                // no decimal entered
                // add commas to number
                // remove all non-digits
                input_val = formatNumber(input_val);
                input_val = input_val;

                // // final formatting
                // if (blur === "blur") {
                // input_val += ".00";
                // }
            }

            // send updated string to input
            input.val(input_val);

            // put caret back in the right position
            var updated_len = input_val.length;
            caret_pos = updated_len - original_len + caret_pos;
            input[0].setSelectionRange(caret_pos, caret_pos);
        }

    </script>
@stop
