@extends('layouts.admin_layout')
@section('title', 'Bất Động Sản')
@section('styles')
<!-- Plugins CSS -->
<link href="{{ asset('asset/clients/css/font-awesome.min.css')}}" rel="stylesheet" />
<link href="{{ asset('asset/clients/css/linearicon.min.css')}}" rel="stylesheet" />
@endsection
@section('topnavdetail')
<div class="page-title d-flex">
    <h4><i class="icon-arrow-left52 mr-2"></i> <span class="font-weight-semibold">Bất Động Sản</span> - Chi tiết</h4>
    <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
</div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.house.index') }}" class="breadcrumb-item"><i class="icon-city mr-2"></i> Bất Động Sản</a>
        <span class="breadcrumb-item active">Chi Tiết</span>
    </div>
    <a href="{{ route('admins.house.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection

@section('content')


    <!-- Inner container -->
    <div class="d-flex align-items-start flex-column flex-md-row">

        <!-- Left content -->
        <div class="w-100 overflow-auto order-2 order-md-1">

            <!-- Course overview -->

            <div class="card">
                <div class="card-header header-elements-md-inline">
                    <h5 class="card-title">{{ $house->house_name }}</h5>

                    {{-- <div class="header-elements">
                        <ul class="list-inline list-inline-dotted mb-0 mt-2 mt-md-0">
                            <li class="list-inline-item">Rating: <span class="font-weight-semibold">4.85</span></li>
                            <li class="list-inline-item">
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <i class="icon-star-full2 font-size-base text-warning-300"></i>
                                <span class="text-muted ml-1">(439)</span>
                            </li>
                        </ul>
                    </div> --}}
                </div>

                <div class="nav-tabs-responsive bg-light border-top">
                    <ul class="nav nav-tabs nav-tabs-bottom flex-nowrap mb-0">
                        <li class="nav-item"><a href="#course-overview" class="nav-link active" data-toggle="tab"><i class="icon-menu7 mr-2"></i> Thông Tin</a></li>
                        <li class="nav-item"><a href="#course-detail" class="nav-link" data-toggle="tab"><i class="icon-newspaper mr-2"></i> Chi Tiết</a></li>
                        <li class="nav-item"><a href="#course-attendees" class="nav-link" data-toggle="tab"><i class="icon-cup2 mr-2"></i> Tiện Nghi</a></li>
                        <li class="nav-item"><a href="#course-schedule" class="nav-link" data-toggle="tab"><i class="icon-image3 mr-2"></i> Hình Ảnh</a></li>
                        <li class="nav-item"><a href="#course-floorplad" class="nav-link" data-toggle="tab"><i class="icon-image3 mr-2"></i> Bản Vẽ</a></li>
                        <li class="nav-item"><a href="#course-review" class="nav-link" data-toggle="tab"><i class="icon-stars mr-2"></i> Đánh Giá</a></li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane fade show active" id="course-overview">
                        <div class="card-body">
                            <div class="table-responsive">
                                <h6 class="font-weight-semibold">Cơ bản</h6>
                                <table class="table">
                                    <tr>
                                        <th width="20%" scope="col">Kinh Doanh</th>
                                        <td width="80%">{{$house->house_business_name}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="col">Danh mục</th>
                                        <td>{{$house->house_category_name}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="col">Dự Án</th>
                                        <td>{{$house->project_name}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="col">Tiêu đề</th>
                                        <td>{{$house->house_name}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="col">Mô tả</th>
                                        <td>{{$house->house_description}}</td>
                                    </tr>
                                    <tr>
                                        <th>Giá</th>
                                        <td>
                                            <span>
                                                @if ($house->house_price<=0)
                                                    <span class="badge badge-warning">Thương lượng</span>
                                                @else
                                                <span
                                                    @if (isset($house->house_price_sub)&&$house->house_price_sub>0) class="text-danger-600" style="text-decoration:line-through" @endif>
                                                        @if (strlen($house->house_price)>9)
                                                        {{ round($house->house_price/1000000000, 2).' Tỷ' }}
                                                    @elseif(strlen($house->house_price)>6)
                                                        {{ is_int(($house->house_price/1000000))?($house->house_price/1000000).' Triệu': number_format($house->house_price,0,'',',') }}
                                                    @else
                                                        {{ number_format($house->house_price,0,'',',') }}
                                                    @endif
                                                    </span>
                                                    <small class="text-danger-600">vnđ</small></span>
                                                @endif
                                                </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Khuyến Mãi</th>
                                        <td>
                                            @if (isset($house->house_price_sub)&&$house->house_price_sub>0)
                                            <span>
                                                @if (Str::length($house->house_price_sub)>9)
                                                    {{ is_int(($house->house_price_sub/1000000000))?($house->house_price_sub/1000000000).' Tỷ': number_format($house->house_price_sub,0,'',',') }}
                                                @elseif(Str::length($house->house_price_sub)>6)
                                                    {{ is_int(($house->house_price_sub/1000000))?($house->house_price_sub/1000000).' Triệu': number_format($house->house_price_sub,0,'',',') }}
                                                @else
                                                    {{ number_format($house->house_price_sub,0,'',',') }}
                                                @endif
                                                <sup class="text-danger-600">vnđ</sup>
                                            </span>
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <hr class="border-danger">
                            <div class="table-responsive">
                                <h6 class="font-weight-semibold">Trạng Thái Quảng Cáo</h6>
                                <table class="table">
                                    <tr>
                                        <th width="20%" scope="col">Hiển thị</th>
                                        <td width="80%">  @if($house->house_status != '')<span class="badge {{ str_replace('bg','badge',$house->house_status_color) }}">{{ $house->house_status }}</span> @endif</td>
                                    </tr>
                                </table>
                            </div>
                            <hr class="border-danger">
                            <div class="table-responsive">
                                <h6 class="font-weight-semibold">Chi tiết</h6>
                                <table class="table">
                                    <tr>
                                        <th width="20%" scope="col">Chủ sở hữu</th>
                                        <td width="80%">{{$house->house_own}}</td>
                                    </tr>
                                    <tr>
                                        <th>Thời gian xây dựng</th>
                                        <td>{{date('d/m/Y',strtotime($house->house_build_time))}}</td>
                                    </tr>
                                    <tr>
                                        <th>Diện Tích</th>
                                        <td>{{$house->house_acreage}}</td>
                                    </tr>
                                    <tr>
                                        <th>Chiều Dài</th>
                                        <td>{{$house->house_length}}m</td>
                                    </tr>
                                    <tr>
                                        <th>Chiều Rộng</th>
                                        <td>{{$house->house_width}}m</td>
                                    </tr>
                                    <tr>
                                        <th>Hướng Nhà</th>
                                        <td>Hướng {{$house->house_direction}}</td>
                                    </tr>
                                    <tr>
                                        <th>Số Phòng</th>
                                        <td>{{$house->house_rooms}} căn</td>
                                    </tr>
                                    <tr>
                                        <th>Nhà Tắm</th>
                                        <td>{{$house->house_bath_rooms}} căn</td>
                                    </tr>
                                    <tr>
                                        <th>Nhà Xe</th>
                                        <td>{{$house->house_garages}} căn</td>
                                    </tr>
                                </table>
                            </div>
                            <hr class="border-danger">
                            <div class="table-responsive">
                                <h6 class="font-weight-semibold">Vị Trí Địa Lý</h6>
                                <table class="table">
                                    <tr>
                                        <th width="20%" scope="col">Mã bưu điện</th>
                                        <td width="80%">{{$house->house_name}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="col">Tỉnh/Thành Phố</th>
                                        <td>{{$house->house_provincial}}</td>
                                    </tr>
                                    <tr>
                                        <th>Quận/ Huyện</th>
                                        <td>{{$house->house_district}}</td>
                                    </tr>
                                    <tr>
                                        <th>Phường/ Xã</th>
                                        <td>{{$house->house_commune}}</td>
                                    </tr>
                                    <tr>
                                        <th>Địa chỉ</th>
                                        <td>{{$house->house_address}}</td>
                                    </tr>
                                    <tr>
                                        <th>Định vị bản đồ</th>
                                        <td>{{$house->house_map_location}}</td>
                                    </tr>
                                </table>
                            </div>
                            <hr class="border-danger">
                            <div class="table-responsive">
                                <h6 class="font-weight-semibold">Tối Ưu SEO</h6>
                                <table class="table">
                                    <tr>
                                        <th width="20%" scope="col">Tiêu đề website</th>
                                        <td width="80%">{{$house->web_title}}</td>
                                    </tr>
                                    <tr>
                                        <th>Từ khóa tìm kiếm</th>
                                        <td>{{$house->web_keywords}}</td>
                                    </tr>
                                    <tr>
                                        <th>Mô tả website</th>
                                        <td>{{$house->web_description}}</td>
                                    </tr>
                                    <tr>
                                        <th>Đường dẫn</th>
                                        <td><a href="{{url('du-an').'/'.$house->category_web_canonical.'/'.$house->web_canonical}}">{{url('').'/'.$house->category_web_canonical.'/'.$house->web_canonical}}</a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="course-detail">
                        <div class="card-body">
                            <div class="container-fluid border border-warning p-5">
                                {!! $house->house_detail !!}
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="course-attendees">
                        <div class="card-body">
                            <div class="row">
                                @forelse ($amenities as $item)
                                <div class="col-xl-3 col-md-6">
                                    <div class="card card-body">
                                        <div class="media">
                                            <div class="mr-3">
                                                <a href="#" class="">
                                                    <i class="{{ $item->amenity_icon }}" style="font-size:2.8rem"></i>
                                                    {{-- <img src="../../../../global_assets/images/demo/users/face11.jpg" class="rounded-circle" width="42" height="42" alt=""> --}}
                                                </a>
                                            </div>

                                            <div class="media-body">
                                                <h6 class="mb-0">{{ $item->amenity_name }}</h6>
                                                <span class="text-muted"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @empty
                                <h1 class="ml-5 p-5">Hiện tại, Dự án chưa có tiện nghi!</h1>
                                @endforelse
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="course-schedule">
                        <div class="card-body">

                            <div class="row">
                                @forelse ($galleries as $item)


                                <div class="col-xl-3 col-sm-6">
                                    <div class="card">
                                        <div class="card-img-actions mx-1 mt-1">
                                            <img class="card-img img-fluid" src="{{ asset('storage/'.$item->house_gallery_link) }}" alt="">
                                            <div class="card-img-actions-overlay card-img">
                                                <a href="{{ asset('storage/'.$item->house_gallery_link) }}" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round" data-popup="lightbox" rel="group">
                                                    <i class="icon-zoomin3"></i>
                                                </a>

                                                {{-- <a href="#" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round ml-2">
                                                    <i class="icon-download"></i>
                                                </a> --}}
                                            </div>
                                        </div>

                                        <div class="card-body">
                                            <div class="d-flex align-items-start flex-wrap">
                                                <div class="font-weight-semibold">{{ $item->house_gallery_name }}</div>
                                                <span class="font-size-sm text-muted ml-auto">
                                                    {{-- @if($item->house_gallery_banner == 1)
                                                    <span class="badge badge-warning">BĂNG RÔN</span>  @else <span class="badge badge-dark">HÌNH ẢNH</span>
                                                    @endif --}}

                                                    @if($item->house_gallery_banner == 1)
                                                        <span class="badge badge-danger">BĂNG RÔN</span>
                                                    @elseif($item->house_gallery_banner == 2)
                                                        <span class="badge badge-warning">ĐẠI DIỆN</span>
                                                    @else
                                                        <span class="badge badge-dark">HÌNH ẢNH</span>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @empty
                                <h1 class="ml-5 p-5">Hiện tại, Dự án chưa có hình ảnh!</h1>
                                @endforelse
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="course-floorplad">
                        <div class="card-body">

                            <div class="row">
                                @forelse ($floorplans as $item)


                                <div class="col-xl-3 col-sm-6">
                                    <div class="card">
                                        <div class="card-img-actions mx-1 mt-1">
                                            <img class="card-img img-fluid" src="{{ asset('storage/'.$item->floor_plan_image) }}" alt="">
                                            <div class="card-img-actions-overlay card-img">
                                                <a href="{{ asset('storage/'.$item->floor_plan_image) }}" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round" data-popup="lightbox" rel="group">
                                                    <i class="icon-zoomin3"></i>
                                                </a>

                                                {{-- <a href="#" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round ml-2">
                                                    <i class="icon-download"></i>
                                                </a> --}}
                                            </div>
                                        </div>

                                        <div class="card-body">
                                            <div class="d-flex align-items-start flex-wrap">
                                                <div class="font-weight-semibold">{{ $item->floor_plan_name }}</div>
                                                <span class="font-size-sm text-muted ml-auto">

                                                    <span class="text-danger-600">
                                                        @if (Str::length($item->floor_plan_price)>9)
                                                            {{ is_int(($item->floor_plan_price/1000000000))?($item->floor_plan_price/1000000000).' Tỷ': number_format($item->floor_plan_price,0,'',',') }}
                                                        @elseif(Str::length($item->floor_plan_price)>6)
                                                            {{ is_int(($item->floor_plan_price/1000000))?($item->floor_plan_price/1000000).' Triệu': number_format($item->floor_plan_price,0,'',',') }}
                                                        @else
                                                            {{ number_format($item->floor_plan_price,0,'',',') }}
                                                        @endif
                                                        <sup>vnđ</sup>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @empty
                                <h1 class="ml-5 p-5">Hiện tại, Dự án chưa có bản vẽ!</h1>
                                @endforelse
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="course-review">
                        <div class="card-body">
                            Chưa Phát Triển
                        </div>
                    </div>
                </div>
            </div>
            <!-- /course overview -->

        </div>
        <!-- /left content -->


        <!-- Right sidebar component -->
        <div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right wmin-350 border-0 shadow-0 order-1 order-md-2 sidebar-expand-md">

            <!-- Sidebar content -->
            <div class="sidebar-content">
                <!-- Task details -->
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="card-title font-weight-semibold">Cơ bản</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                                <a class="list-icons-item" data-action="remove"></a>
                            </div>
                        </div>
                    </div>

                    <table class="table table-borderless table-xs my-2">
                        <tbody>
                            <tr>
                                <td><i class="icon-user-tie mr-2"></i> Quản trị viên:</td>
                                <td class="text-right"><a href="#">{{ $house->admin_full_name }}</a></td>
                            </tr>
                            <tr>
                                <td><i class="icon-circles2 mr-2"></i> Kích hoạt:</td>
                                <td class="text-right">
                                    @if($house->active_status == 1)<span class="badge badge-success">bật</span>
                                        @else <span class="badge badge-danger">tắt</span> @endif
                                </td>
                            </tr>
                            <tr>
                                <td><i class="icon-stars mr-2"></i> Đánh Giá:</td>
                                <td class="text-right text-warning">
                                    @php
                                        $star = $house->house_rating;
                                    @endphp
                                    @for ($i = 0; $i < 5; $i++)
                                        @if ($star>=1)
                                            <i class="icon-star-full2"></i>
                                        @elseif ($star>0)
                                            <i class="icon-star-half"></i>
                                        @else
                                            <i class="icon-star-empty3"></i>
                                        @endif
                                        @php
                                            $star--;
                                        @endphp
                                    @endfor
                                </td>
                            </tr>
                            <tr>
                                <td><i class="icon-users4 mr-2"></i> Lượt Xem:</td>
                                <td class="text-right text-muted">{{$house->house_viewed}} lượt</td>
                            </tr>
                            <tr>
                                <td><i class="icon-alarm-add mr-2"></i> Cập nhật:</td>
                                <td class="text-right text-muted">{{date('H:i - d/m/Y', strtotime($house->updated_at))}}</td>
                            </tr>
                            <tr>
                                <td><i class="icon-alarm-check mr-2"></i> Tạo mới:</td>
                                <td class="text-right text-muted">{{date('H:i - d/m/Y', strtotime($house->created_at))}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="card-footer d-flex align-items-center">
                        <ul class="list-inline list-inline-condensed mb-0">
                            <li class="list-inline-item">
                                <a href="{{ route('admins.housegallery.index', ['id' => $house->house_id]) }}"
                                    data-popup="tooltip" title="Thêm Hình Ảnh" data-placement="bottom"
                                    class="text-default"><i class="icon-images3"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="{{ route('admins.housefloorplan.index', ['id' => $house->house_id]) }}" data-popup="tooltip"
                                    title="Thêm Bản Vẽ" data-placement="bottom" class="text-default"><i class="icon-file-presentation2"></i></a>
                            </li>

                            <li class="list-inline-item">
                                <a href="javascript:void(0)" onclick="form_action('edit', {{$house->house_id}});" role="button"
                                    data-popup="tooltip" title="Cập Nhập" data-placement="bottom"
                                    class="text-default"><i class="icon-pencil7"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript:void(0)" onclick="form_action('delete', {{$house->house_id}});" role="button"
                                    data-popup="tooltip" title="Xóa" data-placement="bottom"
                                    class="text-default"><i class="icon-bin"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="{{ route('admins.house.index') }}"
                                    data-popup="tooltip" title="Trở về danh sách" data-placement="bottom"
                                    class="text-default"><i class="icon-reply"></i></a>
                            </li>
                        </ul>

                    </div>
                </div>
                <!-- /task details -->

            </div>
            <!-- /sidebar content -->

        </div>
        <!-- /right sidebar component -->

    </div>
    <!-- /inner container -->

    <form name="form1" action="" method="">
        {{csrf_field()}}
        <div class="form-group ">
            <input type="hidden" class="form-control" name="house_id" value="">
        </div>
    </form>
@stop

@section('javascript')


<script src="{{ asset('asset/admins/js/plugins/notifications/sweet_alert.min.js')}}"></script>
{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script src="{{ asset('asset/admins/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
<script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
<script>
    function form_action (mode, house_id) {
       let frm = document.form1;
        frm.house_id.value = house_id
       if(mode == 'edit'){
           frm.action = "{{route('admins.house.edit')}}";
           frm.method = 'get';
        frm.submit();
       }else if(mode == 'view'){
           frm.action = "{{route('admins.house.show')}}";
           frm.method = 'get';
           frm.submit();
       }else if(mode == 'delete'){

            // Defaults
            var swalInit = swal.mixin({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-light'
            });
             swalInit.fire({
                title: 'Bạn có chắc muốn xóa đối tượng này?',
                text: "Bạn không thể hoàn tác lại thao tác này!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Vâng, Xóa đối tượng!',
                cancelButtonText: 'Thoát',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function(result) {
                if (result.value) {
                    frm.action = "{{route('admins.house.delete')}}";
                    frm.method = 'post';
                    frm.submit();
                }
            });
        }
    }

    function initialSetup() {
       $hidden = $(".notifyHidden");
        if ($hidden != null) {
            $hidden.delay(3000).fadeOut('slow');
        }
    }
    // Initialize module
    // ------------------------------
    document.addEventListener('DOMContentLoaded', function() {
        initialSetup();
    });
</script>

<script src="{{ asset('asset/admins/js/app.js') }}"></script>
<script src="{{ asset('asset/admins/js/pages/components_popups.js')}}"></script>
<!-- /theme JS files -->

@stop


