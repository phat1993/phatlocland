@extends('layouts.admin_layout')
@section('title', 'Hình ảnh dự án')

@section('topnavdetail')
    <div class="page-title d-flex">
        <h4><i class="icon-arrow-left52 mr-2"></i>
            <span class="font-weight-semibold">Hình Ảnh</span> <span class="text-primary">{{ $project->project_name }}</span>
            - Danh sách</h4>
        <a href="{{ route('admins.project.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
    </div>
@endsection
@section('topnavigation')
    <div class="breadcrumb">
        <a href="{{ route('admins.project.index') }}" class="breadcrumb-item"><i class="icon-city mr-2"></i> Dự Án</a>
        <a href="{{ route('admins.project.show', ['project_id'=>$project->project_id]) }}" class="breadcrumb-item"> {{ $project->project_name }}</a>
        <a href="{{ route('admins.projectgallery.index', ['id'=>$project->project_id]) }}" class="breadcrumb-item"> Danh sách</a>
        <span class="breadcrumb-item active">Tạo mới</span>
    </div>
    <a href="{{ route('admins.project.index') }}" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
@endsection
@section('content')
    <!-- Notifications Set  -->
    @if(session('success'))
    <div class="alert alert-success bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
        <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
        <h5>{{session('success')}}</h5>
        <br>
        <a href="{{ route('admins.projectgallery.index', ['id'=>$project->project_id]) }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
    </div>
    @endif

    @if(count($errors) > 0)
         <div class="alert alert-danger bg-white alert-styled-left alert-arrow-left alert-dismissible notifyHidden">
            <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
            <h4 class="alert-heading font-weight-semibold mb-1 font-weight-bold">Thông Báo</h4>
            @foreach($errors->all() as $error)
            <h5>{{$error}}</h5>
            @endforeach
            <br>
            <a href="{{ route('admins.projectgallery.index', ['id'=>$project->project_id]) }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở về danh sách</a>
        </div>
    @endif
    <!-- /Notifications Set  -->
    <!-- Restore column visibility -->
    <div class="card">
        <div class="card-header header-elements-inline">
            <h5 class="card-title">
                <strong class="mr-4">Tạo Mới</strong>
            </h5>
            <div class="header-elements">
                <div class="list-icons">
                    <a class="list-icons-item" data-action="reload"></a>
                </div>
            </div>
        </div>

        <div class="card-body">

            <form action="{{ route('admins.projectgallery.store', ['id'=>$project->project_id]) }}" method="POST" enctype="multipart/form-data"  name="project-gallery-form" >
                {{csrf_field()}}
                <fieldset class="mb-3">
                    <div class="form-group">
                        <input type="hidden" class="form-control" name="project_id" value="{{$projectGallery->project_id}}">
                    </div>
                    <legend class="text-uppercase font-size-sm font-weight-bold">Thông Tin Cơ Bản</legend>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_gallery_banner">Loại Hình Ảnh<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" id="project_gallery_banner" name="project_gallery_banner" value="1" class="form-check-input-styled-danger"
                                    @if(old('project_gallery_banner', $projectGallery->project_gallery_banner) == 1) checked @endif data-fouc>
                                    Băng Rôn(1400x700)
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" id="project_gallery_banner" name="project_gallery_banner" value="2" class="form-check-input-styled-warning"
                                    @if(old('project_gallery_banner', $projectGallery->project_gallery_banner) == 2) checked @endif data-fouc>
                                    Hình Đại diện(750x500)
                                </label>
                            </div>


                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" id="project_gallery_banner" name="project_gallery_banner" value="0" class="form-check-input-styled-custom"
                                    @if(old('project_gallery_banner', $projectGallery->project_gallery_banner) == 0) checked @endif data-fouc>
                                    Hình Ảnh
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_gallery_name">Tên<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" maxlength="100" placeholder="Nhập vào tên hình ảnh"  id="project_gallery_name" name="project_gallery_name" value="{{ old('project_gallery_name', $projectGallery->project_gallery_name) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="file-input">Hình ảnh:</label>
                        <div class="col-lg-10">
                            <input type="file" class="file-input" id="file_input" name="file_input" data-show-caption="false" data-show-upload="false" data-fouc>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="project_gallery_order">Thứ Tự<span class="text-danger">(*)</span>:</label>
                        <div class="col-lg-10">
                            <input type="number" class="form-control"  maxlength="3" placeholder="Nhập vào thứ tự"  id="project_gallery_order" name="project_gallery_order" value="{{ old('project_gallery_order', $projectGallery->project_gallery_order) }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 cursor-pointer" for="active_status">Trạng Thái:</label>
                        <div class="col-lg-10">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" id="active_status" name="active_status" class="form-check-input-styled-primary"
                                    @if(old('active_status', $projectGallery->active_status)!=null && old('active_status', $projectGallery->active_status) == 1)
                                        checked
                                    @endif data-fouc>
                                    Kích Hoạt
                                </label>
                            </div>
                        </div>
                    </div>

                </fieldset>


                <div class="text-center">
                    <button type="submit" class="btn btn-primary"><i class="icon-download4 mr-2"> Lưu Lại</i></button>
                    <a href="{{ route('admins.projectgallery.index', ['id'=>$project->project_id]) }}" class="btn btn-outline-dark"><i class="icon-arrow-left15 mr-2"></i> Trở Về Danh Sách</a>
                </div>
            </form>
        </div>
    </div>
    <!-- /restore column visibility -->

@stop

@section('javascript')

	<!-- Theme JS files -->
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tagsinput.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/tags/tokenfield.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/ui/prism.min.js')}}"></script>

	<script src="{{ asset('asset/admins/js/plugins/forms/styling/uniform.min.js')}}"></script>
	{{-- <script src="{{ asset('asset/admins/js/plugins/forms/styling/switchery.min.js')}}"></script> --}}
	<script src="{{ asset('asset/admins/js/plugins/forms/styling/switch.min.js')}}"></script>

	{{-- <script src="{{ asset('asset/admins/js/plugins/uploaders/fileinput/plugins/purify.min.js')}}"></script>
	<script src="{{ asset('asset/admins/js/plugins/uploaders/fileinput/plugins/sortable.min.js')}}"></script> --}}
	<script src="{{ asset('asset/admins/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>


    <script src="{{ asset('asset/admins/js/plugins/forms/validation/validate.min.js') }}"></script>

    <script src="{{ asset('asset/admins/js/app.js') }}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_tags_input.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/form_checkboxes_radios.js')}}"></script>
	<script src="{{ asset('asset/admins/js/pages/uploader_bootstrap.js')}}"></script>
	<!-- /theme JS files -->

    <script>
        function initialSetup() {
           $hidden = $(".notifyHidden");
            if ($hidden != null) {
                $hidden.delay(10000).fadeOut('slow');
            }
        }


        $( document ).ready( function () {
            initialSetup();
            $( "form[name=project-gallery-form]" ).validate( {
                rules: {
                    project_gallery_name: {
                        required: true,
                        maxlength: 100
                    },
                    project_gallery_order: {
                        required: true,
                        maxlength: 3
                    },
                },
                messages: {
                    project_gallery_name: {
                        required: "Hãy nhập vào tên hình ảnh.",
                        maxlength: "Bạn chỉ được nhập tối đa 100 ký tự."
                    },
                    project_gallery_order: {
                        required: "Hãy nhập vào vị trí.",
                        maxlength: "Bạn chỉ được nhập tối đa 3 ký tự."
                    },
                },
                errorElement: "span",
                errorPlacement: function ( error, element ) {
                    // Add the `invalid-feedback` class to the error element
                    error.addClass("form-text").addClass( "text-danger" );

                    if ( element.prop( "type" ) === "checkbox" ) {
                        error.insertAfter( element.next( "label" ) );
                    } else {
                        error.insertAfter( element );
                    }
                },
                highlight: function ( element, errorClass, validClass ) {
                    $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
                },
                unhighlight: function (element, errorClass, validClass) {
                    $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
                }
            } );

        } );
    </script>
@stop
